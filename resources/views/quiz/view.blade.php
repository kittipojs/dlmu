@if($setting['view-method'] =='native')
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools pull-left" >
		   		<a href="{{ ($prevnext['prev'] != '' ? url('quiz/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-sm" onclick="ajaxViewDetail('#quiz',this.href); return false; "><i class="fa fa-arrow-left"></i>  </a>	
				<a href="{{ ($prevnext['next'] != '' ? url('quiz/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-sm " onclick="ajaxViewDetail('#quiz',this.href); return false; "> <i class="fa fa-arrow-right"></i>  </a>					
			</div>	

			<div class="sbox-tools" >
				<a href="javascript://ajax" onclick="ajaxViewClose('#{{ $pageModule }}')" class="tips btn btn-sm  " title="{{ __('core.btn_back') }}"><i class="fa  fa-times"></i></a>		
			</div>
		</div>
		<div class="sbox-content">
@endif	

		<table class="table  table-striped" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Quiz Id', (isset($fields['quiz_id']['language'])? $fields['quiz_id']['language'] : array())) }}</td>
						<td>{{ $row->quiz_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Course Id', (isset($fields['course_id']['language'])? $fields['course_id']['language'] : array())) }}</td>
						<td>{{ $row->course_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Quiz Title', (isset($fields['quiz_title']['language'])? $fields['quiz_title']['language'] : array())) }}</td>
						<td>{{ $row->quiz_title}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Quiz Description', (isset($fields['quiz_description']['language'])? $fields['quiz_description']['language'] : array())) }}</td>
						<td>{{ $row->quiz_description}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Quiz Timer', (isset($fields['quiz_timer']['language'])? $fields['quiz_timer']['language'] : array())) }}</td>
						<td>{{ $row->quiz_timer}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Quiz Score To Pass', (isset($fields['quiz_score_to_pass']['language'])? $fields['quiz_score_to_pass']['language'] : array())) }}</td>
						<td>{{ $row->quiz_score_to_pass}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Quiz Is Random Question', (isset($fields['quiz_is_random_question']['language'])? $fields['quiz_is_random_question']['language'] : array())) }}</td>
						<td>{{ $row->quiz_is_random_question}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Quiz Is Random Answer', (isset($fields['quiz_is_random_answer']['language'])? $fields['quiz_is_random_answer']['language'] : array())) }}</td>
						<td>{{ $row->quiz_is_random_answer}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Created At', (isset($fields['created_at']['language'])? $fields['created_at']['language'] : array())) }}</td>
						<td>{{ $row->created_at}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Updates At', (isset($fields['updates_at']['language'])? $fields['updates_at']['language'] : array())) }}</td>
						<td>{{ $row->updates_at}} </td>
						
					</tr>
				
			</tbody>	
		</table>  
			
		 	
		 
@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif		