

		 {!! Form::open(array('url'=>'quiz', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> quiz</legend>
									
									  <div class="form-group  " >
										<label for="Quiz Id" class=" control-label col-md-4 text-left"> Quiz Id <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='quiz_id' id='quiz_id' value='{{ $row['quiz_id'] }}' 
						required     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Course Id" class=" control-label col-md-4 text-left"> Course Id <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='course_id' id='course_id' value='{{ $row['course_id'] }}' 
						required     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Quiz Title" class=" control-label col-md-4 text-left"> Quiz Title <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='quiz_title' id='quiz_title' value='{{ $row['quiz_title'] }}' 
						required     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Quiz Description" class=" control-label col-md-4 text-left"> Quiz Description <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='quiz_description' rows='5' id='quiz_description' class='form-control input-sm '  
				         required  >{{ $row['quiz_description'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Quiz Timer" class=" control-label col-md-4 text-left"> Quiz Timer <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='quiz_timer' id='quiz_timer' value='{{ $row['quiz_timer'] }}' 
						required     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Quiz Score To Pass" class=" control-label col-md-4 text-left"> Quiz Score To Pass <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='quiz_score_to_pass' id='quiz_score_to_pass' value='{{ $row['quiz_score_to_pass'] }}' 
						required     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Quiz Is Random Question" class=" control-label col-md-4 text-left"> Quiz Is Random Question <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='quiz_is_random_question' id='quiz_is_random_question' value='{{ $row['quiz_is_random_question'] }}' 
						required     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Quiz Is Random Answer" class=" control-label col-md-4 text-left"> Quiz Is Random Answer <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='quiz_is_random_answer' id='quiz_is_random_answer' value='{{ $row['quiz_is_random_answer'] }}' 
						required     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Created At" class=" control-label col-md-4 text-left"> Created At <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('created_at', $row['created_at'],array('class'=>'form-control input-sm datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Updates At" class=" control-label col-md-4 text-left"> Updates At <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('updates_at', $row['updates_at'],array('class'=>'form-control input-sm datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 <input type="hidden" name="action_task" value="public" />
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
