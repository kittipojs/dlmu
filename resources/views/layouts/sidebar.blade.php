<?php $sidebar = SiteHelpers::menus('sidebar'); ?>
{{--<div id="sidebar-navigation">--}}
{{--<div class="logo">--}}
{{--<a href="{{ url('') }}">--}}
{{--@if(file_exists(public_path().'/uploads/images/'.config('sximo.cnf_logo') ) && config('sximo.cnf_logo') !='')--}}
{{--<img src="{{ asset('uploads/images/'.config('sximo.cnf_logo')) }}" alt="{{ config('sximo.cnf_appname') }}"  />--}}
{{--@else--}}
{{--{{ config('sximo.cnf_appname')}}--}}
{{--@endif--}}
{{--</a>--}}
{{--</div>--}}

<!-- Sidebar Holder -->
<nav id="sidebar">
    <div id="dismiss">
        <i class="glyphicon glyphicon-arrow-left"></i>
    </div>
    <div class="sidebar-header">
        <h3></h3>
    </div>
    <div class="padding"></div>
    <div class="padding"></div>
    <ul class="list-unstyled components">
        <li class="profile-sidebar">
            <a href="{{ url('user/profile') }}">
                <div align="center">
                    {!! SiteHelpers::avatar(100)!!}
                </div>
            </a>
            <h4 align="center">{{ session('fid') }}</h4>
            <div class="stats-label">
                <a href="{{ url('user/logout') }}" class="btn btn-danger btn-block btn-sm">
                    ออกจากระบบ
                </a>
            </div>
        </li>
        @foreach ($sidebar as $menu)
            <li @if(Request::segment(1) == $menu['module']) class="active" @endif>

                @if($menu['module'] =='separator')
                    <a href="#{{$menu['menu_name']}}" data-toggle="collapse"
                       aria-expanded="false">{{$menu['menu_name']}}</a>
                    {{--<li class="separator"><span> {{$menu['menu_name']}} </span></li>--}}

                @else
                    <a
                            @if(count($menu['childs']) > 0 ) href="#{{$menu['menu_id']}}" data-toggle="collapse"
                            aria-expanded="false"

                            @else

                            @if($menu['menu_type'] =='external')
                            @if( str_contains($menu['url'],"http:" ) || str_contains($menu['url'], "https:" ))
                            href="{{ $menu['url'] }}"
                            @else
                            href="{{ URL($menu['url']) }}"
                            @endif
                            @else
                            href="{{ URL::to($menu['module'])}}"
                            @endif

                            @endif>
                        <i class="{{$menu['menu_icons']}}"></i>
                        <span class="nav-label">
                    {{ (isset($menu['menu_lang']['title'][session('lang')]) ? $menu['menu_lang']['title'][session('lang')] : $menu['menu_name']) }}
                </span>
                        {{--@if(count($menu['childs']))<span class="fa arrow"></span> @endif--}}
                    </a>
                @endif
                @if(count($menu['childs']) > 0)
                    {{--<ul class="nav nav-second-level">--}}
                    <ul class="collapse list-unstyled" id="{{$menu['menu_id']}}">
                        @foreach ($menu['childs'] as $menu2)
                            <li @if(Request::segment(1) == $menu2['module']) class="active" @endif>
                                <a
                                        @if($menu2['menu_type'] =='external')
                                        @if( str_contains($menu2['url'],"http:" ) || str_contains($menu['url'], "https:" ))
                                        href="{{ $menu2['url'] }}"
                                        @else
                                        href="{{ URL($menu2['url']) }}"
                                        @endif
                                        @else
                                        href="{{ URL::to($menu2['module'])}}"
                                        @endif
                                >

                                    <i class="{{$menu2['menu_icons']}}"></i>
                                    {{ (isset($menu2['menu_lang']['title'][session('lang')]) ? $menu2['menu_lang']['title'][session('lang')] : $menu2['menu_name']) }}
                                </a>
                                @if(count($menu2['childs']) > 0)
                                    <ul class="nav nav-third-level">
                                        @foreach($menu2['childs'] as $menu3)
                                            <li @if(Request::segment(1) == $menu3['module']) class="active" @endif>
                                                <a
                                                        @if($menu['menu_type'] =='external')
                                                        @if( str_contains($menu3['url'],"http:" ) || str_contains($menu['url'], "https:" ))
                                                        href="{{ $menu3['url'] }}"
                                                        @else
                                                        href="{{ URL($menu3['url']) }}"
                                                        @endif
                                                        @else
                                                        href="{{ URL::to($menu3['module'])}}"
                                                        @endif

                                                >

                                                    <i class="{{$menu3['menu_icons']}}"></i>
                                                    {{ (isset($menu3['menu_lang']['title'][session('lang')]) ? $menu3['menu_lang']['title'][session('lang')] : $menu3['menu_name']) }}

                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach

    </ul>

    <!--
    <ul class="list-unstyled CTAs">
        <li><a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a></li>
        <li><a href="https://bootstrapious.com/p/bootstrap-sidebar" class="article">Back to article</a></li>
    </ul>
    -->
</nav>


{{--<div class="sidebar-collapse">--}}
{{--<nav role="navigation" class="navbar-default ">--}}
{{--<div class="padding"></div>--}}
{{--<ul id="sidemenu" class="nav expanded-menu">--}}
{{--<li class="profile-sidebar">--}}
{{--<a href="{{ url('user/profile') }}">--}}
{{--{!! SiteHelpers::avatar(100)!!}--}}
{{--</a>--}}
{{--<h4>{{ session('fid') }}</h4>--}}

{{--<div class="stats-label">--}}
{{--<a href="{{ url('user/logout') }}" class="btn btn-danger btn-block btn-sm">--}}
{{--<span class="glyphicon glyphicon-log-out"></span> ออกจากระบบ--}}
{{--</a>--}}
{{--</div>--}}
{{--</li>--}}
{{--@foreach ($sidebar as $menu)--}}

{{--<li @if(Request::segment(1) == $menu['module']) class="active" @endif>--}}

{{--@if($menu['module'] =='separator')--}}
{{--<li class="separator"><span> {{$menu['menu_name']}} </span></li>--}}

{{--@else--}}
{{--<a data-toggle="tooltip" title="{{  $menu['menu_name'] }}" data-placement="right"--}}
{{--@if($menu['menu_type'] =='external')--}}
{{--@if( str_contains($menu['url'],"http:" ) || str_contains($menu['url'], "https:" ))--}}
{{--href="{{ $menu['url'] }}"--}}
{{--@else--}}
{{--href="{{ URL($menu['url']) }}"--}}
{{--@endif--}}
{{--@else--}}
{{--href="{{ URL::to($menu['module'])}}"--}}
{{--@endif--}}

{{--@if(count($menu['childs']) > 0 ) class="expand level-closed" @endif>--}}
{{--<i class="{{$menu['menu_icons']}}"></i>--}}
{{--<span class="nav-label">--}}
{{--{{ (isset($menu['menu_lang']['title'][session('lang')]) ? $menu['menu_lang']['title'][session('lang')] : $menu['menu_name']) }}--}}
{{--</span>--}}
{{--@if(count($menu['childs']))<span class="fa arrow"></span> @endif--}}
{{--</a>--}}
{{--@endif--}}
{{--@if(count($menu['childs']) > 0)--}}
{{--<ul class="nav nav-second-level">--}}
{{--@foreach ($menu['childs'] as $menu2)--}}
{{--<li @if(Request::segment(1) == $menu2['module']) class="active" @endif>--}}
{{--<a--}}
{{--@if($menu2['menu_type'] =='external')--}}
{{--@if( str_contains($menu2['url'],"http:" ) || str_contains($menu['url'], "https:" ))--}}
{{--href="{{ $menu2['url'] }}"--}}
{{--@else--}}
{{--href="{{ URL($menu2['url']) }}"--}}
{{--@endif--}}
{{--@else--}}
{{--href="{{ URL::to($menu2['module'])}}"--}}
{{--@endif--}}
{{-->--}}

{{--<i class="{{$menu2['menu_icons']}}"></i>--}}
{{--{{ (isset($menu2['menu_lang']['title'][session('lang')]) ? $menu2['menu_lang']['title'][session('lang')] : $menu2['menu_name']) }}--}}
{{--</a>--}}
{{--@if(count($menu2['childs']) > 0)--}}
{{--<ul class="nav nav-third-level">--}}
{{--@foreach($menu2['childs'] as $menu3)--}}
{{--<li @if(Request::segment(1) == $menu3['module']) class="active" @endif>--}}
{{--<a--}}
{{--@if($menu['menu_type'] =='external')--}}
{{--@if( str_contains($menu3['url'],"http:" ) || str_contains($menu['url'], "https:" ))--}}
{{--href="{{ $menu3['url'] }}"--}}
{{--@else--}}
{{--href="{{ URL($menu3['url']) }}"--}}
{{--@endif--}}
{{--@else--}}
{{--href="{{ URL::to($menu3['module'])}}"--}}
{{--@endif--}}

{{-->--}}

{{--<i class="{{$menu3['menu_icons']}}"></i>--}}
{{--{{ (isset($menu3['menu_lang']['title'][session('lang')]) ? $menu3['menu_lang']['title'][session('lang')] : $menu3['menu_name']) }}--}}

{{--</a>--}}
{{--</li>--}}
{{--@endforeach--}}
{{--</ul>--}}
{{--@endif--}}
{{--</li>--}}
{{--@endforeach--}}
{{--</ul>--}}
{{--@endif--}}
{{--</li>--}}
{{--@endforeach--}}

{{--</ul>--}}
{{--</nav>--}}
{{--</div>--}}

{{--</div>--}}
<script type="text/javascript">
    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').fadeOut();
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
</script>