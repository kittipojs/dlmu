@extends('layouts.app')

@section('content')
<div class="page-content row">
    <div class="page-content-wrapper no-margin">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/e-admin') }}">แดชบอร์ด</a></li>
            @if(isset($subnav))
                @foreach($subnav as $item)
                <li class="breadcrumb-item"><a href="{{ url($item['link']) }}">{{ $item['text'] }}</a></li>
                @endforeach
            @endif
            <li class="breadcrumb-item active">{{ $title }}</li>
        </ol>
        <div class="sbox">
            <div class="sbox-content">
                @if(strpos($content, 'pages.') === 0)
                @include($content)
                @else
                <?php echo $content; ?>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection