<?php $menus = SiteHelpers::menus('top'); ?>
<ul class="nav navbar-nav navbar-left">
    <li>
        <a href="{{ url('') }}">
            <span class="fa fa-home"></span>
            หน้าแรก
        </a>
    </li>
    @foreach ($menus as $menu)
    @if($menu['module'] =='separator')
    <li class="divider"></li>
    @else
    <li><!-- HOME -->
        <a
           @if($menu['menu_type'] =='external')
           href="{{ URL::to($menu['url'])}}"
           @else
           href="{{ URL::to($menu['module'])}}"
           @endif

           @if(count($menu['childs']) > 0 ) class="dropdown-toggle" data-toggle="dropdown" @endif>
            <i class="{{$menu['menu_icons']}}"></i>
            @if(config('sximo.cnf_multilang') ==1 && isset($menu['menu_lang']['title'][session('lang')]) && $menu['menu_lang']['title'][session('lang')]!='')
            {{ $menu['menu_lang']['title'][session('lang')] }}
            @else
            {{$menu['menu_name']}}
            @endif
            @if(count($menu['childs']) > 0 )
            <b class="caret"></b>
            @endif
        </a>
        @if(count($menu['childs']) > 0)
        <ul class="dropdown-menu dropdown-menu-left">
            @foreach ($menu['childs'] as $menu2)
            @if($menu2['module'] =='separator')
            <li class="divider"></li>
            @else
            <li class="
                       @if(count($menu2['childs']) > 0) dropdown-submenu @endif
                       @if(Request::is($menu2['module'])) active @endif">
                <a
                   @if($menu2['menu_type'] =='external')
                   href="{{ url($menu2['url'])}}"
                   @else
                   href="{{ url($menu2['module'])}}"
                   @endif

                   style="width: 300px; padding:8px;">
                    <i class="{{ $menu2['menu_icons'] }}"></i>
                    @if(config('sximo.cnf_multilang') ==1 && isset($menu2['menu_lang']['title'][session('lang')]))
                    {{ $menu2['menu_lang']['title'][session('lang')] }}
                    @else
                    {{$menu2['menu_name']}}
                    @endif
                </a>
                @endif
                @endforeach
        </ul>
        @endif
    </li>
    @endif
    @endforeach

</ul>

@if(isset($show_uleague_btn))
<ul class="nav navbar-nav navbar-right pure-hidden-sm pure-hidden-xs" style="text-align:center;">
    <a href="{{ url('/uleague') }}" class="btn btn-md btn-warning" style="margin-top:8px;">
        สมัคร U League {{ DataHelpers::year() }}
    </a>
</ul>
@endif

<!-- SIDE BAR -->
<div class="pure-hidden-xl pure-hidden-lg pure-hidden-md">
    <hr>
    @if(Auth::check())
    <ul class="nav navbar-nav navbar-left">
        <li class="divider">ข้อมูลส่วนตัว</li>
        <li>
            <a href="{{ url('user/profile?ref=sm') }}">แก้ไขข้อมูลส่วนตัว</a>
        </li>
        <li class="divider">U-league {{ DataHelpers::year() }} <span class="label label-danger">New</span></li>

        @if(!$uleague)
        <li>
            <a href="{{ url('user/uleague/join') }}">ลงทะเบียน U League {{ DataHelpers::year() }}</a>
        </li>
        @else
        @if($has_notification)
        <li>
            <a href="{{ url('/user/uleague/team/accept/'.$invitation->team_id.'$'.$invitation->invitation_code) }}"><i class="fa fa-bell"></i> มี 1 คำเชิญร่วมทีม</a>
        </li>
        @endif
        <li>
            <a href="{{ url('user/uleague') }}">ข้อมูลส่วนตัว</a>
        </li>
        @endif
        <li>
            <a href="{{ url('user/uleague/camp') }}">กิจกรรม U League Camp {{ DataHelpers::year() }}</a>
        </li>
        <li>
            <a href="{{ url('user/uleague/team') }}">กิจกรรม U League Pitching {{ DataHelpers::year() }}</a>
        </li>
        <li class="divider">อื่นๆ</li>
        <li>
            <a href="{{ url('/user/logout') }}">
                ออกจากระบบ
            </a>
        </li>
    </ul>
    @else
    <ul class="nav navbar-nav navbar-left">
        <li>
            <a href="{{ url('/user/login') }}">
                เข้าสู่ระบบ
            </a>
        </li>
        <li>
            <a href="{{ url('/user/register') }}">
                สมัครสมาชิก
            </a>
        </li>
    </ul>
    @endif
</div>