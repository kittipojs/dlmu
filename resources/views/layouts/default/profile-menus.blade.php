<div class="bg-grey">
    <div class="container container-wide">
        <div class="row profile">
            <div class="nopadding col-xs-12 col-sm-4 col-md-3 pure-hidden-xs pure-hidden-sm">
                <div class="profile-sidebar">
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic">
                        @if(Auth::user()->avatar!=null)
                        <img src="{{ url('uploads/users').'/'.Auth::user()->avatar }}" class="img-responsive" alt="">
                        @else
                        <img src="{{ url('frontend/medilab/img/default-profile.png')}}" class="img-responsive" alt="">
                        @endif
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                            {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        @yield('content')
                        <ul class="nav">
                            <li class="divider">ข้อมูลส่วนตัว</li>
                            <li>
                                <a href="{{ url('user/profile?ref=sm') }}">ข้อมูลส่วนตัว</a>
                            </li>
                            <li class="divider">U-league {{ DataHelpers::year() }} <span class="label label-danger">New</span></li>

                            @if(!$uleague)
                            <li>
                                <a href="{{ url('user/uleague/join') }}">ลงทะเบียน U-league {{ DataHelpers::year() }}</a>
                            </li>
                            @else
                            <li>
                                <a href="{{ url('user/uleague') }}">ข้อมูล U League</a>
                            </li>
                            @endif
                            <li>
                                <a href="{{ url('user/uleague/camp') }}">กิจกรรม U League Camp {{ DataHelpers::year() }}</a>
                            </li>
                            <li>
                                <a href="{{ url('user/uleague/team') }}">กิจกรรม U League Pitching {{ DataHelpers::year() }}</a>
                            </li>

                            <li class="divider">อื่นๆ</li>
                            <li>
                                <a href="{{ url('user/certificates') }}">ประกาศนียบัตร</a>
                            </li>
                            <li>
                                <a href="{{ url('/user/logout') }}">ออกจากระบบ</a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MENU -->
                </div>
            </div>
            <div class="nopadding col-xs-12 col-sm-8 col-md-9">
                <div class="profile-content">
                    @include($sub_content)
                </div>
            </div>
        </div>
    </div>
</div>