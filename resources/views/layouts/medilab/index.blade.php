<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $title }} | {{ config('sximo.cnf_appname') }}</title>
    <link rel="shortcut icon" href="{{ asset('favicon.ico')}}" type="image/x-icon">
    <meta name="description" content="">
    <meta name="keywords" content="">


    @if(isset($og))
        <meta property="fb:app_id" content="225455011359805"/>
        <meta property="og:type" content="website"/>
        <meta property="og:title" content="{{ $og['title'] }}"/>
        <meta property="og:url" content="{{ $og['url'] }}"/>
        <meta property="og:description" content="{{ $og['description'] }}"/>
        <meta property="og:image" content="{{ $og['image'] }}"/>
    @endif

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/medilab/css/font-awesome.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/medilab/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('sximo5/js/plugins/toast/css/jquery.toast.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/datepicker/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/datepicker/custom.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/jquery-loading/css/jquery.loadingModal.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/circliful-master/css/jquery.circliful.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('sximo5/js/plugins/select2/theme/select2-bootstrap.css') }}">

    {{--<!-- Sximo 5 Main CSS -->--}}
    <link rel="stylesheet" href="{{ asset('sximo5/css/animate.css')}}">
    <link rel="stylesheet" href="{{ asset('sximo5/fonts/icomoon.css')}}">

    <link rel="stylesheet" href="{{ asset('frontend/medilab/css/sidebar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/medilab/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/medilab/css/timeline.css')}}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/css/bootstrap-slide.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/css/flickity.css')}}">

    <!-- Slick Carousel -->
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/js/slick-master/slick/slick.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/js/slick-master/slick/slick-theme.css')}}"/>

    <!-- Custom JS -->
    <script type="text/javascript" src="{{ asset('custom/js/flickity.pkgd.min.js') }}"></script>

    <script type="text/javascript" src='https://www.google.com/recaptcha/api.js'></script>

    <script type="text/javascript" src="{{ asset('sximo5/js/plugins/jquery.3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sximo5/js/plugins/parsley.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sximo5/js/plugins/jquery.form.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sximo5/js/plugins/toast/js/jquery.toast.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('frontend/medilab/js/bootstrap.min.js')}}"></script>--}}
    <script type="text/javascript" src="{{ asset('sximo5/sximo.min.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('sximo5/js/sximo.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('custom/datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/jquery-loading/js/jquery.loadingModal.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/circliful-master/js/jquery.circliful.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/print/printThis.js') }}"></script>

    <!-- Slick Carousel -->
    <script type="text/javascript" src="{{ asset('custom/js/slick-master/slick/slick.min.js')}}"></script>

    <script type="text/javascript" src="{{ asset('custom/js/jwplayer-7.11.3/jwplayer.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/owl-carousel/dist/owl.carousel.min.js') }}"></script>
    <script>jwplayer.key = "ysQTVfHC5iQ8flS72k460WTgxEPDzPg90dTu2NzjVT0=";</script>

    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114904155-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', 'UA-114904155-1');
    </script>

</head>
@if(isset($enable_ng))
    <body>
    @else
        <body>
        @endif
        <div id="fb-root"></div>
        <script>
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0&appId=225455011359805&autoLogAppEvents=1';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        <!--top header-->
        <section id="banner" class="banner">
            {{--<div class="bg-color">--}}
            <nav class="navbar navbar-default navbar-fixed-top ">

                <div class="custom-top-nav">
                    <div class="container">

                        <button id="sidebarCollapse" type="button" class="navbar-toggle collapsed" aria-expanded="false"
                                aria-controls="navbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a class="navbar-brand" href="{{ url('') }}" style="margin-left:15px;">
                            <!--set logo-->
                            <img src="{{ asset('frontend/medilab/img/logo.png')}}" class="img-responsive"
                                 style="width: 140px; margin-top: -12px;">
                        </a>
                        @if(!Auth::check())
                            <div class="pull-right pure-hidden-xs pure-hidden-sm">
                                <a href="{{ url('user/login') }}" class="btn btn-primary btn-md">
                                    เข้าสู่ระบบ
                                </a>
                                &nbsp;&nbsp;
                                <a href="{{ url('user/register') }}" class="btn btn-secondary btn-md">
                                    สมัครสมาชิก
                                </a>
                            </div>
                        @else

                            <div class="pull-right pure-hidden-xs pure-hidden-sm">
                                @if(isset($has_notification))
                                    @if($has_notification)
                                        <a class="btn btn-link"
                                           href="{{ url('/user/uleague/team/accept/'.$invitation->team_id.'$'.$invitation->invitation_code) }}">
                                            <i class="fa fa-bell"></i> มี 1 คำเชิญร่วมทีม
                                        </a>
                                    @endif
                                @endif
                                <a class="btn btn-link" href="{{ url('user/profile') }}">
                                    <i class="fa fa-user"></i> ยินดีต้อนรับ {{ Auth::user()->first_name }}
                                </a>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="container">
                    <div class="navbar-header">
                    </div>
                    <div class="navbar-collapse collapse" id="navbar">
                        @include('layouts.default.navigation')
                    </div>
                </div>
            </nav>

            <!-- MOBILE VIEW -->
            <nav class="navbar pure-hidden-md pure-hidden-lg pure-hidden-xl" style="background-color:#FFF;">
                <div class="padding">
                    @if(!Auth::check())
                        <a href="{{ url('user/login') }}" class="btn btn-primary btn-md">
                            เข้าสู่ระบบ
                        </a>
                        &nbsp;&nbsp;
                        <a href="{{ url('user/register') }}" class="btn btn-secondary btn-md">
                            สมัครสมาชิก
                        </a>
                        <br>
                    @else
                        <a href="{{ url('user/profile') }}" class="btn btn-sm btn-link-dark">
                            <i class="fa fa-user"></i> ยินดีต้อนรับ {{ Auth::user()->first_name }}
                        </a>
                    @endif
                    @if(isset($show_uleague_btn))
                        <a href="{{ url('/uleague') }}" class="btn btn-sm btn-link">
                            สมัคร U League 2018
                        </a>
                    @endif
                </div>
            </nav>

            {{--</div>--}}

            <nav id="sidebar" class="pure-hidden-xl pure-hidden-lg">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>

                <div class="sidebar-header"></div>
                <div class="padding"></div>
                <div class="padding">
                    @include('layouts.default.navigation')
                </div>
            </nav>

        </section>
        <!--/ top header-->

        <div class="overlay"></div>

        <div id="content">
            @include($content)
        </div>
        <!--footer-->
        <footer id="footer">
            <div class="row">
                <div class="col-md-12 text-center padding">
                    <p>
                        สอบถามข้อมูลเพิ่มเติม กรุณาติดต่อ คุณพัชรีนาถ โทร 02 0175555 ต่อ 209 อีเมล์ : startup@nia.or.th
                    </p>
                    Powered by <a href="http://www.thailivestream.com" target="_blank">Thailivestream.com</a> version
                    1.0.0
                </div>
            </div>
        </footer>
        <!--/ footer-->


        <script type="text/javascript"
                src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type="text/javascript">

            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#dismiss, .overlay').on('click', function () {
                    $('#sidebar').removeClass('active');
                    $('.overlay').fadeOut();
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').addClass('active');
                    $('.overlay').fadeIn();
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            });
        </script>

        {{--<script src="{{ asset('frontend/medilab')}}/js/jquery.min.js"></script>--}}
        {{--<script src="{{ asset('frontend/medilab')}}/js/jquery.easing.min.js"></script>--}}
        {{--<script src="{{ asset('frontend/medilab')}}/js/custom.js"></script>--}}
        {{--<script src="{{ asset('frontend/medilab')}}/contactform/contactform.js"></script>--}}

        {{ SiteHelpers::showNotification() }}
        </body>
</html>


