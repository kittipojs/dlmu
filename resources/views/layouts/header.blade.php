<!-- Page Content Holder -->
<div id="content">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">

            <div class="navbar-header">
                <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn pull-left">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="{{ url('/e-admin') }}">
                    <img src="{{ asset('frontend/medilab/img/elearning-logo.png')}}" class="img-responsive"
                         style="height:40px; margin-top:-10px;">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right navbar-admin">
                    @if(Auth::user()->group_id <= 1)
                        <li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell-o"></i>
                                <span class="label label-green notif-alert">0</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header"></li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu" id="notification-menu">

                                    </ul>
                                <li><a href="{{ url('notification')}}">View all</a></li>
                            </ul>
                        </li>

                        <li class="dropdown user">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-sliders"></i>
                            </a>
                            <ul class="dropdown-menu navbar-mega-menu animated " style="display: none;">

                                @if(Auth::user()->group_id == 1 or Auth::user()->group_id ==2 )
                                    <li class="col-sm-3">
                                        <ul>
                                            <li class="dropdown-header">  @lang('core.m_setting') </li>
                                            <li class="divider"></li>
                                            <li><a href="{{ url('') }}/sximo/config"><i
                                                            class="fa fa-sliders"></i> @lang('core.t_generalsetting')
                                                </a></li>
                                            <li><a href="{{ url('') }}/sximo/config/email"><i
                                                            class="fa fa-envelope"></i>@lang('core.t_emailtemplate')
                                                </a></li>
                                            <li><a href="{{ url('') }}/sximo/config/security"><i
                                                            class="fa fa-lock"></i> @lang('core.t_loginsecurity') </a>
                                            </li>
                                            <li><a href="{{ url('') }}/sximo/config/translation"><i
                                                            class="fa fa-map"></i> @lang('core.tab_translation') </a>
                                            </li>
                                            <li><a href="{{ url('core/logs')}}"><i
                                                            class="fa fa-archive"></i> @lang('core.m_logs')</a></li>

                                        </ul>
                                    </li>

                                    <li class="col-sm-3">
                                        <ul>
                                            <li class="dropdown-header"> Administrator</li>
                                            <li class="divider"></li>
                                            <li><a href="{{ url('core/users')}}"> <i
                                                            class="fa fa-user-circle-o"></i> @lang('core.m_users') <br/></a>
                                            </li>
                                            <li><a href="{{ url('core/groups')}}"> <i
                                                            class="fa fa-user-plus"></i> @lang('core.m_groups') </a>
                                            </li>
                                            <li><a href="{{ url('core/users/blast')}}"> <i
                                                            class="fa fa-envelope"></i> @lang('core.m_blastemail') </a>
                                            </li>
                                            <li><a href="{{ url('core/pages')}}"> <i
                                                            class="fa fa-text-width"></i> @lang('core.m_pagecms')  </a>
                                            </li>
                                            <li><a href="{{ url('core/posts')}}"> <i
                                                            class="fa fa-text-height"></i> @lang('core.m_post')</a></li>

                                        </ul>
                                    </li>
                                @endif
                                @if(Auth::user()->group_id == 1  )
                                    <li class="col-sm-3">
                                        <ul>
                                            <li class="dropdown-header"> Superadmin</li>
                                            <li class="divider"></li>
                                            <li><a href="{{ url('sximo/module')}}"><i
                                                            class="fa fa-free-code-camp"></i> @lang('core.m_codebuilder')
                                                </a>
                                            </li>
                                            <li><a href="{{ url('sximo/rac')}}"><i class="fa fa-random"></i> RestAPI
                                                    Generator
                                                </a></li>
                                            <li><a href="{{ url('sximo/tables')}}"><i
                                                            class="fa fa-database"></i> @lang('core.m_database') </a>
                                            </li>
                                            <li><a href="{{ url('sximo/form')}}"><i
                                                            class="fa fa-tasks"></i> @lang('core.m_formbuilder') </a>
                                            </li>
                                            <li><a href="{{ url('core/elfinder')}}"><i class="fa fa-cloud-upload"></i>
                                                    Dropzone
                                                    Media </a></li>


                                        </ul>
                                    </li>
                                    <li class="col-sm-3">
                                        <ul>
                                            <li class="dropdown-header"> Utility</li>
                                            <li class="divider"></li>
                                            <li><a href="{{ url('sximo/menu')}}"><i
                                                            class="fa fa-sitemap"></i> @lang('core.m_menu')</a></li>
                                            <li><a href="{{ url('sximo/code')}}"><i
                                                            class="fa fa-file-code-o"></i> @lang('core.m_sourceeditor')
                                                </a>
                                            </li>
                                            <li><a href="{{ url('sximo/config/clearlog')}}" class="clearCache"><i
                                                            class="fa fa-trash-o"></i> @lang('core.m_clearcache')</a>
                                            </li>

                                        </ul>
                                    </li>
                                @endif


                            </ul>
                        </li>
                    @endif
                    <li>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa2x icons-pe"></i> {{ Auth::user()->first_name }}
                        </a>
                        <ul class="dropdown-menu top-dropdown">
                            <!-- dropdown menu links -->
                            <li><a tabindex="-1" href="{{ url('') }}">กลับหน้าหลัก</a></li>
                            <li><a tabindex="-1" href="{{ url('user/logout') }}"> ออกจากระบบ</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>