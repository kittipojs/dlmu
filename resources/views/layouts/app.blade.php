<?php
$set_theme = session('set_theme');
if ($set_theme == '') {
    $set_theme = 'blue-template.css';
}
?>
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Backend - {{ config('sximo.cnf_appname')}} </title>

    <link rel="shortcut icon" href="{{ asset('favicon.ico')}}" type="image/x-icon">

    <link href="{{ asset('sximo5/sximo.min.css')}}" rel="stylesheet">
    <link href="{{ asset('sximo5/js/plugins/iCheck/skins/square/green.css')}}" rel="stylesheet">
    <link href="{{ asset('sximo5/js/plugins/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
    <link href="{{ asset('sximo5/js/plugins/toast/css/jquery.toast.css')}}" rel="stylesheet">
    <!-- Icon CSS -->
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="{{ asset('sximo5/fonts/icomoon.css')}}" rel="stylesheet">
    <link href="{{ asset('sximo5/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}" rel="stylesheet">
    <link href="{{ asset('sximo5/fonts/awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ asset('sximo5/css/colors.css')}}" rel="stylesheet">
    <link href="{{ asset('sximo5/js/plugins/dropzone/dropzone.css')}}" rel="stylesheet">
    <link href="{{ asset('sximo5/js/plugins/lightbox2/dist/css/lightbox.min.css')}}" rel="stylesheet">
    <!-- Sximo 5 Main CSS -->
    <link href="{{ asset('sximo5/css/sximo.css')}}" rel="stylesheet">
    <link href="{{ asset('custom/nia-theme.css')}}" rel="stylesheet" id="switch-theme">
    <link href="{{ asset('custom/css/sidebar.css') }}" rel="stylesheet">
    <link href="{{ asset('custom/css/bootstrap.vertical-tabs.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css"
          rel="stylesheet">
    {{-- <link href="{{ asset('custom/js/scroller/mCustomScrollbar.concat.min.css'}}" rel="stylesheet"> --}}
    <link href="{{ asset('custom/datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{ asset('custom/datepicker/custom.css')}}" rel="stylesheet">
    <link href="{{ asset('sximo5/js/plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{ asset('custom/jquery-loading/css/jquery.loadingModal.min.css')}}" rel="stylesheet">
    <link href="{{ asset('custom/css/jquery.step.css')}}" rel="stylesheet">
    <link href="{{ asset('custom/css/jquery.step.css')}}" rel="stylesheet">
    <link href="{{ asset('custom/js/bootstrap-toggle/bootstrap-toggle.min.css')}}" rel="stylesheet">
    <link href="{{ asset('custom/js/sweet-modal/min/jquery.sweet-modal.min.css')}}" rel="stylesheet" />

    <script type="text/javascript" src="{{ asset('sximo5/sximo.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sximo5/js/sximo.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sximo5/js/plugins/dropzone/dropzone.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sximo5/js/plugins/lightbox2/dist/js/lightbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sximo5/js/plugins/timepicker/bootstrap-timepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/jquery.steps.js') }}"></script>
    <!--DataTable Script -->
    <script type="text/javascript" src="{{ asset('custom/js/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/jquery-loading/js/jquery.loadingModal.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/sweet-modal/min/jquery.sweet-modal.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/jquery-cookie/src/jquery.cookie.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('wizard') }}"></script> -->
    <script type="text/javascript" src="{{ asset('custom/js/jwplayer-7.11.3/jwplayer.js') }}"></script>
    <script>jwplayer.key = "ysQTVfHC5iQ8flS72k460WTgxEPDzPg90dTu2NzjVT0=";</script>

    <script type="text/javascript" src="{{ asset('custom/croppie/js/croppie.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('custom/croppie/js/prism.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('custom/crop/crop.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/bootstrap-toggle/bootstrap-toggle.min.js') }}"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    {{-- <script src="{{ asset('custom/js/scroller/mCustomScrollbar.concat.min.js'}}"></script> --}}

        <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
@if(isset($enable_ng))
    <body class="sxim-init">
    @else
        <body>
        @endif
        <div class="overlay"></div>
        <div id="wrapper">
            @include('layouts.sidebar')
            @include('layouts.header')
        </div>

        @yield('content')
        @include('modal.sidemodal')

        <div class="modal fade" id="sximo-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-default">
                        <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 id="sximo-modal-header" class="modal-title">...</h4>
                    </div>
                    <div id="sximo-modal-content" class="modal-body"></div>
                </div>
            </div>
        </div>

        {{ SiteHelpers::showNotification() }}
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $('#sidemenu').sximMenu();
                scrollmenu()
                loadNotification();
                setInterval(function () {
                    // loadNotification()
                }, 10000);

                $('.switch-template').on('click', function (event) {
                    theme = $(this).attr('code');
                    url_theme = '{!! asset("sximo5") !!}/' + theme;
                    $.get('{{ url("set_theme") }}/' + theme, function () {

                        $('#switch-template').attr('href', url_theme);
                    })
                });

            });

            function loadNotification() {
                $.get('{{ url("home/load") }}', function (data) {
                    $('.notif-alert').html(data.total);
                    var html = '';
                    $.each(data.note, function (key, val) {
                        html += '<li><a href="' + val.url + '"><div class="message-center"><div class="user-img">' + val.image + '</div><div class="message-content"><h5>' + val.title + '</h5><span class="mail-desc">' + val.text + '</span> <span class="time">' + val.date + '</span> </div></div><div class="clr"></div></a></li>';
                    });
                    $('#notification-menu').html(html);
                });
            }
        </script>

        </body>
</html>