<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h3>ตอบกลับ: สมัคร U League {{ DataHelpers::year() }} Pitching </h3>

<div>
    <p> เรียน ผู้สมัคร คุณได้ทำการลงทะเบียนเพื่อเข้าร่วม U League {{ DataHelpers::year() }} Pitching เรียบร้อยแล้ว</p>
    <p>โปรดตรวจสอบรายละเอียดกำหนดการและการเตรียมตัวได้ใน <a href="http://www.startupthailandacamedy.org">www.startupthailandacamedy.org</a></p>
</div>
</body>
</html>