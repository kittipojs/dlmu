<p>
    เรียน หัวหน้าทีม
    <br><br>
    ยินดีด้วย สถานะการสมัครเพื่อขัน U League {{ DataHelpers::year() }} ของคุณได้รับการอนุมัติแล้ว! ดูข้อมูลเพิ่มเติมที่ <a href="http://www.startupthailandacademy.org/">http://www.startupthailandacademy.org/</a>
    <br><br>
    ขอบคุณครับ
</p>