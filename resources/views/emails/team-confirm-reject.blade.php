<p>
    เรียน ผู้สมัคร <br>
    คุณได้รับการปฎิเสธจากเพื่อนเพื่อเข้าร่วมทีมแข่งขัน U League {{ DataHelpers::year() }} Pitching โปรดตรวจสอบการลงทะเบียนอีกครั้ง
</p>