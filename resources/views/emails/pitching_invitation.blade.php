<p>เรียน ผู้สมัคร คุณได้รับคำเชิญจากเพื่อนพื่อเข้าเร่วมทีมแข่งขัน U League {{ DataHelpers::year() }}</p>
<p>โปรดคลิก <a href="{{ $invitation_url}}">{{ $invitation_url}} </a></p>