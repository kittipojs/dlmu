<p>
    เรียน หัวหน้าทีม
    <br><br>
    ตามที่คุณได้สมัครเพื่อเข้าแข่งขัน U League {{ DataHelpers::year() }} Pitching มานั้น ทีมงานได้ทำการตรวจสอบข้อมูลของทีมคุณ และพบว่าทีมของคุณไม่ผ่านคุณสมบัติ กรุณาเข้าสู่ระบบ <a href="http://www.startupthailandacademy.org/">http://www.startupthailandacademy.org/</a> เพื่อดูรายละเอียด
    <br><br>
    ขอบคุณครับ
</p>