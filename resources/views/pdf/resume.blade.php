<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/medilab/css/bootstrap.min.css') }}">
    <style>
        @font-face {
            font-family: 'THSarabun';
            font-style: normal;
            font-weight: normal;
            src: url("{{ asset('custom/font/THSarabunNew/THSarabunNew.ttf') }}") format('truetype');
        }

        @font-face {
            font-family: 'THSarabunBold';
            font-style: normal;
            font-weight: bold;
            src: url("{{ asset('custom/font/THSarabunNew/THSarabunNew Bold.ttf') }}") format('truetype');
        }

        @font-face {
            font-family: 'THSarabunBoldItalic';
            font-style: italic;
            font-weight: normal;
            src: url("{{ asset('custom/font/THSarabunNew/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        @font-face {
            font-family: 'THSarabunItalic';
            font-style: italic;
            font-weight: bold;
            src: url("{{ asset('custom/font/THSarabunNew/THSarabunNew Italic.ttf') }}") format('truetype');
        }

        body {
            font-family: 'THSarabun';
        }

        .page {
            page-break-after: always;
            page-break-inside: avoid;
        }
    </style>
</head>
<body>
<div class="page">
    {{--<img src="{{ public_path('uploads/users') }}/{{$user->avatar}}" class="img-thumbnail" width="300"--}}
         {{--height="300" alt="">--}}
{{--    <p>{{ public_path('uploads/images/1510102158-82083309.jpg') }}</p>--}}
</div>
<div class="page">
    <div class="row">
        <div class="col-xs-2">
            {{ $user->name_title }}
        </div>
        <div class="col-xs-5">
            {{ $user->first_name }}
        </div>
        <div class="col-xs-5">
            {{ $user->last_name }}
        </div>
    </div>
    <div class="row">
        <div class="col-xs-2">
            {{ DataHelpers::Occupation($user->occupation) }}
        </div>
        <div class="col-xs-5">
            {{ DataHelpers::startupLookup($user->startup_lookup) }}
        </div>
        <div class="col-xs-5">
            {{ $user->education }}
        </div>
    </div>
</div>
</body>
</html>