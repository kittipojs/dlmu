<!-- Modal Add-->
<div class="modal fade" id="uleague-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal-header"></h4>
            </div>
            <div class="modal-body">
                <div class="row text-center">
                    <div class="col-sm-6" style="background-color:#a1a1a1;">
                        <div class="padding">
                            <h4>Startup League Camping</h4>

                            <br><br>
                            <a class="btn btn-default" href="{{ url('uleague/camping') }}">เข้าร่วม</a>
                            <br><br>
                        </div>
                    </div>
                    <div class="col-sm-6" style="background-color:#7e7e7e;">
                        <div class="padding">
                            <h4>Startup League Pitching</h4>

                            <br><br>
                            <a class="btn btn-default" href="{{ url('uleague/pitching') }}">เข้าร่วม</a>
                            <br><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<script>
    $(document).ready(function () {

    });
</script>