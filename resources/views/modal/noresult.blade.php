<!-- Modal Confirm-->
<div class="modal" id="noResult-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog modal-sm vertical-align-center" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h5 class="modal-title" id="modal-header">ไม่พบข้อมูล</h5>
                </div>
                <div class="modal-body" id="modal-info">
                    ไม่พบข้อมูลที่คุณเลือก
                </div>
                <div class="modal-footer">
                    <div align="center">
                        <button class="btn btn-success" type="button" data-dismiss="modal"> ตกลง </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

    });
</script>