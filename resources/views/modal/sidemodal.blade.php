<div class="modal right fade" id="side_modal" tabindex="-1" role="dialog" aria-labelledby="mySideModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal-header"></h4>
            </div>

            <div class="modal-body" id="modal-info">
                <!-- Ajax data -->
            </div>

        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<script>
    $(document).ready(function () {

    });
</script>