@if($setting['form-method'] =='native')
    <div class="sbox">
        <div class="sbox-title clearfix">
            <h3> Form Update </h3>
            <div class="sbox-tools">
                <a href="javascript://ajax" onclick="ajaxViewClose('#{{ $pageModule }}')" class="tips btn btn-sm  "
                   title="{{ __('core.btn_back') }}"><i class="fa  fa-times"></i></a>
            </div>
        </div>

        <div class="sbox-content">
            @endif
            {!! Form::open(array('url'=>'e-admin/faculty?return='.$return, 'class'=>'form-horizontal validated','files' => true, 'parsley-validate'=>'','novalidate'=>' ','id'=> 'facultyFormAjax')) !!}
            <div class="col-md-12">
                <fieldset>
                    <legend> faculty</legend>
                    {{--<div class="form-group  " >--}}
                    {{--<label for="Faculty Id" class=" control-label col-md-4 text-left"> Faculty Id <span class="asterix"> * </span></label>--}}
                    <div class="col-md-6 hide">
                        <input type='text' name='faculty_id' id='faculty_id' value='{{ $row['faculty_id'] }}'
                               class='form-control input-sm '/>
                    </div>
                    {{--<div class="col-md-2">--}}

                    {{--</div>--}}
                    {{--</div>--}}
                    <div class="form-group  ">
                        <label for="Faculty Name" class=" control-label col-md-4 text-left"> Faculty Name <span
                                    class="asterix"> * </span></label>
                        <div class="col-md-6">
                            <input type='text' name='faculty_name' id='faculty_name' value='{{ $row['faculty_name'] }}'
                                   required class='form-control input-sm '/>
                        </div>
                        <div class="col-md-2">

                        </div>
                    </div>
                </fieldset>
            </div>


            <div style="clear:both"></div>

            <div class="form-group">
                <label class="col-sm-4 text-right">&nbsp;</label>
                <div class="col-sm-8">
                    <button type="submit" class="btn btn-primary btn-sm" ><i
                                class="fa fa-play-circle"></i> {{ Lang::get('core.sb_save') }} </button>
                    <button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm">
                        <i class="fa fa-remove "></i> {{ Lang::get('core.sb_cancel') }} </button>
                </div>
            </div>
            {!! Form::close() !!}


            @if($setting['form-method'] =='native')
        </div>
    </div>
    @endif


    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.editor').summernote();

            $('.tips').tooltip();
            $(".select2").select2({width: "98%"});
            $('.date').datepicker({format: 'yyyy-mm-dd', autoClose: true})
            $('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
            $('.removeMultiFiles').on('click', function () {
                var removeUrl = '{{ url("e-admin/faculty/removefiles?file=")}}' + $(this).attr('url');
                $(this).parent().remove();
                $.get(removeUrl, function (response) {
                });
                $(this).parent('div').empty();
                return false;
            });

            var form = $('#facultyFormAjax');
            form.parsley();
            form.submit(function () {

                if (form.parsley().isValid()) {
                    var options = {
                        dataType: 'json',
                        beforeSubmit: showRequest,
                        success: showResponse
                    }
                    $(this).ajaxSubmit(options);
                    return false;

                } else {
                    return false;
                }

            });

        });

        function showRequest() {

        }

        function showResponse(data) {
            if (data.status == 'success') {
                ajaxViewClose('#{{ $pageModule }}');
                console.log('#{{ $pageModule }}','{{ $pageUrl }}/data');
                ajaxFilter('#{{ $pageModule }}', '{{ $pageUrl }}/data');
                notyMessage(data.message);
                $('#sximo-modal').modal('hide');
            } else {
                notyMessageError(data.message);
                return false;
            }
        }

    </script>