@if($setting['view-method'] =='native')
    <div class="sbox">
        <div class="sbox-title clearfix">
            <div class="sbox-tools pull-left">
                <a href="{{ ($prevnext['prev'] != '' ? url('courses/'.$prevnext['prev'].'?return='.$return ) : '#') }}"
                   class="tips btn btn-sm" onclick="ajaxViewDetail('#courses',this.href); return false; "><i
                            class="fa fa-arrow-left"></i> </a>
                <a href="{{ ($prevnext['next'] != '' ? url('courses/'.$prevnext['next'].'?return='.$return ) : '#') }}"
                   class="tips btn btn-sm " onclick="ajaxViewDetail('#courses',this.href); return false; "> <i
                            class="fa fa-arrow-right"></i> </a>
            </div>

            <div class="sbox-tools">
                <a href="javascript://ajax" onclick="ajaxViewClose('#{{ $pageModule }}')" class="tips btn btn-sm  "
                   title="{{ __('core.btn_back') }}"><i class="fa  fa-times"></i></a>
            </div>
        </div>
        <div class="sbox-content">
            @endif

            <table class="table table-striped">
                <tbody>
                <tr>
                    <td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Course Id', (isset($fields['course_id']['language'])? $fields['course_id']['language'] : array())) }}</td>
                    <td>{{ $row->course_id}} </td>

                </tr>
                <tr>
                    <td width='30%'
                        class='label-view text-right'>{{ SiteHelpers::activeLang('User Id', (isset($fields['user_id']['language'])? $fields['user_id']['language'] : array())) }}</td>
                    <td>{{ $row->user_id}} </td>

                </tr>
                <tr>
                    <td width='30%'
                        class='label-view text-right'>{{ SiteHelpers::activeLang('Course Code', (isset($fields['course_code']['language'])? $fields['course_code']['language'] : array())) }}</td>
                    <td>{{ $row->course_code}} </td>
                </tr>
                <tr>
                    <td width='30%'
                        class='label-view text-right'>{{ SiteHelpers::activeLang('Course Title', (isset($fields['course_title']['language'])? $fields['course_title']['language'] : array())) }}</td>
                    <td>{{ $row->course_title}} </td>
                </tr>
                <tr>
                    <td width='30%'
                        class='label-view text-right'>{{ SiteHelpers::activeLang('Course Intro', (isset($fields['course_intro']['language'])? $fields['course_intro']['language'] : array())) }}</td>
                    <td>{{ $row->course_intro}} </td>
                </tr>
                <tr>
                    <td width='30%'
                        class='label-view text-right'>{{ SiteHelpers::activeLang('Course Detail', (isset($fields['course_detail']['language'])? $fields['course_detail']['language'] : array())) }}</td>
                    <td>{{ $row->course_detail}} </td>
                </tr>

                <tr>
                    <td width='30%'
                        class='label-view text-right'>{{ SiteHelpers::activeLang('Course Objective', (isset($fields['course_objective']['language'])? $fields['course_objective']['language'] : array())) }}</td>
                    <td>{{ $row->course_objective}} </td>

                </tr>

                <tr>
                    <td width='30%'
                        class='label-view text-right'>{{ SiteHelpers::activeLang('Course Thumbnail', (isset($fields['course_thumbnail']['language'])? $fields['course_thumbnail']['language'] : array())) }}</td>
                    <td>{{ $row->course_thumbnail}} </td>

                </tr>

                <tr>
                    <td width='30%'
                        class='label-view text-right'>{{ SiteHelpers::activeLang('Created At', (isset($fields['created_at']['language'])? $fields['created_at']['language'] : array())) }}</td>
                    <td>{{ $row->created_at}} </td>

                </tr>

                <tr>
                    <td width='30%'
                        class='label-view text-right'>{{ SiteHelpers::activeLang('Updated At', (isset($fields['updated_at']['language'])? $fields['updated_at']['language'] : array())) }}</td>
                    <td>{{ $row->updated_at}} </td>

                </tr>

                </tbody>
            </table>


            @if($setting['form-method'] =='native')
        </div>
    </div>
@endif		