

		 {!! Form::open(array('url'=>'courses', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> courses</legend>
									
									  <div class="form-group  " >
										<label for="Course Id" class=" control-label col-md-4 text-left"> Course Id <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='course_id' id='course_id' value='{{ $row['course_id'] }}' 
						required     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="User Id" class=" control-label col-md-4 text-left"> User Id <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='user_id' id='user_id' value='{{ $row['user_id'] }}' 
						required     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Course Code" class=" control-label col-md-4 text-left"> Course Code <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='course_code' id='course_code' value='{{ $row['course_code'] }}' 
						required     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Course Title" class=" control-label col-md-4 text-left"> Course Title <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='course_title' id='course_title' value='{{ $row['course_title'] }}' 
						required     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Course Intro" class=" control-label col-md-4 text-left"> Course Intro <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='course_intro' id='course_intro' value='{{ $row['course_intro'] }}' 
						required     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Course Detail" class=" control-label col-md-4 text-left"> Course Detail <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='course_detail' rows='5' id='course_detail' class='form-control input-sm '  
				         required  >{{ $row['course_detail'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Course Objective" class=" control-label col-md-4 text-left"> Course Objective <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='course_objective' rows='5' id='course_objective' class='form-control input-sm '  
				         required  >{{ $row['course_objective'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Course Thumbnail" class=" control-label col-md-4 text-left"> Course Thumbnail <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='course_thumbnail' id='course_thumbnail' value='{{ $row['course_thumbnail'] }}' 
						required     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Created At" class=" control-label col-md-4 text-left"> Created At <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('created_at', $row['created_at'],array('class'=>'form-control input-sm datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Updated At" class=" control-label col-md-4 text-left"> Updated At <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('updated_at', $row['updated_at'],array('class'=>'form-control input-sm datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 <input type="hidden" name="action_task" value="public" />
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
