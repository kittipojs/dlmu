@if($setting['form-method'] =='native')
<div class="sbox">
    <div class="sbox-title clearfix">
        <h3> Form Update </h3>
        <div class="sbox-tools" >
            <a href="javascript://ajax" onclick="ajaxViewClose('#{{ $pageModule }}')" class="tips btn btn-sm  " title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a>		
        </div>
    </div>	
    <div class="sbox-content">
        @endif	
        {!! Form::open(array('url'=>'courses?return='.$return, 'class'=>'form-horizontal validated','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'coursesFormAjax')) !!}
        <div class="col-md-12">
            <fieldset><legend> courses</legend>
                <div class="form-group">
                    <label for="Course Id" class=" control-label col-md-4 text-left"> Course Id <span class="asterix"> * </span></label>
                    <div class="col-md-6">
                        <input type='text' name='course_id' id='course_id' value='{{ $row['course_id'] }}' required class='form-control input-sm' /> 
                    </div> 
                    <div class="col-md-2">

                    </div>
                </div> 					
                <div class="form-group">
                    <label for="User Id" class=" control-label col-md-4 text-left"> User Id <span class="asterix"> * </span></label>
                    <div class="col-md-6">
                        <input type='text' name='user_id' id='user_id' value='{{ $row['user_id'] }}' required class='form-control input-sm' /> 
                    </div> 
                    <div class="col-md-2">

                    </div>
                </div> 					
                <div class="form-group">
                    <label for="Course Code" class=" control-label col-md-4 text-left"> Course Code <span class="asterix"> * </span></label>
                    <div class="col-md-6">
                        <input type='text' name='course_code' id='course_code' value='{{ $row['course_code'] }}' required  class='form-control input-sm ' /> 
                    </div> 
                    <div class="col-md-2">

                    </div>
                </div> 					
                <div class="form-group  " >
                    <label for="Course Title" class=" control-label col-md-4 text-left"> Course Title <span class="asterix"> * </span></label>
                    <div class="col-md-6">
                        <input  type='text' name='course_title' id='course_title' value='{{ $row['course_title'] }}' 
                               required     class='form-control input-sm ' /> 
                    </div> 
                    <div class="col-md-2">

                    </div>
                </div> 					
                <div class="form-group  " >
                    <label for="Course Intro" class=" control-label col-md-4 text-left"> Course Intro <span class="asterix"> * </span></label>
                    <div class="col-md-6">
                        <input  type='text' name='course_intro' id='course_intro' value='{{ $row['course_intro'] }}' 
                               required     class='form-control input-sm ' /> 
                    </div> 
                    <div class="col-md-2">

                    </div>
                </div> 					
                <div class="form-group  " >
                    <label for="Course Detail" class=" control-label col-md-4 text-left"> Course Detail <span class="asterix"> * </span></label>
                    <div class="col-md-6">
                        <textarea name='course_detail' rows='5' id='course_detail' class='form-control input-sm '  
                                  required  >{{ $row['course_detail'] }}</textarea> 
                    </div> 
                    <div class="col-md-2">

                    </div>
                </div> 					
                <div class="form-group  " >
                    <label for="Course Objective" class=" control-label col-md-4 text-left"> Course Objective <span class="asterix"> * </span></label>
                    <div class="col-md-6">
                        <textarea name='course_objective' rows='5' id='course_objective' class='form-control input-sm '  
                                  required  >{{ $row['course_objective'] }}</textarea> 
                    </div> 
                    <div class="col-md-2">

                    </div>
                </div> 					
                <div class="form-group  " >
                    <label for="Course Thumbnail" class=" control-label col-md-4 text-left"> Course Thumbnail <span class="asterix"> * </span></label>
                    <div class="col-md-6">
                        <input  type='text' name='course_thumbnail' id='course_thumbnail' value='{{ $row['course_thumbnail'] }}' 
                               required     class='form-control input-sm ' /> 
                    </div> 
                    <div class="col-md-2">

                    </div>
                </div> 					
                <div class="form-group  " >
                    <label for="Created At" class=" control-label col-md-4 text-left"> Created At <span class="asterix"> * </span></label>
                    <div class="col-md-6">

                        <div class="input-group m-b" style="width:150px !important;">
                            {!! Form::text('created_at', $row['created_at'],array('class'=>'form-control input-sm datetime', 'style'=>'width:150px !important;')) !!}
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>

                    </div> 
                    <div class="col-md-2">

                    </div>
                </div> 					
                <div class="form-group  " >
                    <label for="Updated At" class=" control-label col-md-4 text-left"> Updated At <span class="asterix"> * </span></label>
                    <div class="col-md-6">

                        <div class="input-group m-b" style="width:150px !important;">
                            {!! Form::text('updated_at', $row['updated_at'],array('class'=>'form-control input-sm datetime', 'style'=>'width:150px !important;')) !!}
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>

                    </div> 
                    <div class="col-md-2">

                    </div>
                </div> </fieldset>
        </div>




        <div style="clear:both"></div>	

        <div class="form-group">
            <label class="col-sm-4 text-right">&nbsp;</label>
            <div class="col-sm-8">	
                <button type="submit" class="btn btn-primary btn-sm "><i class="fa fa-play-circle"></i>  {{ Lang::get('core.sb_save') }} </button>
                <button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm"><i class="fa fa-remove "></i>  {{ Lang::get('core.sb_cancel') }} </button>
            </div>			
        </div> 		 
        {!! Form::close() !!}


        @if($setting['form-method'] =='native')
    </div>	
</div>	
@endif	


</div>	

<script type="text/javascript">
    $(document).ready(function() { 


        $('.editor').summernote();

        $('.tips').tooltip();	
        $(".select2").select2({ width:"98%"});	
        $('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
        $('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'}); 		
        $('.removeMultiFiles').on('click',function(){
            var removeUrl = '{{ url("courses/removefiles?file=")}}'+$(this).attr('url');
            $(this).parent().remove();
            $.get(removeUrl,function(response){});
            $(this).parent('div').empty();	
            return false;
        });

        var form = $('#coursesFormAjax'); 
        form.parsley();
        form.submit(function(){

            if(form.parsley().isValid()){			
                var options = { 
                    dataType:      'json', 
                    beforeSubmit :  showRequest,
                    success:       showResponse  
                }  
                $(this).ajaxSubmit(options); 
                return false;

            } else {
                return false;
            }		

        });

    });

    function showRequest()
    {

    }  
    function showResponse(data)  {		

        if(data.status == 'success')
        {
            ajaxViewClose('#{{ $pageModule }}');
            ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
            notyMessage(data.message);	
            $('#sximo-modal').modal('hide');	
        } else {
            notyMessageError(data.message);	
            return false;
        }	
    }			 

</script>		 