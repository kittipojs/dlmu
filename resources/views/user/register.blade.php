@extends('layouts.login')

@section('content')
<div class="text-center">
    @if(file_exists(public_path().'/uploads/images/logo-light.png' ))
    <a  href="{{ URL::to('')}}"><img src="{{ asset('uploads/images/logo-light.png') }}" alt="{{ config('sximo.cnf_appname') }}"/></a>
    @else
    <h3 class="text-center"> {{ config('sximo.cnf_appname') }} </h3>
    @endif
</div>
<!-- <p class="text-center"> {{ config('sximo.cnf_appdesc') }}  </p> -->

<h3 class="text-center">สมัครสมาชิกใหม่</h3>
<div class="padding-lg"></div>

{!! Form::open(array('url'=>'user/create', 'class'=>'form-horizontal','parsley-validate'=>'','novalidate'=>' ','id'=>'register-form' )) !!}

@if(Session::has('message'))
{!! Session::get('message') !!}
@endif

<ul class="parsley-error-list">
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>

<div class="form-group ">
    <label for="email" class="col-md-3 control-label"> อีเมล * </label>
    <div class="col-md-9">
        {!! Form::email('email', $autofill['email'], array('class'=>'form-control', 'required'=>'true','placeholder'=> 'sample@mail.com', 'id' => 'email')) !!}
    </div>
</div>

<div class="form-group ">
    <label for="confirm_email" class="col-md-3 control-label"> ยืนยันอีเมล * </label>
    <div class="col-md-9" id="div_confirm_email">
        {!! Form::email('confirm_email', null, array('class'=>'form-control confirmation', 'required'=>'true','placeholder'=> 'โปรดกรอกอีเมลอีกครั้ง', 'id' => 'confirm_email', 'autocomplete'=>'off')) !!}
        <p id="confirm_email_text"></p>
    </div>
</div>

<div class="form-group">
    <label for="password" class="col-md-3 control-label"> รหัสผ่าน * </label>
    <div class="col-md-9">
        {!! Form::password('password', array('class'=>'form-control','required'=>'true', 'placeholder'=> 'ความยาวไม่ต่ำกว่า 6 ตัวอักษร', 'id' => 'password')) !!}
    </div>
</div>

<div class="form-group">
    <label for="password_confirmation" class="col-md-3 control-label"> ยืนยันรหัสผ่าน * </label>
    <div class="col-md-9">
        {!! Form::password('password_confirmation', array('class'=>'form-control confirmation', 'placeholder'=> 'ความยาวไม่ต่ำกว่า 6 ตัวอักษร', 'required'=>'true', 'id' => 'password_confirmation', 'autocomplete'=>'off')) !!}
        <p id="confirm_password_text"></p>
    </div>
</div>

<div class="form-group">
    <label for="name_title" class="col-md-3 control-label"> คำนำหน้า * </label>
    <div class="col-md-3">
        <select name="name_title" id="name_title" class="form-control">
            <option value="นาย">นาย</option>
            <option value="นาง">นาง</option>
            <option value="นางสาว">นางสาว</option>
        </select>
    </div>
    <div class="col-md-8"></div>
</div>

<div class="form-group">
    <label for="name" class="col-md-3 control-label">ชื่อ - นามสกุล  (ภาษาไทย) *</label>
    <div class="col-md-4">
        {!! Form::text('firstname', $autofill['firstname'], array('class'=>'form-control', 'placeholder'=> 'ชื่อ (ภาษาไทย)', 'required'=>'true', 'id' => 'firstname' )) !!}
    </div>
    <div class="col-md-5">
        {!! Form::text('lastname', $autofill['lastname'], array('class'=>'form-control', 'placeholder'=> 'นามสกุล (ภาษาไทย)', 'required'=>'true', 'id' => 'lastname')) !!}
    </div>
</div>

<div class="form-group">
    <label for="citizen_id" class="col-md-3 control-label"> หมายเลขบัตรประชาชน * </label>
    <div class="col-md-9">
        {!! Form::text('citizen_id', null, array('class'=>'form-control', 'placeholder'=> 'หมายเลขบัตรประชาชน 13 หลัก', 'required'=>'true', 'id' => 'citizen_id')) !!}
    </div>
</div>

<div class="form-group">
    <label for="confirm_citizen_id" class="col-md-3 control-label"> ยืนยันหมายเลขบัตรประชาชน * </label>
    <div class="col-md-9">
        {!! Form::text('confirm_citizen_id', null, array('class'=>'form-control confirmation', 'placeholder'=> 'โปรดกรอกหมายเลขบัตรประชาชน 13 หลักอีกครั้ง', 'required'=>'true', 'id' => 'confirm_citizen_id', 'autocomplete'=>'off')) !!}
        <p id="confirm_citizen_id_text"></p>
    </div>
</div>

<div class="form-group">
    <label for="dob" class="col-md-3 control-label"> วันเกิด * </label>
    <div class="col-md-9" id="myDatetimeId">
        {!! Form::text('dob', null, array('class'=>'form-control date', 'placeholder'=> 'วันเกิดของคุณ', 'required'=>'true', 'id' => 'dob')) !!}
    </div>
</div>

<div class="form-group">
    <label for="education" class="col-md-3 control-label"> ระดับการศึกษา * </label>
    <div class="col-md-9">
        <select name="education" id="education" class="form-control" required>
            <option value="" selected="true">--- Please Select ---</option>
            <option value="ปริญญาเอก">ปริญญาเอก</option>
            <option value="ปริญญาโท">ปริญญาโท</option>
            <option value="ปริญญาตรี">ปริญญาตรี</option>
            <option value="ปวช.">ปวช.</option>
            <option value="ปวส.">ปวส.</option>
            <option value="มัธยม 1">มัธยม 1</option>
            <option value="มัธยม 2">มัธยม 2</option>
            <option value="มัธยม 3">มัธยม 3</option>
            <option value="มัธยม 4">มัธยม 4</option>
            <option value="มัธยม 5">มัธยม 5</option>
            <option value="มัธยม 6">มัธยม 6</option>
            <option value="ต่ำกว่าหรือเท่ากับประถมศึกษาปีที่ 6">ต่ำกว่าหรือเท่ากับประถมศึกษาปีที่ 6</option>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="occupation" class="col-md-3 control-label"> อาชีพ * </label>
    <div class="col-md-9">
        <select id="occupation" name="occupation" class="form-control">
            <option value="" selected="true">--- Please Select ---</option>
            <option value="C03">ธุรกิจส่วนตัว</option>
            <option value="C09">อาชีพอิสระ</option>
            <option value="C07">ข้าราชการ/พนักงานรัฐวิสาหกิจ</option>
            <option value="C11">นักลงทุนในหลักทรัพย์มืออาชีพ</option>
            <option value="C02">ผู้แนะนำการลงทุนในหลักทรัพย์</option>
            <option value="C08">พนักงานบริษัทในธุรกิจการเงิน</option>
            <option value="C12">บุคลากรทางการแพทย์</option>
            <option value="C13">วิศวกร</option>
            <option value="C14">บุคลากรทางการศึกษาและวิจัย</option>
            <option value="C05">พนักงานบริษัทเอกชนอื่นๆ</option>
            <option value="C04">นักเรียน/นักศึกษา</option>
            <option value="C01">เกษียณ</option>
            <option value="C06">แม่บ้าน/พ่อบ้าน</option>
            <option value="C10">อื่นๆ</option>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="faculty_id" class="col-sm-3 control-label"> คณะที่จบการศึกษา * </label>
    <div class="col-sm-9">
        <select name="faculty_id" id="faculty_id" class="form-control" required></select>
    </div>
</div>

<div class="form-group">
    <label for="sub_faculty" class="col-sm-3 control-label"> สาขาที่จบการศึกษา * </label>
    <div class="col-sm-9">
        <input type="text" name="sub_faculty" id="sub_faculty" class="form-control" placeholder="สาขาที่จบการศึกษา" required >
    </div>
</div>

<div class="form-group">
    <label for="address" class="col-sm-3 control-label"> ที่อยู่ </label>
    <div class="col-sm-9">
        {!! Form::text('address', null, array('class'=>'form-control', 'placeholder'=> 'ที่อยู่อาศัยปัจจุบัน')) !!}
    </div>
</div>

<div class="form-group">
    <label for="mobile_number" class="control-label col-sm-3"> เบอร์มือถือ * </label>
    <div class="col-sm-8">
        {!! Form::text('mobile_number',  null, array('class'=>'form-control input-sm', 'required'=>'true', 'placeholder'=> 'เบอร์มือถือปัจจุบัน')) !!}
    </div>
</div>

<div class="form-group ">
    <label for="address" class="col-sm-3 control-label"> จังหวัด * </label>
    <div class="col-sm-9">
        <select name="province_id" id="province_id" class="form-control" required>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="postcode" class="col-sm-3 control-label"> รหัสไปรษณีย์ * </label>
    <div class="col-sm-9">
        {!! Form::text('postcode', null, array('class'=>'form-control', 'required'=>'true', 'placeholder'=> 'รหัสไปรษณีย์')) !!}
    </div>
</div>

<div class="form-group">
    <label for="startup_lookup" class="col-sm-3 control-label"> หัวข้อที่สนใจ (เลือกได้มากกว่า 1 ข้อ) * </label>
    <div class="col-sm-9">
        <p>
            <input id="lookup_Checkbox1" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="1" />
            <label>การแพทย์และสาธารณสุข (MedTech/ Health Tech)</label>
        </p>
        <p>
            <input id="lookup_Checkbox2" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="2" />
            <label>เกษตรและอาหาร (AgriTech/ Food Startup)</label>
        </p>
        <p>
            <input id="lookup_Checkbox3" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="3" />
            <label>อสังหาริมทรัพย์ (Property Tech)</label>
        </p>
        <p>
            <input id="lookup_Checkbox4" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="4" />
            <label>การเงินและการธนาคาร (FinTech)</label>
        </p>
        <p>
            <input id="lookup_Checkbox5" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="5" />
            <label>การศึกษา (EdTech)</label>
        </p>
        <p>
            <input id="lookup_Checkbox6" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="6" />
            <label>การท่องเที่ยว (TravelTech)</label>
        </p>
        <p>
            <input id="lookup_Checkbox7" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="7" />
            <label>ไลฟ์สไตล์ (LifeStyle)</label>
        </p>
        <p>
            <input id="lookup_Checkbox8" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="8" />
            <label>พาณิชยกรรมอิเล็กทรอนิกส์ (E-Commerce)</label>
        </p>
        <p>
            <input id="lookup_Checkbox9" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="9" />
            <label>ภาครัฐ (GovTech)</label>
        </p>
        <p>
            <input id="lookup_Checkbox10" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="14" />
            <label>อุตสาหกรรม (Industrial Tech)</label>
        </p>
        <p>
            <input id="lookup_Checkbox11" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="10" />
            <label>แหล่งเงินทุน (Venture Capital/Coperate Venture Capital)</label>
        </p>
        <p>
            <input id="lookup_Checkbox12" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="11" />
            <label>พันธมิตร (Partners)</label>
        </p>
        <p>
            <input id="lookup_Checkbox13" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="12" />
            <label>การสนับสนุนจากรัฐบาล (Government Support)</label>
        </p>
        <p>
            <input id="lookup_Checkbox14" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="13" />
            <label>คอร์สเรียนออนไลน์ (Online Learning Courses)</label>
        </p>
    </div>
</div>

<div class="form-group">
    <label for="address" class="col-sm-3 control-label"> สนใจรับข่าวสารจาก Startup Thailand </label>
    <div class="col-sm-9">
        <input type="radio" class="form-check-input" name="is_subscribe" value="1" checked>
        <label class="form-check-label" for="is_subscribe">ใช่</label>
        <span>&nbsp;&nbsp;&nbsp;</span>
        <input type="radio" class="form-check-input" name="is_subscribe" value="0" >
        <label class="form-check-label" for="is_subscribe">ไม่ใช่</label>
    </div>
</div>

@if(config('sximo.cnf_recaptcha') =='true')
<div class="form-group has-feedback" align="center">
    <div class="g-recaptcha" data-sitekey="6Lf-X0UUAAAAAPIcp_R1VJdPnm_9ZuKG4DkP67f3"></div>
    <div class="clr"></div>
</div>
@endif

<div class="row form-actions padding-lg">
    <div class="col-sm-12">
        <button type="submit" style="width:100%;" class="btn btn-primary pull-right">
            <i class="icon-user-plus"></i> @lang('core.signup')
        </button>
    </div>
</div>
<div class="padding"></div>
<p class="text-center">
    <a href="{{ URL::to('user/login')}}">@lang('core.signin')</a> |
    <a href="{{ URL::to('')}}">@lang('core.backtosite')</a>
</p>

{!! Form::close() !!}
@include('modal.modal')
<script type="text/javascript">
    $(document).ready(function () {
        confirmPassword();
        confirmEmail();
        confirmCitizenID();
        
        $('.confirmation').on("cut copy paste",function(e) {
            e.preventDefault();
        });

        $('#register-form').parsley();

        $('#register-form').submit(function(event){
            //set empty modal text
            var modal           = $('#nia-modal'),
                modal_header    = $('#modal-header'),
                modal_body      = $('#modal-info');
            modal_header.html(``);
            modal_body.html(``);

            event.preventDefault(); //this will prevent the default submit

            var sel = $('input.startup_lookup:checked').map(function(_, el) { 
                return $(el).val();
            }).get();

            var password            = $('#password').val(),
                confirm_password    = $('#password_confirmation').val(),
                email               = $('#email').val(),
                confirm_email       = $('#confirm_email').val(),
                citizen_id          = $('#citizen_id').val(),
                confirm_citizen_id  = $('#confirm_citizen_id').val();

            if( password !== confirm_password ){
                modal_header.html(`โปรดยืนยันข้อมูล`);
                modal_body.html(`โปรดยืนยันรหัสผ่านให้ถูกต้อง`);
                modal.modal('show');
                return false;
            }

            if( email !== confirm_email ){
                modal_header.html(`โปรดยืนยันข้อมูล`);
                modal_body.html(`โปรดยืนยันอีเมลให้ถูกต้อง`);
                modal.modal('show');
                return false;
            }

            if(citizen_id !== confirm_citizen_id ){
                modal_header.html(`โปรดยืนยันข้อมูล`);
                modal_body.html(`โปรดยืนยันหมายเลขบัตรประชาชนให้ถูกต้อง`);
                modal.modal('show');
                return false;
            }

            if(sel!=''){
                if(checkCitizenID( citizen_id )){
                    $(this).unbind('submit').submit(); 
                }
                else{
                    modal_header.html(`ข้อมูลผิดพลาด`);
                    modal_body.html(`หมายเลขบัตรประชาชนไม่ถูกต้อง`);
                    modal.modal('show');
                }
            }
            else{
                modal_header.html(`โปรดเลือดหัวข้อที่สนใจ`);
                modal_body.html(`เลือกอย่างน้อย 1 ข้อ`);
                modal.modal('show');
            }
        });
        

        $('.date').datepicker({container:'#myDatetimeId', format: 'yyyy-mm-dd', autoclose: true, changeMonth: true, changeYear: true});

        $("#faculty_id").jCombo("{!! url('select/faculty') !!}");
        $("#province_id").jCombo("{!! url('select/province') !!}");


        function checkCitizenID(id){
            if(id.length != 13) return false;

            for(i=0, sum=0; i < 12; i++){
                sum += parseFloat(id.charAt(i))*(13-i);
            }

            if((11-sum%11)%10!=parseFloat(id.charAt(12))){
                return false;
            }
            else{
                return true;
            }
        }

    });

    function confirmPassword() {
        $('#password_confirmation').keyup(function () {
            var password        = $('#password').val(),
                confirm_password= $('#password_confirmation').val();
            if( password !== confirm_password){
                $('#confirm_password_text').css({"background": "#c30505","color": "white","display":"inline-block"});
                $('#confirm_password_text').html(`&nbsp;&nbsp; โปรดกรอกรหัสผ่านเดียวกับด้านบนเพื่อยืนยัน * &nbsp;&nbsp;`);
            } else {
                $('#confirm_password_text').removeAttr( 'style' );
                $('#confirm_password_text').html(``);
            }
        })
    }

    function confirmEmail() {
        $('#confirm_email').keyup(function () {
            var email           = $('#email').val(),
                confirm_email   = $('#confirm_email').val();
            if(email !== confirm_email){
                $('#confirm_email_text').css({"background": "#c30505","color": "white","display":"inline-block"});
                $('#confirm_email_text').html(`&nbsp;&nbsp; โปรดกรอกอีเมลเดียวกับด้านบนเพื่อยืนยัน * &nbsp;&nbsp;`);
            } else {
                $('#confirm_email_text').removeAttr( 'style' );
                $('#confirm_email_text').html(``);
            }

        })
    }

    function confirmCitizenID() {
        $('#confirm_citizen_id').keyup(function () {
            var email           = $('#citizen_id').val(),
                confirm_email   = $('#confirm_citizen_id').val();
            if(email !== confirm_email){
                $('#confirm_citizen_id_text').css({"background": "#c30505","color": "white","display":"inline-block"});
                $('#confirm_citizen_id_text').html(`&nbsp;&nbsp; โปรดกรอกหมายเลขบัตรประชาชนเดียวกับด้านบนเพื่อยืนยัน * &nbsp;&nbsp;`);
            } else {
                $('#confirm_citizen_id_text').removeAttr( 'style' );
                $('#confirm_citizen_id_text').html(``);
            }

        })
    }
</script>
@stop
