@extends('layouts.app')

@section('content')
<div class="container">
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            <div class="sbox medium-box">
                <div class="sbox-title">
                    <h4>{{ $pageTitle }}
                        <small> {{ $pageNote }} </small>
                    </h4>
                </div>
                <div class="sbox-content">

                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#info" data-toggle="tab"> {{ Lang::get('core.personalinfo') }} </a>
                        </li>
                        <li><a href="#pass" data-toggle="tab">{{ Lang::get('core.changepassword') }} </a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active m-t" id="info">
                            {!! Form::open(array('url'=>'user/saveprofile/', 'class'=>'form-horizontal validated' ,'files' => true)) !!}
                            {{--<div class="form-group">--}}
                            {{--<label for="ipt" class=" control-label col-sm-4"> Username </label>--}}
                            {{--<div class="col-sm-8">--}}
                            {{--<input name="username" type="text" id="username" disabled="disabled"--}}
                                       {{--class="form-control input-sm" required value="{{ $info->username }}"/>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <label for="ipt" class=" control-label col-sm-4"> อีเมล </label>
                                <div class="col-sm-8">
                                    <input name="email" type="text" id="email" class="form-control input-sm"
                                           value="{{ $info->email }}" readonly/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name_title" class="col-sm-4 control-label"> คำนำหน้า* </label>
                                <div class="col-sm-3">
                                    <select name="name_title" id="name_title" class="form-control">
                                        <option value="นาย">นาย</option>
                                        <option value="นาง">นาง</option>
                                        <option value="นางสาว">นางสาว</option>
                                    </select>
                                </div>
                                <div class="col-sm-7"></div>
                            </div>

                            <div class="form-group">
                                <label for="ipt"
                                       class=" control-label col-sm-4"> ชื่อ </label>
                                <div class="col-sm-8">
                                    <input name="first_name" type="text" id="first_name" class="form-control input-sm"
                                           required value="{{ $info->first_name }}"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="last_name"
                                       class=" control-label col-sm-4">นามสกุล </label>
                                <div class="col-sm-8">
                                    <input name="last_name" type="text" id="last_name" class="form-control input-sm"
                                           required value="{{ $info->last_name }}"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="dob" class=" control-label col-sm-4 ">วันเกิด</label>
                                <div class="col-sm-8">
                                    <input name="dob" type="text" id="dob" class="form-control input-sm date"
                                           required value="{{ is_null($info->dob) ? date('Y-m-d') : $info->dob }}"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="education" class="col-md-4 control-label"> ระดับการศึกษา* </label>
                                <div class="col-md-8">
                                    <select name="education" id="education" class="form-control" required>
                                        <option value="" selected="true">--- Please Select ---</option>
                                        <option value="ปริญญาเอก">ปริญญาเอก</option>
                                        <option value="ปริญญาโท">ปริญญาโท</option>
                                        <option value="ปริญญาตรี">ปริญญาตรี</option>
                                        <option value="ปวช.">ปวช.</option>
                                        <option value="ปวส.">ปวส.</option>
                                        <option value="มัธยม 1">มัธยม 1</option>
                                        <option value="มัธยม 2">มัธยม 2</option>
                                        <option value="มัธยม 3">มัธยม 3</option>
                                        <option value="มัธยม 4">มัธยม 4</option>
                                        <option value="มัธยม 5">มัธยม 5</option>
                                        <option value="มัธยม 6">มัธยม 6</option>
                                        <option value="ต่ำกว่าหรือเท่ากับประถมศึกษาปีที่ 6">
                                            ต่ำกว่าหรือเท่ากับประถมศึกษาปีที่ 6
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="occupation" class="col-md-4 control-label"> อาชีพ* </label>
                                <div class="col-md-8">
                                    <select id="occupation" name="occupation" class="form-control">
                                        <option value="" selected="true">--- Please Select ---</option>
                                        <option value="C03">ธุรกิจส่วนตัว</option>
                                        <option value="C09">อาชีพอิสระ</option>
                                        <option value="C07">ข้าราชการ/พนักงานรัฐวิสาหกิจ</option>
                                        <option value="C11">นักลงทุนในหลักทรัพย์มืออาชีพ</option>
                                        <option value="C02">ผู้แนะนำการลงทุนในหลักทรัพย์</option>
                                        <option value="C08">พนักงานบริษัทในธุรกิจการเงิน</option>
                                        <option value="C12">บุคลากรทางการแพทย์</option>
                                        <option value="C13">วิศวกร</option>
                                        <option value="C14">บุคลากรทางการศึกษาและวิจัย</option>
                                        <option value="C05">พนักงานบริษัทเอกชนอื่นๆ</option>
                                        <option value="C04">นักเรียน/นักศึกษา</option>
                                        <option value="C01">เกษียณ</option>
                                        <option value="C06">แม่บ้าน/พ่อบ้าน</option>
                                        <option value="C10">อื่นๆ</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="faculty_id" class="col-sm-4 control-label"> คณะที่จบการศึกษา* </label>
                                <div class="col-sm-8">
                                    <select name="faculty_id" id="faculty_id" class="form-control" required></select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="sub_faculty" class="col-sm-4 control-label"> สาขาที่จบการศึกษา </label>
                                <div class="col-sm-8">
                                    <input type="text" name="sub_faculty" id="sub_faculty" class="form-control"
                                           placeholder="สาขาที่จบการศึกษา" value="{{ $info->sub_faculty }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="address" class=" control-label col-sm-4"> ที่อยู่ </label>
                                <div class="col-sm-8">
                                    {!! Form::text('address',  $info->address, array('class'=>'form-control input-sm', 'placeholder'=> 'ที่อยู่อาศัยปัจจุบัน')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="mobile_number" class="control-label col-sm-4"> เบอร์มือถือ * </label>
                                <div class="col-sm-8">
                                    {!! Form::text('mobile_number', $info->mobile_number, array('class'=>'form-control input-sm', 'required'=>'true', 'placeholder'=> 'เบอร์มือถือปัจจุบัน')) !!}
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="province_id" class="control-label col-sm-4"> จังหวัด* </label>
                                <div class="col-sm-8">
                                    <select name="province_id" id="province_id" class="form-control input-sm" required>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="startup_lookup" class="control-label col-sm-4"> Startup ที่สนใจ* </label>
                                <div class="col-sm-8">
                                    <select name="startup_lookup" id="startup_lookup" class="form-control" required>
                                        <option value="1"> การแพทย์และสาธารณสุข (MedTech/ Health Tech) </option>
                                        <option value="2"> เกษตรและอาหาร (AgriTech/ Food Startup) </option>
                                        <option value="3"> อสังหาริมทรัพย์ (Property Tech) </option>
                                        <option value="4"> การเงินและการธนาคาร (FinTech) </option>
                                        <option value="5"> การศึกษา (EdTech) </option>
                                        <option value="6"> การท่องเที่ยว (TravelTech) </option>
                                        <option value="7"> ไลฟ์สไตล์ (LifeStyle)  </option>
                                        <option value="8"> พาณิชยกรรมอิเล็กทรอนิกส์ (E-Commerce) </option>
                                        <option value="9"> ภาครัฐ/การศึกษา (GovTech/EdTech) </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group  ">
                                <label for="ipt" class=" control-label col-sm-4 text-right"> Avatar </label>
                                <div class="col-sm-8">

                                    <input type="file" name="avatar" id="avatar" class="inputfile"/>
                                    <label for="avatar"><i class="fa fa-upload"></i> Choose a file</label>
                                    <div class="avatar_preview"></div>
                                    <p>ขนาดของรูปภาพไม่เกิน 512 x 512 px</p>
                                    <?php if( file_exists('./uploads/users/' . $info->avatar) && $info->avatar != '') { ?>
                                    <img src="{{  url('uploads/users').'/'.$info->avatar }} " border="0" width="60"
                                         class="img-circle"/>
                                    <?php  } else { ?>
                                    <img alt="" src="http://www.gravatar.com/avatar/{{ md5($info->email) }}" width="60"
                                         class="img-circle"/>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="ipt" class=" control-label col-sm-4">&nbsp;</label>
                                <div class="col-sm-8">
                                    <button class="btn btn-success"
                                            type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>
                                </div>
                            </div>

                            {!! Form::close() !!}
                        </div>

                        <div class="tab-pane  m-t" id="pass">
                            {!! Form::open(array('url'=>'user/savepassword/', 'class'=>'form-horizontal ')) !!}

                            <div class="form-group">
                                <label for="ipt"
                                       class=" control-label col-sm-4"> {{ Lang::get('core.newpassword') }} </label>
                                <div class="col-sm-8">
                                    <input name="password" type="password" id="password" class="form-control input-sm"
                                           value=""/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="ipt"
                                       class=" control-label col-sm-4"> {{ Lang::get('core.conewpassword') }}  </label>
                                <div class="col-sm-8">
                                    <input name="password_confirmation" type="password" id="password_confirmation"
                                           class="form-control input-sm" value=""/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class=" control-label col-sm-4">&nbsp;</label>
                                <div class="col-sm-8">
                                    <button class="btn btn-success"
                                            type="submit"> {{ Lang::get('core.sb_savechanges') }} </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        
        $('.date').datepicker({format: 'yyyy-mm-dd', autoclose: true});
        
        $("#faculty_id").jCombo("{!! url('select/faculty') !!}", {selected_value: '{{ $info->faculty_id }}'});
        $("#province_id").jCombo("{!! url('select/province') !!}", {selected_value: '{{ $info->province_id }}'});
        $("#name_title").val('{{ $info->name_title }}')
        $("#startup_lookup").val('{{ $info->startup_lookup }}')
        $("#education").val('{{ $info->education }}')
        $("#occupation").val('{{ $info->occupation }}')
    })
</script>
@endsection