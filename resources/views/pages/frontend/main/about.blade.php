<style>
    .card-header {
        color: #333;
        border-color: #ddd;
    }
</style>
<div style="min-height:1000px;">
    <div class="container">
        <div class="padding"></div>
        <h2 class="text-center">เกี่ยวกับเรา</h2>
        <div class="padding"></div>
        <div>
            {!! $info->info_detail !!}
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
    });
</script>