<div style="min-height: 1000px; background-image:url('{{ asset('frontend/medilab')}}/img/bg-1.jpg'); background-position:center;">
    <div class="row">
        <div class="container">
            <!-- course header and button -->
            <div class="row" style="padding-top: 20px; padding-bottom: 20px">
                <div class="col-sm-4">
                </div>
                <div class="col-sm-4" align="center">
                    <h2>{{$title}}</h2>
                </div>
                <div class="col-sm-4" align="right">
                </div>
            </div>

            @if(count($courses)>=1)
            <div class="row">
                @foreach($courses as $item)
                <div class="col-sm-3">
                    <div class="thumbnail">
                        <div>
                            <a href="{{ url('/c/'.$item->course_id) }}" class="demo-video">
                                <img src="{{ url($item->course_thumbnail) }}" style="width:100%;"/>
                            </a>
                        </div>
                        <div class="caption">
                            <div class="row">
                                <i class="fas fa-eye"></i> {{$item->course_view}} | <i class="fas fa-user"></i> {{$item->enroll}}
                            </div>
                            <div class="row"
                                 style="border-width: 0 0 2px 0; border-style: dotted; border-color: #eaeaea;">
                                <h5><a href="{{ url('/c/'.$item->course_id) }}">{{ $item->course_title }}</a></h5>
                                <p style="color: #209f96">Free</p>
                            </div>
                            <div class="row" style="padding-top: 8px; padding-bottom: 8px;">
                                <div class="col-xs-6" style="padding: 0px;">
                                    <p>
                                        <i class="fas fa-calendar-alt"></i>
                                        {{ date("d/m", strtotime($item->created_at)) }}/{{ date("Y", strtotime($item->created_at)) + 543 }}
                                    </p>
                                    <p>
                                        <i class="fas fa-clock"></i>
                                        {{ explode(' ', $item->created_at)[1] }}
                                    </p>
                                    <p style="height:30px;">
                                        @if($item->percentage_lecture==0 && $item->percentage_quiz==0)
                                        @else
                                        <img src="{{url('frontend/medilab/img/quality-icon.png')}}" style="width:15px;">
                                        &nbsp;Certificate
                                        @endif
                                    </p>
                                </div>
                                <div class="col-xs-6"
                                     style="text-align: center; height: 100%; border-width: 0 0 0 2px; border-style: solid; border-color: #eaeaea;">
                                    <h6>
                                        @if($item->course_document!=null)
                                        <a href="#" class="btn btn-link">
                                            <i class="fas fa-download"></i> Document
                                        </a>
                                        @else
                                        <button class="btn btn-link disabled" title="ยังไม่มีเอกสารประกอบการเรียน">
                                            <i class="fas fa-download"></i> Document
                                        </button>
                                        @endif
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @else
            <div class="row">
                <div class="col-12">
                    <p class="text-center">ไม่มีหลักสูตรเรียน</p>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>