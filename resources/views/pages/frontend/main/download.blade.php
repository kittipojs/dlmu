<style>
    .card-header {
        color: #333;
        border-color: #ddd;
    }

    .box-wrapper{
        margin: 4px 0px;
    }

    .box-inner{
        padding: 15px;
        border:1px solid #ccc;
        background-color: #efefef;
        text-align:center;
        border-radius: 4px;
    }
</style>
<div style="min-height:1000px;">
    <div class="container">
        <div class="padding"></div>
        <h2 class="text-center">ดาวน์โหลดเอกสาร</h2>
        <div class="padding"></div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-10">
                <div class="row">
                    @foreach($docs as $item)
                    <div class="col-md-3 box-wrapper">
                        <div class="box-inner">
                            <h4><i class="far fa-file-pdf"></i></h4>
                            <p>{{$item->doc_title}}</p>
                            <p>
                                <a href="{{$item->doc_url}}" target="_blank">ดาวน์โหลด</a>
                            </p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

    });
</script>