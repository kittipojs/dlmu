<div style="width:100%; overflow:hidden;">
    <div class="carousel" data-flickity='{ "autoPlay": false, "wrapAround": true }'>
        <div class="carousel-cell slider" style="background-image:url('{{ asset('frontend/medilab/img/highlight-image-2.jpg')}}'); background-position:center;">
        </div>
        <div class="carousel-cell slider" style="background-image:url('{{ asset('frontend/medilab/img/highlight-image.jpg')}}'); background-position:center;">
        </div>
    </div>
</div>
