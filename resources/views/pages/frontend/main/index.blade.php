@include('modal.modal')

<!-- banner -->
@include('pages.frontend.main.banner')
<!--/banner -->
{{-- 
<!-- newest course-->
@include('pages.frontend.main.newest')
<!--/newest course-->
<!-- popular course -->
@include('pages.frontend.main.popular')
<!--/popular course -->
<!-- recommended course -->
@include('pages.frontend.main.recommended')
<!--/recommended course -->
--}}

{{--
<div class="padding pure-hidden-md pure-hidden-lg pure-hidden-xl text-center">
    <a href="{{ url('/uleague') }}" class="btn btn-md btn-warning" style="margin-top:8px;">
        สมัคร U League {{ DataHelpers::year() }}
    </a>
</div>
--}}

<div class="row"
     style="background-image:url('{{ asset('frontend/medilab')}}/img/bg-1.jpg'); background-position:center;">
    <div class="container">


        @if(count($courses_101)>=1)
        <div class="row" style="padding-top: 20px; padding-bottom: 20px">
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4" align="center">
                <h2>Startup 101</h2>
            </div>
            <div class="col-sm-4" align="right">
                @if($all_courses_101)
                <a class="btn btn-default-2" href="{{ url('course-group') }}/{{ $courses_101[0]->tag_name }}">
                    See All
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </a>
                @endif
            </div>
        </div>

        <div class="row">
            @foreach($courses_101 as $item)
            <div class="col-sm-3">
                <div class="thumbnail">
                    <div>
                        <a href="{{ url('/c/'.$item->course_id) }}" class="demo-video">
                            <img src="{{ url($item->course_thumbnail) }}" style="width:100%;"/>
                        </a>
                    </div>
                    <div class="caption">
                        <div class="row">
                            <i class="fas fa-eye"></i> {{$item->course_view}} | <i class="fas fa-user"></i> {{$item->enroll}}
                        </div>
                        <div class="row"
                             style="border-width: 0 0 2px 0; border-style: dotted; border-color: #eaeaea;">
                            <h5><a href="{{ url('/c/'.$item->course_id) }}">{{ $item->course_title }}</a></h5>
                            <p style="color: #209f96">Free</p>
                        </div>
                        <div class="row vertical-align" style="padding-top: 8px; padding-bottom: 8px;">
                            <div class="col-xs-6 col-md-6 col-lg-6" style="padding: 0px;">
                                <p>
                                    <i class="fas fa-calendar-alt"></i>
                                    &nbsp;&nbsp;{{ date("d/m", strtotime($item->created_at)) }}/{{ date("Y", strtotime($item->created_at)) + 543 }}
                                </p>
                                <!--
                                <p>
                                    <i class="fas fa-clock"></i>
                                    &nbsp;&nbsp;{{ explode(' ', $item->created_at)[1] }}
                                </p>
                                -->
                                <p>
                                    @if($item->percentage_lecture==0 && $item->percentage_quiz==0)
                                    @else
                                    <img src="{{url('frontend/medilab/img/quality-icon.png')}}" style="width:15px;">
                                    &nbsp;Certificate
                                    @endif
                                </p>
                            </div>
                            <div class="col-xs-6 col-md-6 col-lg-6" style="text-align: center; height: 100%; border-width: 0 0 0 2px; border-style: solid; border-color: #eaeaea;">
                                <p>
                                    @if($item->course_document!=null)
                                    <a href="#" class="btn btn-link">
                                        <i class="fas fa-download"></i> Document
                                    </a>
                                    @else
                                    <button class="btn btn-link disabled" title="ยังไม่มีเอกสารประกอบการเรียน">
                                        <i class="fas fa-download"></i> Document
                                    </button>
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endif

        @if(count($courses_warrior)>=1)
        <div class="row" style="padding-top: 20px; padding-bottom: 20px">
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4" align="center">
                <h2>Startup Warrior</h2>
            </div>
            <div class="col-sm-4" align="right">
                @if($all_courses_warrior)
                <a class="btn btn-default-2" href="{{ url('course-group') }}/{{ $courses_warrior[0]->tag_name }}">
                    See All
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </a>
                @endif
            </div>
        </div>
        <div class="row">
            @foreach($courses_warrior as $item)
            <div class="col-sm-3">
                <div class="thumbnail">
                    <div>
                        <a href="{{ url('/c/'.$item->course_id) }}" class="demo-video">
                            <img src="{{ url($item->course_thumbnail) }}" style="width:100%;"/>
                        </a>
                    </div>
                    <div class="caption">
                        <div class="row">
                            <i class="fas fa-eye"></i> {{$item->course_view}} | <i class="fas fa-user"></i> {{$item->enroll}}
                        </div>
                        <div class="row" style="border-width: 0 0 2px 0; border-style: dotted; border-color: #eaeaea;">
                            <h5><a href="{{ url('/c/'.$item->course_id) }}">{{ $item->course_title }}</a></h5>
                            <p style="color: #209f96">Free</p>
                        </div>
                        <div class="row" style="padding-top: 8px; padding-bottom: 8px;">
                            <div class="col-xs-6" style="padding: 0px;">
                                <p>
                                    <i class="fas fa-calendar-alt"></i>
                                    {{ date("d/m", strtotime($item->created_at)) }}/{{ date("Y", strtotime($item->created_at)) + 543 }}
                                </p>
                                <!--
                                <p>
                                    <i class="fas fa-clock"></i>
                                    {{ explode(' ', $item->created_at)[1] }}
                                </p>
                                -->
                                <p style="height:30px;">
                                    @if($item->percentage_lecture==0 && $item->percentage_quiz==0)
                                    @else
                                    <img src="{{url('frontend/medilab/img/quality-icon.png')}}" style="width:15px;">
                                    &nbsp;Certificate
                                    @endif
                                </p>
                            </div>
                            <div class="col-xs-6" style="text-align: center; height: 100%; border-width: 0 0 0 2px; border-style: solid; border-color: #eaeaea;">
                                <h6>
                                    @if($item->course_document!=null)
                                    <a href="#" class="btn btn-link">
                                        <i class="fas fa-download"></i> Document
                                    </a>
                                    @else
                                    <button class="btn btn-link disabled" title="ยังไม่มีเอกสารประกอบการเรียน">
                                        <i class="fas fa-download"></i> Document
                                    </button>
                                    @endif
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endif

    </div>
</div>
{{--
<div class="row" style="background-image:url('{{ asset('frontend/medilab')}}/img/bg-1.jpg'); background-position:center;">
    <div class="container">
        <!-- course header and button -->
        <div class="row" style="padding-top: 20px; padding-bottom: 20px">
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4" align="center">
                <h2>Startup Coaching</h2>
            </div>
            <div class="col-sm-4" align="right">
                <!--
<button class="btn btn-default-2">
See All
<i class="fa fa-angle-right" aria-hidden="true"></i>
</button>
-->
            </div>
        </div>
        <div class="row">
            @foreach($videos_coaching as $item)
            <div class="col-sm-3">
                <div class="thumbnail">
                    <div>
                        <a href="{{ url('/course/'.$item->course_id.'_'.$item->lecture_id.'/lecture') }}" class="demo-video">
                            <img src="{{ $item->lecture_cover }}" style="width:100%;" />
                        </a>
                    </div>
                    <div class="caption" style="height:90px;">
                        <a href="{{ url('/course/'.$item->course_id.'_'.$item->lecture_id.'/lecture') }}">{{ $item->lecture_title }}</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

@if(count($video_online_courses)>0)
<div class="row" style="background-image:url('{{ asset('frontend/medilab')}}/img/bg-1.jpg'); background-position:center;">
    <div class="container">
        <!-- course header and button -->
        <div class="row" style="padding-top: 20px; padding-bottom: 20px">
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4" align="center">
                <h2>Online Courses</h2>
            </div>
            <div class="col-sm-4" align="right">
                <!--
<button class="btn btn-default-2">
See All
<i class="fa fa-angle-right" aria-hidden="true"></i>
</button>
-->
            </div>
        </div>
        <div class="row">
            @foreach($video_online_courses as $item)
            <div class="col-sm-3">
                <div class="thumbnail">
                    <div>
                        <a href="{{ url('/course/'.$item->course_id.'_'.$item->lecture_id.'/lecture') }}" class="demo-video">
                            <img src="{{ $item->lecture_cover }}" style="width:100%;" />
                        </a>
                    </div>
                    <div class="caption" style="height:90px;">
                        <a href="{{ url('/course/'.$item->course_id.'_'.$item->lecture_id.'/lecture') }}">{{ $item->lecture_title }}</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endif
--}}
<div class="events-wrapper">
    <div class="container">
        <h2 class="text-center padding">Highlight Events</h2>
        <div class="padding"></div>
        <div id="htmlHighlight">
            <p class="text-center padding-lg white">
                <img src="{{url('frontend/medilab/img/ring-spinner.gif')}}" style="width:40px;">
                <br>
                กำลังโหลด
            </p>
        </div>
        {{--
        @if(count($events)>0)
        <div id="event-carousel" class="row">
            @foreach($events as $item)
            <div class="col-sm-4">
                <div class="event-item">
                    <a href="{{ $item['url'] }}" target="_blank">
                        <img src="{{ $item['cover'] }}" class="img-responsive"/>
                    </a>
                    <div class="event-content padding">
                        <a href="{{ $item['url'] }}" target="_blank"><h4>{{ $item['name'] }}</h4></a>
                        <p>
                            <i class="far fa-calendar-alt"></i> วันที่ : {{ $item['start_date'] }}
                            - {{ $item['end_date'] }}
                        </p>
                        <p>
                            <i class="fas fa-map-marker-alt"></i> สถานที่ : {{ $item['venue']->name }}
                        </p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @else
        <p class="text-center padding-lg white">ยังไม่มีกิจกรรม</p>
        @endif
        --}}
    </div>

</div>
<script type="text/javascript">
    $(document).ready(function(){

        {{--
        $('#event-carousel').slick({
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 3
        });
          --}}

         $.ajax({
             url: "{{url('/api/zipevent/highlight')}}",
             type: 'GET',
             success: function (response) {
                 $('#htmlHighlight').html(response)
             }, error: function (jqXHR, textStatus, errorThrown) { },
             cache: false,
             contentType: false,
             processData: false
         });
        })
</script>