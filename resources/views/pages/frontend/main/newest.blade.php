<!-- newest course -->
<div class="row"
     style="background-image:url('{{ asset('frontend/medilab')}}/img/bg-1.jpg'); background-position:center;">
    <div class="container">
        <!-- course header and button -->
        <div class="row" style="padding-top: 20px; padding-bottom: 20px">
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4" align="center">
                <h2>Newest Course</h2>
            </div>
            <div class="col-sm-4" align="right">
                <button class="btn btn-default">
                    See All
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </button>
            </div>
        </div>
        <!--/course header and button -->

        <!-- course video and detail -->
        <div class="row">
            <!-- thumbnail 1 -->
            <div class="col-sm-3">
                <div class="thumbnail">
                    <!-- video embed -->
                    <div class="embed-responsive embed-responsive-16by9">
                        {{--<img src="{{ asset('frontend/medilab')}}/img/wireframe-video.png" style="width:100%;" />--}}
                        <img src="{{ asset('uploads/images/ss/1-1.jpg')}}" style="width:100%;" />
                    </div>
                    <!--/video embed -->

                    <!-- video detail -->
                    <div class="caption">
                        {{--<strong><a target = '_blank' href="{{ url('course/view') }}/1518413832814">1. Startup 101</a></strong>--}}
                        <strong><a target = '_blank' href="https://www.youtube.com/watch?v=fQ4rf2ewdVI&list=PL5XvqtJ9CNNVK4Bs3jA5oGumm5q0T_Gn6&index=5">1. Startup 101</a></strong>
                        <small>Free</small>
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td class="align-baseline" width="50%">
                                    <i class="fa fa-calendar" aria-hidden="true"> {{ date('d/m/Y') }}</i>
                                    <br>
                                    <i class="fa fa-clock-o" aria-hidden="true"> {{ date('H:i:s') }}</i>
                                </td>
                                <td class="align-middle" width="50%">
                                    <i class="fa fa-file-text-o" aria-hidden="true"> Document</i>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--/video detail -->
                </div>
            </div>
            <!--/thumbnail 1 -->
            <!-- thumbnail 2 -->
            <div class="col-sm-3">
                <div class="thumbnail">
                    <!-- video embed -->
                    <div class="embed-responsive embed-responsive-16by9">
                        {{--<img src="{{ asset('frontend/medilab')}}/img/wireframe-video.png" style="width:100%;" />--}}
                        <img src="{{ asset('uploads/images/ss/1-2.jpg')}}" style="width:100%;" />
                    </div>
                    <!--/video embed -->

                    <!-- video detail -->
                    <div class="caption">
                        {{--<strong><a target = '_blank' href="{{ url('course/view') }}/1518413832814">1. Startup 101</a></strong>--}}
                        <strong><a target = '_blank' href="https://www.youtube.com/watch?v=FhrERDPtDzs&index=4&list=PL5XvqtJ9CNNVK4Bs3jA5oGumm5q0T_Gn6">1. Startup 101</a></strong>
                        <small>Free</small>
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td class="align-baseline" width="50%">
                                    <i class="fa fa-calendar" aria-hidden="true"> {{ date('d/m/Y') }}</i>
                                    <br>
                                    <i class="fa fa-clock-o" aria-hidden="true"> {{ date('H:i:s') }}</i>
                                </td>
                                <td class="align-middle" width="50%">
                                    <i class="fa fa-file-text-o" aria-hidden="true"> Document</i>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--/video detail -->
                </div>
            </div>
            <!--/thumbnail 2 -->
            <!-- thumbnail 3 -->
            <div class="col-sm-3">
                <div class="thumbnail">
                    <!-- video embed -->
                    <div class="embed-responsive embed-responsive-16by9">
                        {{--<img src="{{ asset('frontend/medilab')}}/img/wireframe-video.png" style="width:100%;" />--}}
                        <img src="{{ asset('uploads/images/ss/1-3.jpg')}}" style="width:100%;" />
                    </div>
                    <!--/video embed -->

                    <!-- video detail -->
                    <div class="caption">
                        <strong><a target = '_blank' href="https://www.youtube.com/watch?v=xzoYpP964nM&index=3&list=PL5XvqtJ9CNNVK4Bs3jA5oGumm5q0T_Gn6">1. Startup 101</a></strong>
                        <small>Free</small>
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td class="align-baseline" width="50%">
                                    <i class="fa fa-calendar" aria-hidden="true"> {{ date('d/m/Y') }}</i>
                                    <br>
                                    <i class="fa fa-clock-o" aria-hidden="true"> {{ date('H:i:s') }}</i>
                                </td>
                                <td class="align-middle" width="50%">
                                    <i class="fa fa-file-text-o" aria-hidden="true"> Document</i>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--/video detail -->
                </div>
            </div>
            <!--/thumbnail 3 -->
            <!-- thumbnail 4 -->
            <div class="col-sm-3">
                <div class="thumbnail">
                    <!-- video embed -->
                    <div class="embed-responsive embed-responsive-16by9">
                        {{--<img src="{{ asset('frontend/medilab')}}/img/wireframe-video.png" style="width:100%;" />--}}
                        <img src="{{ asset('uploads/images/ss/1-4.jpg')}}" style="width:100%;" />
                    </div>
                    <!--/video embed -->

                    <!-- video detail -->
                    <div class="caption">
                        <strong><a target = '_blank' href="https://www.youtube.com/watch?v=4A2tOngK-2w&index=2&list=PL5XvqtJ9CNNVK4Bs3jA5oGumm5q0T_Gn6">1. Startup 101</a></strong>
                        <small>Free</small>
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td class="align-baseline" width="50%">
                                    <i class="fa fa-calendar" aria-hidden="true"> {{ date('d/m/Y') }}</i>
                                    <br>
                                    <i class="fa fa-clock-o" aria-hidden="true"> {{ date('H:i:s') }}</i>
                                </td>
                                <td class="align-middle" width="50%">
                                    <i class="fa fa-file-text-o" aria-hidden="true"> Document</i>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--/video detail -->
                </div>
            </div>
            <!-- thumbnail 4 -->
        </div>
        <!--/course video and detail -->
    </div>
</div>
<!--/newest course -->