<style>
    .card-header {
        color: #333;
        border-color: #ddd;
    }
</style>
<div style="min-height:1000px;">
    <div class="container">
        <div class="padding"></div>
        <h2 class="text-center">คำถามที่พบบ่อยในการใช้งานระบบ</h2>
        <div class="padding"></div>
        <div class="row">
            <div class="col-sm-12">
                <div id="accordion">
                    @foreach($qa as $item)
                    <div class="card">
                        <div class="card-header" id="heading{{$item->id}}">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$item->id}}" aria-expanded="true" aria-controls="collapse{{$item->id}}">
                                    <i class="fa fa-question"></i> {{ $item->question }}
                                </button>
                            </h5>
                        </div>
                        <div id="collapse{{$item->id}}" class="collapse" aria-labelledby="heading{{$item->id}}">
                            <div class="card-body">
                                {!! $item->answer !!}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
    });
</script>