<!-- popular course -->
<div class="row"
     style="background-image:url('{{ asset('frontend/medilab')}}/img/bg-1.jpg'); background-position:center;">
    <div class="container">
        <!-- course header and button -->
        <div class="row" style="padding-top: 20px; padding-bottom: 20px">
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4" align="center">
                <h2>Popular Course</h2>
            </div>
            <div class="col-sm-4" align="right">
                <button class="btn btn-default">
                    See All
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </button>
            </div>
        </div>
        <!--/course header and button -->

        <!-- course video and detail -->
        <div class="row">
            <!-- thumbnail 1 -->
            <div class="col-sm-3">
                <div class="thumbnail">
                    <!-- video embed -->
                    <div class="embed-responsive embed-responsive-16by9">
                        <img src="{{ asset('uploads/images/ss/2-1.jpg')}}" style="width:100%;" />
                    </div>
                    <!--/video embed -->

                    <!-- video detail -->
                    <div class="caption">
                        <strong><a target = '_blank' href="https://www.youtube.com/watch?v=42kDfpgfDlM&index=10&list=PL5XvqtJ9CNNVK4Bs3jA5oGumm5q0T_Gn6">1.
                                Startup 101</a></strong>
                        <small>Free</small>
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td class="align-baseline" width="50%">
                                    <i class="fa fa-calendar" aria-hidden="true"> {{ date('d/m/Y') }}</i>
                                    <br>
                                    <i class="fa fa-clock-o" aria-hidden="true"> {{ date('H:i:s') }}</i>
                                </td>
                                <td class="align-middle" width="50%">
                                    <i class="fa fa-file-text-o" aria-hidden="true"> Document</i>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--/video detail -->
                </div>
            </div>
            <!--/thumbnail 1 -->
            <!-- thumbnail 2 -->
            <div class="col-sm-3">
                <div class="thumbnail">
                    <!-- video embed -->
                    <div class="embed-responsive embed-responsive-16by9">
                        <img src="{{ asset('uploads/images/ss/2-2.jpg')}}" style="width:100%;" />
                    </div>
                    <!--/video embed -->

                    <!-- video detail -->
                    <div class="caption">
                        <strong><a target = '_blank' href="https://www.youtube.com/watch?v=1SvIc-rZT2k&index=9&list=PL5XvqtJ9CNNVK4Bs3jA5oGumm5q0T_Gn6">1.
                                Startup 101</a></strong>
                        <small>Free</small>
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td class="align-baseline" width="50%">
                                    <i class="fa fa-calendar" aria-hidden="true"> {{ date('d/m/Y') }}</i>
                                    <br>
                                    <i class="fa fa-clock-o" aria-hidden="true"> {{ date('H:i:s') }}</i>
                                </td>
                                <td class="align-middle" width="50%">
                                    <i class="fa fa-file-text-o" aria-hidden="true"> Document</i>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--/video detail -->
                </div>
            </div>
            <!--/thumbnail 2 -->
            <!-- thumbnail 3 -->
            <div class="col-sm-3">
                <div class="thumbnail">
                    <!-- video embed -->
                    <div class="embed-responsive embed-responsive-16by9">
                        <img src="{{ asset('uploads/images/ss/2-3.jpg')}}" style="width:100%;" />
                    </div>
                    <!--/video embed -->

                    <!-- video detail -->
                    <div class="caption">
                        <strong><a target = '_blank' href="https://www.youtube.com/watch?v=QlFx9V03hFI&index=8&list=PL5XvqtJ9CNNVK4Bs3jA5oGumm5q0T_Gn6">1.
                                Startup 101</a></strong>
                        <small>Free</small>
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td class="align-baseline" width="50%">
                                    <i class="fa fa-calendar" aria-hidden="true"> {{ date('d/m/Y') }}</i>
                                    <br>
                                    <i class="fa fa-clock-o" aria-hidden="true"> {{ date('H:i:s') }}</i>
                                </td>
                                <td class="align-middle" width="50%">
                                    <i class="fa fa-file-text-o" aria-hidden="true"> Document</i>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--/video detail -->
                </div>
            </div>
            <!--/thumbnail 3 -->
            <!-- thumbnail 4 -->
            <div class="col-sm-3">
                <div class="thumbnail">
                    <!-- video embed -->
                    <div class="embed-responsive embed-responsive-16by9">
                        <img src="{{ asset('uploads/images/ss/2-4.jpg')}}" style="width:100%;" />
                    </div>
                    <!--/video embed -->

                    <!-- video detail -->
                    <div class="caption">
                        <strong><a target = '_blank' href="https://www.youtube.com/watch?v=sf0tJL0pEsU&index=7&list=PL5XvqtJ9CNNVK4Bs3jA5oGumm5q0T_Gn6">1.
                                Startup 101</a></strong>
                        <small>Free</small>
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td class="align-baseline" width="50%">
                                    <i class="fa fa-calendar" aria-hidden="true"> {{ date('d/m/Y') }}</i>
                                    <br>
                                    <i class="fa fa-clock-o" aria-hidden="true"> {{ date('H:i:s') }}</i>
                                </td>
                                <td class="align-middle" width="50%">
                                    <i class="fa fa-file-text-o" aria-hidden="true"> Document</i>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--/video detail -->
                </div>
            </div>
            <!--/thumbnail 4 -->
        </div>
        <!--/course video and detail -->
    </div>
</div>
<!--/popular course -->