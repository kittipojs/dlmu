<section>
    <h3 class="text-center">U League Pitching {{ DataHelpers::year() }}</h3>
    <div class="padding-lg"></div>

    <p class="text-center">
        <i class="fa fa-check-circle"></i> คุณได้เข้าร่วมทีม {{ $team->team_name }} แล้ว
    </p>
</section>

<script>
    $(document).ready(function () {

        $("#university").jCombo("{!! url('select/university') !!}", {selected_value: '{{ $team_member->university_id }}'});

        $('#startup_sector').val('{{ $team_member->startup_sector }}');

        $('#inviteForm').submit(function(event) {

            var formData = { 'team_id' : '{{ $team_member->team_id }}', 'invite_to' : $('#invite_to').val() };

            $.ajax({
                type        : 'POST',
                url         : '{{ url('/user/team/member/invite') }}',
                data        : formData,
                dataType    : 'json',
                encode      : true
            }).done(function(data) {
                if(data.success){
                    location.reload(true)
                }
            });

            event.preventDefault();
        });

    });
</script>