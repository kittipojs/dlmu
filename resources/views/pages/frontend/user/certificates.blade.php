<style>
    .modal {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        overflow: hidden;
    }

    .modal-dialog {
        position: fixed;
        margin: 0;
        width: 100%;
        height: 100%;
        padding: 0;
    }

    .modal-content {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        border: 2px solid #1ab394;
        border-radius: 0;
        box-shadow: none;
    }

    .modal-header {
        position: absolute;
        top: 0;
        right: 0;
        left: 0;
        height: 50px;
        padding: 10px;
        background: #1ab394;
        border: 0;
    }

    .modal-title {
        font-weight: 300;
        font-size: 2em;
        color: #fff;
        line-height: 30px;
    }

    .modal-body {
        position: absolute;
        top: 50px;
        bottom: 60px;
        width: 100%;
        font-weight: 300;
        overflow: auto;
    }

    .modal-footer {
        position: absolute;
        right: 0;
        bottom: 0;
        left: 0;
        height: 60px;
        padding: 10px;
        background: #f1f3f5;
    }

    .btn-modal {
        position: absolute;
        top: 50%;
        left: 50%;
        margin-top: -20px;
        margin-left: -100px;
        width: 200px;
    }

    ::-webkit-scrollbar {
        -webkit-appearance: none;
        width: 10px;
        background: #f1f3f5;
        border-left: 1px solid darken(#f1f3f5, 10%);
    }

    ::-webkit-scrollbar-thumb {
        background: darken(#f1f3f5, 20%);
    }

    /*	--------------------------------------------------
    :: Table Filter
    -------------------------------------------------- */
    .panel {
        border: 1px solid #ddd;
        background-color: #fcfcfc;
    }

    .panel .btn-group {
        margin: 15px 0 30px;
    }

    .panel .btn-group .btn {
        transition: background-color .3s ease;
    }

    .table-filter {
        background-color: #fff;
        border-bottom: 1px solid #eee;
    }

    .table-filter tbody tr:hover {
        cursor: pointer;
        background-color: #eee;
    }

    .table-filter tbody tr td {
        padding: 10px;
        vertical-align: middle;
        border-top-color: #eee;
    }

    .table-filter tbody tr.selected td {
        background-color: #eee;
    }

    .table-filter tr td:first-child {
        width: 38px;
    }

    .table-filter tr td:nth-child(2) {
        width: 35px;
    }

    .table-filter .media-photo {
        width: 50px;
        height: 50px;
    }
</style>
<div class="content">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="row">
                    <div class="col-md-6">
                        <h3>ประกาศนียบัตร</h3>
                        <hr/>
                        <div class="padding"></div>
                    </div>
                    <div class="col-md-6">
                    <!--
<div class="btn-group pull-right">
<button type="button" class="btn btn-default btn-filter" data-target="all">ทั้งหมด</button>
{{-- <button type="button" class="btn btn-default btn-filter" data-target="course">คอร์สเรียน</button> --}}
                            <button type="button" class="btn btn-default btn-filter" data-target="camp">U League Camp</button>
                            </div>
-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 text-center">
                        @if(count($course_certs)>0 && count($event_certs)>0)
                            <p>
                                ยินดีด้วย คุณได้รับประกาศณียบัตรจากการเข้าเรียน {{ count($course_certs) }} หลักสูตร
                                และเข้าร่วม {{count($event_certs)}} กิจกรรม
                            </p>
                            <button type="button" class="btn btn-sm btn-primary print-all">
                                พิมพ์ประกาศนียบัตร
                            </button>
                        @else
                            <p>
                                คุณยังไม่ผ่านการเข้าเรียนหลักสูตรหรือเข้าร่วมกิจกรรมใดๆ
                            </p>
                        @endif
                    </div>
                </div>

                {{--
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="courses-tab" data-toggle="tab" href="#coursesTab" role="tab" aria-controls="coursesTab" aria-selected="true">
                            หลักสูตรเรียน
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="uleague-tab" data-toggle="tab" href="#uleague" role="tab" aria-controls="uleague" aria-selected="false">
                            U League Camp
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="coursesTab" role="tabpanel" aria-labelledby="courses-tab">
                        <table class="table table-filter">
                            <tbody>
                                @if(count($course_certs)>0)
                                @foreach( $course_certs as $cert)
                                <tr data-status="{{ $cert->issue_for }}">
                                    <td>
                                        <div class="media">
                                            <a href="#" class="pull-left">
                                                <img src="{{ asset($cert->course_thumbnail) }}" class="media-photo">
                                            </a>
                                            <div class="media-body">
                                                <span class="media-meta pull-right"> ออกเมื่อ {{ $cert->created_cert }}</span>
                                                <h4 class="title">
                                                    {{ $cert->course_title}}
                                                </h4>
                                                <span class="media-meta pull-right">
                                                    <button type="button" class="btn btn-primary btn-sm preview" data-cert_id="{{ $cert->certification_id }}" data-toggle="modal" data-target="#preview">
                                                        <i class="fas fa-eye"></i> ดูประกาศณียบัตร
                                                    </button>
                                                    <button type="button" class="btn btn-primary btn-sm print" data-cert_id="{{ $cert->certification_id }}">
                                                        <i class="fas fa-print"></i> พิมพ์
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="uleague" role="tabpanel" aria-labelledby="uleague-tab">
                        <table class="table table-filter">
                            <tbody>
                                @if(count($event_certs)>0)
                                @foreach( $event_certs as $event)
                                <tr data-status="camp">
                                    <td>
                                        <div class="media">
                                            <a href="#" class="pull-left">
                                                <img src="{{ $event['cover'] }}" class="media-photo">
                                            </a>
                                            <div class="media-body">
                                                <span class="media-meta pull-right"> ออกเมื่อ {{ $event['end_date']}}</span>
                                                <h4 class="title">
                                                    {{ $event['name'] }}
                                                </h4>
                                                <span class="media-meta pull-right">
                                                    <button type="button" class="btn btn-primary btn-sm preview" data-cert_id="camp" data-toggle="modal" data-target="#preview">
                                                        <i class="fas fa-eye"></i> ดูประกาศณียบัตร
                                                    </button>
                                                    <button type="button" class="btn btn-primary btn-sm print" data-cert_id="camp">
                                                        <i class="fas fa-print"></i> พิมพ์
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="table-container">
                    <table class="table table-filter">
                        <tbody>
                            @if(count($course_certs)>0)
                            @foreach( $course_certs as $cert)
                            <tr data-status="{{ $cert->issue_for }}">
                                <td>
                                    <div class="media">
                                        <a href="#" class="pull-left">
                                            <img src="{{ asset($cert->course_thumbnail) }}" class="media-photo">
                                        </a>
                                        <div class="media-body">
                                            <span class="media-meta pull-right"> ออกเมื่อ {{ $cert->created_cert }}</span>
                                            <h4 class="title">
                                                {{ $cert->course_title}}
                                                <span class="pull-right pagado">({{ $cert->issue_for }})</span>
                                            </h4>
                                            <span class="media-meta pull-right">
                                                <button type="button" class="btn btn-primary btn-sm preview" data-cert_id="{{ $cert->certification_id }}" data-toggle="modal" data-target="#preview">
                                                    <i class="fas fa-eye"></i> ดูประกาศณียบัตร
                                                </button>
                                                <button type="button" class="btn btn-primary btn-sm print" data-cert_id="{{ $cert->certification_id }}">
                                                    <i class="fas fa-print"></i> พิมพ์
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @endif

                            @if(count($event_certs)>0)
                            @foreach( $event_certs as $event)
                            <tr data-status="camp">
                                <td>
                                    <div class="media">
                                        <a href="#" class="pull-left">
                                            <img src="{{ $event['cover'] }}" class="media-photo">
                                        </a>
                                        <div class="media-body">
                                            <span class="media-meta pull-right"> ออกเมื่อ {{ $event['end_date']}}</span>
                                            <h4 class="title">
                                                {{ $event['name'] }}
                                                <span class="pull-right pagado">({{ $cert->issue_for }})</span>
                                            </h4>
                                            <span class="media-meta pull-right">
                                                <button type="button" class="btn btn-primary btn-sm preview" data-cert_id="camp" data-toggle="modal" data-target="#preview">
                                                    <i class="fas fa-eye"></i> ดูประกาศณียบัตร
                                                </button>
                                                <button type="button" class="btn btn-primary btn-sm print" data-cert_id="camp">
                                                    <i class="fas fa-print"></i> พิมพ์
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                --}}
            </div>
        </div>
    </div>
</div>


<!-- Modal -->

<div id="preview" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">ดูตัวอย่างประกาศณียบัตร</h4>
            </div>
            <div class="modal-body">
                <div id="preview_area">
                    <p>Loading...</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-modal btn-default" data-dismiss="modal">ปิด</button>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {

        $('.btn-filter').on('click', function () {
            var $target = $(this).data('target');
            if ($target != 'all') {
                $('.table tr').css('display', 'none');
                $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
            } else {
                $('.table tr').css('display', 'none').fadeIn('slow');
            }
        });

    });

    $('.preview').click(function (e) {

        var data = {"cert_id": $(this).data('cert_id')},
            url = "{{url('/user/certificates/preview')}}";

        $('#preview_area').html('<p>Loading...</p>');

        $.ajax({
            url: url,
            data: data,
            type: 'POST',
            success: function (data) {
                $('#preview_area').html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#preview').modal('hide');
                $.toast({
                    heading: 'error',
                    text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 2500,
                    stack: 6
                });
            }
        });
    });

    $('.print').click(function () {
        var data = {"cert_id": $(this).data('cert_id')},
            url = "{{url('/user/certificates/preview')}}";
        $.ajax({
            url: url,
            data: data,
            type: 'POST',
            success: function (data) {
                // var newWin = window.open("");
                // newWin.document.write(data);
                $('#preview_area').html(data);
                $('#preview_area').printThis();
                // newWin.print();
                // newWin.close();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'error',
                    text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 2500,
                    stack: 6
                });
            }
        });
    });

    $('.print-all').click(function () {
        var url = "{{url('/user/certificates/preview-all')}}";
        $.ajax({
            url: url,
            type: 'POST',
            success: function (data) {
                // var newWin = window.open("");
                // newWin.document.write(data);
                $('#preview_area').html(data);
                $('#preview_area').printThis();
                // newWin.print();
                // newWin.close();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'error',
                    text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 2500,
                    stack: 6
                });
            }
        });
    });

    $('.preview-event-cert').click(function (e) {
        var data = {"event_id": $(this).data('event_id')},
            url = "{{url('/user/event-certificates/preview')}}";
        $('#preview_area').html('<p>Loading...</p>');

        $.ajax({
            url: url,
            data: data,
            type: 'POST',
            success: function (data) {
                $('#preview_area').html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#preview').modal('hide');
                $.toast({
                    heading: 'error',
                    text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 2500,
                    stack: 6
                });
            }
        });
    });

    $('.print-event-cert').click(function () {
        var data = {"event_id": $(this).data('event_id')},
            url = "{{url('/user/event-certificates/preview')}}";

        $.ajax({
            url: url,
            data: data,
            type: 'POST',
            success: function (data) {
                // var newWin = window.open("");
                // newWin.document.write(data);
                $('#preview_area').html(data);
                $('#preview_area').printThis();
                // newWin.print();
                // newWin.close();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'error',
                    text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 2500,
                    stack: 6
                });
            }
        });
    });
</script>