<section>
    <h3 class="text-center">U-league {{ DataHelpers::year() }} (Pitching)</h3>
    <div class="padding-lg"></div>

    <div class="text-center">
        <ul class="steps">
            <li class="active"><a href="{{ url('/user/uleague/team') }}">1. ข้อมูลทีม</a></li>
            <li class="undone"><p>2. ชวนเพื่อน</p></li>
            <li class="undone"><p>3. เลือกรอบ</p></li>
            <li class="undone"><p>4. รอผลการสมัคร</p></li>
            <li class="undone"><p>5. ยืนยันการสมัคร</p></li>
        </ul>
    </div>
    <p class="text-center">
        <i class="fa fa-info-circle"></i> กรอกข้อมูลเกี่ยวกับทีมของคุณให้ครบถ้วนหลังจากที่คุณสร้างทีมแล้วจะไม่สามารถแก้ไขข้อมูลใดๆได้อีก
    </p>
    <div class="padding-lg"></div>
    <form id="createTeamForm" class="form-horizontal" action="{{ url('user/team/create') }}" method="post" role="form" enctype="multipart/form-data">

        <input type="hidden" value="{{ \Auth::user()->id }}" name="user_id">

        <div class="form-group">
            <label for="team_name" class="control-label col-sm-3"> ชื่อทีม *</label>
            <div class="col-sm-8">
                <input name="team_name" type="text" id="team_name" class="form-control input-sm" placeholder="ชื่อทีม" />
            </div>
        </div>

        <div class="form-group">
            <label for="team_info" class="col-sm-3 control-label">ข้อมูลเกี่ยวกับโครงการ *</label>
            <div class="col-sm-8">
                <textarea type="text" class="form-control" name="team_info" id="team_info" placeholder="อธิบายข้อมูลโครงการเริ่มต้น และจุดประสงค์ของโครงการ" rows="6" required ></textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="university" class="col-sm-3 control-label">ส่งในนามมหาวิทยาลัย *</label>
            <div class="col-sm-8">
                <select class="form-control" id="university" name="university" required></select>
            </div>
        </div>

        <div class="form-group">
            <label for="sector" class="col-sm-3 control-label">Startup Sector *</label>
            <div class="col-sm-8">
                <select name="startup_sector" id="startup_sector" class="form-control" required>
                    <option> --- Select Startup Sector ---</option>
                    <option value="1"> การแพทย์และสาธารณสุข (MedTech/ Health Tech) </option>
                    <option value="2"> เกษตรและอาหาร (AgriTech/ Food Startup) </option>
                    <option value="3"> อสังหาริมทรัพย์ (Property Tech) </option>
                    <option value="4"> การเงินและการธนาคาร (FinTech) </option>
                    <option value="5"> การศึกษา (EdTech) </option>
                    <option value="6"> การท่องเที่ยว (TravelTech) </option>
                    <option value="7"> ไลฟ์สไตล์ (LifeStyle)  </option>
                    <option value="8"> พาณิชยกรรมอิเล็กทรอนิกส์ (E-Commerce) </option>
                    <option value="9"> ภาครัฐ (GovTech) </option>
                    <option value="14"> อุตสาหกรรม (Industrial Tech) </option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="image" class="col-sm-3 control-label">ไฟล์ Proposal *<br>
                <small>(ไฟล์ .pdf เท่านั้น)</small>
            </label>
            <div class="col-sm-8">
                <input type="file" name="file" id="file" class="form-control" data-preview-file-type="text" data-upload-url="#" required/>
            </div>
        </div>

        <div class="padding-md"></div>
        <div class="form-group">
            <label class="control-label col-sm-3">&nbsp;</label>
            <div class="col-sm-8">
                <button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#confirm-submit">สร้างทีม</button>
            </div>
        </div>

        <!-- confirm modal -->

        <div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        ยืนยันการสร้างทีม
                    </div>
                    <div class="modal-body text-center">
                        หลังจากสร้างทีมแล้ว คุณจะไม่สามารถเข้าร่วมทีมอื่นๆได้อีก คุณต้องการดำเนินการต่อหรือไม่?
                    </div>

                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-default" data-dismiss="modal">ยังไม่สร้างตอนนี้</button>
                        <button type="submit" class="btn btn-secondary success">ใช่, ฉันต้องการสร้างทีม</button>
                    </div>
                </div>
            </div>
        </div>

    </form>
</section>
<script>
    $(document).ready(function () {
        $("#university").jCombo("{!! url('select/university') !!}");
        $("#faculty").jCombo("{!! url('select/faculty') !!}");
    });
</script>