<section>
    <h3 class="text-center">U-league Pitching {{ DataHelpers::year() }}</h3>
    <div class="padding-lg"></div>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div>
                กิจกรรม U-league Pitching {{ DataHelpers::year() }} เปิดโอกาสให้นักศึกษาส่งทีมเข้าประกวดแข่งขันเพื่อเฟ้นหาสุดยอด Startup ซึ่งก่อนที่จะสร้างทีมได้ คุณต้องมีคุณสมบัติดังนี้
                <br><br>
                <ul>
                    <li>1. สมัคร U League {{ DataHelpers::year() }} เรียบร้อยแล้ว</li>
                    <li>2. ผ่านการเข้าร่วม U League Camp {{ DataHelpers::year() }} อย่างน้อย 1 ครั้ง</li>
                </ul>

            </div>
        </div>
        <div class="col-sm-2"></div>
    </div>
    <div class="padding-lg"></div>
    <div class="text-center">
        @if(!$uleague)
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uleagueAlertModal">เริ่มสร้างทีม</button>
        <div id="uleagueAlertModal" class="modal fade">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">ลงทะเบียน U-league {{ DataHelpers::year() }}</h3>
                    </div>
                    <div class="modal-body">
                        <p class="text-center">
                            <img src="{{ asset('/frontend/medilab/img/Startup_rocket.jpg')}}" class="img-responsive" ><br>
                            คุณต้องลงทะเบียนเข้าร่วมโครงการ U League {{ DataHelpers::year() }} ก่อน จึงจะสามารถเข้าร่วมกิจกรรมนี้ได้
                        </p>
                    </div>
                    <div class="modal-footer">
                        <a href="{{ url('/user/uleague/join') }}" class="btn btn-primary">ลงทะเบียน U League {{ DataHelpers::year() }}</a>
                    </div>
                </div>
            </div>
        </div>
        @else
        @if(!$has_join_camp)
        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#campAlertModal">เริ่มสร้างทีม</button>
        <div id="campAlertModal" class="modal fade">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">คุณยังไม่สามารถสร้างทีมได้</h3>
                    </div>
                    <div class="modal-body">
                        <p class="text-center">
                            <img src="{{ asset('/frontend/medilab/img/Startup_rocket.jpg')}}" class="img-responsive" ><br>
                            คุณต้องเข้าร่วมกิจกรรม U League Camp อย่างน้อย 1 ครั้งก่อน จึงจะสามารถสร้างทีมได้
                        </p>
                    </div>
                    <div class="modal-footer">
                        <a href="{{ url('/user/uleague/camp') }}" class="btn btn-primary">ดูรายการกิจกรรม</a>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @endif
    </div>
</section>
