<section>
    <h3 class="text-center">U-league {{ DataHelpers::year() }} (Pitching)</h3>
    <div class="padding-lg"></div>
    @if($has_join_camp)
    <p class="text-center">
        กิจกรรม U League 2018 (Picthing) เปิดโอกาสให้นักศึกษาที่ผ่านการเข้าร่วมกิจกรรม U League {{ DataHelpers::year() }} (Camp) สามารถสร้างทีมสตาร์ทอัพของตนเอง และชวนเพื่อนๆในมหาวิทยาลัยเข้าร่วมทีมได้ โดยต้องมีสมาชิก 3-5 คน และทุกคนต้องมีคุณสมบัติครบถ้วน<br>ถ้าพร้อมแล้ว กดปุ่มเริ่มสร้างทีมได้เลย!!
    </p>
    <p class="text-center">
        <a href="{{ url('/user/uleague/team') }}" class="btn btn-success">เริ่มสร้างทีม</a>
    </p>
    @else
    <p class="text-center">
        คุณจะเข้าร่วมกิจกรรม U League {{ DataHelpers::year() }} (Pitching) ได้ก็ต่อเมื่อคุณได้เข้าร่วม U League {{ DataHelpers::year() }} (Camp) อย่างน้อย 1 กิจกรรมแล้ว
    </p>
    @endif  
</section>