<section>

    <h3 class="text-center">U League {{ DataHelpers::year() }} Pitching</h3>
    <div class="padding-lg"></div>

    <div class="text-center">
        @if($step==0)
        <ul class="steps">
            <li class="active"><a href="{{ url('/user/uleague/team?step=1') }}">1. ข้อมูลทีม</a></li>
            <li class="undone"><p>2. ชวนเพื่อน</p></li>
            <li class="undone"><p>3. เลือกสนามแข่ง</p></li>
            <li class="undone"><p>4. รอผลการสมัคร</p></li>
            <li class="undone"><p>5. ยืนยันการสมัคร</p></li>
        </ul>
        @elseif($step==1)
        <ul class="steps">
            <li class="done"><a href="#" data-toggle="modal" data-target="#teamInfoModal">1. ข้อมูลทีม</a></li>
            <li class="active"><p>2.ชวนเพื่อน</p></li>
            <li class="undone"><p>3. เลือกสนามแข่ง</p></li>
            <li class="undone"><p>4. รอผลการสมัคร</p></li>
            <li class="undone"><p>5. ยืนยันการสมัคร</p></li>
        </ul>
        @elseif($step==2)
        <ul class="steps">
            <li class="done"><a href="#" data-toggle="modal" data-target="#teamInfoModal">1. ข้อมูลทีม</a></li>
            <li class="done"><a href="{{ url('/user/uleague/team?step=2') }}">2.ชวนเพื่อน</a></li>
            <li class="active"><p>3. เลือกสนามแข่ง</p></li>
            <li class="undone"><p>4. รอผลการสมัคร</p></li>
            <li class="undone"><p>5. ยืนยันการสมัคร</p></li>
        </ul>
        @elseif($step==3)
        <ul class="steps">
            <li class="done"><a href="#" data-toggle="modal" data-target="#teamInfoModal">1. ข้อมูลทีม</a></li>
            <li class="done"><p>2.ชวนเพื่อน</p></li>
            <li class="done"><a href="#" data-toggle="modal" data-target="#selectPitchingModal">3. เลือกสนามแข่ง</a></li>
            <li class="active"><p>4. รอผลการสมัคร</p></li>
            <li class="undone"><p>5. ยืนยันการสมัคร</p></li>
        </ul>
        @elseif($step==4)
        <ul class="steps">
            <li class="done"><a href="#" data-toggle="modal" data-target="#teamInfoModal">1. ข้อมูลทีม</a></li>
            <li class="done"><p>2.ชวนเพื่อน</p></li>
            <li class="done"><p>3. เลือกสนามแข่ง</p></li>
            <li class="done"><p>4. รอผลการสมัคร</p></li>
            <li class="active"><p>5. ยืนยันการสมัคร</p></li>
        </ul>
        @endif
    </div>

    @if($step==0)
    <hr class="divider">
    <form class="form-horizontal validated" action="{{ url('user/team/update') }}" method="post" role="form" enctype="multipart/form-data">

        <input type="hidden" value="{{ $team->team_id }}" name="team_id">

        <div class="form-group">
            <label for="team_name" class="control-label col-sm-3"> ชื่อทีม *</label>
            <div class="col-sm-8">
                <input name="team_name" type="text" id="team_name" class="form-control input-sm" value="{{ $team->team_name }}" placeholder="ชื่อทีม" />
            </div>
        </div>

        <div class="form-group">
            <label for="team_info" class="col-sm-3 control-label">ข้อมูลเกี่ยวกับโครงการ *</label>
            <div class="col-sm-8">
                <textarea type="text" class="form-control" name="team_info" id="team_info" placeholder="อธิบายข้อมูลโครงการเริ่มต้น และจุดประสงค์ของโครงการ" rows="6" required>{{ $team->team_info }}</textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="university" class="col-sm-3 control-label">ส่งในนามมหาวิทยาลัย *</label>
            <div class="col-sm-8">
                <select class="form-control" id="university" name="university" required></select>
            </div>
        </div>

        <div class="form-group">
            <label for="sector" class="col-sm-3 control-label">Startup Sector *</label>
            <div class="col-sm-8">
                <select name="startup_sector" id="startup_sector" class="form-control" required>
                    <option> --- Select Startup Sector ---</option>
                    <option value="1"> การแพทย์และสาธารณสุข (MedTech/ Health Tech) </option>
                    <option value="2"> เกษตรและอาหาร (AgriTech/ Food Startup) </option>
                    <option value="3"> อสังหาริมทรัพย์ (Property Tech) </option>
                    <option value="4"> การเงินและการธนาคาร (FinTech) </option>
                    <option value="5"> การศึกษา (EdTech) </option>
                    <option value="6"> การท่องเที่ยว (TravelTech) </option>
                    <option value="7"> ไลฟ์สไตล์ (LifeStyle)  </option>
                    <option value="8"> พาณิชยกรรมอิเล็กทรอนิกส์ (E-Commerce) </option>
                    <option value="9"> ภาครัฐ (GovTech) </option>
                    <option value="14"> อุตสาหกรรม (Industrial Tech) </option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="image" class="col-sm-3 control-label">ไฟล์ Proposal *<br>
                <small>(ไฟล์ .pdf เท่านั้น)</small>
            </label>
            <div class="col-sm-8">
                <input type="file" name="file" id="file" class="form-control" value="{{ asset( $team->file_url) }}" data-preview-file-type="text" data-upload-url="#" required/>
            </div>
        </div>

        <div class="padding-md"></div>
        <div class="form-group">
            <label class="control-label col-sm-3">&nbsp;</label>
            <div class="col-sm-8">
                <button class="btn btn-success" type="submit">อัพเดทข้อมูลทีม</button>
            </div>
        </div>

    </form>
    @endif

    @if($step==1)
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="alert alert-primary">
                <p class="padding text-center">
                    กรอกอีเมลของเพื่อนที่คุณต้องการชวนเข้าร่วมทีม โดยเงื่อนไขการรวมทีม ดังนี้
                </p>
                <ul>
                    <li>1. ต้องมีสมาชิกในทีมตั้งแต่ 3  คนขึ้นไป แต่ไม่เกิน 5 คน </li>
                    <li>2. สมาชิกในทีมทุกคนต้องตอบรับคำเชิญจากหัวหน้าทีม </li>
                    <li>3. สมาชิกในทีมทุกคนต้องเคยผ่านการเข้า Camp หรือเรียนออนไลน์ </li>
                    <li>4. สมาชิกในทีมต้องมาจาก 2 คณะ และ 3 สาขา </li>
                </ul>

            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
    <div class="row">
        <div class="col-sm-2 col-md-1"></div>
        <div class="col-sm-8 col-md-10">
            <div class="text-center">
                <form class="form-inline" id="inviteForm">
                    <input type="email" class="form-control" name="invite_to" id="invite_to" placeholder="name@example.com" style="width:200px;">
                    <button class="btn btn-primary" style="margin-bottom:4px;">ชวนเพื่อน</button>
                </form>
            </div>

            <hr class="divider" />
            <div class="padding-md"></div>

            @foreach($team_members as $item)
            <div class="row has-divider">
                <div class="col-xs-2 nopadding">
                    @if($item->avatar!=null)
                    <img src="{{ url('uploads/users').'/'.$item->avatar }}" class="img-responsive avatar" />
                    @else
                    <img src="{{ url('frontend/medilab/img/default-profile.png')}}" class="img-responsive avatar" />
                    @endif
                </div>
                <div class="col-xs-7 nopadding">
                    {{ $item->first_name }} {{ $item->last_name }}
                    <p class="text-small">
                        @if($item->id==Auth::user()->id)
                        <span>ฉัน &middot;</span>
                        @endif
                        <i class="fa fa-check color-green"></i> เข้าร่วมทีมแล้ว
                        @if(!$item->pass_camp)
                        <span class="color-red">&middot; ยังไม่ผ่าน Camp</span>
                        @endif
                        <br>
                        {{$item->faculty_name}} > {{$item->sub_faculty}}
                    </p>
                </div>
                <div class="col-xs-3 text-center nopadding"></div>
            </div>
            @endforeach

            @if(count($waiting_list) > 0)
            @foreach($waiting_list as $item)
            <div class="row has-divider">
                <div class="col-xs-9 nopadding">
                    {{ $item->invite_to }}
                    <p class="text-small">รอการตอบกลับ</p>
                </div>
                <div class="col-xs-3 text-center nopadding">
                    <button type="button" class="btn btn-danger btn-xs" onclick="removeInvite('{{ $item->invite_to }}')">
                        ลบออก
                    </button>
                </div>
            </div>
            @endforeach
            @endif

            @if($team_is_eligible)
            <a href="{{ url('/user/uleague/team?step=3') }}" class="btn btn-primary pull-right">เลือกรอบแข่งขัน</a>
            <div class="clearfix"></div>
            @endif
        </div>
        <div class="col-sm-2 col-md-1"></div>
    </div>
    @endif

    @if($step==3)
    <hr class="divider" />
    @if(!$has_reject)
    <div class="text-center">
        ทีมงานได้รับข้อมูลทีม {{ $team->team_name }} ของคุณแล้ว กรุณารอการตอบกลับ
        <p class="padding text-center">
            <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#teamInfoModal">ดูข้อมูลทีม</button> 
            &nbsp;&nbsp; หรือ &nbsp;&nbsp;
            <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#selectPitchingModal">ส่งแข่งขันรอบอื่นๆ</button>
        </p>
    </div>
    @else
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <h1 class="text-center"><i class="fas fa-times color-red"></i></h1>
            <p class="text-center">ทีมของคุณไม่ผ่านเกณฑ์ เนื่องจาก {{ $reject['reason'] }}</p>
            <div class="padding">
                <h5 class="text-center">แนวทางการปฏิบัติ</h5>
                @if($reject['code']==-99)
                <ul style="list-style:square;">
                    <li>ตรวจสอบว่าคุณอัพโหลด Proposal ถูกต้องหรือไม่</li>
                </ul>
                @elseif($reject['code']==-98)
                <ul style="list-style:square;">
                    <li>ตรวจสอบว่าสมาชิกภายในทีมของคุณว่า อัพโหลดบัตรนักศึกษาถูกต้องหรือไม่</li>
                    <li>ตรวจสอบว่าสมาชิกภายในทีมของคุณว่า ผ่านการเข้าร่วมกิจกรรมประเภท U League {{ DataHelpers::year() }} Camp มาครบทุกคนหรือไม่</li>
                    <li>ตรวจสอบว่าสมาชิกภายในทีมของคุณว่า มาจาก 2 คณะ และ 3 สาขาหรือไม่</li>
                </ul>
                @endif
            </div>
            <div class="padding text-center">
                <button class="btn btn-success" type="button" data-toggle="modal" data-target="#confirmSubmit">ส่งสมัครใหม่</button>
                <div id="confirmSubmit" class="modal fade">
                    <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">ยืนยันการสมัคร</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p class="text-center">
                                    หลังจากที่คุณส่งทีมเข้าแข่งขัน U League {{ DataHelpers::year() }} Pitching แล้ว คุณจะไม่สามารถแก้ไขข้อมูลของทีมคุณได้อีก
                                </p>
                            </div>
                            <div id="btns" class="modal-footer">
                                <a onclick="toggle()" href="{{ url('/user/uleague/pitching/reapply?team_id='.$team->team_id) }}" class="btn btn-secondary">ยืนยัน</a>
                                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @elseif($step==4)
    <hr class="divider" />
    <div class="text-center">

        <h1><i class="fas fa-check color-green"></i></h1>
        ทีม {{ $team->team_name }} ของคุณได้รับการยืนยันแล้ว

        <p class="padding text-center">
            <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#teamInfoModal">ดูข้อมูลทีม</button> 
            &nbsp;&nbsp; หรือ &nbsp;&nbsp;
            <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#selectPitchingModal">ส่งแข่งขันรอบอื่นๆ</button>
        </p>
    </div>
    @elseif($step==2)
    <hr class="divider" />

    @foreach( $pitching_events as $item )

    <div class="row" style="border-bottom:2px solid #eee; padding:10px;">
        <div class="col-sm-4 col-md-3">
            <img src='{{ url($item['cover']) }}' alt="" class="img-responsive">
        </div>

        <div class="col-sm-8 col-md-9">
            <h4>{{ $item['name'] }}</h4>
            <p>
                <i class="fa fa-calendar"></i> วันที่ : {{ $item['start_date'] }} - {{ $item['end_date'] }}
            </p>
            <p>
                <i class="fa fa-map-marker"></i> สถานที่ : {{ $item['venue']->name }}
            </p>
            <p>
                <i class="fa fa-sign-in"></i> รับสมัคร : {{ $item['registration'] }} | คงเหลือ : {{ $item['remain'] }}
            </p>
            <p>
                @if($item['is_checkin'])
                <span class="text-success">เข้าร่วมแล้ว</span>
                @elseif($item['is_register'])
                <span class="text-warning">สมัครแล้ว</span>
                @else
                <button class="btn btn-primary btn-sm" data-event-id="{{$item['id']}}" data-toggle="modal" data-target="#comfirmPitchingModal_{{$item['id']}}">เข้าร่วม</button>
                @endif
            </p>
            <div id="comfirmPitchingModal_{{$item['id']}}" class="modal fade">
                <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">ยืนยันรอบสมัคร {{ $item['name'] }}</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p class="text-center">
                                หลังจากที่คุณส่งทีมเข้าแข่งขัน U League {{ DataHelpers::year() }} Pitching แล้ว คุณจะไม่สามารถแก้ไขข้อมูลของทีมคุณได้อีก
                            </p>
                        </div>
                        <div id="btns" class="modal-footer">
                            <a id="conf-btn" onclick="toggle()" href="{{ url('/user/uleague/pitching/apply?event_id='.$item['id'].'&team_name='.$team->team_name) }}" class="btn btn-secondary">ยืนยัน</a>
                            <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endforeach
    @endif

    <div class="padding-lg"></div>

    <div id="teamInfoModal" class="modal fade">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">ข้อมูลทีม {{$team->team_name}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form class="form-horizontal validated" action="{{ url('user/team/update') }}" method="post" role="form" enctype="multipart/form-data">

                        <input type="hidden" value="{{ $team->team_id }}" name="team_id">
                        <div class="form-group">
                            <label for="team_name" class="control-label col-sm-3"> ชื่อทีม </label>
                            <div class="col-sm-8">
                                <input name="team_name" type="text" id="team_name" class="form-control input-sm" value="{{ $team->team_name }}" placeholder="ชื่อทีม" @if($team->status==2) readonly @endif />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="team_info" class="col-sm-3 control-label">ข้อมูลเกี่ยวกับโครงการ *</label>
                            <div class="col-sm-8">
                                <textarea type="text" class="form-control" name="team_info" id="team_info" placeholder="อธิบายข้อมูลโครงการเริ่มต้น และจุดประสงค์ของโครงการ" rows="6" required @if($team->status==2) readonly @endif>{{ $team->team_info }}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="university" class="col-sm-3 control-label">ส่งในนามมหาวิทยาลัย</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="university" name="university" required @if($team->status==2) readonly @endif></select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sector" class="col-sm-3 control-label">Startup Sector</label>
                            <div class="col-sm-8">
                                <select name="startup_sector" id="startup_sector" class="form-control" required @if($team->status==2) readonly @endif>
                                    <option value="1"> การแพทย์และสาธารณสุข (MedTech/ Health Tech) </option>
                                    <option value="2"> เกษตรและอาหาร (AgriTech/ Food Startup) </option>
                                    <option value="3"> อสังหาริมทรัพย์ (Property Tech) </option>
                                    <option value="4"> การเงินและการธนาคาร (FinTech) </option>
                                    <option value="5"> การศึกษา (EdTech) </option>
                                    <option value="6"> การท่องเที่ยว (TravelTech) </option>
                                    <option value="7"> ไลฟ์สไตล์ (LifeStyle)  </option>
                                    <option value="8"> พาณิชยกรรมอิเล็กทรอนิกส์ (E-Commerce) </option>
                                    <option value="9"> ภาครัฐ (GovTech) </option>
                                    <option value="14"> อุตสาหกรรม (Industrial Tech) </option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="image" class="col-sm-3 control-label">ไฟล์ Proposal
                            </label>
                            <div class="col-sm-8">
                                <p style="margin-top:6px;"><a href="{{ asset( $team->file_url) }}" target="_blank">ดาวน์โหลด</a> </p>
                            </div>
                        </div>
                        @if($team->status!=2)
                        <div class="form-group">
                            <label for="image" class="col-sm-3 control-label">แก้ไขไฟล์ Proposal *<br>
                                <small>(ไฟล์ .pdf เท่านั้น)</small>
                            </label>
                            <div class="col-sm-8">
                                <input type="file" name="file" id="file" class="form-control" value="{{ asset( $team->file_url) }}" data-preview-file-type="text" data-upload-url="#" required/>
                            </div>
                        </div>

                        <div class="padding-md"></div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">&nbsp;</label>
                            <div class="col-sm-8">
                                <button class="btn btn-primary" type="submit">อัพเดทข้อมูลทีม</button>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div id="selectPitchingModal" class="modal fade">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">เลือกรอบแข่งขัน</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @foreach( $pitching_events as $item )

                    <div class="row" style="border-bottom:2px solid #eee; padding:10px;">
                        <div class="col-sm-4 col-md-3">
                            <img src='{{ url($item['cover']) }}' alt="" class="img-responsive">
                        </div>

                        <div class="col-sm-8 col-md-9">
                            <h4>{{ $item['name'] }}</h4>
                            <p>
                                <i class="fa fa-calendar"></i> วันที่ : {{ $item['start_date'] }} - {{ $item['end_date'] }}
                            </p>
                            <p>
                                <i class="fa fa-map-marker"></i> สถานที่ : {{ $item['venue']->name }}
                            </p>
                            <p>
                                <i class="fa fa-sign-in"></i> รับสมัคร : {{ $item['registration'] }} | คงเหลือ : {{ $item['remain'] }}
                            </p>
                            <p>
                                @if($item['is_checkin'])
                                <span class="text-success">เข้าร่วมแล้ว</span>
                                @elseif($item['is_register'])
                                <span class="text-warning">สมัครแล้ว</span>
                                @else
                                <button class="btn btn-primary btn-sm" data-event-id="{{$item['id']}}" data-toggle="modal" data-target="#comfirmPitchingModal_{{$item['id']}}">เข้าร่วม</button>
                                @endif
                            </p>
                            <div id="comfirmPitchingModal_{{$item['id']}}" class="modal fade">
                                <div class="modal-dialog modal-md" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">ยืนยันรอบสมัคร {{ $item['name'] }}</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p class="text-center">
                                                หลังจากที่คุณส่งทีมเข้าแข่งขัน U League {{ DataHelpers::year() }} Pitching แล้ว คุณจะไม่สามารถแก้ไขข้อมูลของทีมคุณได้อีก
                                            </p>
                                        </div>
                                        <div id="btns" class="modal-footer">
                                            <a id="conf-btn" onclick="toggle()" href="{{ url('/user/uleague/pitching/apply?event_id='.$item['id'].'&team_name='.$team->team_name) }}" class="btn btn-secondary">ยืนยัน</a>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){

        var allowSubmit = true;

        $("#university").jCombo("{!! url('select/university') !!}", {selected_value: '{{ $team->university_id }}'});

        $('#startup_sector').val('{{ $team->startup_sector }}');

        $('#inviteForm').submit(function(event) {

            if (allowSubmit){

                if($('#invite_to').val()!=''){

                    $('body').loadingModal({text: 'กำลังชวนเพื่อน...'});
                    allowSubmit = false;

                    var formData = { 'team_id' : '{{ $team->team_id }}', 'invite_to' : $('#invite_to').val() };

                    $.ajax({
                        type        : 'POST',
                        url         : '{{ url('/user/team/member/invite') }}',
                        data        : formData,
                        dataType    : 'json',
                        encode      : true
                    }).done(function(data) {

                        if(data.success){ location.reload(true); }

                        else{ alert(data.message); }

                        $('body').loadingModal('destroy');
                        allowSubmit = true;
                    });
                }
                event.preventDefault();
            }
            else{
                return false;
            }
        });

        $('#comfirmPitchingModal').on('show.bs.modal', function(e) {

            var eventId = $(e.relatedTarget).data('event-id');

            $("a#conf-btn").attr("href", "{{ url('/user/uleague/pitching/apply?event_id=') }}"+eventId+"&team_name={{ $team->team_id }}");

        });
    });

    function toggle(){
        $('body').loadingModal({text: 'กำลังโหลด...'});
        $("#btns").hide();
    }

    function removeMember(id){
        alert('removeMember '+id)
    }

    function removeInvite(email){

        $('body').loadingModal({text: 'กำลังลบเพื่อน...'});

        var formData = { 'team_id' : '{{ $team->team_id }}', 'email' : email };

        $.ajax({
            type        : 'POST',
            url         : '{{ url('/user/team/member/rminvite') }}',
            data        : formData,
            dataType    : 'json',
            encode      : true
        }).done(function(data) {
            if(data.success){ location.reload(true); }

            $('body').loadingModal('destroy');
        });
    }
</script>