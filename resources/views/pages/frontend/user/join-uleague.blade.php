<section>
    <h3 class="text-center">ลงทะเบียน Startup Thailand League {{ DataHelpers::year() }} : U-league</h3>
    <div class="padding-lg"></div>

    <form id="register-form" class="form-horizontal parsley-validate" action="{{ url('user/uleague/join') }}" method="post" role="form" enctype="multipart/form-data">
        @if (Session::has('message'))
        <div class="alert alert-danger text-center">{{ Session::get('message') }}</div>
        @endif

        <input type="hidden" value="{{ \Auth::user()->id }}" name="user_id" >

        <div class="form-group">
            <label for="Nickname" class="control-label col-sm-3 text-left"> ชื่อเล่น <span class="asterix"> * </span></label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="nickname" name="nickname" placeholder="ชื่อเล่น" required>
            </div>
        </div>

        <div class="form-group">
            <label for="University Id" class="control-label col-sm-3 text-left"> มหาวิทยาลัย <span class="asterix"> * </span></label>
            <div class="col-sm-8">
                <select class="form-control" id="university" name="university" required></select>
            </div>
        </div>

        <div class="form-group">
            <label for="student_id" class="control-label col-sm-3 text-left"> รหัสนักศึกษา <span class="asterix"> * </span></label>
            <div class="col-sm-8">
                <input type="number" class="form-control" id="student_id" name="student_id" placeholder="รหัสนักศึกษา" required>
            </div>
        </div>

        <div class="form-group">
            <label for="Faculty Id" class="control-label col-sm-3 text-left"> คณะที่กำลังศึกษา <span class="asterix"> * </span></label>
            <div class="col-sm-8">
                <select class="form-control" id="faculty" name="faculty"></select>
            </div>
        </div>

        <div class="form-group">
            <label for="sub_faculty" class="control-label col-sm-3 text-left"> สาขาที่กำลังศึกษา <span class="asterix"> * </span></label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="sub_faculty" name="sub_faculty" placeholder="สาขาที่กำลังศึกษา" value="{{$user_info->sub_faculty}}" required>
            </div>
        </div>

        <div class="form-group">
            <label for="College Year" class="control-label col-sm-3 text-left"> ชั้นปีที่ศึกษา <span class="asterix"> * </span></label>
            <div class="col-sm-8">
                <select class="form-control" id="year" name="year" required>
                    <option value="">เลือกชั้นปีที่ศึกษาอยู่</option>
                    <option value="1">ปีที่ 1</option>
                    <option value="2">ปีที่ 2</option>
                    <option value="3">ปีที่ 3</option>
                    <option value="4">ปีที่ 4</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="image" class="col-sm-3 control-label">อัพโหลดบัตรนักศึกษา *<br>
                <small>(นามสกุล .jpg, .png เท่านั้น)</small>
            </label>
            <div class="col-sm-8">
                <input type="file" name="image" id="image" class="form-control" data-preview-file-type="text"data-upload-url="#"/>
            </div>
        </div>
        @if(config('sximo.cnf_recaptcha') =='true')
        <div class="form-group has-feedback" align="center">
            <label class="text-left"> </label>
            <div class="g-recaptcha" data-sitekey="6Lf-X0UUAAAAAPIcp_R1VJdPnm_9ZuKG4DkP67f3"></div>
            <div class="clr"></div>
        </div>
        @endif
        <div class="col-sm-offset-3 col-sm-5">
            <div class="padding-lg"></div>
            <button type="submit" class="btn btn-sm btn-primary">ลงทะเบียน</button>
            <button type="reset" class="btn btn-sm btn-link">รีเซท</button>
        </div>
        <div class="padding-lg clearfix"></div>

        <div id="confirm-modal" class="modal fade">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">กรุณายืนยันข้อมูล</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="text-center">
                            <img src="{{ asset('/frontend/medilab/img/Startup_rocket.jpg')}}" class="img-responsive" ><br>
                            ข้อมูลการสมัคร U League {{ DataHelpers::year() }} จะไม่สามารถเปลี่ยนแปลงข้อมูลได้อีก คุณต้องการยืนยันข้อมูลนี้หรือไม่
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button id="submit-btn" type="button" class="btn btn-secondary btn-default">ยืนยันข้อมูล</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

<script type="text/javascript">
    $(document).ready(function () {

        $('#register-form').parsley();

        $('#register-form').submit(function(event){

            event.preventDefault(); //this will prevent the default submit
            
            if($('#image').val()!=''){
                $('#confirm-modal').modal("show")
            }
            else{
                alert('อัพโหลดบัตรนักศึกษาสำหรับตรวจสอบ')
            }
        });
        
        $('#submit-btn').click(function(){
            $('body').loadingModal({text: 'กำลังลงทะเบียน U League {{ DataHelpers::year() }}...'});
            $('#register-form').unbind('submit').submit(); 
        });

        $("#university").jCombo("{!! url('select/university') !!}");
        $("#faculty").jCombo("{!! url('select/faculty') !!}", {selected_value: '{{ $user_info->faculty_id }}'});

    })
</script>