<section>
    <h3 class="text-center">ข้อมูล U League {{ DataHelpers::year() }} ของคุณ</h3>
    <div class="padding-lg"></div>

    <ul class="nav nav-tabs">
        <li class="active"><a href="#uleague_profile" data-toggle="tab"> ข้อมูลนักศึกษา </a></li>
        <li><a href="#uleague_student_card" data-toggle="tab"> อัพโหลดบัตรนักศึกษา </a></li>
        {{--<li><a href="#uleague_activities" data-toggle="tab"> กิจกรรม U League คุณเข้าร่วม </a></li>--}}
    </ul>
    <div class="tab-content">

        <div class="tab-pane active m-t" id="uleague_profile">

            <form class="form-horizontal" action="{{ url('user/uleague/update') }}" method="post">
                <div class="form-group">
                    <label for="fullname" class="control-label col-sm-3"> ชื่อเล่น </label>
                    <div class="col-sm-8">
                        <input name="nickname" type="text" id="nickname" class="form-control input-sm" value="{{ $uleague->nickname }}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="university" class="control-label col-sm-3"> มหาวิทยาลัย </label>
                    <div class="col-sm-8">
                        <!-- <input name="university" type="text" id="university" class="form-control input-sm" value="{{ $uleague->name }}" /> -->
                        <select class="form-control input-sm" id="university_id" name="university_id" required></select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="faculty" class="control-label col-sm-3"> คณะที่กำลังศึกษา </label>
                    <div class="col-sm-8">
                        <!-- <input name="faculty" type="text" id="faculty" class="form-control input-sm" value="{{ $uleague->faculty_name }}" /> -->
                        <select class="form-control input-sm" id="faculty_id" name="faculty_id"></select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="sub_faculty" class="control-label col-sm-3"> สาขาที่กำลังศึกษา </label>
                    <div class="col-sm-8">
                        <input name="sub_faculty" type="text" id="sub_faculty" class="form-control input-sm" value="{{ $uleague->sub_faculty }}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="college_year" class="control-label col-sm-3"> ชั้นปี </label>
                    <div class="col-sm-8">
                        <input name="college_year" type="number" id="college_year" class="form-control input-sm" value="{{ $uleague->college_year }}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="student_id" class="control-label col-sm-3"> รหัสนักศึกษา </label>
                    <div class="col-sm-8">
                        <input name="student_id" type="text" id="student_id" class="form-control input-sm" value="{{ $uleague->student_id }}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="student_id" class="control-label col-sm-3"> บัตรนักศึกษา </label>
                    <div class="col-sm-8">
                        <a class="btn-link" href="{{ asset($uleague->photo_src) }}" data-lightbox="image-1">
                            ดูบัตรนักศึกษา
                        </a>
                    </div>
                </div>
                <div class="form-group">
                    <label for="student_id" class="control-label col-sm-3"></label>
                    <div class="col-sm-8">
                        <button type="submit" class="btn btn-sm btn-primary">อัพเดท</button>
                    </div>
                </div>
            </form>
        </div>

        <div class="tab-pane m-t" id="uleague_student_card">
            <form id="register-form" class="form-horizontal parsley-validate" action="{{ url('user/uleague/upload') }}" method="post" role="form" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="image" class="col-sm-3 control-label">อัพโหลดบัตรนักศึกษา *<br>
                        <small>(นามสกุล .jpg, .png เท่านั้น)</small>
                    </label>
                    <div class="col-sm-8">
                        <input type="file" name="image" id="image" class="form-control" data-preview-file-type="text"data-upload-url="#"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <div class="padding-lg"></div>
                        <button type="submit" class="btn btn-sm btn-primary">อัพเดท</button>
                    </div>
                </div>
            </form>
        </div>
        {{--
        <div class="tab-pane m-t" id="uleague_activities">
            @if(count($events)==0)
            <p class="padding text-center">คุณยังไม่ได้เข้าร่วมกิจกรรม U League Camp ใดๆ</p>
            @else
            @foreach( $events as $item )
            <div class="row" style="border-bottom:2px solid #eee; padding:10px;">
                <div class="col-sm-4 col-md-3">
                    <img src='{{ url($item['cover']) }}' alt="" class="img-responsive">
                </div>
                <div class="col-sm-8 col-md-9">
                    <h5>
                        <a href="{{ $item['url'] }}" target="_blank">{{ $item['name'] }}</a>
                    </h5>
                    <p>
                        <i class="far fa-calendar-alt"></i> วันที่ : {{ $item['start_date'] }} - {{ $item['end_date'] }}
                    </p>
                    <p>
                        <i class="fas fa-map-marker-alt"></i> สถานที่ : {{ $item['venue']->name }}
                    </p>
                    <p>
                        <i class="fas fa-arrow-right"></i> ประเภท : {{ $item['type_text'] }}
                    </p>
                    <p>
                        <i class="fas fa-users"></i> รับสมัคร : {{ $item['registration'] }} | คงเหลือ : {{ $item['remain'] }}
                    </p>
                    <p>
                        @if($item['is_register'])
                        <span class="color-blue"><i class="fa fa-check"></i> ลงทะเบียนแล้ว</span>
                        @endif
                        @if($item['is_checkin'])
                        &middot; <span class="color-green"><i class="fa fa-check"></i> เข้าร่วมแล้ว</span>
                        @endif
                    </p>
                </div>
            </div>
            @endforeach
            @endif
        </div>
        --}}
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {

        $("#university_id").jCombo("{!! url('select/university') !!}", {selected_value: '{{ $uleague->university_id }}'});
        $("#faculty_id").jCombo("{!! url('select/faculty') !!}", {selected_value: '{{ $uleague->faculty_id }}'});

    });

</script>