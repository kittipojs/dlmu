<section>
    <h3 class="text-center">ตอบรับคำเชิญ</h3>

    <div class="padding-lg"></div>

    <p class="text-center">
        คุณ {{ $team->first_name }} ได้ส่งคำเชิญให้คุณเข้าร่วมทีม {{ $team->team_name }} เพื่อทำการแข่งขัน U League {{ DataHelpers::year() }}
    </p>
    <p class="text-center">
        ต้องการเข้าร่วมหรือไม่? 
    </p>

    <div class="padding-lg text-center">
        <button id="accept" type="button" class="btn btn-success">เข้าร่วม</button>
        &nbsp;&nbsp;
        <button id="deny" type="button" class="btn btn-danger">ปฏิเสธ</button>
    </div>
</section>

<script>
    $(document).ready(function () {

        var allowSubmit = true;
        
        $('#accept').click(function(event) {
            
            $('body').loadingModal({text: 'กำลังเข้าร่วมทีม...'});
            allowSubmit = false;
            
            var formData = { "invitation_code":'{{ $invite_code }}', "status":1, "team_id":'{{ $team->team_id }}' } 

            $.ajax({
                type        : 'POST',
                url         : '{{ url('/user/team/member/rpinvite') }}',
                data        : formData,
                dataType    : 'json',
                encode      : true
            }).done(function(data) {
                if(data.success){
                    window.location = '{{ url('/user/uleague/team') }}'
                }
                else{
                    alert(data.message)
                }
                $('body').loadingModal('destroy');
                allowSubmit = true;
            });

            event.preventDefault();
        });

        $('#deny').click(function(event) {
            
            $('body').loadingModal({text: 'กำลังปฏิเสธ...'});
            allowSubmit = false;

            var formData = { "invitation_code":'{{ $invite_code }}', "status":-1, "team_id":'{{ $team->team_id }}' } 

            $.ajax({
                type        : 'POST',
                url         : '{{ url('/user/team/member/rpinvite') }}',
                data        : formData,
                dataType    : 'json',
                encode      : true
            }).done(function(data) {
                if(data.success){
                    window.location = '{{ url('/user/uleague/team') }}'
                }
                else{
                    alert(data.message)
                }
                $('body').loadingModal('destroy');
                allowSubmit = true;
            });

            event.preventDefault();
        });

    });
</script>