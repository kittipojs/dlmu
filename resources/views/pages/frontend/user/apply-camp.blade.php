<section>
    <h3 class="text-center">U-league {{ DataHelpers::year() }} (Camp)</h3>
    <div class="padding-lg"></div>

    @if(count($events)==0)
    <p class="text-center">ยังไม่มีกิจกรรม U League {{ DataHelpers::year() }} Camp</p>
    @else
    @foreach( $events as $item )
    <div class="row" style="border-bottom:2px solid #eee; padding:10px;">
        <div class="col-sm-4 col-md-4" style="padding-top:40px;">
            <a href="{{ $item['url'] }}" target="_blank">
                <img src='{{ url($item['cover']) }}' alt="" style="width:100%;">
            </a>
        </div>
        <div class="col-sm-8 col-md-8">
            <h4>
                <a href="{{ $item['url'] }}" target="_blank">{{ $item['name'] }}</a>
            </h4>
            <p>
                <i class="far fa-calendar-alt"></i> วันที่ : {{ $item['start_date'] }} - {{ $item['end_date'] }}
            </p>
            <p>
                <i class="fas fa-map-marker-alt"></i> สถานที่ : {{ $item['venue']->name }}
            </p>
            <p>
                <i class="fas fa-arrow-right"></i> ประเภท : {{ $item['type_text'] }}
            </p>
            <p>
                <i class="fas fa-users"></i> รับสมัคร : {{ $item['registration'] }} | คงเหลือ : {{ $item['remain'] }}
            </p>
            <p>
                @if($item['type']=='Camp')
                @if($item['is_checkin'])
                <span class="text-success"><i class="fa fa-check-circle"></i> เข้าร่วมแล้ว</span>
                @elseif($item['is_register'])
                <span class="text-success"><i class="fa fa-check-circle"></i> สมัครแล้ว</span><br>
                <span class="color-red">กรุณาตรวจสอบ Email ของท่าน เพื่อนำ QR Code ไป Check in ที่หน้างาน</span>
                @else
                @if(isset($uleague))
                @if($has_join_camp)
                <span class="text-danger">คุณสามารถสมัครกิจกรรมประเภท Camp กลางได้เพียง 1 ครั้งเท่านั้น</span>
                @else
                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#confirmModal" id="{{$item['id']}}">เข้าร่วม</button>
                @endif
                @else
                <button class="btn btn-sm  btn-primary" data-toggle="modal" data-target="#alertModal">เข้าร่วม</button>
                @endif
                @endif
                @elseif($item['type']=='UCamp')
                @if($item['is_checkin'])
                <span class="text-success"><i class="fa fa-check-circle"></i> เข้าร่วมแล้ว</span>
                @elseif($item['is_register'])
                <span class="text-success"><i class="fa fa-check-circle"></i> สมัครแล้ว</span><br>
                <span class="color-red">กรุณาตรวจสอบ Email ของท่าน เพื่อนำ QR Code ไป Check in ที่หน้างาน</span>
                @else
                @if(isset($uleague)) 
                <button class="btn btn-sm  btn-primary" onclick="applyCamp('{{$item['id']}}')">เข้าร่วม</button>
                @else
                <button class="btn btn-sm  btn-primary" data-toggle="modal" data-target="#alertModal">เข้าร่วม</button>
                @endif
                @endif
                @endif
            </p>
        </div>
    </div>
    @endforeach
    @endif
    {{-- @foreach( $events as $item )
    <div class="row" style="border-bottom:2px solid #eee; padding:10px;">
        <div class="col-sm-4 col-md-3">
            <div style="background-color:#eee; width:100%; height:180px;"></div>
        </div>
        <div class="col-sm-8 col-md-9">
            <h4>{{ $item['info']->event_title }}</h4>
            <p>
                วันที่ : {{ $item['info']->event_start }} - {{ $item['info']->event_end }}
            </p>
            <p>
                จำนวนที่รับสมัคร : {{ $item['info']->limit_participant }} | คงเหลือ : {{ $item['info']->limit_participant-$item['participants'] }}
            </p>

            <p style="margin-top:10px; padding-top:10px;">
                @if($item['participants'] > $item['info']->limit_participant)
                <span class="text-danger">เต็มแล้ว</span>
                @elseif( $item['info']->apply_before > date('Y-m-d') )

                @if( count($item['has_apply'])>=1 )
                @if($item['has_apply']->status==1)
                <span class="text-warning">สมัครแล้ว</span>
                @elseif($item['has_apply']->status==2)
                <span class="text-success">เข้าร่วมแล้ว</span>
                @endif
                @else
                @if(isset($uleague))
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#confirmModal" id="{{$item['info']->event_id}}">เข้าร่วม</button>
                @else
                <button class="btn btn-success" data-toggle="modal" data-target="#alertModal">เข้าร่วม</button>
                @endif
                @endif

                @else
                <span class="text-danger">หมดเวลาสมัคร</span>
                @endif
            </p>
        </div>
    </div>
    @endforeach --}}

    <div id="alertModal" class="modal fade">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">ลงทะเบียน U League {{ DataHelpers::year() }}</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="text-center">
                        <img src="{{ asset('/frontend/medilab/img/Startup_rocket.jpg')}}" class="img-responsive" ><br>
                        คุณต้องลงทะเบียนเข้าร่วมโครงการ U League {{ DataHelpers::year() }} ก่อน จึงจะสามารถเข้าร่วมกิจกรรมนี้ได้
                    </p>
                </div>
                <div class="modal-footer">
                    <a href="{{ url('/user/uleague/join') }}" class="btn btn-secondary">ลงทะเบียน U League {{ DataHelpers::year() }}</a>
                </div>
            </div>
        </div>
    </div>

    <div id="confirmModal" class="modal fade">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">ยืนยันการเข้าร่วมกิจกรรม</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="text-center">
                        <img src="{{ asset('/frontend/medilab/img/Startup_rocket.jpg')}}" class="img-responsive" ><br>
                        คุณสามารถเลือกเข้าร่วม กิจกรรม U League Camp {{ DataHelpers::year() }} ที่จัดโดย NIA ได้เพียง 1 กิจกรรมเท่านั้น
                    </p>
                </div>
                <div class="modal-footer">
                    <button id="apply-camp-btn" type="button" class="btn btn-secondary">ยืนยัน</button>
                </div>
            </div>
        </div>
    </div>

    <div id="mobileNumberModal" class="modal fade">
        <div class="modal-dialog modal-lg" role="document">
            <form id="updateMobileNumberForm" class="form-horizontal validated">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">ข้อมูลบางส่วนของคุณยังไม่ครบถ้วน</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="text-center">
                            เราพบว่าคุณยังไม่ได้กรอกข้อมูลต่อไปนี้ กรุณากรอกข้อมูลด้านล่างเพิ่มเติมเพื่อใช้ในการลงทะเบียนกิจกรรม
                        </p>
                        <div class="padding"></div>
                        <div class="form-group">
                            <label for="mobile_number" class="control-label col-sm-3"> เบอร์มือถือ * </label>
                            <div class="col-sm-8">
                                <input type="number" id="mobile_number" name="mobile_number" class="form-control input-sm" placeholder="เบอร์มือถือของคุณ" required />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="apply-camp-btn" type="submit" class="btn btn-secondary">อัพเดทข้อมูล</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

</section>

<script type="application/javascript">

    $('#updateMobileNumberForm').submit(function(){
        event.preventDefault();

        if( $('#mobile_number').val().length!=10 ){
            alert('เบอร์มือถือของคุณไม่ถูกต้อง')
        }else{

            $('body').loadingModal({text: 'กำลังอัพเดทเบอร์มือถือ...'});

            var formData = { "mobile_number":$('#mobile_number').val() } 

            $.ajax({
                type        : 'POST',
                url         : '{{ url('/user/mobilenumber') }}',
                data        : formData,
                dataType    : 'json',
                encode      : true
            }).done(function(data) {
                if(data.success){
                    location.reload(true);
                }
                else{
                    alert(data.message)
                }
            });
        }

        //$(this).unbind('submit').submit(); 
    })

    function confirm(event_id){
        $("#confirmModal").modal('show');
    }

    $('#confirmModal').on('show.bs.modal', function(e) {
        var event_id = e.relatedTarget.id;
        $("#apply-camp-btn").attr("onclick", "applyCamp('"+event_id+"')");
    })

    function applyCamp(event_id){

        $("#confirmModal").modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();

        @if($user['mobile_number']=='')
        $('#mobileNumberModal').modal('show');
        @else

        $('body').loadingModal({text: 'กำลังลงทะเบียนกิจกรรม...'});

        var formData = { "event_id":event_id } 

        $.ajax({
            type        : 'POST',
            url         : '{{ url('/user/uleague/camp/apply') }}',
            data        : formData,
            dataType    : 'json',
            encode      : true
        }).done(function(data) {

            if(data.success){
                window.location.reload(true);
            }
            else{
                alert(data.message)
            }

            $('body').loadingModal('destroy');

            window.location.reload(true);
        });

        @endif
    }

</script>