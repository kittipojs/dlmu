<section>
    <h3 class="text-center">ข้อมูลส่วนตัว</h3>

    <div class="padding-lg"></div>

    <ul class="nav nav-tabs">
        <li class="active"><a href="#user_info" data-toggle="tab">{{ Lang::get('core.personalinfo') }}</a></li>
        <li><a href="#startup_lookup" data-toggle="tab">หัวข้อที่สนใจ</a></li>
        <li><a href="#change_password" data-toggle="tab">{{ Lang::get('core.changepassword') }}</a></li>
        <li><a href="#uleague_activities" data-toggle="tab"> กิจกรรมที่เข้าร่วม </a></li>
        <li><a href="#courses" data-toggle="tab"> หลักสูตรที่ลงทะเบียน </a></li>
    </ul>

    <div class="tab-content">

        <div class="tab-pane active m-t" id="user_info">
            <div class="padding-lg"></div>
            {!! Form::open(array('url'=>'user/saveprofile', 'class'=>'form-horizontal validated' ,'files' => true)) !!}

            <div class="form-group">
                <label for="ipt" class="control-label col-sm-3"> อีเมล * </label>
                <div class="col-sm-8">
                    <input name="email" type="text" id="email" class="form-control input-sm"
                           value="{{ $user->email }}" readonly/>
                </div>
            </div>

            <div class="form-group">
                <label for="citizen_id" class="col-md-3 control-label"> หมายเลขบัตรประชาชน * </label>
                <div class="col-md-8">
                    <input name="citizen_id" type="text" id="citizen_id" class="form-control input-sm" required value="{{ $user->citizen_id }}" readonly />
                </div>
            </div>

            <div class="form-group">
                <label for="name_title" class="control-label col-sm-3"> คำนำหน้า * </label>
                <div class="col-sm-3">
                    <select name="name_title" id="name_title" class="form-control input-sm">
                        <option value="นาย">นาย</option>
                        <option value="นาง">นาง</option>
                        <option value="นางสาว">นางสาว</option>
                    </select>
                </div>
                <div class="col-sm-7"></div>
            </div>

            <div class="form-group">
                <label for="ipt"
                       class=" control-label col-sm-3"> ชื่อ * </label>
                <div class="col-sm-8">
                    <input name="first_name" type="text" id="first_name" class="form-control input-sm"
                           required value="{{ $user->first_name }}"/>
                </div>
            </div>

            <div class="form-group">
                <label for="last_name"
                       class=" control-label col-sm-3">นามสกุล * </label>
                <div class="col-sm-8">
                    <input name="last_name" type="text" id="last_name" class="form-control input-sm"
                           required value="{{ $user->last_name }}"/>
                </div>
            </div>

            <div class="form-group">
                <label for="dob" class="control-label col-sm-3">วันเกิด *</label>
                <div class="col-sm-8">
                    <input name="dob" type="text" id="dob" class="form-control input-sm date"
                           required value="{{ is_null($user->dob) ? date('Y-m-d') : $user->dob }}"/>
                </div>
            </div>

            <div class="form-group">
                <label for="education" class="control-label col-sm-3"> ระดับการศึกษา * </label>
                <div class="col-sm-8">
                    <select name="education" id="education" class="form-control input-sm" required>
                        <option value="" selected="true">--- โปรดเลือก ---</option>
                        <option value="ปริญญาเอก">ปริญญาเอก</option>
                        <option value="ปริญญาโท">ปริญญาโท</option>
                        <option value="ปริญญาตรี">ปริญญาตรี</option>
                        <option value="ปวช.">ปวช.</option>
                        <option value="ปวส.">ปวส.</option>
                        <option value="มัธยม 1">มัธยม 1</option>
                        <option value="มัธยม 2">มัธยม 2</option>
                        <option value="มัธยม 3">มัธยม 3</option>
                        <option value="มัธยม 4">มัธยม 4</option>
                        <option value="มัธยม 5">มัธยม 5</option>
                        <option value="มัธยม 6">มัธยม 6</option>
                        <option value="ต่ำกว่าหรือเท่ากับประถมศึกษาปีที่ 6">
                            ต่ำกว่าหรือเท่ากับประถมศึกษาปีที่ 6
                        </option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="occupation" class="control-label col-sm-3"> อาชีพ * </label>
                <div class="col-sm-8">
                    <select id="occupation" name="occupation" class="form-control input-sm">
                        <option value="" selected="true">--- โปรดเลือก ---</option>
                        <option value="C03">ธุรกิจส่วนตัว</option>
                        <option value="C09">อาชีพอิสระ</option>
                        <option value="C07">ข้าราชการ/พนักงานรัฐวิสาหกิจ</option>
                        <option value="C11">นักลงทุนในหลักทรัพย์มืออาชีพ</option>
                        <option value="C02">ผู้แนะนำการลงทุนในหลักทรัพย์</option>
                        <option value="C08">พนักงานบริษัทในธุรกิจการเงิน</option>
                        <option value="C12">บุคลากรทางการแพทย์</option>
                        <option value="C13">วิศวกร</option>
                        <option value="C14">บุคลากรทางการศึกษาและวิจัย</option>
                        <option value="C05">พนักงานบริษัทเอกชนอื่นๆ</option>
                        <option value="C04">นักเรียน/นักศึกษา</option>
                        <option value="C01">เกษียณ</option>
                        <option value="C06">แม่บ้าน/พ่อบ้าน</option>
                        <option value="C10">อื่นๆ</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="faculty_id" class="control-label col-sm-3"> คณะที่จบการศึกษา * </label>
                <div class="col-sm-8">
                    <select name="faculty_id" id="faculty_id" class="form-control input-sm" required></select>
                </div>
            </div>

            <div class="form-group">
                <label for="sub_faculty" class="col-sm-3 control-label"> สาขาที่จบการศึกษา * </label>
                <div class="col-sm-8">
                    <input type="text" name="sub_faculty" id="sub_faculty" class="form-control input-sm"
                           placeholder="สาขาที่จบการศึกษา" value="{{ $user->sub_faculty }}" required>
                </div>
            </div>

            <div class="form-group">
                <label for="address" class="control-label col-sm-3"> ที่อยู่ </label>
                <div class="col-sm-8">
                    {!! Form::text('address',  $user->address, array('class'=>'form-control input-sm', 'placeholder'=> 'ที่อยู่อาศัยปัจจุบัน')) !!}
                </div>
            </div>

            <div class="form-group ">
                <label for="province_id" class="control-label col-sm-3"> จังหวัด * </label>
                <div class="col-sm-8">
                    <select name="province_id" id="province_id" class="form-control input-sm" required>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="mobile_number" class="control-label col-sm-3"> เบอร์มือถือ * </label>
                <div class="col-sm-8">
                    {!! Form::text('mobile_number',  $user->mobile_number, array('class'=>'form-control input-sm', 'required'=>'true', 'placeholder'=> 'เบอร์มือถือของคุณ')) !!}
                </div>
            </div>

            <div class="form-group">
                <label for="postcode" class="col-sm-3 control-label"> รหัสไปรษณีย์ * </label>
                <div class="col-sm-8">
                    {!! Form::text('postcode', $user->postcode, array('class'=>'form-control input-sm', 'required'=>'true', 'placeholder'=> 'รหัสไปรษณีย์')) !!}
                </div>
            </div>

            <div class="form-group">
                <label for="ipt" class="control-label col-sm-3 text-right"> รูปภาพโปรไฟล์ </label>
                <div class="col-sm-8">

                    <input type="file" name="avatar" id="avatar" class="inputfile"/>
                    <label for="avatar"><i class="fa fa-upload"></i> เลือกไฟล์</label>
                    <div class="avatar_preview"></div>
                    <p>ขนาดของรูปภาพไม่เกิน 512 x 512 px</p>
                    <?php if( file_exists('./uploads/users/' . $user->avatar) && $user->avatar != '') { ?>
                    <img src="{{  url('uploads/users').'/'.$user->avatar }}" border="0" width="60"
                         class="img-responsive"/>
                    <?php  } else { ?>
                    <img alt="" src="http://www.gravatar.com/avatar/{{ md5($user->email) }}" width="60"
                         class="img-responsive"/>
                    <?php } ?>
                </div>
            </div>
            <div class="padding-md"></div>
            <div class="form-group">
                <label class="control-label col-sm-3">&nbsp;</label>
                <div class="col-sm-8">
                    <button class="btn btn-primary" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>
                </div>
            </div>

            {!! Form::close() !!}
        </div>

        <div class="tab-pane m-t" id="startup_lookup">

            <div class="padding-lg"></div>

            {!! Form::open(array('url'=>'user/saveprofile', 'id'=>'lookups-form', 'class'=>'form-horizontal validated' )) !!}

            <div class="form-group">
                <label class="control-label col-sm-3"></label>
                <div class="col-sm-8">
                    <p>
                        หัวข้อที่สนใจ (เลือกได้มากกว่า 1 ข้อ)
                    </p>
                    <div class="padding"></div>

                    <p>
                        <input id="lookup_Checkbox1" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="1" />
                        <label>การแพทย์และสาธารณสุข (MedTech/ Health Tech)</label>
                    </p>
                    <p>
                        <input id="lookup_Checkbox2" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="2" />
                        <label>เกษตรและอาหาร (AgriTech/ Food Startup)</label>
                    </p>
                    <p>
                        <input id="lookup_Checkbox3" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="3" />
                        <label>อสังหาริมทรัพย์ (Property Tech)</label>
                    </p>
                    <p>
                        <input id="lookup_Checkbox4" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="4" />
                        <label>การเงินและการธนาคาร (FinTech)</label>
                    </p>
                    <p>
                        <input id="lookup_Checkbox5" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="5" />
                        <label>การศึกษา (EdTech)</label>
                    </p>
                    <p>
                        <input id="lookup_Checkbox6" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="6" />
                        <label>การท่องเที่ยว (TravelTech)</label>
                    </p>
                    <p>
                        <input id="lookup_Checkbox7" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="7" />
                        <label>ไลฟ์สไตล์ (LifeStyle)</label>
                    </p>
                    <p>
                        <input id="lookup_Checkbox8" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="8" />
                        <label>พาณิชยกรรมอิเล็กทรอนิกส์ (E-Commerce)</label>
                    </p>
                    <p>
                        <input id="lookup_Checkbox9" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="9" />
                        <label>ภาครัฐ/การศึกษา (GovTech/EdTech)</label>
                    </p>
                    <p>
                        <input id="lookup_Checkbox10" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="14" />
                        <label>อุตสาหกรรม (Industrial Tech)</label>
                    </p>
                    <p>
                        <input id="lookup_Checkbox11" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="10" />
                        <label>แหล่งเงินทุน (Venture Capital/Coperate Venture Capital)</label>
                    </p>
                    <p>
                        <input id="lookup_Checkbox12" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="11" />
                        <label>พันธมิตร (Partners)</label>
                    </p>
                    <p>
                        <input id="lookup_Checkbox13" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="12" />
                        <label>การสนับสนุนจากรัฐบาล (Government Support)</label>
                    </p>
                    <p>
                        <input id="lookup_Checkbox14" class="startup_lookup" name="startup_lookup[]" type="checkbox" value="13" />
                        <label>คอร์สเรียนออนไลน์ (Online Learning Courses)</label>
                    </p>
                </div>
            </div>

            <div class="padding-md"></div>
            <div class="form-group">
                <label class="control-label col-sm-3">&nbsp;</label>
                <div class="col-sm-8">
                    <button class="btn btn-primary" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="tab-pane m-t" id="change_password">

            <div class="padding-lg"></div>

            {!! Form::open(array('url'=>'user/savepassword/', 'class'=>'form-horizontal ')) !!}

            <div class="form-group">
                <label for="ipt"
                       class=" control-label col-sm-3"> {{ Lang::get('core.newpassword') }} </label>
                <div class="col-sm-8">
                    <input name="password" type="password" id="password" class="form-control input-sm"
                           value=""/>
                </div>
            </div>

            <div class="form-group">
                <label for="ipt"
                       class=" control-label col-sm-3"> {{ Lang::get('core.conewpassword') }}  </label>
                <div class="col-sm-8">
                    <input name="password_confirmation" type="password" id="password_confirmation"class="form-control input-sm" value=""/>
                </div>
            </div>

            <div class="form-group">
                <label class=" control-label col-sm-3">&nbsp;</label>
                <div class="col-sm-8">
                    <button class="btn btn-primary" type="submit"> {{ Lang::get('core.sb_savechanges') }} </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="tab-pane m-t" id="uleague_activities">
            @if(count($events)==0)
            <p class="padding text-center">คุณยังไม่ได้เข้าร่วมกิจกรรม U League Camp ใดๆ</p>
            @else
            @foreach( $events as $item )
            <div class="row" style="border-bottom:2px solid #eee; padding:10px;">
                <div class="col-sm-4 col-md-3">
                    <img src='{{ url($item['cover']) }}' alt="" class="img-responsive">
                </div>
                <div class="col-sm-8 col-md-9">
                    <h5>
                        <a href="{{ $item['url'] }}" target="_blank">{{ $item['name'] }}</a>
                    </h5>
                    <p>
                        <i class="far fa-calendar-alt"></i> วันที่ : {{ $item['start_date'] }} - {{ $item['end_date'] }}
                    </p>
                    <p>
                        <i class="fas fa-map-marker-alt"></i> สถานที่ : {{ $item['venue']->name }}
                    </p>
                    <p>
                        <i class="fas fa-arrow-right"></i> ประเภท : {{ $item['type_text'] }}
                    </p>
                    <p>
                        <i class="fas fa-users"></i> รับสมัคร : {{ $item['registration'] }} | คงเหลือ : {{ $item['remain'] }}
                    </p>
                    <p>
                        @if($item['is_register'])
                        <span class="color-blue"><i class="fa fa-check"></i> ลงทะเบียนแล้ว</span>
                        @endif
                        @if($item['is_checkin'])
                        &middot; <span class="color-green"><i class="fa fa-check"></i> เข้าร่วมแล้ว</span>
                        @endif
                    </p>
                </div>
            </div>
            @endforeach
            @endif
        </div>


        <div class="tab-pane m-t" id="courses">
            @if(count($events)==0)
            <p class="padding text-center">คุณยังไม่ได้เข้าเรียนหลักสูตรใดๆ</p>
            @else
            <table width="100%">
                <thead style="border-bottom:2px solid #000; background-color:#efefef;">
                    <td width="55%" style="padding:10px;">หลักสูตร</td>
                    <td width="15%" style="padding:10px;">วันลงทะเบียน</td>
                    <td width="15%" style="padding:10px;">ใช้งานล่าสุด</td>
                    <td width="15%" style="padding:10px;">สถานะ</td>
                </thead>
                @foreach( $apply_courses as $item )
                <tr style="border-bottom:1px solid #eee;">
                    <td style="padding:10px;">
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div style="background:transparent url('{{ url($item['course_thumbnail']) }}') center center no-repeat; width:100%; height:110px; border:1px solid #eee; background-size:cover;"></div>
                            </div>
                            <div class="col-sm-6 col-md-6" style="font-weight:bold;">
                                <a href="{{url('/c/'.$item['course_id'])}}">{{ $item['course_code'] }}</a>
                                <br>
                                <a href="{{url('/c/'.$item['course_id'])}}">{{ $item['course_title'] }}</a>
                            </div>
                        </div>
                    </td>
                    <td style="padding:10px;">{{$item['first']}}</td>
                    <td style="padding:10px;">{{$item['last']}}</td>
                    <td style="padding:10px;">
                        @if($item['pass']['status'])
                        <p class="text-center" style="color:#00b053;">
                            ผ่าน
                            <br>
                            ({{ $item['pass']['datetime'] }})
                        </p>
                        @else
                        <p class="text-center">
                            กำลังเรียน
                        </p>
                        @endif
                    </td>
                </tr>
                {{--}
                <div class="row" style="border-bottom:2px solid #eee; padding:10px;">
                    <div class="col-sm-3 col-md-2">

                    </div>
                    <div class="col-sm-4 col-md-3">

                    </div>
                    <div class="col-sm-8 col-md-9">

                    </div>
                </div>
                --}}
                @endforeach
            </table>
            @endif
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {

        $('.date').datepicker({format: 'yyyy-mm-dd', autoclose: true});

        $("#faculty_id").jCombo("{!! url('select/faculty') !!}", {selected_value: '{{ $user->faculty_id }}'});
        $("#province_id").jCombo("{!! url('select/province') !!}", {selected_value: '{{ $user->province_id }}'});

        $("#name_title").val('{{ $user->name_title }}')
        $("#education").val('{{ $user->education }}')
        $("#occupation").val('{{ $user->occupation }}')

        var lookups = [{{ $user->startup_lookup }}];

        $.each(lookups, function( index, value ) {
            $('#lookup_Checkbox'+value).prop('checked', true);
        });

        $('#lookups-form').submit(function(event) {

            event.preventDefault();

            var sel = $('input.startup_lookup:checked').map(function(_, el) {
                return $(el).val();
            }).get();

            if(sel!=''){
                $(this).unbind('submit').submit(); 
            }
            else{
                alert('เลือกอย่างน้อย 1 ข้อ')
            }
        });
    })
</script>