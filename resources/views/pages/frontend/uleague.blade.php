<div style="min-height:400px; background-image:url({{ asset('frontend/medilab/img/bg-u_league.jpg') }}); background-size:cover;">
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">

                <img src="{{ asset('frontend/medilab/img/Step-ULeague-20180302.png')}}" style="width:100%;" class="img-responsive" />
                <div class="padding text-center">
                    <i class="fa fa-angle-down" style="font-size:50px; color:#aaa;"></i>
                </div>

                <div style="background-color:#eee; -webkit-box-shadow: 0px 3px 3px 1px rgba(179,179,179,1); -moz-box-shadow: 0px 3px 3px 1px rgba(179,179,179,1); box-shadow: 0px 3px 3px 1px rgba(179,179,179,1);">
                    <div style="background-color:#284958; color:#FFF; padding:7px;">
                        <h2 class="padding text-center color-white">เลือกสมัครเข้าร่วมโครงการ</h2>
                    </div>
                    <div class="padding-lg"></div>
                    <div class="row">
                        <div class="col col-md-6">
                            <div class="text-center">
                                {{--
                                <div class="text-center uleague-banner" style="background-image:url('{{ asset('frontend/medilab/img/startup_camping.jpg')}} ');"></div>
                                <h4>U League Camp</h4>
                                <div style="height:70px;"></div>
                                --}}
                                <a href="{{ url('/user/uleague/camp') }}" class="btn btn-block btn-lg btn-warning btn-round">สมัคร U League Camp</a>
                                <div class="padding"></div>
                            </div>
                        </div>
                        <div class="col col-md-6">
                            <div class="text-center">
                                {{--
                                <div class="text-center uleague-banner" style="background-image:url('{{ asset('frontend/medilab/img/startup_pitching.jpg') }}');"></div>
                                <h4>U League Pitching</h4>
                                <div style="height:70px;"></div>
                                --}}
                                <a href="{{ url('/user/uleague/pitching') }}" class="btn btn-block btn-lg btn-success btn-round">สมัคร U League Pitching</a>
                                <div class="padding"></div>
                            </div>
                        </div>
                    </div>
                    <div class="padding text-center"><span class="color-red">*</span> หมายเหตุ : ทั้งสองโครงการสมัครเข้าร่วม<span class="color-blue">ฟรี</span></div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div style="height:30px;"></div>
    </div>
</div>