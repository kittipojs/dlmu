<div style="min-height:800px;" ng-controller="InvitiationController">
    <div class="container" style="padding-top: 40px">
        <h4 class="text-center">
            คุณมี 1 คำเชิญเข้าร่วมทีม
        </h4>
        <p class="text-center">
            คุณ {{ $team->first_name }} ได้ส่งคำเชิญให้คุณเข้าร่วมทีม {{ $team->team_name }} เพื่อทำการแข่งขัน Startup League Thailand {{ DataHelpers::year() }}
        </p>
        <p class="text-center">
            ต้องการเข้าร่วมหรือไม่? 
        </p>

        <div class="text-center">
            <button id="accept" type="button" class="btn btn-success" ng-click="replyInvite(1, '{{ $invite_code }}', '{{ $team->team_id }}')">เข้าร่วม</button>
            &nbsp;&nbsp;
            <button id="deny" type="button" class="btn btn-danger" ng-click="replyInvite(1, '{{ $invite_code }}', '{{ $team->team_id }}')">ปฏิเสธ</button>
        </div>
    </div>
</div>

@if(isset($enable_ng))
<script type="text/javascript">
    var app = angular.module('elearningApp', ['invitation.controllers'], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });
</script>
@endif
<script type="text/javascript">

    var app = angular.module('invitation.controllers', [])


    app.controller('InvitiationController', function($scope, $http, $q) { 
        
        $scope.replyInvite = function(status, invitation_code, team_id){ 

            var formData = { "invitation_code":invitation_code, "status":status, "team_id":team_id } 
            var request = $http({ method: "POST", 
                                 url: "{{ url('/user/pitch/reply-invitation')}} ", 
                                 data: JSON.stringify(formData) 
                                }); 

            return(request.then( function(response){ 
                if(response.data.success){ 
                    window.location = "{{ url('/uleague/pitching') }}"
                }
                else{
                    alert(response.data.message)
                }
            }, handleError)); 

        }
        
        function handleError( response ){
            if (!angular.isObject( response.data ) || !response.data.message) {
                return( $q.reject("An unknown error occurred.") );
            }
            return( $q.reject( response.data.message ) );
        }
    });
    
</script>