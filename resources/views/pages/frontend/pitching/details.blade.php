<script type="text/javascript">

    var app = angular.module('pitching.controllers', [])

    app.controller('STLPitchingController', function($scope, WizardHandler) {

        $scope.step = {{ $step }};
        $scope.f = {"memberEmail":""};
        $scope.teamMembers = ["wasin@mail.com"];


        setTimeout(function(){
            if($scope.step!=undefined){
                WizardHandler.wizard().goTo($scope.step);
            }
        }, 200);



    $scope.text = function(){
        WizardHandler.wizard().next();
    }

    $scope.addMember = function(){

        if( $.inArray($scope.f.memberEmail, $scope.teamMembers)!=-1 ){
            alert('อีเมลซ้ำ')
        }
        else if(!isValidEmailAddress($scope.f.memberEmail)){
            alert('รูปแบบอีเมลไม่ถูกต้อง')
        }
        else if($scope.teamMembers.length>=5){
            alert('สมาชิกในทีมสูงสุดได้ 5 คน')
        }
        else{

            $scope.teamMembers.push( $scope.f.memberEmail )

            $scope.f.memberEmail = '';
        }

    }

    $scope.enterValidation = function(){
        return true;
    };


    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
        return pattern.test(emailAddress);
    };
    });
</script>
<div class="container" ng-controller="STLPitchingController">
    @if(!$has_register)
    <div align="center">
        <h4 class="text-center">ยังไม่ได้สมัคร U-League Pitching</h4>
        <a class="btn btn-default" href="{{ url('uleague/pitching') }}">
            สมัครเข้าร่วม U-League Pitching
        </a>
    </div>
    @else
    <div class="header">
        <h4>การสมัคร U-League pitching</h4>
    </div>

    <wizard indicators-position="top" on-finish="finishedWizard()" on-cancel="cancelledWizard()"> 
        <wz-step wz-title="สมัคร STL {{ DataHelpers::year() }}" canenter="enterValidation">
            <div class="padding" style="margin-top:20px; border-radius:6px;">
                <input type="hidden" value="1" name="step">
                <div class="row">
                    <div class="col-xs-4 col-md-3 col-lg-2" align="center">
                        <img src="{{ asset( $row->photo_src ) }}" alt="" class="img-thumbnail">
                    </div>
                    <div class="col-xs-8 col-md-9 col-lg-10">
                        <form class="form-vertical">

                            <div class="form-group has-feedback">
                                <span><i class="fa fa-user prefix grey-text"></i> {{ $row->first_name }} {{ $row->last_name }}</span>
                            </div>

                            <div class="form-group has-feedback">
                                <span><i class="fa fa-envelope prefix grey-text"></i> {{ $row->email }}</span>
                            </div>

                            <div class="form-group has-feedback">
                                <span><i class="fa fa-university prefix grey-text"></i> {{ $row->name }} {{ $row->faculty_name }}</span>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="padding"></div>

                <div class="row">
                    <div class="col-sm-12">
                        @if(!$has_team)
                        <p>
                            เริ่มสร้างทีมสำหรับเข้าร่วมแข่งขัน U-League Pitching {{ DataHelpers::year() }} ได้ทันที
                            หรือรอรับคำเชิญเข้าร่วมทีมจากเพื่อน
                        </p>
                        <button class="btn btn-success" wz-next>เริ่มสร้างทีมของฉัน</button>
                        @else
                        <button class="btn btn-success" wz-next>เริ่มสร้างทีมของฉัน</button>
                        @endif
                    </div>
                </div>
            </div>
        </wz-step>
        <wz-step wz-title="สร้างทีม" canenter="enterValidation">
            <div class="padding" style="margin-top:20px; border-radius:6px;">
                <div>
                    <div class="row text-center">
                        <div class="col-sm-12">
                            ใส่ข้อมูลเกี่ยวกับทีมที่ของคุณลงในแบบฟอร์มด้านล่างนี้
                            หลังจากที่คุณได้ทำการสร้างทีมแล้วจะไม่สามาถเข้าร่วมกับทีมอื่นได้อีก
                        </div>
                    </div>
                    <div class="padding"></div>
                    <div class="padding"></div>

                    <form class="form-horizontal" action="{{ url($team_submit_url) }}" method="post" role="form"
                          enctype="multipart/form-data">
                        <div class="col-sm-12">
                            <input type="hidden" value="{{ Auth::user()->id }}" name="user_id">
                            @if($has_team)
                            <input type="hidden" value="{{ $team->team_id }}" name="team_id">
                            @endif
                            <input type="hidden" value="2" name="step">

                            <div class="form-group">
                                <label for="team_name" class="col-sm-4 control-label">ชื่อทีม *</label>
                                <div class="col-sm-8">
                                    <input type="text" name="team_name" class="form-control" id="team_name" placeholder="ชื่อทีม" value="{{ $team->team_name }}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="team_info" class="col-sm-4 control-label">แนะนำทีม *</label>
                                <div class="col-sm-8">
                                    <textarea type="text" class="form-control" name="team_info" id="team_info" placeholder="ข้อความแนะนำทีม" value="{{ $team->team_info }}" rows="6" required>{{ $team->team_info }}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="university" class="col-sm-4 control-label">ส่งในนามมหาวิทยาลัย *</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="university" name="university" required></select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="faculty" class="col-sm-4 control-label">Startup Sector *</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="sector" id="sector" required>
                                        <option value="1">Fintech</option>
                                        <option value="2">Foodtech</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="image" class="col-sm-4 control-label">อัพโหลด Pitch Deck *<br>
                                <small>(ไฟล์ .pdf เท่านั้น)</small>
                            </label>
                            <div class="col-sm-5">
                                <input type="file" name="file" id="file" class="form-control" data-preview-file-type="text" data-upload-url="#" required/>
                            </div>
                            <div class="col-sm-5">
                                &nbsp;&nbsp;<a href="{{ url($team->file_url) }}">ดู pitch deck</a>
                            </div>
                        </div>
                        <div class="padding"></div>
                        <div class="padding"></div>
                        @if($has_team)
                        <div class="text-center">
                            <button type="submit" class="btn btn-default">อัพเดทข้อมูล</button>
                            &nbsp;&nbsp;หรือ&nbsp;&nbsp;
                            <button type="button" class="btn btn-success" id="to_invite_friends_btn">เริ่มชวนเพื่อน</button>
                        </div>
                        @else
                        <div class="text-center">
                            <button type="submit" class="btn btn-success">สร้างทีม</button>
                        </div>
                        @endif
                    </form>
                    <div class="padding"></div>
                    <div class="padding"></div>
                </div>
                <input type="submit" wz-next value="Go on" />
            </div>
        </wz-step>
        <wz-step wz-title="ชวนเพื่อน" canenter="enterValidation">
            <div class="padding" style="background-color:#efefef; margin-top:20px; border-radius:6px;">
                <section>
                    <div class="row text-center">
                        <div class="col-sm-12">
                            ชวนเพื่อนร่วมที่ {{$team->team_name}} ได้สูงสุด 5 คน โดยกรอกอีเมลของเพื่อนลงในแบบฟอร์มด้านล่าง และรอการตอบรับจากเพื่อนๆของคุณ
                        </div>
                    </div>
                    <div class="padding"></div>
                    <div class="padding"></div>
                    <form id="invite-form" onsubmit="return inviteFriends()" class="form-horizontal">
                        <div class="col-sm-12">
                            <input type="hidden" value="" name="user_id">

                            <div class="form-group">
                                <label for="faculty" class="col-sm-4 control-label">หัวหน้าทีม (ฉัน)</label>
                                <div class="col-sm-8">
                                    <input type="text" name="lead_name" class="form-control" id="lead_name"
                                           value="{{ $row->first_name }} {{ $row->last_name }}" disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">
                                    <!--สมาชิก-->
                                </label>
                                <div class="col-sm-7">

                                    <div class="input-group">
                                        <input type="email" id="member" ng-model="f.memberEmail" class="form-control" placeholder="อีเมลแอดเดรสของเพื่อน">
                                        <span class="input-group-btn">
                                            <button ng-click="addMember()" class="btn btn-success" type="button">เพิ่มอีเมล</button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                            </div>

                            <div class="form-group" ng-if="teamMembers.length>=1">
                                <label class="col-sm-4 control-label">สมาชิก</label>
                                <div class="col-sm-7">
                                    <ul class="list">
                                        <li ng-repeat="item in teamMembers"><% item %></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-success">ชวนเพื่อน</button>
                            </div>
                        </div>
                    </form>
                </section>
                <input type="submit" wz-next value="Finish now" />
            </div>
        </wz-step>
        <wz-step wz-title="เลือกรอบ" canenter="enterValidation">
            <div class="padding" style="background-color:#efefef; margin-top:20px; border-radius:6px;">


                <input type="submit" wz-next value="Finish now" />
            </div>
        </wz-step>
    </wizard>

    <div style="height:50px;"></div>

    @endif
</div>
@include('modal.noresult')
<script>

    $(document).ready(function () {

        /*
        $("#step").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "fade",
            autoFocus: true,
            enablePagination: false,
            saveState: false
        });
        */

        $("#university").jCombo("{!! url('select/university') !!}",
                                {selected_value: '{{ $team->university_id }}'});

        //$("#faculty").jCombo("{!! url('select/faculty') !!}");
        //$("#add_member").jCombo("{!! url('select/pitch/member') !!}");
        @if(isset($team->sector))
        $('#sector').val({{ $team->sector }})
        @endif

        @if($has_team)

        //$("#step").steps("next")
        //$("#step").steps("next")

        var team_members = [];

        team_members.push('{{$row->email}}');

        /*
        $('#add_member_btn').click(function(){

            var member = $('#member')

            if( $.inArray(member.val(), team_members)!=-1 ){
                alert('อีเมลซ้ำ')
            }
            else if(!isValidEmailAddress(member.val())){
                alert('รูปแบบอีเมลไม่ถูกต้อง')
            }
            else if(team_members.length>=5){
                alert('สมาชิกในทีมสูงสุดได้ 5 คน')
            }
            else{
                team_members.push(member.val())

                $('#members').append('<li>'+member.val()+'</li>')

                member.val('');
            }

        });
        */

        @endif

        //here it will remove the current value of the remove button which has been pressed
        $("body").on("click", ".remove", function () {
            $(this).parents(".form-group").remove();
        });

        $("#to_create_team_btn").click(function () {
            //TODO : check if user has create team before. If yes, go to step 2 otherwise, remain this step 
            $("#step").steps("next")
        });

        $("#to_invite_friends_btn").click(function () {
            //TODO : check if user has create team before. If yes, go to step 2 otherwise, remain this step 
            $("#step").steps("next")
        });



        function inviteFriends(){
            alert(team_members)
            return false;
        }
    });

    /*
    function addMember() {
        var add_member = $('#add_member');
        add_member.click(function () {
            $.ajax({
                url: 'ajax/pitch/member',
                type: 'GET',
                data: {text: $('#add_member').val()},
                success: function (data) {
                    if (data.success) {
                        console.log("success",data)
                        $("[data-field='username']").text(data.username);
                    } else {
                        // show some errors
                        console.log("error",data)
                    }
                },
                error: function () {
                    alert("Error, Please try again")
                }
            });
        })
    }
    */
</script>