<div style="min-height:800px;">
    <div class="container" style="padding-top: 40px">

        @if($has_invitation)
        <p class="text-center">
            คุณมีคำเชิญเข้าร่วมทีม Startup Thailand League {{ DataHelpers::year() }} <br>
            <button class="btn btn-link" data-toggle=modal data-target="#confirm-invitation-modal">
                ดูรายละเอียด
            </button>
        </p>
        <hr>
        @endif
        <h1 class="text-center padding">
            U-League Pitching {{ DataHelpers::year() }}
        </h1>
        <div class="padding"></div>
    </div>

    <div class="container" ng-controller="STLPitchingController">
        @if($step_to_go==0)

        @if($is_team_member)
        <div class="text-center">
            คุณได้ตอบรับเข้าร่วมทีม {{ $is_team_member->team_name }} แล้ว กรุณากรอกข้อมูลด้านล่างเพื่อใช้ประกอบการสมัคร
        </div>
        <div class="padding"></div>
        @endif

        <h3 class="text-center padding color-grey">
            กรอกข้อมูลส่วนตัว
        </h3>
        <form class="form-horizontal" action="{{ url('user/pitch/register') }}" method="post" role="form"
              enctype="multipart/form-data">

            <div class="row setup-content" id="step-1">
                <div class="col-sm-12">
                    <input type="hidden" value="{{ $user->id }}" name="user_id">

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-4" style="padding-top: 7px">
                            <a href=""><i class="fa fa-file-pdf-o" aria-hidden="true"></i> ดาวน์โหลดตารางอบรมค่าย</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">ชื่อ - นามสกุล *</label>
                        <div class="col-sm-5">
                            <input type="text" name="firstname" class="form-control" id="name" placeholder="ชื่อ"
                                   value="{{ $user->first_name }}" readonly>
                        </div>
                        <div class="col-sm-5">
                            <input type="text" name="lastname" class="form-control" id="name" placeholder="นามสกุล"
                                   value="{{ $user->last_name }}" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">อีเมลแอดเดรส *</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" placeholder="Email"
                                   value="{{ $user->email }}" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="nickname" class="col-sm-2 control-label">ชื่อเล่น *</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="nickname" name="nickname"
                                   placeholder="ชื่อเล่นของน้อง" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="university" class="col-sm-2 control-label">มหาวิทยาลัย *</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="university" name="university" ></select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="faculty" class="col-sm-2 control-label">คณะ *</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="faculty" name="faculty" ></select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="year" class="col-sm-2 control-label">ชั้นปี *</label>
                        <div class="col-sm-5">
                            <select class="form-control" id="year" name="year" >
                                <option value="-1">เลือกชั้นปีที่ศึกษาอยู่</option>
                                <option value="1">ปีที่ 1</option>
                                <option value="2">ปีที่ 2</option>
                                <option value="3">ปีที่ 3</option>
                                <option value="4">ปีที่ 4</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="image" class="col-sm-2 control-label">อัพโหลดบัตรนักศึกษา *<br>
                            <small>(ไฟล์ .jpg, .png เท่านั้น)</small>
                        </label>
                        <div class="col-sm-5">
                            <input type="file" name="image" id="image" class="form-control" data-preview-file-type="text"
                                   data-upload-url="#" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="padding"></div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-5">
                    <button type="button" class="btn btn-success btn-default" data-toggle=modal data-target="#confirm-modal">สมัคร</button>
                    <button type="reset" class="btn btn-link btn-default">รีเซท</button>
                </div>
            </div>

            <!-- Modal Confirm-->
            <div class="modal" id="confirm-modal" role="dialog" aria-labelledby="myModalLabel">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog modal-sm vertical-align-center" role="document" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h5 class="modal-title" id="modal-header">ยืนยันการสมัคร</h5>
                            </div>
                            <div class="modal-body text-center" id="modal-info">
                                คุณมีสิทธิ์สมัครได้เพียง 1 ครั้งเท่านั้น<br>
                                ไม่สามารถเปลี่ยนแปลงได้
                            </div>
                            <div class="modal-footer">
                                <div align="center">
                                    <button class="btn btn-success" type="submit"> ตกลง </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        @elseif($step_to_go==1)

        <h3 class="text-center padding color-grey">
            เริ่มสร้างทีม
        </h3>
        <section class="padding">
            <div>
                <div class="row text-center">
                    <div class="col-sm-12">
                        ใส่ข้อมูลเกี่ยวกับทีมที่ของคุณลงในแบบฟอร์มด้านล่างนี้
                        หลังจากที่คุณได้ทำการสร้างทีมแล้วจะไม่สามารถเข้าร่วมกับทีมอื่นได้อีก
                    </div>
                </div>
                <div class="padding"></div>
                <div class="padding"></div>
                <form class="form-horizontal" action="{{ url('user/pitch/create/team') }}" method="post" role="form" enctype="multipart/form-data">
                    <div class="col-sm-12">
                        
                        <input type="hidden" value="{{ Auth::user()->id }}" name="user_id">
                        @if($has_team)
                        <input type="hidden" value="{{ $team->team_id }}" name="team_id">
                        @endif
                        <!-- <input type="hidden" value="2" name="step"> -->
                        
                        <div class="form-group">
                            <label for="team_name" class="col-sm-4 control-label">ชื่อทีม *</label>
                            <div class="col-sm-8">
                                <input type="text" name="team_name" class="form-control" id="team_name" placeholder="ชื่อทีม" value="{{ $team->team_name }}" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="team_info" class="col-sm-4 control-label">แนะนำทีม *</label>
                            <div class="col-sm-8">
                                <textarea type="text" class="form-control" name="team_info" id="team_info" placeholder="ข้อความแนะนำทีม" value="{{ $team->team_info }}" rows="6" required>{{ $team->team_info }}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="university" class="col-sm-4 control-label">ส่งในนามมหาวิทยาลัย *</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="university" name="university" required></select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sector" class="col-sm-4 control-label">Startup Sector *</label>
                            <div class="col-sm-8">
                                <select name="sector" id="sector" class="form-control" required>
                                    <option value="1"> การแพทย์และสาธารณสุข (MedTech/ Health Tech) </option>
                                    <option value="2"> เกษตรและอาหาร (AgriTech/ Food Startup) </option>
                                    <option value="3"> อสังหาริมทรัพย์ (Property Tech) </option>
                                    <option value="4"> การเงินและการธนาคาร (FinTech) </option>
                                    <option value="5"> การศึกษา (EdTech) </option>
                                    <option value="6"> การท่องเที่ยว (TravelTech) </option>
                                    <option value="7"> ไลฟ์สไตล์ (LifeStyle)  </option>
                                    <option value="8"> พาณิชยกรรมอิเล็กทรอนิกส์ (E-Commerce) </option>
                                    <option value="9"> ภาครัฐ/การศึกษา (GovTech/EdTech) </option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="image" class="col-sm-4 control-label">อัพโหลด Pitch Deck *<br>
                            <small>(ไฟล์ .pdf, .jpg, .png เท่านั้น)</small>
                        </label>
                        <div class="col-sm-5">
                            <input type="file" name="file" id="file" class="form-control" data-preview-file-type="text" data-upload-url="#" required/>
                        </div>
                    </div>
                    <div class="padding-lg"></div>
                    @if($has_team)
                    <div class="text-center">
                        <button type="submit" class="btn btn-default">อัพเดทข้อมูล</button>
                        &nbsp;&nbsp;หรือ&nbsp;&nbsp;
                        <button type="button" class="btn btn-success" id="to_invite_friends_btn">เริ่มชวนเพื่อน</button>
                    </div>
                    @else
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">สร้างทีม</button>
                    </div>
                    @endif
                </form>
            </div>
        </section>


        @elseif($step_to_go==2)

        @if($is_team_member && !$is_team_leader)

        <h4 class="text-center">คุณเป็นสมาชิกของทีม {{ $is_team_member->team_name }} แล้ว</h4>

        <p class="text-center">
            <a href="{{ url('/uleague/team/'.$is_team_member->team_id) }}" class="btn btn-default">ดูรายละเอียดทีม</a>
        </p>
        @elseif($is_team_leader)
        <section class="padding">

            <h3 class="text-center padding color-grey">
                ชวนเพื่อนเข้าทีมของคุณ
            </h3>
            <div class="row text-center">
                <div class="col-sm-12">
                    ชวนเพื่อนร่วมทีม {{$team->team_name}} ได้ 3-5 คน โดยกรอกอีเมลของเพื่อนลงในแบบฟอร์มด้านล่าง และรอการตอบรับจากเพื่อนๆของคุณ
                </div>
            </div>
            <div class="padding"></div>
            <div class="padding"></div>
            <form id="invite-form" class="row form-horizontal">
                <div class="col-sm-12">
                    <input type="hidden" value="" name="user_id">

                    <div class="form-group">
                        <label for="faculty" class="col-sm-4 control-label">หัวหน้าทีม (ฉัน)</label>
                        <div class="col-sm-8">
                            <input type="text" name="lead_name" class="form-control" id="lead_name"
                                   value="{{ $user->first_name }} {{ $user->last_name }} ({{ $user->email }})" disabled>
                        </div>
                    </div>

                    @if(!$invite_accepted)
                    <div class="form-group">
                        <label class="col-sm-4 control-label">
                            <!--สมาชิก-->
                        </label>
                        <div class="col-sm-8">

                            <div class="input-group">
                                <input type="email" id="member" ng-model="f.memberEmail" class="form-control" placeholder="อีเมลแอดเดรสของเพื่อน">
                                <span class="input-group-btn">
                                    <button ng-click="addMember()" class="btn btn-success" type="button">เพิ่มอีเมล</button>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-1"></div>
                    </div>
                    @endif

                    <!--
<div class="form-group">
<label class="col-sm-4 control-label">เพื่อนที่ชวนแล้ว</label>
<div class="col-sm-8" style="padding-top:4px;">

<p ng-if="$index>0" ng-repeat="item in team_members">
<i class="icon fa fa-person"></i> <% item.invite_to %> --
<span ng-if="item.status==0">รอการตอบรับ</span>
<span ng-if="item.status==1">เข้าร่วมทีมแล้ว</span>
</p>
</div>
</div>
-->


                    <div class="form-group" ng-if="team_members.length>=1">
                        <label class="col-sm-4 control-label"></label>
                        <div class="col-sm-8">
                            <ul class="list-group">
                                <li ng-if="$index>0" class="list-group-item d-flex justify-content-between align-items-center" ng-repeat="item in team_members">
                                    <% item.invite_to %>
                                        <span ng-if="item.status==-1 || item.status==0 || item.status==-99" class="badge badge-pill" ng-click="removeMember($index)">x</span>
                                        <span ng-if="item.status==1" class="badge badge-primary badge-pill">ยืนยันแล้ว</span>
                                        <span ng-if="item.status==-1" class="badge badge-primary badge-pill">ปฏิเสธ</span>
                                        <span ng-if="item.status==-0" class="badge badge-pill">รอการตอบกลับ</span>
                                        </li>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group" ng-if="team_members.length>=1">
                        <label class="col-sm-4 control-label"></label>
                        <div class="col-sm-8">
                            @if(!$invite_accepted)
                            <button type="button" ng-if="has_new" class="btn btn-default" ng-click="inviteFriends()">ชวนเพื่อน</button>
                            @endif
                        </div>
                    </div>
                </div>
            </form>
        </section>
        @endif

        @if($invite_accepted)
        <section class="padding">
            <h3 class="text-center padding color-grey">
                เลือกรอบแข่งขัน
            </h3>
            <form class="form-horizontal" action="{{ url('user/pitch/apply') }}" method="post" role="form"
                  enctype="multipart/form-data">
                <div class="col-sm-12">
                    <input type="hidden" value="{{ $user->id }}" name="user_id">
                    <input type="hidden" value="{{ $team->team_id }}" name="team_id">
                    <div class="form-group">
                        <label for="round" class="col-sm-4 control-label">รอบการสมัคร *</label>
                        <div class="col-sm-8">
                            <div class="radio">
                                <p style="color:red;">รับสมัครเพียงรอบละ 200 คน เท่านั้น</p>
                            </div>
                            @foreach( $rounds as $round)
                            <div class="radio">
                                <label>
                                    @if($round_count[$round->round_id] >= 200)
                                    <input type="radio" name="round" id="round" value="{{ $round->round_id }}"
                                           disabled required>
                                    <p>{{ $round->round_title }} ( เต็ม )</p>
                                    @elseif($round->apply_before <= date('Y-m-d'))
                                                                        <input type="radio" name="round" id="round" value="{{ $round->round_id }}"
                                    disabled required>
                                    <p>{{ $round->round_title }} ( หมดเวลาสมัคร )</p>
                                    @else
                                    <input type="radio" name="round" id="round" value="{{ $round->round_id }}"
                                           required>
                                    <p>{{ $round->round_title }} (
                                        จำนวนผู้สมัครขณะนี้ {!! $round_count[$round->round_id] !!} คน )</p>
                                    @endif
                                </label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-success" ng-click="selectRound()">เลือกรอบแข่งขัน</button>
                </div>
            </form>
        </section>
        @endif

        @elseif($step_to_go==3)
        <p class="text-center">
            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Yes_Check_Circle.svg/1024px-Yes_Check_Circle.svg.png" style="width:100px;">
            <br>
            <br>
            ทีม {{ $team->team_name }} ได้ทำการสมัคร Startup League Pitching เรียบร้อยแล้ว
            <br>
            <br>
            รอผลการพิจารณาจากเจ้าหน้าที่

        <p class="text-center">
            <a href="{{ url('/uleague/team/'.$is_team_member->team_id) }}" class="btn btn-default">ดูรายละเอียดทีม</a>
        </p>
        </p>
    @endif

    @if($has_invitation) 
    <!-- Modal Invitation Confirm--> 
    <div class="modal" id="confirm-invitation-modal" role="dialog" aria-labelledby="myModalLabels"> 
        <div class="vertical-alignment-helper"> 
            <div class="modal-dialog modal-sm vertical-align-center" role="document" > 
                <div class="modal-content"> 
                    <div class="modal-header"> 
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                        <h5 class="modal-title" id="modal-header">ยืนยันการเข้าร่วมทีม</h5> 
                    </div> 
                    <div class="modal-body text-center" id="modal-info"> 
                        คุณมีคำเชิญเข้าร่วมทีม {{$invitation->team_name}} จากคุณ {{$invitation->first_name}} {{$invitation->last_name}} คุณต้องการเข้าร่วมทีมนี้หรือไม่? 
                    </div> 
                    <div class="modal-footer"> 
                        <div align="center"> 
                            <a class="btn btn-success" ng-click="replyInvite(1, '{{ $invitation->invitation_code }}', '{{ $invitation->team_id }}')"> ยอมรับ </a> 
                            &nbsp;&nbsp; 
                            <button class="btn btn-danger" type="button" ng-click="replyInvite(-1, '{{ $invitation->invitation_code }}', '{{ $invitation->team_id }}')"> ปฎิเสธ </button> 
                        </div> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </div> 
    @endif
</div>
</div>
@include('modal.noresult')
<div class="padding"></div>


@if(isset($enable_ng))
<script type="text/javascript">
    var app = angular.module('elearningApp', ['pitching.controllers'], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });
</script>
@endif
<script type="text/javascript">

    var app = angular.module('pitching.controllers', [])

    app.controller('STLPitchingController', function($scope, $http, $q) { 
        
        $scope.lead_name
        
        $scope.f = { "memberEmail":"" };
        $scope.m_index = ["{{ $user->email }}"];
        $scope.team_members = ["{{ $user->email }}"];
        $scope.has_new = false;

        $scope.removeMember = function(index){
            $scope.team_members.splice(index, 1)
            $scope.m_index.splice(index, 1)
        }

        $scope.addMember = function(){
            
            $('#modal-header').html('');
            
            if( $.inArray($scope.f.memberEmail, $scope.m_index)!=-1 ){
                $('#modal-info').html(`<p>อีเมลซ้ำ</p>`);
                $('#noResult-modal').modal('toggle');
            }
            else if(!isValidEmailAddress($scope.f.memberEmail)){
                $('#modal-info').html(`<p>รูปแบบอีเมลไม่ถูกต้อง</p>`);
                $('#noResult-modal').modal('toggle');
            }
            else if($scope.team_members.length >= 5 ){
                $('#modal-info').html(`<p>สมาชิกในทีมสูงสุดได้ 5 คน</p>`);
                $('#noResult-modal').modal('toggle');
            }
            else{

                $scope.team_members.push( { invite_to:$scope.f.memberEmail, status:-99 } )
                $scope.m_index.push($scope.f.memberEmail)
                $scope.f.memberEmail = '';

                $scope.has_new = true;

            }

        }

            @if(isset($has_team) && $has_team)

        $scope.inviteFriends = function(){

            if($scope.team_members.length<3){
                $('#modal-info').html(`<p>ชวนเพื่อนอย่างน้อย 2 คน</p>`);
                $('#noResult-modal').modal('toggle');
            }
            else if($scope.team_members.length >= 5 ){
                $('#modal-info').html(`<p>ชวนเพื่อนได้สูงสุด 5 คน</p>`);
                $('#noResult-modal').modal('toggle');
            }
            else{
                var formData = { "team_id":"{{ $team->team_id }}", "team_members":$scope.team_members }
                var request = $http({ method: "POST",
                                     url: "{{ url('/user/pitch/invite-friends')}}",
                                     data: JSON.stringify(formData)
                                    });

                return(request.then( function(response){
                    if(response.data.success){
                        window.location = "{{ url('/uleague/pitching') }}"
                    }   
                }, handleError));
            }
        }
            @endif

        $scope.replyInvite = function(status, invitation_code, team_id){ 

            var formData = { "invitation_code":invitation_code, "status":status, "team_id":team_id } 
            var request = $http({ method: "POST", 
                                 url: "{{ url('/user/pitch/reply-invitation')}}", 
                                 data: JSON.stringify(formData) 
                                }); 

            return(request.then( function(response){ 
                if(response.data.success){ 
                    window.location = "{{ url('/uleague/pitching') }}"
                } 
            }, handleError)); 

        }

        var request = $http({ method: "GET",
                             url: "{{ url('/user/pitch/invite-friends')}} ",
                            });

        return(request.then( function(response){
            angular.forEach(response.data.data, function(value, key) {
                $scope.team_members.push(value)
                $scope.m_index.push(value.invite_to)
            });

        }, handleError));

        function handleError( response ){
            if (!angular.isObject( response.data ) || !response.data.message) {
                return( $q.reject("An unknown error occurred.") );
            }
            return( $q.reject( response.data.message ) );
        }


        function isValidEmailAddress(emailAddress) {
            var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
            return pattern.test(emailAddress);
        };
    });
</script>
<script>
    $(document).ready(function () {
        $("#university").jCombo("{!! url('select/university') !!}");
        $("#faculty").jCombo("{!! url('select/faculty') !!}");

    });
</script>