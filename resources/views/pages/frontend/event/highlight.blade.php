@if(count($events)>0)
<div id="event-carousel" class="row">
    @foreach($events as $item)
    <div class="col-sm-4">
        <div class="event-item">
            <a href="{{ $item['url'] }}" target="_blank">
                <img src="{{ $item['cover'] }}" class="img-responsive"/>
            </a>
            <div class="event-content padding">
                <a href="{{ $item['url'] }}" target="_blank"><h4>{{ $item['name'] }}</h4></a>
                <p>
                    <i class="far fa-calendar-alt"></i> วันที่ : {{ $item['start_date'] }}
                    - {{ $item['end_date'] }}
                </p>
                <p>
                    <i class="fas fa-map-marker-alt"></i> สถานที่ : {{ $item['venue']->name }}
                </p>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endif

<script type="text/javascript">
    $(document).ready(function(){

        $('#event-carousel').slick({
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 3
        });
        
    });
</script>