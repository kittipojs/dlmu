@include('modal.modal')
<!--camp-->
<div class="row" style="min-height: 400px; background-image:url('{{ asset('frontend/medilab')}}/img/bg-1.jpg'); background-position:center;">
    <div class="container">
        <div class="container">
            <div class="row" style="padding-top: 20px; padding-bottom: 20px">
                <div class="col-sm-4">
                </div>
                <div class="col-sm-4" align="center">
                    <h2>Camp Events</h2>
                </div>
                <div class="col-sm-4" align="right">
                    @if($all_camp)
                    <button class="btn btn-default-2" onclick="allEvent($(this))" data-type="Camp">
                        See All
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </button>
                    @endif
                </div>
            </div>
            @if(count($events_camp)>0)
            <div class="row">
                @foreach($events_camp as $item)
                <div class="col-xs-6 col-sm-6 col-md-4">
                    <div class="event-item-2">
                        <a href="{{ $item->url }}" target="_blank">
                            <img src="{{ $item->cover }}" class="img-responsive"/>
                        </a>
                        <div class="event-content padding">
                            <a href="{{ $item->url }}" target="_blank"><h5 style="font-weight:bold;">{{ $item->name }}</h5></a>
                            <small>
                                <i class="far fa-calendar-alt"></i> วันที่ : {{ $item->start_date }}
                                - {{ $item->end_date }}
                            </small>
                            <br>
                            <small>
                                <i class="fas fa-map-marker-alt"></i> สถานที่ : {{ $item->venue->name }}
                            </small>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @else
            <p class="text-center padding-lg"><i class="far fa-times-circle"></i> ยังไม่มีกิจกรรม</p>
            @endif
        </div>
    </div>
</div>


<!--pitching-->
<div class="row" style="min-height: 400px; background-image:url('{{ asset('frontend/medilab')}}/img/bg-1.jpg'); background-position:center;">
    <div class="container">
        <div class="container">
            <div class="row" style="padding-top: 20px; padding-bottom: 20px">
                <div class="col-sm-4">
                </div>
                <div class="col-sm-4" align="center">
                    <h2>Pitching Events</h2>
                </div>
                <div class="col-sm-4" align="right">
                    @if($all_pitch)
                    <button class="btn btn-default-2" onclick="allEvent($(this))" data-type="Pitching">
                        See All
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </button>
                    @endif
                </div>
            </div>
            @if(count($events_pitch)>0)
            <div class="row">
                @foreach($events_pitch as $item)
                <div class="col-xs-6 col-sm-6 col-md-4">
                    <div class="event-item-2">
                        <a href="{{ $item->url }}" target="_blank">
                            <img src="{{ $item->cover }}" class="img-responsive"/>
                        </a>
                        <div class="event-content padding">
                            <a href="{{ $item->url }}" target="_blank"><h5 style="font-weight:bold;">{{ $item->name }}</h5></a>
                            <small>
                                <i class="far fa-calendar-alt"></i> วันที่ : {{ $item->start_date }}
                                - {{ $item->end_date }}
                            </small>
                            <br>
                            <small>
                                <i class="fas fa-map-marker-alt"></i> สถานที่ : {{ $item->venue->name }}
                            </small>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @else
            <p class="text-center padding-lg"><i class="far fa-times-circle"></i> ยังไม่มีกิจกรรม</p>
            @endif
        </div>
    </div>
</div>


<!--general-->
<div class="row" style="min-height: 400px; background-image:url('{{ asset('frontend/medilab')}}/img/bg-1.jpg'); background-position:center;">
    <div class="container">
        <div class="container">
            <div class="row" style="padding-top: 20px; padding-bottom: 20px">
                <div class="col-sm-4">
                </div>
                <div class="col-sm-4" align="center">
                    <h2>General Events</h2>
                </div>
                <div class="col-sm-4" align="right">
                    @if(count($all_general)>4)
                    <button class="btn btn-default-2" onclick="allEvent($(this))" data-type="General">
                        See All
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </button>
                    @endif
                </div>
            </div>
            @if(count($events_general)>0)
            <div class="row">
                @foreach($events_general as $item)
                <div class="col-xs-6 col-sm-6 col-md-4">
                    <div class="event-item-2">
                        <a href="{{ $item->url }}" target="_blank">
                            <img src="{{ $item->cover }}" class="img-responsive"/>
                        </a>
                        <div class="event-content padding">
                            <a href="{{ $item->url }}" target="_blank"><h5 style="font-weight:bold;">{{ $item->name }}</h5></a>
                            <small>
                                <i class="far fa-calendar-alt"></i> วันที่ : {{ $item->start_date }}
                                - {{ $item->end_date }}
                            </small>
                            <br>
                            <small>
                                <i class="fas fa-map-marker-alt"></i> สถานที่ : {{ $item->venue->name }}
                            </small>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @else
            <p class="text-center padding-lg"><i class="far fa-times-circle"></i> ยังไม่มีกิจกรรม</p>
            @endif
        </div>
    </div>
</div>

<script type="text/javascript">
    function allEvent(type) {
        window.location.href = '{{ url('events/all') }}' + "?type=" + type.data('type');
    }
</script>