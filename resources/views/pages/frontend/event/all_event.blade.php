<div style="min-height: 1000px; background-image:url('{{ asset('frontend/medilab')}}/img/bg-1.jpg'); background-position:center;">
    <div class="row">
        <div class="container">
            <div class="container">
                <div class="row" style="padding-top: 20px; padding-bottom: 20px">
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-4" align="center">
                        <h2 style="margin-bottom:3px;">All {{$type}} Events ({{count($events)}})</h2>
                        <small>อีเว้นท์ที่กำลังมาถึง</small>
                    </div>
                    <div class="col-sm-4" align="right">
                    </div>
                </div>
                
                @if(count($events)>0)
                <div class="row row-eq-height">
                    @foreach($events as $item)
                    <div class="col-xs-6 col-sm-6 col-md-4">
                        <div class="event-item-2">
                            <a href="{{ $item->url }}" target="_blank">
                                <img src="{{ $item->cover }}" class="img-responsive"/>
                            </a>
                            <div class="event-content padding">
                                <a href="{{ $item->url }}" target="_blank"><h5 style="font-weight:bold;">{{ $item->name }}</h5></a>
                                <small>
                                    <i class="far fa-calendar-alt"></i> วันที่ : {{ $item->start_date }} - {{ $item->end_date }}
                                </small>
                                <br>
                                <small>
                                    <i class="fas fa-map-marker-alt"></i> สถานที่ : {{ $item->venue->name }}
                                </small>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @else
                <p class="text-center padding-lg white">ยังไม่มีกิจกรรม</p>
                @endif
            </div>
        </div>
    </div>
</div>