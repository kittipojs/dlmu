<style>

    @font-face {
        font-family: 'THSarabun';
        font-style: normal;
        font-weight: normal;
        src: url("{{ asset('custom/font/THSarabunNew/THSarabunNew.ttf') }}") format('truetype');
    }

    @font-face {
        font-family: 'THSarabunBold';
        font-style: normal;
        font-weight: bold;
        src: url("{{ asset('custom/font/THSarabunNew/THSarabunNew Bold.ttf') }}") format('truetype');
    }

    @font-face {
        font-family: 'THSarabunBoldItalic';
        font-style: italic;
        font-weight: normal;
        src: url("{{ asset('custom/font/THSarabunNew/THSarabunNew BoldItalic.ttf') }}") format('truetype');
    }

    @font-face {
        font-family: 'THSarabunItalic';
        font-style: italic;
        font-weight: bold;
        src: url("{{ asset('custom/font/THSarabunNew/THSarabunNew Italic.ttf') }}") format('truetype');
    }

    @page {
        size: A4 landscape;
        width: 100%;
        height: 100%!important;
        margin: 0;
    }

    #pageNumber {
        content: counter(page)
    }

    div.chapter, div.appendix {
        page-break-after: always;
    }

    .th-sarabun {
        font-family: 'THSarabun';
    }

    .a4 {
        font-size: 60px !important;
        position: relative;
        width: 100%;
        height: 100%!important;
        zoom: 30%;
    }

    @media print {
        .page-break {
            border: 50px solid #1b4a5a!important;
            page-break-after: always;
        }
    }


</style>

@php
    $page = ceil(count($certs)/4);
    $certListPerPage = [];
    $certList = [];
       foreach($certs as $indexKey => $item){
            $certList[] = $item;
            if(count($certList) == 4 || $indexKey == ( count($certs) - 1 ) ){
               $certListPerPage[] = $certList;
               $certList = [];
            }
       }

@endphp

@for($i = 0 ; $i < $page ; $i++)
    <div class="th-sarabun a4 page-break">
        <div class="text-center">
            <img src="{{url('frontend/medilab/img/print-logo.png')}}" width="650px" style="margin-top: 5px"/>
            <div style="font-size: 100px; margin-top:80px; font-weight:bold; color:#00507f;">
                สำนักงานนวัตกรรมแห่งชาติ
                (NIA)
            </div>

            <h1 style="font-size: 80px; margin-top:80px;">ประกาศนียบัตรนี้ให้เพื่อแสดงว่า</h1>

            <h1 style="font-size: 120px; margin-top:100px;">{{$user->name_title}} {{$user->first_name}} {{$user->last_name}}</h1>

            <h1 style="font-size: 80px; margin-top:80px;">ได้มีส่วนร่วมกับทางสำนักงานนวัตกรรมแห่งชาติ ดังนี้</h1>
        </div>
        <div class="row" style="margin-top:100px; min-height: 985px">
            @foreach($certListPerPage[$i] as $indexKey => $item)

                <div class="col-xs-offset-4 col-xs-8" style="padding-top:60px;">
                    @if($item['item_type']=='event')
                        <p>การเข้าร่วมกิจกรรม <u>ประเภท</u> {{$item['type']}} <u>ชื่อกิจกรรม</u> {{$item['name']}}
                        </p>
                        <p style="margin-top:20px;">วันที่เข้าร่วม {{$item['start_date']}}
                            ถึง {{$item['end_date']}}</p>
                    @endif

                    @if($item['item_type']=='course')
                        <p>การเรียนออนไลน์ <u>ชื่อหลักสูตร</u> {{$item['name']}}</p>
                        <p style="margin-top:20px;">วันที่ผ่านหลักสูตร {{$item['end_date']}}</p>
                    @endif
                </div>
            @endforeach
        </div>

        <div style=" margin-top:150px;">
            <div class="text-center">
                ประกาศนียบัตรฉบับนี้ถูกสร้างขึ้นโดยระบบอัตโนมัติของสำนักนวัตกรรมแห่งชาติ (NIA)
            </div>
        </div>
        <div style=" margin-top:20px;">
            <div class="text-center">
                สอบถามข้อมูลเพิ่มเติมกรุณาติดต่อ 02 017 5555 อีเมล์ : startup@nia.or.th
            </div>
        </div>
        <div style=" margin-top:20px;">
            <div class="text-center">
                {{ url('') }}
            </div>
        </div>
    </div>
@endfor