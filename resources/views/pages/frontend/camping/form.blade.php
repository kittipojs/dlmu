<div class="container" style="padding-top: 20px">
    <h1 class="text-center padding">
        สมัครเข้าร่วม U League Camping
    </h1>
    <div class="padding"></div>
    <form class="form-horizontal" action="{{ url('user/camp/register') }}" method="post" role="form"
          enctype="multipart/form-data">

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-4" style="padding-top: 7px">
                <a href=""><i class="fa fa-file-pdf-o" aria-hidden="true"></i> ดาวน์โหลดตารางอบรมค่าย</a>
            </div>
        </div>

        <input type="hidden" value="{{ $user->id }}" name="user_id">
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">ชื่อ - นามสกุล *</label>
            <div class="col-sm-5">
                <input type="text" name="firstname" class="form-control" id="name" placeholder="ชื่อ"
                       value="{{ $user->first_name }}" readonly>
            </div>
            <div class="col-sm-5">
                <input type="text" name="lastname" class="form-control" id="name" placeholder="นามสกุล"
                       value="{{ $user->last_name }}" readonly>
            </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">อีเมลแอดเดรส *</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="email" placeholder="Email"
                       value="{{ $user->email }}" readonly>
            </div>
        </div>

        <div class="form-group">
            <label for="nickname" class="col-sm-2 control-label">ชื่อเล่น *</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="nickname" name="nickname" placeholder="ชื่อเล่นของน้อง">
            </div>
        </div>

        <div class="form-group">
            <label for="university" class="col-sm-2 control-label">มหาวิทยาลัย *</label>
            <div class="col-sm-10">
                <select class="form-control" id="university" name="university" required></select>
            </div>
        </div>

        <div class="form-group">
            <label for="faculty" class="col-sm-2 control-label">คณะ *</label>
            <div class="col-sm-10">
                <select class="form-control" id="faculty" name="faculty"></select>
            </div>
        </div>

        <div class="form-group">
            <label for="year" class="col-sm-2 control-label">ชั้นปี *</label>
            <div class="col-sm-5">
                <select class="form-control" id="year" name="year" required>
                    <option value="-1">เลือกชั้นปีที่ศึกษาอยู่</option>
                    <option value="1">ปีที่ 1</option>
                    <option value="2">ปีที่ 2</option>
                    <option value="3">ปีที่ 3</option>
                    <option value="4">ปีที่ 4</option>
                </select>
            </div>
        </div>

        <div class="form-group">

            <label for="image" class="col-sm-2 control-label">อัพโหลดบัตรนักศึกษา *<br>
                <small>(ไฟล์ .jpg, .png เท่านั้น)</small>
            </label>
            <div class="col-sm-5">
                <input type="file" name="image" id="image" class="form-control" data-preview-file-type="text"
                       data-upload-url="#"/>
            </div>
        </div>

        <div class="form-group">
            <label for="room" class="col-sm-2 control-label">ต้องการให้ NIA หาที่พักให้หรือไม่? *</label>
            <div class="col-sm-10" style="padding-top:10px;">
                <input type="radio" name="room" id="room" value="0" required>
                <label>&nbsp;ต้องการ</label>
                &nbsp;&nbsp;&nbsp;
                <input type="radio" name="room" id="room" value="1" checked required>
                <label>&nbsp;ไม่ต้องการ</label>
            </div>
        </div>

        <div class="form-group">
            <label for="round" class="col-sm-2 control-label">รอบการสมัคร *</label>
            <div class="col-sm-10">
                <div class="radio">
                    <p style="color:red;">รับสมัครเพียงรอบละ 150 คน เท่านั้น</p>
                </div>
                @foreach( $rounds as $round)
                <div class="radio">
                    <label>
                        @if($round_count[$round->round_id] >= 150)
                        <input type="radio" name="round" id="round" value="{{ $round->round_id }}"
                               disabled required>
                        <p>{{ $round->round_title }} ( เต็ม )</p>
                        @elseif($round->apply_before <= date('Y-m-d'))
                                                            <input type="radio" name="round" id="round" value="{{ $round->round_id }}"
                        disabled required>
                        <p>{{ $round->round_title }} ( หมดเวลาสมัคร )</p>
                        @else
                        <input type="radio" name="round" id="round" value="{{ $round->round_id }}"
                               required>
                        <p>{{ $round->round_title }} (
                            จำนวนผู้สมัครขณะนี้ {!! $round_count[$round->round_id] !!} คน )</p>
                        @endif
                    </label>
                </div>
                @endforeach
            </div>
        </div>
        <div class="padding"></div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-5">
                <button type="submit" class="btn btn-success btn-default">สมัคร</button>
                <button type="reset" class="btn btn-link btn-default">รีเซท</button>
            </div>
        </div>
    </form>
    <div class="padding"></div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#university").jCombo("{!! url('select/university') !!}");
        $("#faculty").jCombo("{!! url('select/faculty') !!}");
    })
</script>