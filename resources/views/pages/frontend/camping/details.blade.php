<div class="container">
    @if($row === null)
    <div align="center">
        <h4>ยังไม่ได้สมัคร Startup Camping</h4>
        <a class="btn btn-default" href="{{ url('uleague/camping') }}">
            สมัครเข้าร่วม Startup League Camping
        </a>
    </div>
    @else
    <div class="header">
        <h4> สถานะการสมัคร startup camping</h4>
    </div>
    <div class="divider"></div>
    <div class="padding"></div>
    <div class="row">
        <div class="col-sm-4" align="center">
            <img src="{{ asset( $row->photo_src ) }}" alt="" class="img-thumbnail">
        </div>
        <div class="col-sm-6">
            <form class="form-vertical">

                <div class="form-group has-feedback">
                    <span><i class="fa fa-user prefix grey-text"></i> {{ $row->first_name }} {{ $row->last_name }}</span>
                </div>

                <div class="form-group has-feedback">
                    <span><i class="fa fa-envelope prefix grey-text"></i> {{ $row->email }}</span>
                </div>

                <div class="form-group has-feedback">
                    <span><i class="fa fa-university prefix grey-text"></i> {{ $row->name }} {{ $row->faculty_name }}</span>
                </div>

                <div class="form-group has-feedback">
                    <span><i class="fa fa-info-circle prefix grey-text"></i> รอบที่สมัคร {{ $row->round_title }}</span>
                </div>

            </form>
        </div>
        <div class="col-sm-2">
        </div>
    </div>
    @endif
</div>