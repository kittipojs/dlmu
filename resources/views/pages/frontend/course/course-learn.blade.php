
<div style="background-image:url('{{ asset('frontend/medilab')}}/img/bg-1.jpg'); background-position:center;">
    <div class="container">
        <div class="padding-lg"></div>
        <h2>{{ $course->course_title }}</h2>
        <div class="padding"></div>
        <div class="row">
            <div class="card course">
                @include('pages.frontend.course.layout-header')
                <div class="tab-content course">
                    <div role="tabpanel" class="tab-pane course active" id="learn">
                        <div class="row">
                            <div class="col-sm-3">
                                <!-- START TIMELINE -->
                                @include('pages.frontend.course.layout-timeline')
                                <!-- END TIMELINE -->
                            </div>

                            @if(!$welcome)
                            @if(!$is_eligible)
                            <div class="col-sm-9 main-content border-left-lg" style="min-height:600px;">
                                <p class="text-center">คุณยังไม่มีสิทธิ์เรียนบทเรียนนี้</p>
                            </div>
                            @else

                            @if($content_type=='lecture')
                            <!-- START LECTURE -->
                            @include('pages.frontend.course.content-lecture')
                            <!-- END LECTURE -->

                            @elseif($content_type=='quiz')
                            <!-- START QUIZ -->
                            @include('pages.frontend.course.content-quiz')
                            <!-- END QUIZ -->
                            @endif

                            @endif
                            @else
                            <div class="col-sm-9 main-content text-center" style="min-height:600px; border-left: 1px solid #cecece">
                                <p>ยินดีต้อนรับเข้าสู่หลักสูตร</p>
                                <h3>{{ $course->course_title }}</h3>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    var course_id = '{{ $course->course_id }}';

    $(document).ready(function () {

        @if($welcome)
        {{-- @if($content_object->enable_progress)
          /*
            var startTime = new Date().valueOf();

            setInterval(function(){
                var loadedSeconds = (new Date().valueOf() - startTime) / 1000;
                console.log(loadedSeconds)
                $.post('{{url('api/csession/update-progress')}}', {'id' : '', 'duration': ''});
            }, 5000);
            */
          @endif --}}
        @endif

    });
</script>

