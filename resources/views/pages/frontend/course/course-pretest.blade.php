<div style="background-image:url('{{ asset('frontend/medilab')}}/img/bg-1.jpg'); background-position:center;">
    <div class="container">
        <div class="padding-lg"></div>
        <h2>{{ $course->course_title }}</h2>
        <div class="padding"></div>
        <div class="row">
            <div class="card course">
                @include('pages.frontend.course.layout-header')
                <div class="tab-content course">
                    <div role="tabpanel" class="tab-pane course active" id="learn">
                        <div class="row">
                            <div class="col-sm-12">
                                @include('pages.frontend.course.content-pretest')
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    var course_id = '{{ $course->course_id }}';

    $(document).ready(function () {

    });
</script>

