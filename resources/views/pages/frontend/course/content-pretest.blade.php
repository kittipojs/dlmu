<link rel="stylesheet" type="text/css" href="{{ asset('custom/wizard/custom.wizard.css')}}">

<div class="col-sm-12" style="min-height:600px;">
    <div id="quizWelcome">

        <h2>{{ $quiz->quiz_title }}</h2>
        <br>

        <h3>
            <strong><u>คำชี้แจง</u></strong>
        </h3>
        <h3>{{ $quiz->quiz_description }}</h3>
        <br>

        <h3>
            <strong>จำนวนคำถาม</strong> {{ count($questions) }} ข้อ
        </h3>
        <hr>

        <h3>
            <strong>เวลาในการทำข้อสอบ</strong> {{ $quiz->quiz_timer }} นาที
        </h3>
        <hr>

        <div>
            @if(count($questions) > 0)
                <button type="button" class="btn btn-lg btn-success" onclick="startQuiz()">เริ่มทำแบบทดสอบ</button>
            @else
                <button type="button" class="btn btn-lg btn-warning success">เริ่มแบบทดสอบ</button>
            @endif
        </div>

    </div>
    <div id="quizDo" style="display:none;">
        <form action="{{ url('c/pretest/save') }}" method="post" id="quizForm">
            <input type="hidden" id="quiz_id" name="quiz_id" value="{{ $quiz->quiz_id }}">
            {{-- <input type="hidden" id="object_id" name="object_id" value="{{$content_object->object_id}}"> --}}
            <input type="hidden" id="course_id" name="object_id" value="{{$course->course_id}}">
            <div class="">
                <div class="sbox" id="quiz_area">
                    <div class="sbox-title clearfix">
                        <div class="col-xs-6">
                            <h3> {{ $quiz->quiz_title }} </h3>
                        </div>
                        <div class="col-xs-6">
                            <div class="countdown pull-right"></div>
                        </div>
                    </div>
                    <hr>
                    <div class="sbox-content">
                        <div style="min-height: 500px">
                            <ul class="nav nav-tabs hide">
                                @foreach($questions as $question)
                                    <li class="{{ ($question->active)? 'active' : '' }}">
                                        <a href="#tab{{$question->question_id}}" data-toggle="tab">
                                            {{$question->question_number}}
                                        </a>
                                    </li>
                                @endforeach
                                <li>
                                    <a href="#tab99" data-toggle="tab">
                                        99
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                @foreach($questions as $question)
                                    <div class="tab-pane {{ ($question->active)? 'active' : '' }} quiz"
                                         id="tab{{$question->question_id}}">
                                        <h3>
                                            <strong>
                                                ข้อคำถามที่ {{ $loop->index + 1 }}
                                                : {{ $question->question_title }}</strong>
                                        </h3>
                                        <div class="padding"></div>
                                        <div class="padding"></div>
                                        <div class="">
                                            @foreach($question->choices as $choice)
                                                <div class="radio radio-primary">
                                                    <h3>
                                                        <input type="radio" name="choice[{{$question->question_id}}]"
                                                               id="choice[{{$loop->index}}]"
                                                               value="{{ $choice->name }}"
                                                               data-id="{{$question->question_id}}"
                                                               onclick="checkedPaginate($(this))">
                                                        <label for="choice[{{$loop->index}}]">
                                                            {{ $choice->name }}
                                                        </label>
                                                    </h3>
                                                </div>
                                                <hr>
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                                <div class="tab-pane quiz text-center" id="tab99">
                                    <h4>
                                        <strong>สิ้นสุดการตอบคำถาม</strong>
                                    </h4>
                                    <div>
                                        <button type="submit" class="btn btn-success">ส่งคำตอบ</button>
                                        
                                        <div class="padding"></div>
                                        <div class="padding"></div>
                                        <div class="padding"></div>
                                        
                                        <div>
                                            <strong>
                                                กดข้อก่อนหน้าเพื่อตรวจเช็คคำตอบ
                                            </strong>
                                            &nbsp;หรือ&nbsp;
                                            <strong>
                                                กดส่งคำตอบเพื่อคำนวณคะแนน
                                            </strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="text-center">
                        <a class="btn btn-secondary btnPrevious" disabled><i class="fas fa-arrow-left"></i> ข้อก่อนหน้า</a>
                        <a class="btn btn-warning btnNext"> ข้อถัดไป <i class="fas fa-arrow-right"></i></a>
                    </div>

                    <div class="text-center">
                        <ul class="pagination" role="menubar" aria-label="Pagination">
                            @foreach($questions as $question)
                                <li id="page{{$question->question_id}}">
                                    <a href="#tab{{$question->question_id}}" data-toggle="tab">
                                        {{$loop->index + 1}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="sbox" id="result_area"></div>
            </div>
        </form>
    </div>
    <div id="quizResult" class="text-center" style="display:none;">
        <div id="result_status">

        </div>
        <div class="padding"></div>
        <div class="text-center">
            <a href="{{url('course/'.$course->course_id.'/learn')}}" class="btn btn-primary">เริ่มเรียนต่อ</a>
        </div>
    </div>
</div>

<div class="modal" id="nia-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog modal- vertical-align-center" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal-header"></h4>
                </div>
                <div class="modal-body" id="modal-info">
                    <!-- Ajax data -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close">
                        ตกลง
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var course_id = '{{$course->course_id}}',
        timerId;

    $(document).ready(function () {

        var total_quiz = ($('.tab-content').find('.tab-pane.quiz').length) - 1,
            setPage = $('.btnNext,.btnPrevious'),
            next = $('.btnNext'),
            previous = $('.btnPrevious');

        previous.click(function () {
            $('.nav-tabs > .active').prev('li').find('a').trigger('click');
        });

        next.click(function () {
            $('.nav-tabs > .active').next('li').find('a').trigger('click');
        });

        setPage.click(function () {
            var current_quiz = $('.tab-content').find('.tab-pane.active.quiz').index();

            if (current_quiz === 0) {
                previous.attr('disabled', true);
            }
            else {
                previous.attr('disabled', false);
            }

            if (current_quiz === total_quiz) {
                next.attr('disabled', true);
            }
            else {
                next.attr('disabled', false);
            }
        });

        $("#quizForm").submit(function (event) {

            var totalAnswer = $("input:checked").length;

            if (totalAnswer < total_quiz) {
                $('#nia-modal').modal('show');
                $('#modal-header').html('ไม่สามารถส่งคำตอบได้');
                $('#modal-info').html('โปรดตอบคำถามให้ครบทุกข้อ');
                return false
            }

            var confirm_submit = confirm("คุณต้องการส่งคำตอบ ?");
            if (confirm_submit === true) {
                $.ajax({
                    type: "POST",
                    url: $(this).attr('action'),
                    data: $(this).serialize(), // serializes the form's elements.
                    success: function (data) {
                        clearInterval(timerId);

                        $.toast({
                            heading: data.success,
                            text: data.message,
                            position: 'bottom-right',
                            icon: data.success,
                            hideAfter: 2500,
                            stack: 6
                        });

                        $('#quizDo').hide();
                        $('#quizResult').show();
                        $('#result_status').html(`<h4>ผลสอบ : <span class='label label-${data.result_class}'>${data.result_status}</span></h4>
                                                  <div class="padding"></div>
                                                  <h4>คะแนนที่ได้ : ${data.score} (${data.percentage} %)</h4>`)

                    }
                });
            } else {
                return false
            }


            // $(this).submit();
            event.preventDefault();
        });


    });

    function startQuiz() {
        startTimer()
    }

    function startTimer() {

        $('#quizWelcome').hide();
        $('#quizDo').show();

        var quizTime = parseInt('{{ $quiz->quiz_timer }}');
        // var quizTime = 0.1;//cd 5 sec for test
        var countdown = quizTime * 60 * 1000;

        timerId = setInterval(function () {
            countdown -= 1000;
            var min = Math.floor(countdown / (60 * 1000));
            var sec = Math.floor((countdown - (min * 60 * 1000)) / 1000);

            if (countdown < 0) {
                clearInterval(timerId);
                $('#nia-modal').modal('show');
                $('#modal-header').html('หมดเวลาตอบคำถาม');
                $('#modal-info').html('content');

                $('#nia-modal').on('hidden.bs.modal', function () {
                    window.location.href = '{{ url('course') }}/' + course_id + '/pre-test';
                })
                //doSomething();
            } else {
                $(".countdown").html("<h3><i class=\"fas fa-clock\"></i> " + pad(min, 2) + " : " + pad(sec, 2) + "</h3>");
            }

        }, 1000); //1000ms. = 1sec.


    }

    function checkedPaginate(ele) {
        if (ele.is(':checked')) {
            $('#page' + ele.data('id')).find('a').css("background-color", "black");
        }
    }

    function pad(str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }
</script>