<div style="background-image:url('{{ asset('frontend/medilab')}}/img/bg-1.jpg'); background-position:center;">
    <div class="container">

        <div class="padding-lg"></div>
        @if($course->course_status!=1)
        <div class="alert alert-danger text-center" role="alert">
            หลักสูตรนี้ยังไม่เปิดให้เข้าเรียน เฉพาะผู้ดูแลระบบเท่านั้นที่เห็นหน้านี้
        </div>
        @endif

        <h2>{{ $course->course_title }}</h2>
        <div class="padding"></div>
        <div class="row">
            <!-- Nav tabs -->
            <div class="card course">

                <ul class="nav nav-tabs course" role="tablist">
                    <li class="active">
                        <a href="{{url('/c').'/'.$course->course_id}}" aria-controls="home">รายละเอียดหลักสูตร</a>
                    </li>
                    <li role="presentation">
                        <a href="#" class="first-enroll-btn" aria-controls="learn">เข้าเรียน</a>
                    </li>
                    @if(!$free_course)
                    <li>
                        <a href="{{url('/course').'/'.$course->course_id.'/result'}}" aria-controls="result">ผลการเรียน</a>
                    </li>
                    @endif
                </ul>

                <div class="tab-content course">
                    <div role="tabpanel" class="tab-pane  course active" id="home">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-offset-2 col-sm-8">
                                    <div class="">
                                        @if($course->course_preview_type=='mp4')
                                        <div id="player"></div>
                                        @elseif($course->course_preview_type=='youtube')
                                        <iframe id="video" src="https://www.youtube.com/embed/{{ $course->course_preview }}" frameborder="0" allow="autoplay; encrypted-media" style="width: 100%!important;" allowfullscreen></iframe>
                                        @else
                                        <img src="{{asset($course->course_thumbnail)}}" class="img-responsive" />
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="padding"></div>
                            <div class="row" style="border: 1px solid #cecece">
                                <div class="col-sm-3 custom">
                                    <i class="fas fa-user fa-3x pull-left blue-color"></i>
                                    <div class="text">
                                        <h4 class="cust-margin blue-color">วิทยากร</h4>
                                        {{ $course->course_instructor }}
                                    </div>
                                </div>
                                <div class="col-sm-3 custom">
                                    <i class="fas fa-dollar-sign fa-3x pull-left blue-color"></i>
                                    <div class="text">
                                        <h4 class="cust-margin blue-color">ค่าลงทะเบียน</h4>
                                        ฟรี
                                    </div>
                                </div>
                                <div class="col-sm-3 text-center custom" style="margin-top:-4px;">
                                    <div class="fb-share-button" data-href="http://www.startupthailandacademy.org/c/{{ $course->course_id }}" data-layout="box_count" data-size="large" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http://www.startupthailandacademy.org/c/{{ $course->course_id }}" class="fb-xfbml-parse-ignore">Share</a></div>
                                    <!--
<i class="far fa-clock fa-3x pull-left blue-color"></i>
<div class="text">
<h4 class="cust-margin blue-color">ระยะเวลารวม</h4>
{{-- 163 นาที --}}
</div>
-->
                                </div>
                                <div class="col-sm-3 custom">
                                    @if(!$has_session)
                                    <button class="btn btn-primary btn-lg btn-block first-enroll-btn">
                                        ลงทะเบียนเรียน
                                    </button>
                                    @else
                                    <a href="{{url('/course/'.$course->course_id.'/learn')}}" class="btn btn-success btn-lg btn-block">
                                        เข้าเรียนต่อ
                                    </a>
                                    @endif
                                </div>
                            </div>
                            <div class="padding"></div>
                            <div class="row">
                                <div class="col-sm-12" id="login">
                                    <div class="comment-tabs">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li class="active">
                                                <a href="#course-info" role="tab" data-toggle="tab">
                                                    <h5 class="reviews text-capitalize">รายละเอียดหลักสูตร</h5>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#curriculum-disabled" role="tab" data-toggle="tab">
                                                    <h5 class="reviews text-capitalize">โครงสร้างหลักสูตร</h5>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="course-info">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="section">
                                                            <h4 class="blue-color">
                                                                <i class="far fa-file-alt"></i>
                                                                คำอธิบายหลักสูตร
                                                            </h4>
                                                            <div class="detail">
                                                                {{ $course->course_intro }}
                                                            </div>
                                                        </div>

                                                        <div class="section">
                                                            <h4 class="blue-color">
                                                                <i class="far fa-user"></i>
                                                                หลักสูตรนี้เหมาะสำหรับ
                                                            </h4>
                                                            <div class="detail">{{ $course->course_for_text }}</div>
                                                        </div>

                                                        @if($course->course_prerequisite_id!=null)
                                                        <div class="section">
                                                            <h4 class="blue-color">
                                                                <i class="fas fa-graduation-cap"></i>
                                                                หลักสูตรแนะนำก่อนเข้าเรียน
                                                            </h4>
                                                            <div class="detail">
                                                                {{ $course_prerequisite->course_title }}
                                                            </div>
                                                        </div>
                                                        @endif

                                                        <div class="section">
                                                            <h4 class="blue-color">
                                                                <i class="fas fa-align-left"></i>
                                                                รายละเอียดเพิ่มเติม
                                                            </h4>
                                                            <div class="detail">{!! $course->course_detail !!}</div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="section">
                                                            <h4 class=" blue-color">
                                                                <i class="fas fa-flag-checkered"></i>
                                                                วัตถุประสงค์
                                                            </h4>
                                                            <div class="detail" id="objective_area"></div>
                                                        </div>

                                                        <div class="section">
                                                            <h4 class="blue-color">
                                                                <i class="far fa-check-circle"></i>
                                                                ระดับเนื้อหา
                                                            </h4>
                                                            <div class="detail">
                                                                @if($course->course_level==1)
                                                                ระดับเริ่มต้น (Beginner)
                                                                @elseif($course->course_level==2)
                                                                ระดับกลาง (Intermediate)
                                                                @elseif($course->course_level==3)
                                                                ระดับสูง (Advance)
                                                                @endif
                                                            </div>
                                                        </div>
                                                        @if(!$free_course)
                                                        <div class="section">
                                                            <h4 class="blue-color">
                                                                <i class="fas fa-certificate"></i>
                                                                การรับวุฒิบัตร
                                                            </h4>
                                                            <div class="detail">เข้าเรียนไม่น้อยกว่า {{ $course->percentage_lecture }}% ของเวลาเรียนทั้งหมด และสอบผ่านไม่น้อยกว่า {{ $course->percentage_quiz }}%</div>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="curriculum-disabled">
                                                <div id="timeline">
                                                    @if($course->pre_test_id != null)
                                                    <div class="timeline-item">
                                                        <div class="timeline-icon">
                                                            <i class="fas fa-edit"></i>
                                                        </div>
                                                        <div class="timeline-content right">
                                                            <a href="#">
                                                                <h2>แบบทดสอบก่อนเรียน</h2>
                                                                <p>&nbsp;</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @foreach( $curriculum as $item)
                                                    <div class="timeline-item">
                                                        <div class="timeline-icon">
                                                            <i class="fas fa-play"></i>
                                                        </div>
                                                        <div class="timeline-content right">
                                                            <a href="#">
                                                                <h2>บทที่ {{ $item['object_order'] }}</h2>
                                                                <p>{{ $item['title'] }}</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                    @if($course->post_test_id != null)
                                                    <div class="timeline-item" style="padding-top: 19px;">
                                                        <div class="timeline-icon">
                                                            <i class="fas fa-edit"></i>
                                                        </div>
                                                        <div class="timeline-content right">
                                                            <a href="#">
                                                                <h2>แบบทดสอบหลังเรียน</h2>
                                                                <p>&nbsp;</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane course" id="learn"></div>
                    <div role="tabpanel" class="tab-pane course" id="result"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="enrollCourseModal" class="modal modal-center fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ยืนยันการเข้าเรียน</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-center">คุณต้องการเข้าเรียนหลักสูตร <strong>{{ $course->course_title }}</strong> ใช่หรือไม่?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                <a href="{{url('/course').'/'.$course->course_id.'/learn'}}" class="btn btn-secondary">ยืนยัน</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var course_id = '{{ $course->course_id }}',
        learn_area = $("#learn"),
        result_area = $("#result");

    $(document).ready(function () {
        appendObjective()
        /*
        var playerInstance = jwplayer("player").setup({
            "file": "{{asset($course->course_preview)}}",
            aspectratio: "16:9",
            controls: true, 
            width: "100%",
            autostart: "false"
        });
        */      
        $('.first-enroll-btn').click(function(){
            $('#enrollCourseModal').modal('show')
        })
    });

    function learnThisCourse() {

        //TODO CHECK USER PERMISSION

        $.ajax({
            type: 'GET',
            url: "{{ url('/course-session/check/') }}/{{$course->course_id}}",
            success: function (data) {
                if(!data.success){
                    if(data.code==101){
                        alert('ยังไม่ได้ลงทะเบียนเรียนหลักสูตรนี้')
                    }
                }
                else{
                    alert(data.session)
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(JSON.stringify(textStatus))
            }
        });

        /*
        var data_area = learn_area,
            url;
        url = "{{ url('c') }}" + "/" + course_id + "/learn";
        if( !$.trim( $(data_area).html() ).length ) {
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });
        }
        */
    }

    function viewCourseResult() {

        var data_area = result_area,
            url;
        url = "{{ url('c') }}" + "/" + course_id + "/result";
        if( !$.trim( $(data_area).html() ).length ) {
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });
        }
    }

    {{--
        @if(!$has_session)
      function applyThisCourse(){

          var formData = { 'course_id' : '{{ $course->course_id }}', 'previous':'1522306444897' };

          $.ajax({
              type        : 'POST',
              url         : '{{ url('/csession/create') }}',
              data        : formData,
              dataType    : 'json',
              encode      : true
          }).done(function(data) {
              if(data.success){
                  window.location = data.url;
              }
              else{
                  alert(data.message)
              }
          });
      }
      @endif
      --}}

     function appendObjective(){

         var course_objective = JSON.parse('{{ $course->course_objective }}'.replace(/&quot;/g, '"'));

         $.each(course_objective, function (index, value) {

             $('#objective_area').append('<li>'+value+'</li>');
         });
     }
</script>

