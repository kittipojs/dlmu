<div style="background-image:url('{{ asset('frontend/medilab')}}/img/bg-1.jpg'); background-position:center;">
    <div class="container">
        <div class="padding-lg"></div>
        <h2>{{ $course->course_title }}</h2>
        <div class="padding"></div>
        <div class="row">
            <div class="card course">

                <ul class="nav nav-tabs course" role="tablist">
                    <li>
                        <a href="{{url('/c').'/'.$course->course_id}}" aria-controls="home">รายละเอียดหลักสูตร</a>
                    </li>
                    <li class="active">
                        <a href="{{url('/course').'/'.$course->course_id.'/learn'}}" aria-controls="learn" role="tab"
                           data-toggle="tab">เข้าเรียน</a>
                    </li>
                    <li>
                        <a href="{{url('/course').'/'.$course->course_id.'/result'}}"
                           aria-controls="result">ผลการเรียน</a>
                    </li>
                </ul>

                <div class="tab-content course">
                    <div role="tabpanel" class="tab-pane course active" id="learn">
                        
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

</script>

