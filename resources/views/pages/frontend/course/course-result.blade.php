<style>
    .alignleft {
        float: left;
    }

    .alignright {
        float: right;
    }
    .table > tbody > tr > td{
        vertical-align: center;
    }
</style>

<div style="background-image:url('{{ asset('frontend/medilab')}}/img/bg-1.jpg'); background-position:center;">
    <div class="container">
        <div class="padding-lg"></div>
        <h2>
            {{ $course->course_title }}
        </h2>
        <div class="padding"></div>
        <div class="row">
            <div class="card course">
                <ul class="nav nav-tabs course" role="tablist">
                    <li>
                        <a href="{{url('/c').'/'.$course->course_id}}" aria-controls="home">รายละเอียดหลักสูตร</a>
                    </li>
                    <li>
                        <a href="{{url('/course').'/'.$course->course_id.'/learn'}}" aria-controls="learn">เข้าเรียน</a>
                    </li>
                    <li class="active">
                        <a href="{{url('/course').'/'.$course->course_id.'/result'}}" aria-controls="result">ผลการเรียน</a>
                    </li>
                </ul>

                <div class="tab-content course">
                    <div role="tabpanel" class="tab-pane course active" id="result">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <h3>
                                        สถานะการเรียน <span style="color:#2a7294;">คุณ{{\Auth::user()->first_name}} {{\Auth::user()->last_name}}</span>
                                        @if($graduated['lecture'] && $graduated['quiz'])
                                         <span class="badge badge-success">ผ่านแล้ว</span>
                                        @else
                                        <span class="badge badge-warning">ยังไม่ผ่าน</span>
                                        @endif
                                    </h3>
                                </div>
                                <div class="col-sm-6"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="padding-lg"></div>
                                <div class="col-sm-6">
                                    <div class="panel-default">
                                        <div class="panel-header text-center">การเข้าเรียน</div>
                                        <div class="panel-body">
                                            <div class="">
                                                <div id="circle-participate"></div>
                                                <p class="text-center"> {{$user_progress_percentage}}% </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="panel-default">
                                        <div class="panel-header text-center">
                                            แบบทดสอบเพื่อวัดความรู้(Examination)
                                        </div>
                                        @if($course->pre_test_id==null || $post_test==null)
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="text-center">
                                                    บทเรียนนี้ไม่มีแบบทดสอบ
                                                </div>
                                            </div>
                                        </div>
                                        @else
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="">
                                                    <div id="circle-2"></div>
                                                    <p class="text-center" style="color:#2a7294;"> 
                                                        {{ ($post_test->percentage/100)*$post_test->score }}/{{$post_test->score}}  คะแนน 
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-12" style="padding-top: 20px">

                                    <div class="panel-default">
                                        <div class="panel-header text-center">
                                            แบบทดสอบก่อนเรียนและหลังเรียน(Pre-Test และ Post-Test)
                                        </div>
                                        @if($course->pre_test_id==null && $course->post_test_id==null)
                                        <div class="panel-body text-center">
                                            หลักสูตรเรียนนี้ไม่มีแบบทดสอบ
                                        </div>
                                        @else
                                        <div class="panel-body">
                                            
                                            @if($pre_test==null)
                                            <p class="padding text-center">ยังไม่ได้ทำแบบทดสอบหลังเรียน</p>
                                            @endif
                                            @if($course->pre_test_id!=null && $pre_test!=null)
                                            <div class="row">
                                                <div id="textbox">
                                                    <p class="alignleft">Pre-Test</p>
                                                    <p class="alignright">{{ ($pre_test->percentage/100)*$pre_test->score }}/{{$pre_test->score}} คะแนน</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="{{$pre_test->percentage}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$pre_test->percentage}}%;">
                                                        <span class="sr-only">{{$pre_test->percentage}}% Complete</span>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif

                                            <hr />
                                            
                                            @if($post_test==null)
                                            <p class="padding text-center">ยังไม่ได้ทำแบบทดสอบหลังเรียน</p>
                                            @endif
                                            @if($course->post_test_id!=null && $post_test!=null)
                                            <div class="row">
                                                <div id="textbox">
                                                    <p class="alignleft">Post-Test</p>
                                                    <p class="alignright">{{ ($post_test->percentage/100)*$post_test->score }}/{{$post_test->score}} คะแนน</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="{{$post_test->percentage}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$pre_test->percentage}}%;">
                                                        <span class="sr-only">{{$post_test->percentage}}% Complete</span>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-6">
                                <div class="padding-lg"></div>

                                <div class="col-sm-12">

                                    <div class="cirriculum-table">

                                        @if($course->pre_test_id!=null)
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <h6 style="font-weight:bold;">แบบทดสอบก่อนเรียน (Pre Test)</h6>
                                            </div>
                                            <div class="col-xs-4">

                                            </div>
                                        </div>
                                        @endif

                                        @foreach($curriculum as $index => $item)
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <h6 style="font-weight:bold;">บทที่ {{$index+1}} {{ $item['title'] }}</h6>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="pull-right">
                                                    @if(isset($item['session']))
                                                    {{$item['session']->progress}}%
                                                    @else
                                                    0%
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach

                                        @if($course->post_test_id!=null)
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <h6 style="font-weight:bold;">แบบทดสอบหลังเรียน (Post Test)</h6>
                                            </div>
                                            <div class="col-xs-4">

                                            </div>
                                        </div>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $( document ).ready(function() { // 6,32 5,38 2,34
        
        $("#circle-participate").circliful({
            animationStep: 5,
            foregroundBorderWidth: 10,
            backgroundBorderWidth: 10,
            percent: {{$user_progress_percentage}} });

        @if($post_test)
        $("#circle-2").circliful({
            animationStep: 5,
            foregroundBorderWidth: 10,
            backgroundBorderWidth: 10,
            percent: {{$post_test->percentage}} });
        @endif

        $("#circle-3").circliful({
            animationStep: 5,
            foregroundBorderWidth: 15,
            backgroundBorderWidth: 15,
            percent: 100,
            percentageTextSize: 0,
        });

        $("#circle-4").circliful({
            animationStep: 5,
            foregroundBorderWidth: 15,
            backgroundBorderWidth: 15,
            percent: 100,
            percentageTextSize: 0,
        });

        $("#circle-5").circliful({
            animationStep: 5,
            foregroundBorderWidth: 15,
            backgroundBorderWidth: 15,
            percent: 100,
            percentageTextSize: 0,
        });
    });
</script>