<ul class="nav nav-tabs course" role="tablist">
    <li>
        <a href="{{url('/c').'/'.$course->course_id}}" aria-controls="home">รายละเอียดหลักสูตร</a>
    </li>
    <li class="active">
        <a href="{{url('/course').'/'.$course->course_id.'/learn'}}" aria-controls="learn" role="tab"
           data-toggle="tab">เข้าเรียน</a>
    </li>
    @if(!$free_course)
    <li>
        <a href="{{url('/course').'/'.$course->course_id.'/result'}}"
           aria-controls="result">ผลการเรียน</a>
    </li>
    @endif
</ul>