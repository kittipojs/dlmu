<div class="col-sm-9 main-content border-left-lg" style="min-height:600px;">
    <div class="text-right">
        <button id="disable_slide_btn" class="btn btn-sm btn-link">
            <i class="fas fa-video"></i> &nbsp;Video Only
        </button>
        |
        <button id="enable_slide_btn" class="btn btn-sm btn-link">
            <i class="far fa-clone"></i> &nbsp;Video &nbsp; + &nbsp;Slide
        </button>
    </div>
    <div class="padding"></div>

    <div class="row">
        <div class="col-md-12">
            @if($content_object->media_type=='youtube')
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="{{$content_object->media_src}}" allowfullscreen></iframe>
            </div>
            @elseif($content_object->media_type=='mp4' || $content_object->media_type=='album' || $content_object->media_type=='streaming')
            <div id="player"></div>
            @endif
        </div>
        <div id="slide_wrapper" class="col-md-12" style="background-color:#eee; margin-top:10px;">
            <div class="padding"></div>
            @if($has_slide)
            <div class="slider-for" style="width:100%;">
                @foreach($slides as $item)
                <div>
                    <img src="{{$item->slide_image}}" class="img-responsive" />
                </div>
                @endforeach
            </div>
            <div id="lectureSlide" class="slider-nav">
                @foreach($slides as $item)
                <div style="margin:6px;">
                    <div style="background-image:url('{{$item->slide_image}}');  width:100%; height:105px; background-size:cover;"></div>
                </div>
                @endforeach
            </div>
            @endif
        </div>
    </div>


    <h3>{{ $content_object->lecture_title }}</h3>
    {{--<p>
    ระยะเวลา : {{$content_object->time_format}}
    </p>
    --}}
    <div class="padding"></div>
    <h4 class="blue-color">
        <i class="fas fa-graduation-cap"></i>
        คำนำ
    </h4>
    <p>{{$content_object->lecture_intro}}</p>

    <div class="padding"></div>
    <h4 class="blue-color">
        <i class="fas fa-align-left"></i>
        รายละเอียด
    </h4>
    <p>{!!$content_object->lecture_description!!}</p>

</div>


<script type="application/javascript">

    var curriculum_id = '{{$curriculum_id}}',
        object_id = '{{$content_object->object_id}}',
        course_id = '{{$course->course_id}}';

    $(document).ready(function () {

        setTimeout(function(){
            addNewSession()


        }, 5000);

        @if($course->can_skip)
        setTimeout(function(){
            updateSession(100, true)
        }, 10000);
        @endif



        @if($has_slide)
        var slideTransition = [];

        @foreach($slide_transition as $item)
        slideTransition.push({{$item}})
        @endforeach

        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav'
        });

        var lectureSlide = $('.slider-nav');
        lectureSlide.slick({
            asNavFor: '.slider-for',
            dots: true,
            arrows: false, 
            centerMode: false, 
            draggable: false,
            swipe: false,
            swipeToSlide: false,
            touchMove: false,
            accessibility: true,
            slidesToShow: 6, 
            slidesToScroll: 1,
            focusOnSelect: true,
            infinite: true
        });

        lectureSlide.slick('slickGoTo', 0);

        $('#enable_slide_btn').click(function(){
            $('#slide_wrapper').show()
        })

        $('#disable_slide_btn').click(function(){
            $('#slide_wrapper').hide()
        })
        @endif

        @if($content_object->media_type=='mp4' || $content_object->media_type=='album' || $content_object->media_type=='streaming')

        var timer,
            counter = 0,
            start_time = second('{{$content_object->start_time}}'),
            end_time = second('{{$content_object->end_time}}'),
            isPaused = true,
            isFirst = true,
            playerInstance = jwplayer("player").setup({
                "file": "{{url($content_object->media_src)}}",
                image: "{{url($content_object->lecture_cover)}}",
                aspectratio: "16:9",
                controls: true,
                width: "100%",
                autostart: "false",
                @if(!$course->can_skip)
                skin : {
                    "url":"{{url('frontend/medilab/css/skin-player.css')}}",
                    "name" : "setskin"
                },
                @endif
                sources: [{
                    file: "{{url($content_object->media_src)}}",
                    label: "360p SD"
                },{
                    file: "{{url($content_object->media_src)}}",
                    label: "720p HD",
                    "default": "true"
                },{
                    file: "{{url($content_object->media_src)}}",
                    label: "1280p HD"
                }]
            });

        end_time = (end_time === 0) ? playerInstance.getDuration() : second('{{$content_object->end_time}}');

        playerInstance.onTime(function (event) {
            var position = Math.floor(event.position);
            if (position > 0 && position % 5 == 0) {
                // console.log(position)
                // $.post('{{url('api/csession/update-progress')}}', {'id': '', 'duration': ''});
            }
            /*
            if(position >= end_time){
                playerInstance.stop();
            }
            */
            for(var slide in slideTransition) {

                if(position==slideTransition[slide]){
                    //console.log('match! '+slideTransition[slide])
                    lectureSlide.slick('slickGoTo', slide);
                }
            }
        });

        @if($content_object->start_time=='00:00:00' && $content_object->end_time=='00:00:00')

        @else
        playerInstance.onBeforePlay(function(){
            playerInstance.seek({{$content_object->time_from_millisec/1000}});
        });

        playerInstance.onTime(function(event) {
            var currentSecond = parseInt(event.position)
            var timeFrom = {{$content_object->time_from_millisec/1000}}
            var timeTo = {{$content_object->time_to_millisec/1000}}
            //console.log(currentSecond)
            if(currentSecond > timeTo){
                playerInstance.seek({{$content_object->time_from_millisec/1000}});
            }
            if(currentSecond < timeFrom){
                playerInstance.seek({{$content_object->time_from_millisec/1000}});
            }
        });
        @endif


        //TODO: set min max video time set
        /*
        playerInstance.onBeforePlay(function () {
            $('.jw-icon.jw-icon-inline.jw-button-color.jw-reset.jw-icon-rewind').remove();
            $('.jw-text.jw-reset.jw-text-elapsed').remove();
            $('.jw-controlbar-center-group').html('');
            $('.jw-text-duration').remove();
            if(isFirst){
                isFirst = false;
            }
            else {
                playerInstance.stop();
            }
        });

        playerInstance.onPlay(function () {
            isPaused = false;
            if(start_time !== 0 && end_time !== 0){
                playerInstance.seek(start_time);
            }
        });

        playerInstance.onPause(function () {
            isPaused = true;
        });
        */
        //TODO: count duration
        // timer = setInterval(function () {
        //     if (!isPaused) {
        //         counter++;
        //         console.log('timer = ' + counter);
        //         var videoDuration = end_time - start_time;
        //         if(counter >= videoDuration){
        //             playerInstance.stop();
        //         }
        //     }
        // }, 1000);


    });

    function second(time) {
        var times = time,
            time_split = times.split(':'),
            h = parseInt(time_split[0]), m = parseInt(time_split[1]), s = parseInt(time_split[2]);
        return (h * 60 * 60) + (m * 60) + s;
    }

    function addNewSession(){

        $.ajax({
            type: 'POST',
            url: "{{url('/e-admin/course-session/add')}}",
            data: { curriculum_id:curriculum_id, object_id:object_id, course_id:course_id },
            dataType: 'json',
            encode: true
        }).done(function (data) {
            console.log(JSON.stringify(data))
        });

    }

    function updateSession(progress, is_pass){

        $.ajax({
            type: 'POST',
            url: "{{url('/e-admin/course-session/update')}}",
            data: { curriculum_id:curriculum_id, object_id:object_id, course_id:course_id, progress:progress, is_pass:is_pass },
            dataType: 'json',
            encode: true
        }).done(function (data) {
            console.log(JSON.stringify(data))
        });
    }

    @endif
</script>
