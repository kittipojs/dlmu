<div id="timeline">
    @if($course->course_document!=null)
    <div class="timeline-item">
        <div class="timeline-icon">
            <i class="fas fa-arrow-circle-down"></i>
        </div>
        <div class="timeline-content right">
            <h2>
                <a target="_blank" href="{{ url($course->course_document) }}">
                    ดาวน์โหลดเอกสาร
                </a>
            </h2>
        </div>
    </div>
    @endif

    @if($course->pre_test_id != null)
    <div class="timeline-item">
        <div class="timeline-icon">
            <i class="fas fa-edit"></i>
        </div>
        <div class="timeline-content right">
            <a href="{{url('course/'.$course->course_id.'/pre-test')}}">
                <h2>แบบทดสอบก่อนเรียน</h2>
                <p>&nbsp;</p>
            </a>
        </div>
    </div>
    @endif

    @foreach( $curriculum as $item)
    <div class="timeline-item @if($item['active']) active @endif @if($item['is_current']) highlight @endif">
        <div class="timeline-icon">
            <i class="fas fa-play"></i>
        </div>
        <div class="timeline-content right">
            <a href="{{url('course/'.$course->course_id.'/learn?curriculum='.$item['id'])}}">
                @if($item['object_type']=='lecture')
                <h2>บทที่ {{ $item['object_order'] }}</h2>
                <p>{{ $item['title'] }}</p>
                @elseif($item['object_type']=='quiz')
                <h2>แบบทดสอบ</h2>
                <p>{{ $item['title'] }}</p>
                @endif
            </a>
        </div>
    </div>
    @endforeach

    @if($course->post_test_id != null)
    <div class="timeline-item">
        <div class="timeline-icon">
            <i class="fas fa-edit"></i>
        </div>
        <div class="timeline-content right">
            <a href="{{url('course/'.$course->course_id.'/post-test')}}">
                <h2>แบบทดสอบหลังเรียน</h2>
                <p>&nbsp;</p>
            </a>
        </div>
    </div>
    @endif

    <div class="timeline-item">
        <div class="timeline-icon">
            <i class="fas fa-graduation-cap"></i>
        </div>
        <div class="timeline-content right">
            <h2>จบหลักสูตร</h2>
            <p>&nbsp;</p>
        </div>
    </div>
</div>