<link rel="stylesheet" type="text/css" href="{{ asset('custom/wizard/custom.wizard.css')}}">
<form action="{{ url('c/quiz/save') }}" method="post" id="quizForm">
    <input type="hidden" id="quiz_id" name="quiz_id" value="{{ $quiz->quiz_id }}">
    <div class="container">
        <div class="sbox">
            <div class="sbox-title clearfix">
                <div class="col-xs-6">
                    <h1> {{ $quiz->quiz_title }} </h1>
                </div>
                <div class="col-xs-6">
                    <div class="countdown pull-right"></div>
                </div>
            </div>
            <hr>
            <div class="sbox-content">
                <div class="container" style="min-height: 500px">
                    <ul class="nav nav-tabs hide">
                        @foreach($questions as $question)
                        <li class="{{ ($question->active)? 'active' : '' }}">
                            <a href="#tab{{$question->question_id}}" data-toggle="tab">
                                {{$question->question_number}}
                            </a>
                        </li>
                        @endforeach
                        <li>
                            <a href="#tab99" data-toggle="tab">
                                99
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        @foreach($questions as $question)
                            <div class="tab-pane {{ ($question->active)? 'active' : '' }}"
                                 id="tab{{$question->question_id}}">
                                <p><strong>{{ $question->question_number }}. {{ $question->question_title }}</strong></p>
                                <div class="container">
                                    @foreach($question->choices as $choice)
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="choice[{{$question->question_id}}]"
                                                       id="choice[{{$question->question_id}}]"
                                                       value="{{ $choice->name }}" data-id="{{$question->question_id}}"
                                                       onclick="checkedPaginate($(this))">
                                                {{ $choice->name }}
                                            </label>
                                        </div>
                                        <hr>
                                    @endforeach
                                </div>
                                <hr>
                                @endforeach
                            </div>
                        </div>
                        @endforeach
                        <div class="tab-pane" id="tab99">
                            <p>
                                <strong>
                                    สิ้นสุดการตอบคำถาม
                                </strong>
                            </p>
                            <div class="container">
                                <button class="btn btn-default">ส่งคำตอบ</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-center">
                    <a class="btn btn-secondary btnPrevious"><i class="fas fa-arrow-left"></i> ข้อก่อนหน้า</a>
                    <a class="btn btn-warning btnNext"> ข้อถัดไป <i class="fas fa-arrow-right"></i></a>
                </div>

                <div class="text-center">
                    <ul class="pagination" role="menubar" aria-label="Pagination">
                        @foreach($questions as $question)
                        <li id="page{{$question->question_id}}">
                            <a href="#tab{{$question->question_id}}" data-toggle="tab">
                                {{$question->question_number}}
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="modal" id="nia-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog modal- vertical-align-center" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal-header"></h4>
                </div>
                <div class="modal-body" id="modal-info">
                    <!-- Ajax data -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close">
                        ตกลง
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        var total_quiz = ($('.tab-content').find('.tab-pane').length) - 1,
            setPage = $('.btnNext,.btnPrevious'),
            next = $('.btnNext'),
            previous = $('.btnPrevious');

        previous.click(function () {
            $('.nav-tabs > .active').prev('li').find('a').trigger('click');
        });

        next.click(function () {
            $('.nav-tabs > .active').next('li').find('a').trigger('click');
        });

        setPage.click(function () {
            var current_quiz = $('.tab-content').find('.tab-pane.active').index();

            if(current_quiz === 0){
                previous.attr('disabled',true);
            }
            else {
                previous.attr('disabled',false);
            }

            if(current_quiz === total_quiz){
                next.attr('disabled',true);
            }
            else {
                next.attr('disabled',false);
            }
        });
        var quizTime = parseInt('{{ $quiz->quiz_timer }}');
        // var quizTime = 0.1;//cd 5 sec for test
        var countdown = quizTime * 60 * 1000;
        var timerId = setInterval(function () {
            countdown -= 1000;
            var min = Math.floor(countdown / (60 * 1000));
            var sec = Math.floor((countdown - (min * 60 * 1000)) / 1000);

            if (countdown < 0) {
                clearInterval(timerId);
                $('#nia-modal').modal('show');
                $('#modal-header').html('หมดเวลาตอบคำถาม');
                $('#modal-info').html('content');

                $('#nia-modal').on('hidden.bs.modal', function () {
                    window.location.href = `{{ url('c/quiz') }}/{{ $quiz->quiz_id }}/question`;
                })

                //doSomething();
            } else {
                $(".countdown").html("<h1><i class=\"fas fa-clock\"></i> " + min + " : " + sec + "</h1>");
            }

        }, 1000); //1000ms. = 1sec.

        $( "#quizForm" ).submit(function( event ) {
            var totalAnswer = $( "input:checked" ).length;
            if(totalAnswer < total_quiz){
                $('#nia-modal').modal('show');
                $('#modal-header').html('ไม่สามารถส่งคำตอบได้');
                $('#modal-info').html('โปรดตอบคำถามให้ครบทุกข้อ');
                return false
            }
            $(this).submit();
            event.preventDefault();
        });


    });

    function checkedPaginate(ele) {
        if (ele.is(':checked')) {
            $('#page' + ele.data('id')).find('a').css("background-color", "black");
        }
    }
</script>