<div class="container">
    <p><strong><u>คำชี้แจง</u></strong></p>
    <p>{{ $quiz->quiz_title }}</p>
    <br>
    <p>{{ $quiz->quiz_description }}</p>
    <br>
    <p><strong>จำนวนคำถาม</strong> {{ $quiz->question }} ข้อ</p>
    <hr>
    <p><strong>จำกัดเวลา</strong> {{ $quiz->quiz_timer }} นาที</p>
    <hr>
    <div class="text-center">
        @if($quiz->question > 0)
            <a href="{{ url('c/quiz') }}/{{ $quiz->quiz_id }}/do" class="btn btn-warning">เริ่มแบบทดสอบ</a>
        @else
            <button type="button" class="btn btn-warning disabled">เริ่มแบบทดสอบ</button>
        @endif
    </div>
</div>