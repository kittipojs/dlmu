<div class="row">
    <table id="tb_user" class="table table-striped table-hover nowrap" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>ชื่อ - นามสกุล</th>
                <th>อีเมล</th>
                <th>บัตรประชาชน</th>
                <th>เพศ</th>
                <th>อาชีพ</th>
                <th>การศึกษา</th>
                <th>อายุ</th>
                <th>จังหวัด</th>
            </tr>
        </thead>
        <tobody>
            @foreach( $users as $user )
            <tr>
                <td>
                    <button class="btn btn-primary btn-sm view-info-btn" type="button"
                            data-id="{{ $user->user_id }}"
                            data-toggle="modal" data-target="#side_modal">
                        <i class="fas fa-edit" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-danger btn-sm delete-user-btn" type="button"
                            data-id="{{ $user->user_id }}"
                            data-name="{{ $user->first_name }} {{ $user->last_name }}"
                            data-toggle="modal" data-target="#confirm_delete_modal">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </button>
                </td>
                <td>{{ $user->first_name }} {{ $user->last_name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->citizen_id }}</td>
                <td>{{ ($user->gender === 'm')? 'ชาย' : 'หญิง' }}</td>
                <td>{{ DataHelpers::Occupation($user->occupation) }}</td>
                <td>{{ $user->education }}</td>
                <td>{{ DataHelpers::Age($user->birthday) }}</td>
                <td>{{ $user->name_th }}</td>
            </tr>
            @endforeach
        </tobody>
    </table>
</div>
<div class="modal fade" id="confirm_delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                ...
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn btn-danger confirm_delete" data-dismiss="modal" data-url="">ลบ</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        drawTable();
        setPaginageAction();
        
        $(".sorting_desc").click(setPaginageAction);
        $(".sorting_asc").click(setPaginageAction);
        $(".sorting").click(setPaginageAction);

        viewInfo();
        deleteUser();
        confirmDelete();
    });

    function drawTable() {
        var table = $('#tb_user').dataTable({
            dom: 'Bfrtip',
            searching: true,
            scrollX: true,
            aaSorting: [],
            pageLength: 50,
            buttons: [{
                extend: 'excel',
                text: 'Export รายงาน',
                title: 'รายงานสมาชิก',
                filename: 'รายงานสมาชิก',
                exportOptions: {
                    columns: [1,2,3,4,5,6,7,8],
                    format: {
                        body: function (data, row, column, node) {
                            return column >= 3 ? data.split("  ")[0] : data;
                        }
                    }
                }
            }],
            "oLanguage": {
                "sSearch": "ค้นหา : "
            }


        });

        $("#tb_user_filter").on("keyup", 'input', function() {
            setPaginageAction()
        });
    }

    function deleteUser(){
        $('.delete-user-btn').click(function () {
            var id   = $(this).data('id'),
                name = $(this).data('name');
            var header_area = $(".modal-header"),
                data_area   = $(".modal-body"),
                url         = "{{ url('e-admin/report/user/delete') }}" + "/" + id;
            header_area.html('ยืนยันการลบ');
            data_area.html('คุณต้องการลบ ' + name + ' ออกจากระบบ ?');
            $('.btn.btn-danger.confirm_delete').data('url', url);
        })
    }

    function confirmDelete() {
        $('.confirm_delete').click(function () {
            var url         = $(this).data('url');
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    $('#search').trigger('click');
                    $.toast({
                        heading: 'success',
                        text: 'ลบสำเร็จ',
                        position: 'bottom-right',
                        icon: 'success',
                        hideAfter: 2500,
                        stack: 6
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $.toast({
                        heading: 'error',
                        text: 'เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง',
                        position: 'bottom-right',
                        icon: 'error',
                        hideAfter: 2500,
                        stack: 6
                    });
                }
            });
        })
    }

    function setPaginageAction() {
        $(".paginate_button").click(setPaginageAction);
        $('.view-info-btn').unbind("click");
        $('.delete_user').unbind("click");
        $('.confirm_delete').unbind("click");
        viewInfo();
        deleteUser();
        confirmDelete();
    }

    function viewInfo(){
        $('.view-info-btn').click(function() {
            var id = $(this).data('id');

            var header_area = $("#modal-header"),
                data_area = $("#modal-info"),
                url = '{{ url('e-admin/user/') }}/'+id;

            header_area.html('ตรวจสอบข้อมูลผู้ใข้งาน');
            data_area.html('กำลังโหลด...');
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });
        });
    }
</script>