<!-- Search Area -->
<style>
    .text-right {
        text-align: right;
        padding-right: 10px;
        width: 40%;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading" align="center">ภาพรวมสมาชิก</div>
                <div class="panel-body">
                    <div class="row text-center">
                        <div class="col-sm-4">
                            <h1> {{ $today_registerd }} </h1>
                            <span>
                                สมาชิกใหม่ (วันนี้)
                            </span>
                        </div>
                        <div class="col-sm-4">
                            <h1> {{ number_format($nia_user) }} </h1>
                            <span>
                                สมาชิกทั้งหมด
                            </span>
                        </div>
                        <div class="col-sm-4">
                            <h1> {{ number_format($uleague_user) }} </h1>
                            <span>
                                สมัคร U League {{ DataHelpers::year() }} แล้ว
                            </span>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-warning">
                <div class="panel-heading" align="center">ค้นหาข้อมูล</div>
                <div class="panel-body">
                    <table width="100%">

                        <tr class="tr-search">
                            <td class="text-right">การศึกษา</td>
                            <td>
                                <select name="education" id="education" class="form-control" required>
                                    <option value="" selected="true">--- Please Select ---</option>
                                    <option value="ปริญญาเอก">ปริญญาเอก</option>
                                    <option value="ปริญญาโท">ปริญญาโท</option>
                                    <option value="ปริญญาตรี">ปริญญาตรี</option>
                                    <option value="ปวช.">ปวช.</option>
                                    <option value="ปวส.">ปวส.</option>
                                    <option value="มัธยม 1">มัธยม 1</option>
                                    <option value="มัธยม 2">มัธยม 2</option>
                                    <option value="มัธยม 3">มัธยม 3</option>
                                    <option value="มัธยม 4">มัธยม 4</option>
                                    <option value="มัธยม 5">มัธยม 5</option>
                                    <option value="มัธยม 6">มัธยม 6</option>
                                    <option value="ต่ำกว่าหรือเท่ากับประถมศึกษาปีที่ 6">
                                        ต่ำกว่าหรือเท่ากับประถมศึกษาปีที่ 6
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr class="tr-search">
                            <td class="text-right">อาชีพ</td>
                            <td>
                                <select id="occupation" name="occupation" class="form-control">
                                    <option value="" selected="true">--- Please Select ---</option>
                                    <option value="C03">ธุรกิจส่วนตัว</option>
                                    <option value="C09">อาชีพอิสระ</option>
                                    <option value="C07">ข้าราชการ/พนักงานรัฐวิสาหกิจ</option>
                                    <option value="C11">นักลงทุนในหลักทรัพย์มืออาชีพ</option>
                                    <option value="C02">ผู้แนะนำการลงทุนในหลักทรัพย์</option>
                                    <option value="C08">พนักงานบริษัทในธุรกิจการเงิน</option>
                                    <option value="C12">บุคลากรทางการแพทย์</option>
                                    <option value="C13">วิศวกร</option>
                                    <option value="C14">บุคลากรทางการศึกษาและวิจัย</option>
                                    <option value="C05">พนักงานบริษัทเอกชนอื่นๆ</option>
                                    <option value="C04">นักเรียน/นักศึกษา</option>
                                    <option value="C01">เกษียณ</option>
                                    <option value="C06">แม่บ้าน/พ่อบ้าน</option>
                                    <option value="C10">อื่นๆ</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="tr-search">
                            <td class="text-right">สาขา Startup ที่สนใจ</td>
                            <td>
                                <select name="startup_lookup" id="startup_lookup" class="form-control" required>
                                    <option value="" selected="true">--- Please Select ---</option>
                                    <option value="1"> การแพทย์และสาธารณสุข (MedTech/ Health Tech)</option>
                                    <option value="2"> เกษตรและอาหาร (AgriTech/ Food Startup)</option>
                                    <option value="3"> อสังหาริมทรัพย์ (Property Tech)</option>
                                    <option value="4"> การเงินและการธนาคาร (FinTech)</option>
                                    <option value="5"> การศึกษา (EdTech)</option>
                                    <option value="6"> การท่องเที่ยว (TravelTech)</option>
                                    <option value="7"> ไลฟ์สไตล์ (LifeStyle)</option>
                                    <option value="8"> พาณิชยกรรมอิเล็กทรอนิกส์ (E-Commerce)</option>
                                    <option value="9"> ภาครัฐ/การศึกษา (GovTech/EdTech)</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="tr-search">
                            <td class="text-right">จังหวัด</td>
                            <td>
                                <select name="province_id" id="province_id" class="form-control" required></select>
                            </td>
                        </tr>
                        <tr class="tr-search">
                            <td class="text-right">รับข่าวสารจาก Startup Thailand</td>
                            <td>
                                <select id="is_subscribe" name="is_subscribe" class="form-control">
                                    <option value="" selected="true">--- Please Select ---</option>
                                    <option value="0">ไม่รับข่าวสาร</option>
                                    <option value="1">รับข่าวสาร</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="tr-search">
                            <td class="text-right"></td>
                            <td>
                                <button id="search" class="btn btn-success">ค้นหา</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <script>
        $(document).ready(function () {

            $("#province_id").jCombo("{!! url('select/province') !!}");


            var data_area = $("#details"),
                url = "{{url('e-admin/report/user/today')}}",
                params = param();
            data_area.html('กำลังโหลดข้อมูล . . .');
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(JSON.stringify(jqXHR))
                    data_area.html('เกิดข้อผิดพลาด');
                }
            });

        });

        function param() {

            var education = $('#education').val();
            var occupation = $('#occupation').val();
            var startup_lookup = $('#startup_lookup').val();
            var is_subscribe = $('#is_subscribe').val();
            var province_id = $('#province_id').val();

            var params = {
                education: education,
                occupation: occupation,
                startup_lookup: startup_lookup,
                is_subscribe: is_subscribe,
                province_id: province_id
            };
            return params;
        }

        $('#search').click(function () {
            var data_area = $("#details"),
                url,
                params = param();
            url = "{{url('e-admin/report/user/result')}}" + "?" + $.param(params);
            data_area.html('กำลังโหลดข้อมูล . . .');
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(JSON.stringify(jqXHR))
                    data_area.html('เกิดข้อผิดพลาด');
                }
            });
        })
    </script>
    <!--/Search Area -->

    <!-- Data Area -->
    <div id="details"></div>
    <!--/Data Area -->

</div>