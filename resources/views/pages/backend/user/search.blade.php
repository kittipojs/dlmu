<!-- Search Area -->
<style>
    .text-right {
        text-align: right;
        padding-right: 10px;
        width: 40%;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-warning">
                <div class="panel-heading" align="center">ค้นหาข้อมูล</div>
                <div class="panel-body">
                    <p>
                        ค้นหาสมาชิกด้วย อีเมลหรือเลขบัตรประชาชน
                    </p>

                    <div class="form-inline">
                        <label for="email">อีเมล</label>
                        <input name="email" id="email" class="form-control" />


                        <label for="citizen_id">&nbsp;&nbsp;บัตรประชาชน&nbsp;&nbsp;</label>
                        <input name="citizen_id" id="citizen_id" class="form-control" />
                        &nbsp;&nbsp;
                        <label for="citizen_id">&nbsp;&nbsp;ชื่อ&nbsp;&nbsp;</label>
                        <input name="first_name" id="first_name" class="form-control" />
                        &nbsp;&nbsp;
                        <label for="citizen_id">&nbsp;&nbsp;นามสกุล&nbsp;&nbsp;</label>
                        <input name="last_name" id="last_name" class="form-control" />
                        &nbsp;&nbsp;
                        <button id="search" type="submit" class="btn btn-primary mb-2">ค้นหา</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <script>

        $(document).ready(function () {

        });

        function param() {

            var email        = $('#email').val();
            var citizen_id   = $('#citizen_id').val();
            var first_name   = $('#first_name').val();
            var last_name    = $('#last_name').val();

            var params = {
                email: email,
                citizen_id: citizen_id,
                first_name: first_name,
                last_name: last_name
            };
            return params;
        }

        $('#search').click(function () {

            var citizen_id = $('#citizen_id').val(),
                email = $('#email').val(),
                first_name = $('#first_name').val(),
                last_name = $('#last_name').val();

            if(citizen_id=='' && email=='' && first_name=='' && last_name==''){
                alert('กรุณากรอกข้อมูลอย่างใดอย่างหนึ่ง')
            }
            else if(citizen_id.length<3 && email.length<3 && first_name.length<1 && last_name.length<1){
                alert('กรุณากรอกข้อมูลอย่างน้อย 3 ตัวอักษร')
            }
            else{
                var data_area = $("#details"),
                    url,
                    params = param();
                url = "{{url('e-admin/report/user/detailsDelete')}}" + "?" + $.param(params);
                data_area.html('กำลังโหลดข้อมูล . . .');
                $.ajax({
                    type: 'GET',
                    url: url,
                    success: function (data) {
                        data_area.html(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(JSON.stringify(jqXHR))
                        data_area.html('เกิดข้อผิดพลาด');
                    }
                });
            }
        })
    </script>
    <!--/Search Area -->

    <!-- Data Area -->
    <div id="details"></div>
    <!--/Data Area -->
</div>