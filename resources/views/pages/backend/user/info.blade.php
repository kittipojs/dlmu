<ul class="nav nav-tabs">
    <li class="active"><a href="#info" data-toggle="tab">ข้อมูลส่วนตัว</a></li>
    <li><a href="#uleague" data-toggle="tab">ข้อมูล U League</a></li>
    <li><a href="#team" data-toggle="tab">ข้อมูลทีม</a></li>
    <li><a href="#student_card" data-toggle="tab"><i class="fas fa-upload"></i> อัพโหลด บัตรนักศึกษา</a></li>
    <li><a href="#camp" data-toggle="tab">ข้อมูล Camp</a></li>
</ul>

<div class="tab-content">
    <div class="tab-pane active m-t" id="info">
        <div class="padding-lg"></div>
        {!! Form::open(array('url'=>'e-admin/user/'.$user_id, 'id'=>'edit_user_info', 'class'=>'form-horizontal validated', 'method' => 'post')) !!}

        <div class="form-group">
            <label for="ipt" class="control-label col-sm-3"> อีเมล * </label>
            <div class="col-sm-8">
                <input name="email" type="text" id="email" class="form-control input-sm" value="{{ $user->email }}"/>
            </div>
        </div>

        <div class="form-group">
            <label for="citizen_id" class="col-md-3 control-label"> หมายเลขบัตรประชาชน * </label>
            <div class="col-md-8">
                <input name="citizen_id" type="text" id="citizen_id" class="form-control input-sm" required value="{{ $user->citizen_id }}" />
            </div>
        </div>

        <div class="form-group">
            <label for="name_title" class="control-label col-sm-3"> คำนำหน้า * </label>
            <div class="col-sm-3">
                <select name="name_title" id="name_title" class="form-control">
                    <option value="นาย">นาย</option>
                    <option value="นาง">นาง</option>
                    <option value="นางสาว">นางสาว</option>
                </select>
            </div>
            <div class="col-sm-7"></div>
        </div>

        <div class="form-group">
            <label for="ipt"
                   class=" control-label col-sm-3"> ชื่อ * </label>
            <div class="col-sm-8">
                <input name="first_name" type="text" id="first_name" class="form-control input-sm"
                       required value="{{ $user->first_name }}"/>
            </div>
        </div>

        <div class="form-group">
            <label for="last_name"
                   class=" control-label col-sm-3">นามสกุล * </label>
            <div class="col-sm-8">
                <input name="last_name" type="text" id="last_name" class="form-control input-sm"
                       required value="{{ $user->last_name }}"/>
            </div>
        </div>

        <div class="form-group">
            <label for="education" class="control-label col-sm-3"> ระดับการศึกษา * </label>
            <div class="col-sm-8">
                <select name="education" id="education" class="form-control" required>
                    <option value="" selected="true">--- Please Select ---</option>
                    <option value="ปริญญาเอก">ปริญญาเอก</option>
                    <option value="ปริญญาโท">ปริญญาโท</option>
                    <option value="ปริญญาตรี">ปริญญาตรี</option>
                    <option value="ปวช.">ปวช.</option>
                    <option value="ปวส.">ปวส.</option>
                    <option value="มัธยม 1">มัธยม 1</option>
                    <option value="มัธยม 2">มัธยม 2</option>
                    <option value="มัธยม 3">มัธยม 3</option>
                    <option value="มัธยม 4">มัธยม 4</option>
                    <option value="มัธยม 5">มัธยม 5</option>
                    <option value="มัธยม 6">มัธยม 6</option>
                    <option value="ต่ำกว่าหรือเท่ากับประถมศึกษาปีที่ 6">
                        ต่ำกว่าหรือเท่ากับประถมศึกษาปีที่ 6
                    </option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="occupation" class="control-label col-sm-3"> อาชีพ * </label>
            <div class="col-sm-8">
                <select id="occupation" name="occupation" class="form-control">
                    <option value="" selected="true">--- Please Select ---</option>
                    <option value="C03">ธุรกิจส่วนตัว</option>
                    <option value="C09">อาชีพอิสระ</option>
                    <option value="C07">ข้าราชการ/พนักงานรัฐวิสาหกิจ</option>
                    <option value="C11">นักลงทุนในหลักทรัพย์มืออาชีพ</option>
                    <option value="C02">ผู้แนะนำการลงทุนในหลักทรัพย์</option>
                    <option value="C08">พนักงานบริษัทในธุรกิจการเงิน</option>
                    <option value="C12">บุคลากรทางการแพทย์</option>
                    <option value="C13">วิศวกร</option>
                    <option value="C14">บุคลากรทางการศึกษาและวิจัย</option>
                    <option value="C05">พนักงานบริษัทเอกชนอื่นๆ</option>
                    <option value="C04">นักเรียน/นักศึกษา</option>
                    <option value="C01">เกษียณ</option>
                    <option value="C06">แม่บ้าน/พ่อบ้าน</option>
                    <option value="C10">อื่นๆ</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="faculty_id" class="control-label col-sm-3"> คณะที่จบการศึกษา * </label>
            <div class="col-sm-8">
                <select name="faculty_id" id="faculty_id" class="form-control" required></select>
            </div>
        </div>

        <div class="form-group">
            <label for="sub_faculty" class="col-sm-3 control-label"> สาขาที่จบการศึกษา * </label>
            <div class="col-sm-8">
                <input type="text" name="sub_faculty" id="sub_faculty" class="form-control"
                       placeholder="สาขาที่จบการศึกษา" value="{{ $user->sub_faculty }}" required>
            </div>
        </div>

        <div class="form-group">
            <label for="address" class="control-label col-sm-3"> ที่อยู่ </label>
            <div class="col-sm-8">
                {!! Form::text('address',  $user->address, array('class'=>'form-control input-sm', 'placeholder'=> 'ที่อยู่อาศัยปัจจุบัน')) !!}
            </div>
        </div>

        <div class="form-group ">
            <label for="province_id" class="control-label col-sm-3"> จังหวัด * </label>
            <div class="col-sm-8">
                <select name="province_id" id="province_id" class="form-control input-sm" required>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="mobile_number" class="control-label col-sm-3"> เบอร์มือถือ * </label>
            <div class="col-sm-8">
                {!! Form::text('mobile_number',  $user->mobile_number, array('class'=>'form-control input-sm', 'required'=>'true', 'placeholder'=> 'เบอร์มือถือของคุณ')) !!}
            </div>
        </div>

        <div class="form-group">
            <label for="postcode" class="col-sm-3 control-label"> รหัสไปรษณีย์ * </label>
            <div class="col-sm-8">
                {!! Form::text('postcode', $user->postcode, array('class'=>'form-control', 'required'=>'true', 'placeholder'=> 'รหัสไปรษณีย์')) !!}
            </div>
        </div>
        <div class="padding-md"></div>
        <div class="form-group">
            <label class="control-label col-sm-3">&nbsp;</label>
            <div class="col-sm-8">
                <button class="btn btn-primary" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="tab-pane m-t" id="uleague">
        @if($uleague!=null)
        {!! Form::open(array('url'=>'e-admin/uleague/'.$user_id, 'id'=>'edit_uleague_info', 'class'=>'form-horizontal validated', 'method' => 'post')) !!}
        <div class="form-group">
            <label for="fullname" class="control-label col-sm-3"> ชื่อเล่น </label>
            <div class="col-sm-8">
                <input name="nickname" type="text" id="nickname" class="form-control input-sm" value="{{ $uleague->nickname }}" />
            </div>
        </div>
        <div class="form-group">
            <label for="University Id" class="control-label col-sm-3"> มหาวิทยาลัย </label>
            <div class="col-sm-8">
                <select class="form-control" id="university" name="university" required></select>
            </div>
        </div>
        <div class="form-group">
            <label for="faculty_id" class="control-label col-sm-3"> คณะที่กำลังศึกษา </label>
            <div class="col-sm-8">
                <select name="faculty" id="faculty" class="form-control" required></select>
            </div>
        </div>
        <div class="form-group">
            <label for="sub_faculty" class="control-label col-sm-3"> สาขาที่กำลังศึกษา </label>
            <div class="col-sm-8">
                <input name="sub_faculty" type="text" id="sub_faculty" class="form-control input-sm" value="{{ $uleague->sub_faculty }}" />
            </div>
        </div>
        <div class="form-group">
            <label for="college_year" class="control-label col-sm-3"> ชั้นปี </label>
            <div class="col-sm-8">
                <input name="college_year" type="number" id="college_year" class="form-control input-sm" value="{{ $uleague->college_year }}" />
            </div>
        </div>
        <div class="form-group">
            <label for="student_id" class="control-label col-sm-3"> รหัสนักศึกษา </label>
            <div class="col-sm-8">
                <input name="student_id" type="text" id="student_id" class="form-control input-sm" value="{{ $uleague->student_id }}" />
            </div>
        </div>
        <div class="form-group">
            <label for="ipt" class="control-label col-sm-3"> ภาพบัตรนักศึกษา </label>
            <div class="col-sm-8">

                <a class="btn-link" href="{{ asset($uleague->photo_src) }}" data-lightbox="image-1">
                    <img src="{{ asset( $uleague->photo_src ) }}" class="img-responsive" style="max-width:300px;" />
                </a>
            </div>
        </div>
        <div class="padding-md"></div>
        <div class="form-group">
            <label class="control-label col-sm-3">&nbsp;</label>
            <div class="col-sm-8">
                <button class="btn btn-primary" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>
            </div>
        </div>
        {!! Form::close() !!}

        @else
        <p class="text-center">สมาชิกคนนี้ยังไม่ได้สมัคร U League</p>
        @endif
    </div>
    <div class="tab-pane m-t" id="student_card">
        <div id="file-preview" class="dropzone"></div>
    </div>
    <div class="tab-pane m-t" id="team">
        <div class="padding text-center">
            @if($team)
            <h3>สมาชิกคนนี้อยู่ทีม {{ $team->team_name}}</h3>
            @else
            <h3>สมาชิกคนนี้ยังไม่มีทีม</h3>
            @endif
        </div>
    </div>
    <div class="tab-pane m-t" id="camp">
        @if(count($events)==0)
        <p class="padding text-center">คุณยังไม่ได้เข้าร่วมกิจกรรม U League Camp ใดๆ</p>
        @else
        @foreach( $events as $item )
        <div class="row" style="border-bottom:2px solid #eee; padding:10px;">
            <div class="col-sm-4 col-md-3">
                <img src='{{ url($item['cover']) }}' alt="" class="img-responsive">
            </div>
            <div class="col-sm-8 col-md-9">
                <h5>
                    <a href="{{ $item['url'] }}" target="_blank">{{ $item['name'] }}</a>
                </h5>
                <p>
                    <i class="far fa-calendar-alt"></i> วันที่ : {{ $item['start_date'] }} - {{ $item['end_date'] }}
                </p>
                <p>
                    <i class="fas fa-map-marker-alt"></i> สถานที่ : {{ $item['venue']->name }}
                </p>
                <p>
                    <i class="fas fa-arrow-right"></i> ประเภท : {{ $item['type_text'] }}
                </p>
                <p>
                    <i class="fas fa-users"></i> รับสมัคร : {{ $item['registration'] }} | คงเหลือ : {{ $item['remain'] }}
                </p>
                <p>
                    @if($item['is_register'])
                    <span class="color-blue"><i class="fa fa-check"></i> ลงทะเบียนแล้ว</span>
                    @endif
                    @if($item['is_checkin'])
                    &middot; <span class="color-green"><i class="fa fa-check"></i> เข้าร่วมแล้ว</span>
                    @endif
                </p>
            </div>
        </div>
        @endforeach
        @endif
    </div>

    <script type="text/javascript">
        $(document).ready(function () {

            $('.date').datepicker({format: 'yyyy-mm-dd', autoclose: true});

            $("#faculty_id").jCombo("{!! url('select/faculty') !!}", {selected_value: '{{ $user->faculty_id }}'});
            $("#province_id").jCombo("{!! url('select/province') !!}", {selected_value: '{{ $user->province_id }}'});

            @if($uleague!=null)
            $("#faculty").jCombo("{!! url('select/faculty') !!}", {selected_value: '{{ $uleague->faculty_id }}'});
            $("#university").jCombo("{!! url('select/university') !!}", {selected_value: '{{ $uleague->university_id }}'});
            @endif

            $("#name_title").val('{{ $user->name_title }}')
            $("#education").val('{{ $user->education }}')
            $("#occupation").val('{{ $user->occupation }}')


            $('#edit_user_info').submit(function (event) {
                $.ajax({
                    type: 'POST',
                    data: $(this).serialize(),
                    cache: false,
                    url: $(this).attr('action'),
                    success: function (data) {

                        $('#side_modal').modal('hide');
                        $.toast({
                            heading: 'success',
                            text: 'บันทึกสำเร็จ',
                            position: 'bottom-right',
                            icon: 'success',
                            hideAfter: 2500,
                            stack: 6
                        });

                        $('#search').click();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $.toast({
                            heading: 'error',
                            text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                            position: 'bottom-right',
                            icon: 'error',
                            hideAfter: 2500,
                            stack: 6
                        });
                    }
                });
                event.preventDefault();
            });

            @if($uleague!=null)
            $('#edit_uleague_info').submit(function (event) {

                $.ajax({
                    type: 'POST',
                    data: $(this).serialize(),
                    cache: false,
                    url: $(this).attr('action'),
                    success: function (data) {
                        $('#side_modal').modal('hide');
                        $.toast({
                            heading: 'success',
                            text: 'บันทึกสำเร็จ',
                            position: 'bottom-right',
                            icon: 'success',
                            hideAfter: 2500,
                            stack: 6
                        });

                        $('#search').click();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $.toast({
                            heading: 'error',
                            text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                            position: 'bottom-right',
                            icon: 'error',
                            hideAfter: 2500,
                            stack: 6
                        });
                    }
                });
                event.preventDefault();
            });
            @endif
        });
    </script>

