<div class="row">
    <table id="tb_user" class="table table-striped table-hover nowrap" width="100%">
        <thead>
            <tr>
                <th>ชื่อ - นามสกุล</th>
                <th>อีเมล</th>
                <th>บัตรประชาชน</th>
                <th>เพศ</th>
                <th>อาชีพ</th>
                <th>การศึกษา</th>
                <th>อายุ</th>
                <th>จังหวัด</th>
                <!-- <th>Startup ที่สนใจ</th> -->
            </tr>
        </thead>
        <tobody>
            @foreach( $users as $user )
            <tr>
                <td>{{ $user->first_name }} {{ $user->last_name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->citizen_id }}</td>
                <td>{{ ($user->gender === 'm')? 'ชาย' : 'หญิง' }}</td>
                <td>{{ DataHelpers::Occupation($user->occupation) }}</td>
                <td>{{ $user->education }}</td>
                <td>{{ DataHelpers::Age($user->birthday) }}</td>
                <td>{{ $user->name_th }}</td>
                <!-- <td>{!! DataHelpers::startupLookup($user->startup_lookup) !!}</td> -->
            </tr>
            @endforeach
        </tobody>
    </table>
</div>
<script>
    $(document).ready(function () {
        drawTable();
    });

    function drawTable() {
        var table = $('#tb_user').dataTable({
            dom: 'Bfrtip',
            searching: true,
            scrollX: true,
            aaSorting: [],
            pageLength: 50,
            buttons: [{
                extend: 'excel',
                text: 'Export รายงาน',
                title: 'รายงานสมาชิก',
                filename: 'รายงานสมาชิก',
                exportOptions: {
                    columns: ':visible',
                    format: {
                        body: function (data, row, column, node) {
                            return column >= 3 ? data.split("  ")[0] : data;
                        }
                    }
                }
            }],
            "oLanguage": {
                "sSearch": "ค้นหา :"
            }
        });

        $("#tb_user_filter").on("keyup", 'input', function() {
            setPaginageAction()
        });
    }
</script>