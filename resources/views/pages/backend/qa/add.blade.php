<div class="container">
    <h4 align="center">{{ $title }}</h4>
    <div class="padding"></div>
    <form class="form-horizontal" action="{{ url('e-admin/qa/save') }}" method="post">
        <div class="form-group">
            <label class="control-label col-sm-2" for="question">คำถาม </label>
            <div class="col-sm-8">
                <input type="text" name="question" id="question" class="form-control" placeholder="ใส่คำถาม" required>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="answer">คำตอบ </label>
            <div class="col-sm-8">
                <textarea name="answer" rows="10" id="answer" class="form-control" required></textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="status">สถานะ </label>
            <div class="col-sm-8">
                <input type="checkbox" id="status" name="status" checked data-toggle="toggle">
            </div>
        </div>
        <hr class="divider">
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <button type="submit" class="btn btn-success">ตกลง</button>
            </div>
        </div>
    </form>
</div>
<script>
    
    $('#status').bootstrapToggle({
        on: 'เปิดใช้งาน',
        off: 'ปิด'
    });

    $('#answer').summernote({
        placeholder: 'ใส่คำตอบตรงนี้...',
        height: 200,
        disableResizeEditor: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['insert', ['link', 'picture']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ],
        onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable);
        }
    });

    function sendFile(file, editor, welEditable) {
        data = new FormData();
        data.append("file", file);
        $.ajax({
            data: data,
            type: "POST",
            url: "{{url('e-admin/qa/upload')}}",
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                alert('upload image success!')
                var image = $('<img>').attr('src', data.file);
                $('#answer').summernote("insertNode", image[0]);
            }
        });
    }
</script>