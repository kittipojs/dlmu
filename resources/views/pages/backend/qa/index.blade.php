<div class="container">
    <div class="row">
        <div class="col-sm-4 pull-left">
            <a href="{{ url('e-admin/qa/add') }}" class="btn btn-primary"><i class="fa fa-plus"></i> สร้าง Q&A</a>
        </div>
        <div class="col-sm-4" align="center">

        </div>
        <div class="col-sm-4" align="right">
            <div class="input-group">
                <input type="text" name="searchInput" id="searchInput" class="form-control"
                       placeholder="ค้นหาคำถาม-คำตอบ">
                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
            </div>
        </div>

    </div>
    <div class="row padding" id="tb_area">
        <form action="{{ url('e-admin/qa/sort') }}" method="post" id="sort_qa">
            <table class="table table-striped table-hover" id="tb_qa" width="100%">
                <thead>
                <tr>
                    <th>คำถาม</th>
                    <th>สถานะ</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach( $qa as $item)
                    <tr>
                        <input type="hidden" name="sort[]" value="{{ $item->id }}">
                        <td>
                            {{ $item->question }}
                        </td>
                        <td>
                            {{ ($item->status == 1)? "Enable" : "Disable" }}
                        </td>
                        <td data-values="action" data-key="{{ $item->id }}">
                            <div class=" action ">
                                <div class="dropdown">
                                    <button type="button" data-target="#sximo-modal" data-toggle="modal" class="btn btn-primary edit" data-id="{{ $item->id }}">
                                        <i class="fa fa-edit"></i> Edit
                                    </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </form>
        @include('modal.modal')
    </div>
</div>

<script>
    var table;
    $(document).ready(function () {
        table = $('#tb_qa').dataTable({
            dom: 'Bfrtip',
            "searching": true,
            "responsive": true,
            "aaSorting": [],
            columnDefs: [
                {orderable: false, targets: -1}
            ],
            buttons: [{
                extend: 'excel',
                title: 'ข้อมูลคำถาม',
                filename: 'ข้อมูลคำถาม',
                exportOptions: {
                    columns: [0,1],
                    format: {
                        body: function (data, row, column, node) {
                            return column >= 3 ? data.split("  ")[0] : data;
                        }
                    }
                }
            }],
        });
        dragTB();
        Edit();

        $("#searchInput").on('keyup', function () {
            $('#tb_qa').dataTable().fnFilter(this.value);
        });
    });

    function dragTB() {
        $('tbody', table).sortable({});
        table.droppable({
            drop: function () {
                setTimeout(
                    function(){
                        $('#sort_qa').submit();
                    }, 1000);
            }
        });
    }

    function Edit() {
        $('.edit').click(function () {
            
            $('#sximo-modal-header').text('อัพเดทข้อมูล');
            
            var id = $(this).data('id');
            var data_area = $("#sximo-modal-content"),
                url = "{{ url('e-admin/qa/update') }}" + "/" + id;
            
            data_area.html('Loading . . .');
            
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });
        })

    }

</script>