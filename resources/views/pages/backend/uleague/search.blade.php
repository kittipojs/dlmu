<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading" align="center">ภาพรวมของทีม</div>
                <div class="panel-body">
                    <div class="row text-center">
                        <div class="col-sm-4">
                            <h1> {{ number_format($all_team) }} </h1>
                            <span>
                                ทีมในระบบทั้งหมด
                            </span>
                        </div>
                        <div class="col-sm-4">
                            <h1> {{ number_format($all_apply_team) }} </h1>
                            <span>
                                ส่งเข้าแข่งขันแล้ว
                            </span>
                        </div>
                        <div class="col-sm-4">
                            <h1> {{ number_format($all_verify_team)}} </h1>
                            <span>
                                ผ่านการตรวจสอบ
                            </span>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-warning">
                <div class="panel-heading" align="center">ค้นหาข้อมูล</div>
                <div class="panel-body">
                    <div class="form-group">
                        <select class="form-control" name="event_id" id="event_id">
                            <option>--- เลือกกิจกรรม Pitching ที่ต้องการ ---</option>
                            @foreach($pitching_events as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <button id="search" class="btn btn-success">ค้นหา</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div id="details"></div>
</div>

<script type="text/javascript">
    
    $(document).ready(function(){
        
        var data_area = $("#details"),
            params = { event_id: $('#event_id').val() },
            url = "{{url('e-admin/report/uleague/pitching')}}";
        
        data_area.html('กำลังโหลดข้อมูล . . .');
        
        $.ajax({
            type: 'GET',
            url: url,
            success: function (data){
                data_area.html(data);
            },
            error: function (jqXHR, textStatus, errorThrown){
                data_area.html('เกิดข้อผิดพลาด');
            }
        });
        
    });
    
    $('#search').click(function () {
        
        var data_area = $("#details"),
            params = { event_id: $('#event_id').val() },
            url = "{{url('e-admin/report/uleague/pitching')}}" + "?" + $.param(params);
        
        data_area.html('กำลังโหลดข้อมูล . . .');
        
        $.ajax({
            type: 'GET',
            url: url,
            success: function (data){
                data_area.html(data);
            },
            error: function (jqXHR, textStatus, errorThrown){
                data_area.html('เกิดข้อผิดพลาด');
            }
        });
    });
</script>