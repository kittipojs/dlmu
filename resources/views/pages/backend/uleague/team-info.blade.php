<div class="form-horizontal">
    <div class="form-group">
        <label for="team_name" class="col-sm-3 control-label">ชื่อทีม :</label>
        <div class="col-sm-9">
            <p> {{ $team->team_name }} </p>
        </div>
    </div>
    <div class="form-group">
        <label for="team_info" class="col-sm-3 control-label">รายละเอียดทีม :</label>
        <div class="col-sm-9">
            <p> {{ $team->team_info }} </p>
        </div>
    </div>

    <div class="form-group">
        <label for="team_info" class="col-sm-3 control-label">มหาวิทยาลัย :</label>
        <div class="col-sm-9">
            <p> {{ $team->name }} </p>
        </div>
    </div>

    <div class="form-group">
        <label for="team_info" class="col-sm-3 control-label">Startup Sector :</label>
        <div class="col-sm-9">
            <p> {!! DataHelpers::startupLookup($team->startup_sector) !!} </p>
        </div>
    </div>
    <div class="form-group">
        <label for="member" class="col-sm-3 control-label">สมาชิก :</label>
        <div class="col-sm-9">
            @if(count($team->member) > 0)
            @foreach( $team->member as $member)
            <div class="padding">
                <p> 
                    {{ $member->first_name }} {{ $member->last_name }}
                    <a class="btn-link" href="{{ asset($member->photo_src) }}" data-lightbox="image-1" data-title="{{ $member->first_name }} {{ $member->last_name }}">(ดูบัตรนักศึกษา)</a>
                </p>
            </div>
            @endforeach
            @else
            <p> ยังไม่ได้ทำการชวนสมาชิก </p>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="team_info" class="col-sm-3 control-label">ไฟล์ pitch desk :</label>
        <div class="col-sm-9">
            <p>
                <a href="{{ asset($team->file_url) }}" class="btn-link" target="_blank">
                    <i class="fa fa-download"></i> ดาวน์โหลด
                </a>
            </p>
        </div>
    </div>
    <div class="form-group">
        <label for="team_info" class="col-sm-3 control-label">สถานะทีม :</label>
        <div class="col-sm-9">
            <p> 
                @if($team->apply_status==1)
                รออนุมัติ
                @elseif($team->apply_status==2)
                <i class="fa fa-check"></i> อนุมัติแล้ว
                @elseif($team->apply_status==-99)
                <i class="fa fa-close"></i> ไม่อนุมัติ : เอกสารไม่ถูกต้อง/ครบถ้วน
                @elseif($team->apply_status==-98)
                <i class="fa fa-close"></i> ไม่อนุมัติ : สมาชิกภายในทีมขาดคุณสมบัติ
                @endif
            </p>
        </div>
    </div>
    <hr class="divider" />
    @if($team->apply_status===1)
    <div class="form-group">
        <div class="form-group">
            <label class=" control-label col-sm-3">&nbsp;</label>
            <div class="col-sm-8">

                <button class="btn btn-primary" id="approve" type="button" data-id="{{ $team->team_id }}"> 
                    <i class="fa fa-check"></i> อนุมัติ
                </button>
                &nbsp;หรือ&nbsp;
                <button class="btn btn-danger" id="show_reason" type="button">
                    <i class="fa fa-close"></i> ไม่อนุมัติ
                </button>

                <div class="padding"></div>
                <div id="reason" class="padding" style="display:none; border:1px solid #CCC; background-color:#efefef;">
                    <label class="my-1 mr-2" for="reason-reject">เลือกเหตุผล</label>
                    <select class="custom-select my-1 mr-sm-2" id="reason-reject">
                        <option value="-99" selected>เอกสารไม่ถูกต้อง/ครบถ้วน</option>
                        <option value="-98">สมาชิกภายในทีมขาดคุณสมบัติ</option>
                    </select>
                    <button class="btn btn-dark btn-sm" id="unapprove" type="button" data-id="{{ $team->team_id }}">
                        ตกลง
                    </button>
                </div>

            </div>
        </div>
    </div>
    @endif
</div>

<script>
    $(document).ready(function () {

        $('#unapprove').click(function(){
            var id = $(this).data('id');
            var status = $('#reason-reject').val();
            reply(id, status)
        });

        $('#approve').click(function(){
            var id = $(this).data('id');
            reply(id, 2)
        });

        $('#show_reason').click(function(){
            $('#reason').show();
        });

        function reply(team_id, status){

            var formData = { 'team_id' : team_id, 'status' : status };

            $.ajax({
                type        : 'POST',
                url         : '{{ url('/e-admin/team/verify') }}',
                data        : formData,
                dataType    : 'json',
                encode      : true
            }).done(function(data) {
                if(data.success){
                    $("#side_modal").modal('hide');
                    $('.modal-backdrop').remove();

                    reloadData(data.data.event_id)
                }
            });
        }

        function reloadData(event_id){
            var data_area = $("#details"),
                url,
                params = { event_id: event_id };

            url = "{{url('e-admin/report/uleague/pitching')}}" + "?" + $.param(params);

            $.ajax({
                type: 'GET',
                url: url,
                success: function (data){
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    data_area.html('เกิดข้อผิดพลาด');
                }
            });
        }
    });
</script>