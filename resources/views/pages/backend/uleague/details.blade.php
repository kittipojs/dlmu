<div class="row">
    <h3>{{ $report_title }}</h3>
    <div class="row">
        <button class="btn btn-apply" onclick="downloadZip()">
            <i class="fa fa-download"></i> ดาวน์โหลด Pitch Deck
        </button>
    </div>
    <table id="tb_user" class="table table-striped table-hover nowrap" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>ชื่อทีม</th>
                <th>หัวหน้าทีม</th>
                <th>มหาวิทยาลัย</th>
                <th>Startup Sector</th>
                <th>สร้างทีมเมื่อ</th>
                <th>สถานะ</th>
            </tr>
        </thead>
        <tobody>
            @foreach( $teams as $item )
            <tr>
                <td>
                    <button type="button" class="btn btn-sm btn-primary view-info-btn" data-target="#side_modal" data-toggle="modal" data-id="{{ $item->user_id }}">
                        <i class="fa fa-eye"></i>
                    </button>
                </td>
                <td>{{ $item->team_name }}</td>
                <td>{{ $item->first_name }} {{ $item->last_name }}</td>
                <td>{{ $item->name }}</td>
                <td>{!! DataHelpers::startupLookup($item->startup_sector) !!}</td>
                <td>{{ $item->created_at }}</td>
                <td>
                    @if($item->status===1)
                    รออนุมัติ
                    @elseif($item->status===2)
                    <i class="fa fa-check"></i> อนุมัติแล้ว
                    @elseif($item->status===-99)
                    <i class="fa fa-close"></i> เอกสารไม่ถูกต้อง/ครบถ้วน
                    @elseif($item->status===-98)
                    <i class="fa fa-close"></i> สมาชิกภายในทีมขาดคุณสมบัติ
                    @endif
                </td>
            </tr>
            @endforeach
        </tobody>
    </table>
</div>


<div class="modal" id="nia-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog modal- vertical-align-center" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modal-header"></h4>
                </div>
                <div class="modal-body" id="modal-info">
                    <!-- Ajax data -->
                </div>
                <div class="modal-footer" id="modal-info">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        drawTable();
        viewInfo();

        setPaginageAction();
        $(".sorting_desc").click(setPaginageAction);
        $(".sorting_asc").click(setPaginageAction);
        $(".sorting").click(setPaginageAction);
        $('.view-info-btn').click(setPaginageAction);
    });

    function drawTable() {
        var table = $('#tb_user').dataTable({
            dom: 'Bfrtip',
            searching: true,
            scrollX: true,
            aaSorting: [],
            pageLength: 50,
            buttons: [{
                extend: 'excel',
                text: 'Export รายงาน',
                title: 'รายชื่อทีม',
                filename: 'รายชื่อทีม',
                exportOptions: {
                    columns: [1,2,3,4,5,6],
                    format: {
                        body: function (data, row, column, node) {
                            return column >= 3 ? data.split("  ")[0] : data;
                        }
                    }
                }
            }],
            "oLanguage": {
                "sSearch": "ค้นหา :"
            }
        });
        $("#tb_user_filter").on("keyup", 'input', function() {
            setPaginageAction()
        });
    }

    function viewInfo(){
        $('.view-info-btn').click(function () {
            var id = $(this).data('id');

            var header_area = $("#modal-header"),
                data_area = $("#modal-info"),
                url;
            url = "{{ url('e-admin/report/uleague/details/team') }}" + "/" + id;
            header_area.html('ข้อมูลทีม');
            data_area.html('Loading . . .');
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });
        });
    }

    function downloadZip(){

        var event_id_id = $('#event_id').val();

        var header_area = $("#modal-header"),
            data_area = $("#modal-info"),
            url;
        url = "{{ url('e-admin/team/download/pitch_deck') }}" + "/" + event_id_id;
        header_area.html('โปรดเลือกสนามแข่งขัน');
        data_area.html('ไม่สามรถดาวโหลด pitch deck ได้โปลดเลือก สนามแข่งขัน');

        if($('#key').val() !== 'pitching_events' || $('#event_id').val() === '--- เลือกกิจกรรม Pitching ---'){
            $('#nia-modal').modal('toggle');
            return
        }
        $('body').loadingModal({text: 'กำลังดาวโหลดไฟล์...'});
        $.ajax({
            type: 'GET',
            url: url,
            success: function (data) {
                $('body').loadingModal('destroy');
                console.log(data.url);
                window.location.href = data.url;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('body').loadingModal('destroy');
                data_area.html('Error Please try again');
            }
        });
    }

    function setPaginageAction() {
        $(".paginate_button").click(setPaginageAction);

        $('.view-info-btn').unbind("click");

        viewInfo();
    }
</script>