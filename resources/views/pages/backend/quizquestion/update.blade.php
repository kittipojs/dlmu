<div class="container">
    <h4 align="center">แก้ไขคำถาม</h4>
    <div class="padding"></div>
    <form class="form-horizontal" action="{{ url('e-admin/question/save') }}" method="post">
        <input type="hidden" name="edit" id="edit" value="true">
        <input type="hidden" name="quiz_id" id="quiz_id" value="{{ $question->quiz_id }}">
        <input type="hidden" name="question_id" id="question_id" value="{{ $question->question_id }}">
        <div class="form-group">
            <label class="control-label col-sm-3" for="quiz_id">ชุดคำถาม </label>
            <div class="col-sm-8">
                <select name="quiz_id_select" id="quiz_id_select" class="form-control" disabled required></select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="question_number">ข้อคำถาม</label>
            <div class="col-sm-8">
                <input type="number" class="form-control" id="question_number" name="question_number" placeholder="ข้อคำถามเป็นตัวเลข เช่น 1,2,3" value="{{ $question->question_number }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="question_title">คำถาม</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="question_title" name="question_title"
                       placeholder="คำถาม" value="{{ $question->question_title }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="tab_logic">เพิ่มคำตอบ</label>
            <div class="col-sm-8">
                <div class="row clearfix">
                    <div class="col-md-12 column">
                        <table class="table table-bordered table-hover" id="tab_logic">
                            <thead>
                                <tr>
                                    <th class="text-center">
                                        คำตอบ
                                    </th>
                                    <th class="text-center">
                                        เลือกข้อถูก
                                    </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append from json data-->
                            </tbody>
                        </table>
                    </div>
                </div>
                <a id="add_row" class="btn btn-black pull-left">เพิ่มคำตอบ</a>
            </div>
        </div>
        <hr />
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
                <button type="submit" class="btn btn-primary">บันทึก</button>
            </div>
        </div>
    </form>
    @include('modal.noresult')
</div>

<script>
    $(document).ready(function () {
        
        $('#quiz_id_select').jCombo("{!! url('select/quiz') !!}", {selected_value: '{{ $question->quiz_id }}'});

        var question_choices = '{{ $question->question_choices }}';
        
        
        var choice = JSON.parse(question_choices.replace(/&quot;/g, '"'));
        
        var countChoice= choice.length;
        var i = countChoice;
        //Loop for append choice list from json
        $.each(choice, function (index, value) {
            $('#tab_logic').append('<tr id="addr' + (index) + '"></tr>');
            var select = (value.correct == 'true') ? 'checked' :'';
            $('#addr' + index).html(
                `<td><input type="checkbox" name='choice_type[${index}]' class="form-control" ${select}/></td><td><input type="text" name='choice_val[${index}]' placeholder='คำตอบ' class="form-control" value='${value.name}'></td><td align='center'><button type='button' class='btn btn-danger' onclick='removeRow($(this))'><i class="fa fa-close"></i></button></td>`
            )
        });


        $("#add_row").click(function () {
            var countRow = $('#tab_logic >tbody >tr').length;
            if (countRow >= 4) {
                alert(`ไม่สามารถเพิ่มมากกว่า 4 ช้อย`);
                return false
            }

            $('#tab_logic').append('<tr id="addr' + i + '">' +
                                   "<td>" +
                                   "<input type=\"checkbox\" " +
                                   "name='choice_type[" + i + "]' " +
                                   "class=\"form-control\"/>" +
                                   "</td>" +
                                   "<td>" +
                                   "<input " +
                                   "type=\"text\" name='choice_val[" + i + "]' " +
                                   "placeholder='คำตอบ' " +
                                   "class=\"form-control\">" +
                                   "</td>" +
                                   "<td align='center'>" +
                                   "<button type='button' class='btn btn-danger' onclick='removeRow($(this))'><i class=\"fa fa-close\"></i></button>" +
                                   '</tr>');
            i++;
        });
    });
    
    function removeRow(row) {
        var countRow = $('#tab_logic >tbody >tr').length;
        if (countRow <= 1) {
            return false
        }
        row.closest('tr').remove();
    }
</script>