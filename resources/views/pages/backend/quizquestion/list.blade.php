
{{--
<div id="accordion">
    @foreach($questions as $question)
    <div class="card">
        <div class="card-header" id="heading-{{$question->question_id}}">
            <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    {{ $question->question_title }}
                </button>
            </h5>
        </div>

        <div id="collapse-{{$question->question_id}}" class="collapse show" aria-labelledby="heading-{{$question->question_id}}" data-parent="#accordion">
            <div class="card-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
        </div>
    </div>
    @endforeach
</div>
--}}


<table class="table table-striped table-hover" id="tb_questions" width="100%">
    <tbody>
        @foreach($questions as $question)
        <tr>
            <td>{{ $question->question_number }}</td>
            <td>{{ $question->question_title }}</td>
            <td data-values="action" data-key="{{ $question->question_id }}">
                <div class=" action ">
                    <div class="dropdown">
                        {{-- <button type="button" data-target="#side-modal" data-toggle="modal" class="btn btn-primary question-edit" data-id="{{ $question->question_id }}">
                            <i class="fa fa-edit"></i> แก้ไข 
                        </button>--}}
                        <a class="btn btn-primary" href="{{url('e-admin/question/update/'.$question->question_id)}}">
                            <i class="fa fa-edit"></i> แก้ไข 
                        </a>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<script>
    $(document).ready(function () {
        drawTable()
        Edit()
    });

    function drawTable() {
        var table = $('#tb_questions').dataTable({
            dom: 'Bfrtip',
            "searching": false,
            "scrollX": true,
            "responsive": true,
            "aaSorting": [],
            columnDefs: [
                { orderable: false, targets: -1 }
            ],
            buttons: [{
                extend: 'excel',
                title: 'ข้อมูลคำถาม',
                filename: 'ข้อมูลคำถาม',
                exportOptions: {
                    columns: ':visible',
                    format: {
                        body: function (data, row, column, node) {
                            return column >= 3 ? data.split("  ")[0] : data;
                        }
                    }
                }
            }],

        });
        $("#tb_question_filter").on("keyup", 'input', function() {
            setPaginageAction()
        });
    }

    function Edit() {
        $('.question-edit').click(function () {
            $('#modal-header').text('อัพเดทข้อมูล');
            var id = $(this).data('id');
            var data_area = $("#modal-info"),
                url;
            url = "{{ url('e-admin/question/update') }}" + "/" + id;
            data_area.html('Loading . . .');
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });
        })

    }
</script>