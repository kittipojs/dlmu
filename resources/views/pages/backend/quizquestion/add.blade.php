<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="text-center">แบบทดสอบ : {{ $quiz->quiz_title}}</h3>
        </div>
    </div>
    <hr>
    <div class="text-left">
        {{-- <a href="{{ url('/e-admin/quiz/update/'.$quiz->quiz_id) }}" class="btn btn-default">แก้ไขข้อมูลแบบทดสอบ</a> --}}
        <a href="{{ url('e-admin/course/manage/'.$quiz->course_id) }}" class="btn btn-warning"><- กลับไปยังคอร์สเรียน </a>
    </div>
    <hr>
    <div class="padding"></div>
    <div class="row">
        <div class="col-sm-6">

            <div class="panel panel-default">
                <div class="panel-heading" align="center">คำถามทั้งหมด</div>
                <div class="panel-body">
                    <table class="table table-hover" id="tb_questions" width="100%">
                        <tbody>
                            @foreach($questions as $question)
                            <tr>
                                {{--<td>{{ $question->question_number }}</td>--}}
                                <td>{{ $question->question_title }}</td>
                                <td data-values="action" data-key="{{ $question->question_id }}">
                                    <div class=" action ">
                                        <div class="dropdown">
                                            {{-- <button type="button" data-target="#side-modal" data-toggle="modal" class="btn btn-primary question-edit" data-id="{{ $question->question_id }}">
                                            <i class="fa fa-edit"></i> แก้ไข 
                                            </button>--}}
                                            <a class="btn btn-primary" href="{{url('e-admin/question/update/'.$question->question_id)}}">
                                                <i class="fa fa-edit"></i> แก้ไข 
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                            @if(count($questions)==0)
                            <p class="text-center">ไม่มีคำถาม <br> คุณสามารถเพิ่มคำถามได้จากกล่องเพิ่มคำถามใหม่ด้านขวามือ</p>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading" align="center">เพิ่มคำถามใหม่</div>
                <div class="panel-body">

                    <form class="form-horizontal" action="{{ url('e-admin/question/save') }}" method="post">
                        @if(!isset($quiz_id))
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="quiz_id">ชุดคำถาม </label>
                            <div class="col-sm-10">
                                <select name="quiz_id" id="quiz_id" class="form-control" required></select>
                            </div>
                        </div>
                        @else

                        <input type="hidden" name="quiz_id" id="quiz_id" value="{{ $quiz_id }}">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="quiz_id">ชุดคำถาม </label>
                            <div class="col-sm-10">
                                {{ $quiz->quiz_title }}
                            </div>
                        </div>
                        @endif
                        {{--
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="question_number">ข้อคำถาม</label>
                            <div class="col-sm-6">
                                <input type="number" class="form-control" id="question_number" name="question_number" placeholder="ข้อคำถามเป็นตัวเลข เช่น 1,2,3">
                            </div>
                        </div>
                        --}}
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="question_title">คำถาม</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="question_title" name="question_title"
                                       placeholder="คำถาม" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="tab_logic">เพิ่มคำตอบ</label>
                            <div class="col-sm-10">
                                <div class="row clearfix">
                                    <div class="col-md-12 column">
                                        <table class="table table-bordered table-hover" id="tab_logic">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">
                                                        คำตอบ
                                                    </th>
                                                    <th class="text-center">
                                                        เลือกข้อถูก
                                                    </th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr id='addr0'>
                                                    <td>
                                                        <input type="checkbox" name='choice_type[0]' class="form-control"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" name='choice_val[0]' placeholder='คำตอบ' class="form-control"/>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr id='addr1'></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <a id="add_row" class="btn btn-black pull-left">เพิ่มคำตอบ</a>
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">บันทึกคำถาม</button>
                                {{-- <a href="{{ url('question/view') }}" class="btn btn-default">Back</a> --}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('modal.noresult')
</div>
<script>
    $(document).ready(function () {

        $('#quiz_id').jCombo("{!! url('select/quiz') !!}");

        var i = 1;

        $("#add_row").click(function () {
            var countRow = $('#tab_logic >tbody >tr').length;
            if (countRow > 4) {
                $('#modal-header').html('');
                $('#modal-info').html('<p>ไม่สามารถเพิ่มคำตอบได้มากกว่า 4 ข้อ</p>');
                $('#noResult-modal').modal('toggle');
                return false
            }

            $('#tab_logic').append('<tr id="addr' + (i + 1) + '">' +
                                   "<td>" +
                                   "<input type=\"checkbox\" " +
                                   "name='choice_type[" + i + "]' " +
                                   "class=\"form-control\"/>" +
                                   "</td>" +
                                   "<td>" +
                                   "<input " +
                                   "type=\"text\" name='choice_val[" + i + "]' " +
                                   "placeholder='คำตอบ' " +
                                   "class=\"form-control\">" +
                                   "</td>" +
                                   "<td align='center'>" +
                                   "<button type='button' class='btn btn-danger' onclick='removeRow($(this))'><i class=\"fa fa-close\"></i></button>" +
                                   '</tr>');
            i++;
        });
    });

    function removeRow(row) {
        var countRow = $('#tab_logic >tbody >tr').length;
        if (countRow <= 1) {
            return false
        }
        row.closest('tr').remove();
    }
</script>