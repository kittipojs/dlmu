<table class="table table-hover" id="tb_question" width="100%">
    <thead>
    <tr>
        <th>ชุดคำถาม</th>
        <th>ข้อคำถาม</th>
        <th>คำถาม</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($questions as $question)
        <tr>
            <td>{{ $question->quiz_title }}</td>
            <td>{{ $question->question_number }}</td>
            <td>{{ $question->question_title }}</td>
            <td data-values="action" data-key="{{ $question->question_id }}">
                <div class=" action ">
                    <div class="dropdown">
                        <button type="button" data-target="#nia-modal" data-toggle="modal"
                                class="btn btn-primary edit" data-id="{{ $question->question_id }}">
                            <i class="fa fa-edit"></i> Edit </button>
                    </div>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@include('modal.modal')
<script>
    $(document).ready(function () {
        drawTable()
        Edit()
    });

    function drawTable() {
        var table = $('#tb_question').dataTable({
            dom: 'Bfrtip',
            "searching": true,
            "scrollX": true,
            "responsive": true,
            "aaSorting": [],
            columnDefs: [
                { orderable: false, targets: -1 }
            ],
            buttons: [{
                extend: 'excel',
                title: 'ข้อมูลคำถาม',
                filename: 'ข้อมูลคำถาม',
                exportOptions: {
                    columns: ':visible',
                    format: {
                        body: function (data, row, column, node) {
                            return column >= 3 ? data.split("  ")[0] : data;
                        }
                    }
                }
            }],

        });
        $("#tb_question_filter").on("keyup", 'input', function() {
            setPaginageAction()
        });
    }

    function Edit() {
        $('.edit').click(function () {
            $('#modal-header').text('อัพเดทข้อมูล');
            var id = $(this).data('id');
            var data_area = $("#modal-info"),
                url;
            url = "{{ url('e-admin/question/update') }}" + "/" + id;
            data_area.html('Loading . . .');
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });
        })

    }
</script>