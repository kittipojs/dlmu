<div class="row">
    <div class="col-sm-8 padding">
        <div class="panel panel-primary">
            <div class="panel-heading" align="center">ภาพรวมสมาชิก</div>
            <div class="panel-body" style="min-height: 190px;">
                <div class="row text-center">
                    <div class="col-sm-3">
                        <h3> {{ $today_registerd }} </h3>
                        <span>
                            สมาชิกใหม่ (วันนี้)
                        </span>
                    </div>
                    <div class="col-sm-3">
                        <h3> {{ number_format($nia_user) }} </h3>
                        <span>
                            สมาชิกทั้งหมด
                        </span>
                    </div>
                    <div class="col-sm-3">
                        <h3> {{ number_format($uleague_user) }} </h3>
                        <span>
                            สมัคร U League {{ DataHelpers::year() }} แล้ว
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading" align="center">ภาพรวมของทีม</div>
            <div class="panel-body" style="min-height: 190px">
                <div class="row text-center">
                    <div class="row text-center">
                        <div class="col-sm-3">
                            <h3> {{ number_format($all_team) }} </h3>
                            <small>
                                ทีมในระบบทั้งหมด
                            </small>

                            <p>
                                <a class="btn btn-xs btn-outline-info" href="{{ url('/e-admin/team/search') }}">ดูรายการ</a>
                            </p>
                        </div>
                        {{--
                        <div class="col-sm-3">
                            <h3> {{ number_format($all_apply_team) }} </h3>
                            <small>
                                ส่งเข้าแข่งขันแล้ว
                            </small>
                        </div>
                        --}}
                        <div class="col-sm-3">
                            <h3> {{ number_format($all_waiting_team) }} </h3>
                            <small>
                                รออนุมัติ
                            </small>
                            <p>
                                <a class="btn btn-xs btn-outline-info" href="{{ url('/e-admin/team/approve') }}">ดูรายการ</a>
                            </p>
                        </div>
                        <div class="col-sm-3">
                            <h3> {{ number_format($all_approve_team)}} </h3>
                            <small>
                                ผ่านการอนุมัติ
                            </small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4 padding">
        <div class="panel panel-primary">
            <div class="panel-heading" align="center">รายชื่อสมาชิกล่าสุด</div>
            <div class="panel-body" style="min-height: 190px">
                <div class="row text-left">
                    <table class="table" width="100%">
                        <tbody>
                        @foreach($lasted_register as $user)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $user->name_title }}</td>
                                <td>{{ $user->first_name }}</td>
                                <td>{{ $user->last_name }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading" align="center">รายชื่อทีมล่าสุด</div>
            <div class="panel-body" style="min-height: 190px">
                <div class="row text-left">
                    <table class="table" width="100%">
                        <tbody>
                        @foreach($lasted_team as $team)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $team->team_name }}</td>
                                <td>{{ $team->name }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {{--
        <div class="col-sm-2 padding">
            <a class="btn btn-block btn-md btn-success" href="{{ url('/e-admin/user/report') }}">
                <span class="texto_grande"><i class="fa fa-list"></i> รายงานสมาชิก</span></a>
        </div>
        <div class="col-sm-2 padding">
            <a class="btn btn-block btn-md btn-success" href="{{ url('/e-admin/user/search') }}">
                <span class="texto_grande"><i class="fa fa-search"></i> ค้นหาสมาชิก</span></a>
        </div>
        <div class="col-sm-2 padding">
            <a class="btn btn-block btn-md btn-success" href="{{ url('/e-admin/team/search') }}">
                <span class="texto_grande"><i class="fa fa-search"></i> ค้นหาทีม</span></a>
        </div>
        <div class="col-sm-2 padding">
            <a class="btn btn-block btn-md btn-success" href="{{ url('/e-admin/pitching/report') }}">
                <span class="texto_grande"><i class="fa fa-user"></i> ตรวจสอบทีม</span></a>
        </div>
        --}}
    </div>
</div>