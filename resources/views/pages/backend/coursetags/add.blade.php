<div class="container">
    <form class="form-horizontal" method="post" action="{{ url('e-admin/course/tag/save') }}" role="form" enctype="multipart/form-data">
        <input type="hidden" value="{{$course_id}}" name="course_id" id="course_id" />
        <div class="form-group">
            <label class="control-label col-sm-3" for="quiz_title">ชื่อหมวดหมู่ * </label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="tag_name" name="tag_name" placeholder="ชื่อหมวดหมู่" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-7">
                <button type="submit" class="btn btn-primary">เพิ่ม</button>
            </div>
        </div>
    </form>
</div>
<script>
    $(document).ready(function () {

    });
</script>