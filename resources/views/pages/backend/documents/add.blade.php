<div class="container">
    <h4 align="center">{{ $title }}</h4>
    <div class="padding"></div>
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <form id="addForm" class="form-horizontal" action="{{ url('e-admin/document/save') }}" method="post">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="question">ชื่อเอกสาร </label>
                    <div class="col-sm-8">
                        <input type="text" name="doc_title" id="doc_title" class="form-control" placeholder="ชื่อเอกสารเช่น คู่มือการใช้งาน" required>
                    </div>
                </div>
                <div class="form-group">
                    <input type="hidden" name="doc_url" id="doc_url" >
                    <label class="control-label col-sm-2" for="status"> ไฟล์ </label>
                    <div class="col-sm-8">
                        <div id="document" class="dropzone"></div>

                        <p class="padding">หรือใส่ url ในช่องด้านล่าง</p>

                        <input type="text" class="form-control mb-2 mr-sm-2" name="doc_link"
                               id="doc_link" placeholder="https://drive.google.com"
                               style="width:400px;">
                    </div>
                </div>
                <hr class="divider">
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-8">
                        <button type="submit" class="btn btn-success">ตกลง</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>

    Dropzone.autoDiscover = false;

    $(document).ready(function () {

        $("div#document").dropzone({
            maxFilesize: 104857600,
            timeout: 1800000,
            url: "{{url('e-admin/document/upload')}}",
            dictDefaultMessage: '<i class="fas fa-upload"></i> เลือกเอกสาร .pdf, .doc, .docx จากเครื่องคอมพิวเตอร์ของคุณ',
            success: function (file, response) {
                if(response.success) {
                    $('#doc_url').val(response.file)
                }
            },
            acceptedFiles: ".pdf, .doc, .docx"
        });

        $('#addForm').submit(function (e) {

            if($('#doc_url').val()=='' && $('#doc_link').val()==''){
                $.toast({
                    heading: 'เกิดข้อผิดพลาด',
                    text: 'กรุณาเลือกไฟล์เอกสาร',
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 2500,
                    stack: 6
                });
            }
            else{
                $(this).submit();
            }


            e.preventDefault();
        })


    })
</script>