<div class="container">
    <div class="row">
        <div class="col-sm-4 pull-left">
            <a href="{{ url('e-admin/document/add') }}" class="btn btn-primary"><i class="fa fa-plus"></i> เพิ่มเอกสาร</a>
        </div>
        <div class="col-sm-4" align="center">

        </div>
        <div class="col-sm-4" align="right">

        </div>

    </div>
    <div class="row padding" id="tb_area">

        <table class="table table-hover" id="tb_qa" width="100%">
            <thead>
                <tr>
                    <th></th>
                    <th>ชื่อเอกสาร</th>
                    <th>URL</th>
                </tr>
            </thead>
            <tbody>
                @foreach( $documents as $item)
                <tr>
                    <td data-values="action">
                        <div class=" action ">
                            <div class="dropdown">
                                <button type="button" class="btn btn-danger delete" data-id="{{ $item->doc_id }}" data-title="{{ $item->doc_title }}">
                                    <i class="fa fa-trash"></i> ลบ
                                </button>
                            </div>
                        </div>
                    </td>
                    <td>
                        {{ $item->doc_title }}
                    </td>
                    <td>
                        <a href="{{ $item->doc_url }}" target="_blank">{{ $item->doc_url }}</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @include('modal.modal')
    </div>
</div>

<script>
    var table;
    $(document).ready(function () {
        table = $('#tb_qa').dataTable({
            dom: 'Bfrtip',
            "searching": true,
            "responsive": true,
            "aaSorting": [],
            columnDefs: [
                {orderable: false, targets: -1}
            ],
            buttons: [{
                extend: 'excel',
                title: 'ข้อมูลคำถาม',
                filename: 'ข้อมูลคำถาม',
                exportOptions: {
                    columns: [0,1],
                    format: {
                        body: function (data, row, column, node) {
                            return column >= 3 ? data.split("  ")[0] : data;
                        }
                    }
                }
            }],
        });

        initDelete();

    });

    function initDelete() {
        $('.delete').click(function () {

            $('#sximo-modal-header').text('อัพเดทข้อมูล');

            var doc_id = $(this).data('id');
            var title = $(this).data('title');


            var txt;
            var r = confirm('คุณต้องการลบเอกสาร '+title+' ใช่หรือไม่?');
            if (r == true) {
                $.ajax({
                    type: 'POST',
                    url: '{{ url('e-admin/document/delete/') }}',
                    data: {
                    'doc_id': doc_id
                },
                success: function (data) {
                    if(data.success){
                        $.toast({
                            heading: data.status,
                            text: data.message,
                            position: 'bottom-right',
                            icon: data.status,
                            hideAfter: 2500,
                            stack: 6
                        });
                        location.reload();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                        $.toast({
                            heading: 'เกิดข้อผิดพลาด',
                            text: 'โปรดลองใหม่อีกครั้ง',
                            position: 'bottom-right',
                            icon: 'error',
                            hideAfter: 2500,
                            stack: 6
                        });
                 }
            });
        }


                           })

    }

</script>