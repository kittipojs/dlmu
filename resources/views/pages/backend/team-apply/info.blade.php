<div>
    <div class="row">
        <div class="col-sm-4">
            ทีม
        </div>
        <div class="col-sm-8">
            {{ $team->team_name }}
        </div>
    </div>

    <div class="padding"></div>
    <div class="row">
        <div class="col-sm-4">
            ข้อมูลเกี่ยวกับโครงการ
        </div>
        <div class="col-sm-8">
            {{ $team->team_info }}
        </div>
    </div>

    <div class="padding"></div>
    <div class="row">
        <div class="col-sm-4">
            ส่งในนามมหาวิทยาลัย
        </div>
        <div class="col-sm-8">
            {{ $team->name }}
        </div>
    </div>

    <div class="padding"></div>
    <div class="row">
        <div class="col-sm-4">
            สนามแข่งขัน
        </div>
        <div class="col-sm-8">
            {{ $pitching_event->name }}
        </div>
    </div>

    <div class="padding"></div>
    <div class="row">
        <div class="col-sm-4">
            ไฟล์ Proposal
        </div>
        <div class="col-sm-8">
            <a href="{{ asset($team->file_url) }}" class="btn-link" target="_blank">
                <i class="fa fa-download"></i> ดาวน์โหลด
            </a>
        </div>
    </div>

    <div class="padding"></div>
    <form action="{{ url('e-admin/team/edit-apply-status') }}" class="form-horizontal" id="edit-team-status" method="post" role="form" enctype="multipart/form-data">
        <div class="row">
            <div class="col-sm-4">
                สถานะ
            </div>
            <div class="col-sm-4">
                <input type="hidden" id="id" name="id" value="{{ $apply->id }}" />
                <select class="custom-select form-control" id="status" name="status">
                    {{-- <option value="0">ยังไม่สมบูรณ์</option> --}}
                    <option value="1">รออนุมัติ</option>
                    <option value="2">อนุมัติ</option>
                    <option value="-99">เอกสารไม่ถูกต้อง/ครบถ้วน</option>
                    <option value="-98">สมาชิกภายในทีมขาดคุณสมบัติ</option>
                </select>
            </div>
            <div class="col-sm-2">
                <button class="btn btn-success" type="submit">
                    บันทึก
                </button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#status').val({{ $apply->status }})

        viewTeamInfo()
    });

    function viewTeamInfo(){
        $('.view-info-btn').click(function () {
            var id = $(this).data('id');

            var header_area = $("#modal-header"),
                data_area = $("#modal-info"),
                url;
            url = "{{ url('e-admin/team/results/info') }}" + "/" + id;
            header_area.html('ข้อมูลทีม');
            data_area.html('Loading . . .');
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });
        });
    }

</script>