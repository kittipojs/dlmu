<div class="row">
    <div class="row text-center">
        <h3>
            <i class="fa fa-search"></i> ผลการค้นหา
            @if(array_key_exists($event_id, $pitching_events))
                สนามแข่งขัน {{ $pitching_events[$event_id] }}
            @else
                ทั้งหมด
            @endif
        </h3>
    </div>
    <div class="row">
        <div class="col-sm-offset-3 col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading" align="center">ภาพรวมผลการส่งแข่งขัน</div>
                <div class="panel-body">
                    <div class="row text-center">
                        <div class="col-sm-3">
                            <h3> {{$team_total}} </h3>
                            <small>
                                ส่งทั้งหมด
                            </small>
                        </div>
                        <div class="col-sm-3">
                            <h3> {{$waiting}} </h3>
                            <small>
                                รออนุมัติ
                            </small>
                        </div>
                        <div class="col-sm-3">
                            <h3> {{$approved}} </h3>
                            <small>
                                อนุมัติแล้ว
                            </small>
                        </div>
                        <div class="col-sm-3">
                            <h3> {{$fail}} </h3>
                            <small>
                                ไม่ผ่านการอนุมัติ
                            </small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($show_download_btn)
        <div class="text-right">
            <a href="{{url('e-admin/team/download/pitch_deck/'.$event_id)}}" class="btn btn-sm btn-dark">ดาวน์โหลด
                Proposal</a>
        </div>
    @endif
    <div class="padding">
        <form class="form-horizontal">
            <div class="form-group">
                <label for="status_filter" class="col-sm-offset-8 col-sm-2 control-label">สถานะ :</label>
                <div class="col-sm-2">
                    <select class="form-control" name="status_filter" id="status_filter">
                        <option value="">ทั้งหมด</option>
                        <option value="รออนุมัติ">รออนุมัติ</option>
                        <option value="อนุมัติแล้ว">อนุมัติแล้ว</option>
                        <option value="ไม่ผ่านอนุมัติ">ไม่ผ่านอนุมัติ</option>
                        <option value="เอกสารไม่ถูกต้อง/ครบถ้วน">เอกสารไม่ถูกต้อง/ครบถ้วน</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="input_filter" class="col-sm-offset-8 col-sm-2 control-label">ค้นหา :</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" id="input_filter" name="input_filter">
                </div>
            </div>
        </form>
    </div>
</div>
<table id="tb_user" class="table table-hover nowrap" width="100%">
    <thead>
    <tr>
        <th></th>
        <th>#</th>
        <th>ชื่อทีม</th>
        <th>หัวหน้าทีม</th>
        <th>มหาวิทยาลัย</th>
        <th>Startup Sector</th>
        <th>สนามแข่งขัน</th>
        <th>ส่งคำขอเมื่อ</th>
        <th>สถานะ</th>
    </tr>
    </thead>
    <tobody>
        @foreach( $apply as $item )
            <tr>
                <td>
                    <button type="button" class="btn btn-sm btn-primary view-apply-btn" data-target="#side_modal"
                            data-toggle="modal" data-id="{{ $item->user_id }}">
                        <i class="fa fa-eye"></i>
                    </button>
                </td>
                <td>{{-- $item->team_id --}}</td>
                <td>{{ $item->team_name }}</td>
                <td>{{ $item->first_name }} {{ $item->last_name }}</td>
                <td>{{ $item->name }}</td>
                <td>{!! DataHelpers::startupLookup($item->startup_sector) !!}</td>
                <td>
                    @if(array_key_exists($item->reference_id, $pitching_events))
                        {{ $pitching_events[$item->reference_id] }}
                    @else
                        ไม่สามารถระบุสนามแข่งได้
                    @endif
                </td>
                <td>{{ $item->created_at }}</td>
                @php
                    if($item->status===0){
                        $status = 'ยังไม่สมบูรณ์';
                        $filter = 'ไม่ผ่านอนุมัติ';
                    }
                    elseif ($item->status===1){
                        $status = 'รออนุมัติ';
                        $filter = 'รออนุมัติ';
                    }
                    elseif ($item->status===2){
                        $status = 'อนุมัติแล้ว';
                        $filter = 'อนุมัติแล้ว';
                    }
                    elseif ($item->status===-99){
                        $status = 'เอกสารไม่ถูกต้อง/ครบถ้วน';
                        $filter = 'เอกสารไม่ถูกต้อง/ครบถ้วน';
                    }
                    else{
                        $status = 'สมาชิกภายในทีมขาดคุณสมบัติ';
                        $filter = 'ไม่ผ่านอนุมัติ';
                    }

                @endphp
                <td data-order="{{ $item->data_status }}" data-filter="{{ $filter }}">
                <!--
                    @if($item->status===0)
                    {{--<i class="fas fa-exclamation"></i>--}} ยังไม่สมบูรณ์
                    @elseif($item->status===1)
                    {{--<i class="far fa-clock"></i>--}} รออนุมัติ
                    @elseif($item->status===2)
                    {{--<i class="far fa-smile"></i>--}} อนุมัติแล้ว
                    @elseif($item->status===-99)
                    {{--<i class="far fa-frown"></i>--}}  เอกสารไม่ถูกต้อง/ครบถ้วน
                    @elseif($item->status===-98)
                    {{--<i class="far fa-frown"></i>--}} สมาชิกภายในทีมขาดคุณสมบัติ
                    @endif
                        -->
                    {{ $status }}
                </td>
            </tr>
        @endforeach
    </tobody>
</table>
</div>

<script>
    $(document).ready(function () {

        drawTable();
        viewInfo();

        setPaginageAction();

        $(".sorting_desc").click(setPaginageAction);
        $(".sorting_asc").click(setPaginageAction);
        $(".sorting").click(setPaginageAction);
    });

    function drawTable() {
        var table = $('#tb_user').dataTable({
            dom: 'Bfrtip',
            searching: true,
            scrollX: true,
            order: [[6, "desc"]],
            pageLength: 50,
            buttons: [{
                extend: 'excel',
                text: 'Export รายงาน',
                title: 'รายชื่อทีม',
                filename: 'รายชื่อทีม',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6, 7, 8],
                    format: {
                        body: function (data, row, column, node) {
                            return column >= 3 ? data.split("  ")[0] : data;
                        }
                    }
                }
            }],
            "oLanguage": {
                "sSearch": "ค้นหา :"
            }
        });
        $("#tb_user_filter").hide();
        $("#tb_user_filter").on("keyup", 'input', function () {
            setPaginageAction()
        });

        $('#status_filter').on('change', function () {
            table.fnFilter($(this).val());
        });

        $('#input_filter').on('keyup', function () {
            table.fnFilter($(this).val());
        });
    }

    function viewInfo() {
        $('.view-apply-btn').click(function () {
            var id = $(this).data('id');

            var header_area = $("#modal-header"),
                data_area = $("#modal-info"),
                url;
            //url = "{{ url('e-admin/team/apply-info') }}" + "/" + id;
            url = "{{ url('e-admin/team/results/info') }}" + "/" + id;
            header_area.html('ข้อมูลทีม');
            data_area.html('Loading . . .');
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });
        });
    }

    function setPaginageAction() {
        $(".paginate_button").click(setPaginageAction);
        $('.view-apply-btn').unbind("click");
        viewInfo();
    }
</script>
