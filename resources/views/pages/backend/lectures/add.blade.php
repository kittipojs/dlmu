<style>
    .imageBox {
        position: relative;
        height: 500px;
        width: 100%;
        border: 1px solid #aaa;
        background: #fff;
        overflow: hidden;
        background-repeat: no-repeat;
        cursor: move;
    }

    .imageBox .thumbBox {
        position: absolute;
        top: 50%;
        left: 50%;
        width: 200px;
        height: 200px;
        margin-top: -100px;
        margin-left: -100px;
        box-sizing: border-box;
        border: 1px solid rgb(102, 102, 102);
        box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
        background: none repeat scroll 0% 0% transparent;
    }

    .imageBox .spinner {
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        text-align: center;
        line-height: 400px;
        background: rgba(0, 0, 0, 0.7);
    }
</style>
<div class="container">
    <div class="sbox large-box">
        <div class="sbox-title">
            <h4 align="center">{{ $title }}</h4>
        </div>
        <div class="sbox-content">
            <form class="form-horizontal" method="post" action="{{ url('e-admin/lectures/save') }}" role="form"
                  enctype="multipart/form-data">
                <input type="hidden" name="lecture_id" value="{{ $lecture_id }}">
                <input type="hidden" name="course_id" value="{{ $to }}">
                <input type="hidden" name="media_type" value="{{ $media_type }}">
                <div class="form-group">

                    @if($media_type=='youtube')
                        <label class="control-label col-sm-3" for="quiz_title">URL ของ Youtube * </label>
                        <div class="col-sm-7">
                            <div id="source-youtube" class="media_source">
                                <label class="sr-only" for="youtube-link">URL</label>
                                <input type="text" class="form-control mb-2 mr-sm-2" name="media_src" id="media_src"
                                       placeholder="https://youtu.be/nSOG99qClU8">
                            </div>
                        </div>
                    @elseif($media_type=='streaming')
                        <label class="control-label col-sm-3">URL * </label>
                        <div class="col-sm-7">
                            <div id="source-link" class="media_source">
                                <input type="text" class="form-control mb-2 mr-sm-2" name="media_src" id="media_src"
                                       placeholder="eg. https://content.jwplatform.com/manifests/yp34SRmf.m3u8">
                                <p>
                                    <br>
                                    <small>
                                        รองรับ Adobe HDS (Adobe Flash Player), Apple HLS (iPhone, iPad, iPod touch,
                                        QuickTime, and more), Microsoft Smooth Streaming (Microsoft Silverlight and
                                        more),
                                        MPEG-DASH streaming (DASH clients), Adobe RTMP (Adobe Flash Player), RTSP/RTP
                                        (QuickTime, VLC, 3GPP devices, set-top boxes, and more)
                                    </small>
                                </p>
                            </div>
                        </div>
                    @elseif($media_type=='mp4')
                        <input type="hidden" id="media_duration" name="media_duration"/>
                        <label class="control-label col-sm-3">เลือกไฟล์ * </label>
                        <div class="col-sm-7">
                            <div id="source-mp4" class="media_source">
                                <div id="video-preview" class="dropzone"></div>
                            </div>
                            <input type="hidden" id="media_src" name="media_src" value="">
                            <div id="mp4Player" style="display:none;">

                                <div class="padding"></div>
                                <div id="player"></div>
                                <div class="padding"></div>
                                {{--
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="start">เริ่ม * </label>
                                    <div class="col-sm-3">
                                        <div class="input-group bootstrap-timepicker timepicker">
                                            <input id="timepickerStart" type="text" class="form-control input-small timepicker2" name="start">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-time"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <label class="control-label col-sm-1" for="end">จบ * </label>
                                    <div class="col-sm-3">
                                        <div class="input-group bootstrap-timepicker timepicker">
                                            <input id="timepickerEnd" type="text" class="form-control input-small timepicker2" name="end">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-time"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                --}}
                            </div>
                        </div>
                    @elseif($media_type=='album')
                        <style>
                            .inventory_related thead {
                                display: none;
                            }
                        </style>
                        <input type="hidden" id="media_duration" name="media_duration"/>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="quiz_title">เลือกวิดีโอ * </label>
                            <div class="col-sm-12" style="">
                                <div class="row">
                                    <div class="col-sm-4" style=" max-height: 450px; overflow: scroll;">
                                        <table class="table table-hover inventory_related" id="tb_list_video"
                                               width="100%">
                                            <thead>
                                            <th>header</th>
                                            </thead>
                                            <tbody>
                                            @foreach($video_list as $list)
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" class="form-check-input"
                                                               id="media_src[{{$list->id}}]" name="media_src"
                                                               data-src="{{$list->media_src}}"
                                                               value="{{ $list->media_src }}"/>
                                                        <label class="form-check-label" for="media_src[{{$list->id}}]">
                                                            &nbsp;&nbsp; <i
                                                                    class="fa fa-file-video-o"></i> {{ $list->lecture_title }}
                                                        </label>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-sm-8" style="">
                                        <div id="mp4Player" style="display:none;">
                                            <div class="padding"></div>
                                            <div id="player"></div>
                                            <div class="padding"></div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="quiz_title">ความยาวของวิดีโอ
                                                    * </label>
                                                <div class="col-sm-9">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio1" name="media_length"
                                                               value="0" class="custom-control-input" checked>
                                                        <label class="custom-control-label" for="customRadio1">ใช้ทั้งหมด</label>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio2" name="media_length"
                                                               value="1" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio2">ใช้บางส่วน</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="mediaTrimmer" class="form-group" style="display: none;">

                                                <label class="control-label col-sm-3" for="start">เริ่ม * </label>
                                                <div class="col-sm-3">
                                                    <div class="input-group bootstrap-timepicker timepicker">
                                                        <input id="timepickerStart" type="text"
                                                               class="form-control input-small timepicker2"
                                                               name="start">
                                                        <span class="input-group-addon">
                                                            <i class="glyphicon glyphicon-time"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <label class="control-label col-sm-1" for="end">จบ * </label>
                                                <div class="col-sm-3">
                                                    <div class="input-group bootstrap-timepicker timepicker">
                                                        <input id="timepickerEnd" type="text"
                                                               class="form-control input-small timepicker2"
                                                               name="end">
                                                        <span class="input-group-addon">
                                                            <i class="glyphicon glyphicon-time"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="start">เริ่ม * </label>
                            <div class="col-sm-3">
                                <div class="input-group bootstrap-timepicker timepicker">
                                    <input id="start" type="text" class="form-control input-small timepicker2" name="start">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-time"></i>
                                    </span>
                                </div>
                            </div>
                            <label class="control-label col-sm-1" for="end">จบ * </label>
                            <div class="col-sm-3">
                                <div class="input-group bootstrap-timepicker timepicker">
                                    <input id="end" type="text" class="form-control input-small timepicker2" name="end">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-time"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        --}}
                    @endif
                </div>

                <div class="padding"></div>
                <hr/>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="quiz_title">ชื่อบทเรียน * </label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="lecture_title" name="lecture_title"
                               placeholder="ชื่อบทเรียน" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="quiz_title">คำนำ * </label>
                    <div class="col-sm-7">
                        <textarea rows="3" class="form-control" id="lecture_intro" name="lecture_intro"
                                  placeholder="คำนำของบทเรียน ความยาวไม่เกิน 500 ตัวอักษร" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="quiz_title">รายละเอียด * </label>
                    <div class="col-sm-7">
                        <textarea rows="7" class="form-control" id="lecture_description" name="lecture_description"
                                  placeholder="รายละเอียดของบทเรียน" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="file">ภาพปก * </label>
                    <div class="col-sm-7">
                        <!--
<div class="row">
<div class="imageBox">
<div class="thumbBox">Upload Your Cover</div>
<div class="spinner" style="display: none">Loading...</div>
</div>
</div>
<div class="row">
<div class="action">
<input class="form-control" type="file" id="file" name="file" style="float:left; width: 250px">
<input class="btn btn-default btn-sm" type="button" id="btnCrop" value="Crop"
style="float: right">
<input class="btn btn-default btn-sm" type="button" id="btnZoomIn" value="+"
style="float: right">
<input class="btn btn-default btn-sm" type="button" id="btnZoomOut" value="-"
style="float: right">
</div>
</div>
-->
                        <input type="file" name="image" id="image" class="inputfile"/>
                        <label for="avatar"><i class="fa fa-upload"></i> เลือกไฟล์ (รูปภาพ16:9) </label>
                    </div>
                </div>
                <!--
<div class="form-group">
<input type="hidden" name="cropCover" id="cropCover">
<label class="control-label col-sm-3" for="file">ภาพปกตัวอย่าง * </label>
<div class="cropped">

</div>
</div>
-->
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-7">
                        <button type="submit" class="btn btn-primary">บันทึก</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

    Dropzone.autoDiscover = false;

    $(document).ready(function () {

        //$('.media_source').hide();
        //$('#source-youtube').show();

        var timepicker_start = $('#timepickerStart').timepicker({
            showSeconds: true,
            showMeridian: false,
            format: 'hh:mm:ss',
            minuteStep: 1,
            secondStep: 1,
            autoclose: true,
            defaultTime: '00:00:00'
        });

        var timepicker_end = $('#timepickerEnd').timepicker({
            showSeconds: true,
            showMeridian: false,
            format: 'hh:mm:ss',
            minuteStep: 1,
            secondStep: 1,
            autoclose: true,
            defaultTime: '00:00:00'
        });

        $('#lecture_description').summernote({
            height: 150, toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'hr']],
            ]
        });

                @if($media_type=='mp4')
        var duration = 0;
        var myDropzone = new Dropzone("div#video-preview", {
            url: "{{url('e-admin/lectures/upload')}}?lecture_id={{$lecture_id}}&filename={{$file_name}}",
            maxFilesize: 104857600,
            timeout: 3600000,
            method: "POST",
            chunking: true,
            chunkSize: 3000000,
            parallelChunkUploads: true,
            dictDefaultMessage: '<i class="fas fa-upload"></i> เลือกไฟล์วิดีโอ .mp4 จากเครื่องคอมพิวเตอร์ของคุณ',
            acceptedFiles: ".mp4",
            uploadMultiple: false,
            /*
            success: function (file, response) {

                alert(JSON.stringify(response))
                setTimeout(function(){
                    var playerInstance = jwplayer("player").setup({
                        file : response.video_path,
                        aspectratio: "16:9",
                        controls: true,
                        width: "100%",
                        autostart: "true"
                    });

                    playerInstance.onPlay(function () {

                        if (duration == 0) {
                            // we don't have a duration, so it's playing so we can discover it...
                            duration = playerInstance.getDuration();
                            playerInstance.stop();
                            //alert(this.getDuration())
                        }

                    });
                }, 3000)


                setTimeout(function () {

                    $('#media_duration').val(duration);

                    var hours = Math.floor(duration / 3600);
                    duration %= 3600;
                    var minutes = Math.floor(duration / 60);
                    var seconds = Math.floor(duration % 60);

                    //console.log(hours + ':' + minutes + ':' + seconds)
                    timepicker_end.timepicker().timepicker('setTime', hours + ':' + minutes + ':' + seconds);


                }, 5000);


                $('#media_src').val(response.url)
                $('#mp4Player').show();
            }
            */
        });


        myDropzone.on("success", function (file, response) {

            setTimeout(function () {
                var playerInstance = jwplayer("player").setup({
                    file: '{{url('/uploads/courses/videos')}}/{{$file_name}}.mp4',
                    aspectratio: "16:9",
                    controls: true,
                    width: "100%",
                    autostart: "true"
                });

                playerInstance.onPlay(function () {
                    if (duration == 0) {
                        // we don't have a duration, so it's playing so we can discover it...
                        duration = playerInstance.getDuration();
                        playerInstance.stop();
                        //alert(this.getDuration())
                    }
                });
                $('#media_src').val('/uploads/courses/videos/{{$file_name}}.mp4')

                //$('#media_src').val('{{url('/uploads/courses/videos')}}/{{$file_name}}.mp4')
                $('#mp4Player').show();
            }, 3000)
        });

        /*
        // Append token to the request - required for web routes
        myDropzone.on('sending', function (file, xhr, formData) {
            console.log(JSON.stringify(file))
        });
        */

        @endif

        @if($media_type=='album')

        drawTableListVideo();

        $("input:checkbox").on('click', function () {
            // in the handler, 'this' refers to the box clicked on
            var box = $(this);
            if (box.is(":checked")) {
                // the name of the box is retrieved using the .attr() method
                // as it is assumed and expected to be immutable
                var group = "input:checkbox[name='" + box.attr("name") + "']";
                // the checked state of the group/box on the other hand will change
                // and the current value is retrieved using .prop() method
                $(group).prop("checked", false);
                box.prop("checked", true);
            } else {
                box.prop("checked", false);
            }

            var selectedUrl = '{{url('/')}}/' + $(this).data('src');
            //alert(selectedUrl)

            var playerInstance = jwplayer("player").setup({
                file: selectedUrl,
                aspectratio: "16:9",
                controls: true,
                width: "100%",
                autostart: "true"
            });

            $('#media_src').val('{{url('/uploads/courses/videos')}}/{{$file_name}}.mp4')
            $('#mp4Player').show();
        });


        $("input[name='media_length']").on('change', function() {

            //use media in all length
            if($(this).val()==0){
                $('#mediaTrimmer').hide();
                timepicker_start.timepicker().timepicker('setTime', '00:00:00');
                timepicker_end.timepicker().timepicker('setTime', '00:00:00');
            }
            //use media in some length
            else if($(this).val()==1){
                $('#mediaTrimmer').show();
            }
        });
        @endif

    });

    function drawTableListVideo() {
        var table_list_video = $('#tb_list_video').dataTable({
            pageLength: 100,
            "bLengthChange": false,
            "bInfo": false,
            "aaSorting": [],
            columnDefs: [
                {orderable: false, targets: -1}
            ],
            "searching": false,
            "scrollY": '50vh',
            "scrollCollapse": true,
            "paging": false,
            "responsive": true,
        });
    }
</script>

<!--------------------------------------------------------------------------------------------------------------------->
<!--
<script type="text/javascript">
window.onload = function () {
var options =
{
imageBox: '.imageBox',
thumbBox: '.thumbBox',
spinner: '.spinner',
imgSrc: 'avatar.png'
}
var cropper;
document.querySelector('#file').addEventListener('change', function () {
var reader = new FileReader();
reader.onload = function (e) {
options.imgSrc = e.target.result;
options.width = 1600;
options.height = 900;
cropper = new cropbox(options);
}
reader.readAsDataURL(this.files[0]);
this.files = [];
})
document.querySelector('#btnCrop').addEventListener('click', function () {
var img = cropper.getDataURL()
document.querySelector('.cropped').innerHTML = '<img src="' + img + '">';
document.getElementById("cropCover").value = img;
})
document.querySelector('#btnZoomIn').addEventListener('click', function () {
cropper.zoomIn();
})
document.querySelector('#btnZoomOut').addEventListener('click', function () {
cropper.zoomOut();
})
};
</script>
-->
