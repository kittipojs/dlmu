<div class="padding text-center">
    <p>
        เลือกประเภทของสื่อวิดีโอที่คุณต้องการใช้สำหรับบทเรียนนี้
    </p>
    <div class="padding"></div>
    <div class="row">
        <div class="col-sm-3">
            <img src="{{ url('/frontend/medilab/img/icon-youtube.png') }}" style="height:60px; width:60px;" />
            <p>ลิงก์จาก Youtube</p>
            <a href="{{ url('e-admin/lectures/add?to='.$course_id.'&media_type=youtube') }}" class="btn btn-dark btn-sm">เลือก</a>
        </div>
        <div class="col-sm-3">
            <img src="{{ url('/frontend/medilab/img/icon-mp4.png') }}" style="height:60px; width:60px;" />
            <p>อัพโหลด ไฟล์ .mp4 ใหม่</p>
            <a href="{{ url('e-admin/lectures/add?to='.$course_id.'&media_type=mp4') }}" class="btn btn-dark btn-sm">เลือก</a>
        </div>
        <div class="col-sm-3">
            <img src="{{ url('/frontend/medilab/img/icon-album.png') }}" style="height:60px; width:60px;" />
            <p>เลือกไฟล์ .mp4 จากอัลบั้ม</p>
            <a href="{{ url('e-admin/lectures/add?to='.$course_id.'&media_type=album') }}" class="btn btn-dark btn-sm">เลือก</a>
        </div>
        <div class="col-sm-3">
            <img src="{{ url('/frontend/medilab/img/icon-streaming.png') }}" style="height:60px; width:60px;" />
            <p>สตรีมมิ่ง URL</p>
            <a href="{{ url('e-admin/lectures/add?to='.$course_id.'&media_type=streaming') }}" class="btn btn-dark btn-sm">เลือก</a>
        </div>
        {{--
        <div class="col-sm-3">
            <img src="{{ url('/frontend/medilab/img/icon-album.png') }}" style="height:60px; width:60px;" />
            <p>จากอัลบั้ม</p>
            <a href="{{ url('e-admin/lectures/add?to='.$course_id.'&media_type=album') }}" class="btn btn-dark btn-sm">เลือก</a>
        </div>

        --}}
    </div>
</div>