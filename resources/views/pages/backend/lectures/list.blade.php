<div class="padding text-center">
    <a href="{{url('/e-admin/lectures/add?to='.$course_id)}}" class="btn btn-primary btn-md">สร้างบทเรียนใหม่</a>
    <p>หรือ</p>
</div>

<table class="table table-striped table-hover" id="tb_lecture" width="100%">
    <thead>
    <tr>
        <th>ชื่อบทเรียน</th>
        <th>รายละเอียด</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($lectures as $item)
        <tr>
            <td>{{ $item->lecture_title }}</td>
            <td>{{ $item->lecture_intro }}</td>
            <td data-values="action" data-key="{{ $item->lecture_id }}">
                <div class=" action ">
                    <div class="dropdown">
                        <button type="button" data-target="#nia-modal" data-toggle="modal"
                                class="btn btn-xs btn-dark edit append" data-id="{{ $item->lecture_id }}">
                            <i class="fa fa-plus"></i> เลือก
                        </button>
                    </div>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<script>

    $(document).ready(function () {
        drawTable()
        addCurriculum()
    });

    function drawTable() {

        var table = $('#tb_lecture').dataTable({
            searching: true,
            scrollX: true,
            destroy: true,
            responsive: true,
            pageLength: 7,
            sSearch: "ค้นหา"
        });


        $("#tb_lecture_filter").on("keyup", 'input', function() {
            setPaginageAction()
        });
    }

    function Edit() {
        $('.edit').click(function () {
            $('#modal-header').text('อัพเดทข้อมูล');
            var id = $(this).data('id');
            var data_area = $("#modal-info"),
                url;
            url = "{{ url('e-admin/quiz/update') }}" + "/" + id;
            data_area.html('Loading . . .');
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });
        })

    }

    function addCurriculum() {
        $('.append').click(function () {
            var id = $(this).data('id'),
                row = $(this).closest("tr"),
                title = $(row).children('td').eq(0).text(),
                alert = $(row).children('td').eq(2).find('button'),
                checkList = $('span.check'),
                duplicate = false;
            if ($(this).hasClass('disabled')) {
                $.toast({
                    heading: 'บทเรียนซ้ำ',
                    text: `เพิ่ม ${title} แล้ว`,
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 2500,
                    stack: 6
                });
                return false;
            }
            checkList.each(function (index, object) {
                if ($(object).text() === title) {
                    duplicate = true;
                }
            });
            if (duplicate) {
                alert.removeClass('btn-dark');
                alert.addClass('btn-warning disabled');
                alert.html('<i class="fa fa-warning"></i> เพิ่มซ้ำ');
                $.toast({
                    heading: 'บทเรียนซ้ำ',
                    text: `เพิ่ม ${title} อยู่ในหลักสูตรนี้แล้ว`,
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 2500,
                    stack: 6
                });
            }
            else {

                alert.removeClass('btn-dark');
                alert.addClass('btn-primary disabled');
                alert.html('<i class="fa fa-check"></i> เพิ่มสำเร็จ');

                $('div.sortable').append(`<div class="padding" data-id="${id}" style="margin: 10px 0; background-color:#eee; border:1px solid #aaa; position:relative;">
                <input type="hidden" name="object_order[]" value="${id}">
                <i class="fas fa-bars" style="position:absolute; left:10px; top: 10px;"></i>
                <div style="margin-left:30px;">
                <div>
                บทเรียน - <span class="check">${title}</span>
                    </div>
                <div class="padding"></div>
                <button type="button" class="btn btn-xs btn-dark" onclick="editCurriculum($(this))" data-target="#sximo-modal" data-toggle="modal">
                <i class="far fa-edit"></i> แก้ไข
                    </button>
                    <div class="btn-group dropdown">
                                                    <button class="btn btn-xs btn-danger dropdown-toggle"
                                                            type="button" data-toggle="dropdown">
                                                        <i class="far fa-trash-alt"></i> ลบ
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <button type="button"
                                                                    class="btn btn-xs btn-block btn-danger"
                                                                    onclick="removeCurriculum($(this))">
                                                                ยืนยัน
                                                            </button>
                                                        </li>
                                                    </ul>
                                                </div>

                <div class="padding"></div>
                    </div>
                    </div>`);
            }
        })
    }
</script>