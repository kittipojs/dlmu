<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="row">
            <div class="col-sm-5" style="background-color:#eee;">
                <h4>วิดีโอ</h4>
                <p>
                    @if($lecture->start_time=='00:00:00' && $lecture->end_time=='00:00:00')

                    @else
                        ตั้งแต่ {{$lecture->start_time}} ถึง {{$lecture->end_time}}
                    @endif
                </p>
                <div class="padding-sm"></div>
                <div id="player-sync"></div>
                <div class="row padding">
                    <div class="col-sm-6">
                        <small>กดปุ่ม snap เพื่อเพิ่ม slide</small>
                    </div>
                    <div class="col-sm-6 text-right">
                        <button id="snap-btn" type="button" class="btn btn-sm btn-dark">SNAP</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                <h4>รายการสไลด์</h4>
                <hr/>
                <div class="padding-sm"></div>
                <form class="form-row align-items-center" role="form"
                      action="{{ url('e-admin/lectures/syncslide/save') }}"
                      method="post" enctype="multipart/form-data">
                    <input type="hidden" name="lecture_id" value="{{ $lecture_id }}">
                    <div id="snap-list">
                        @if(count($lecture_slide) > 0)
                            @foreach( $lecture_slide as $slide)
                                <div class="input-group row padding" style="border-bottom:1px solid #eee;">
                                    <div class="col-sm-3">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <input type="text" class="form-control" name="slide_time[]"
                                                       value="{{ $slide->slide_time }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <input required type="file" class="form-control" name="slide_image[]"/>
                                    </div>
                                    <div class="col-sm-1">
                                        <button type="button" class="btn btn-sm" onclick="removeRow($(this))">x</button>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div id="emptySlide" class="padding-lg" style="display:none;">
                                <div class="paddding">ยังไม่มีรายการสไลด์</div>
                            </div>
                        @endif
                    </div>
                    <div class="padding"></div>
                    <div class="form-group">
                        <div class="col-sm-7">
                            <button type="submit" class="btn btn-sm btn-primary">บันทึก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    var time_array = []

    $(document).ready(function () {


        var time = '00:00:00';

        var playerInstance = jwplayer("player-sync").setup({
            "file": "{{url($lecture->media_src)}}",
            image: "{{url($lecture->lecture_cover)}}",
            aspectratio: "16:9",
            controls: true,
            width: "100%",
            autostart: "false"
        });

        playerInstance.onSeek(function (event) {

            var offset = Math.floor(event.offset);
            var sec_num = parseInt(offset, 10); // don't forget the second param
            var hours = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours < 10) {
                hours = "0" + hours;
            }
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }

            time = hours + ':' + minutes + ':' + seconds
        });

        //start play only part of video
        @if($lecture->start_time=='00:00:00' && $lecture->end_time=='00:00:00')
        //$('#customRadio1').attr('checked', true)
        @else
        //$('#customRadio2').attr('checked', true)
        playerInstance.onBeforePlay(function(){
            playerInstance.seek({{$lecture->time_from_millisec/1000}});
        });

        playerInstance.onTime(function(event) {
            var currentSecond = parseInt(event.position)

            var timeFrom = {{$lecture->time_from_millisec/1000}}
            var timeTo = {{$lecture->time_to_millisec/1000}}

            //console.log(currentSecond)
            if(currentSecond > timeTo){
                playerInstance.seek({{$lecture->time_from_millisec/1000}});
            }
            if(currentSecond < timeFrom){
                playerInstance.seek({{$lecture->time_from_millisec/1000}});
            }
        });
        @endif

        checkTimeArray();

        $('#snap-btn').click(function () {

            var mediaTimeFrom = {{$lecture->time_from_millisec/1000}}
            var mediaTimeTo = {{$lecture->time_to_millisec/1000}}

            var offset = Math.floor(playerInstance.getPosition());

            console.log(mediaTimeFrom+' '+offset+' '+mediaTimeTo)

            var sec_num = parseInt(offset, 10); // don't forget the second param
            var hours = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours < 10) {
                hours = "0" + hours;
            }
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }

            time = hours + ':' + minutes + ':' + seconds;

            $('.intro').fadeOut(function () {
                $(this).remove();
            });

            var snap_row = `<div class="input-group row padding" style="border-bottom:1px solid #eee;">
<div class="col-sm-3">
<div class="input-group-prepend">
<div class="input-group-text">
<input type="text" class="form-control" name="slide_time[]" value="${time}" readonly>
    </div>
    </div>
    </div>
<div class="col-sm-8">
<input type="file" class="form-control" required name="slide_image[]"/>
    </div>
<div class="col-sm-1">
<button type="button" class="btn btn-sm" onclick="removeRow($(this))">x</button>
    </div>
    </div>`;

            @if($lecture->start_time=='00:00:00' && $lecture->end_time=='00:00:00')
            time_array.push(time)
            @else
            if( offset > mediaTimeFrom && offset < mediaTimeTo ){
                time_array.push(time)
            }
            @endif

            checkTimeArray()

            $(snap_row).hide().appendTo("#snap-list").fadeIn(300);
        })
    });

    function checkTimeArray() {

        //console.log(time_array.length)
        if (time_array.length < 1) {
            $('#emptySlide').show()
        }
        else {
            $('#emptySlide').hide()
        }
    }

    function removeRow(row) {
        console.log(row)

        var countRow = $("#snap-list div.input-group").length;
        if (countRow <= 1) {
            return false
        }
        row.closest('div.input-group').fadeOut(function () {
            $(this).remove();
        });
    }

</script>