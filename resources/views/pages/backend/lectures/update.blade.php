
<div class="container">
    <form class="form-horizontal" method="post" action="{{ url('e-admin/lectures/update/save') }}" role="form"
          enctype="multipart/form-data">
        <input type="hidden" name="lecture_id" value="{{ $lecture->lecture_id }}">
        <input type="hidden" name="course_id" value="{{ $lecture->course_id }}">
        <input type="hidden" name="media_type" value="{{ $lecture->media_type }}">

        <div class="form-group">

            @if($lecture->media_type=='youtube')
                <label class="control-label col-sm-2" for="quiz_title">URL ของ Youtube * </label>
                <div class="col-sm-9">
                    <div id="source-youtube" class="media_source">
                        <label class="sr-only" for="youtube-link">URL</label>
                        <input type="text" class="form-control mb-2 mr-sm-2" name="media_src" id="media_src"
                               placeholder="https://youtu.be/nSOG99qClU8" value="{{$lecture->media_src}}">
                    </div>
                </div>
            @elseif($lecture->media_type=='link')
                <label class="control-label col-sm-2">URL * </label>
                <div class="col-sm-9">
                    <div id="source-link" class="media_source">
                        <input type="text" class="form-control mb-2 mr-sm-2" name="media_src" id="media_src"
                               placeholder="Adobe HDS (Adobe Flash Player), Apple HLS (iPhone, iPad, iPod touch, QuickTime, and more), Microsoft Smooth Streaming (Microsoft Silverlight and more), MPEG-DASH streaming (DASH clients), Adobe RTMP (Adobe Flash Player), RTSP/RTP (QuickTime, VLC, 3GPP devices, set-top boxes, and more)">
                        <p>
                            <br>
                            <small>
                                รองรับ Adobe HDS (Adobe Flash Player), Apple HLS (iPhone, iPad, iPod touch,
                                QuickTime, and more), Microsoft Smooth Streaming (Microsoft Silverlight and more),
                                MPEG-DASH streaming (DASH clients), Adobe RTMP (Adobe Flash Player), RTSP/RTP
                                (QuickTime, VLC, 3GPP devices, set-top boxes, and more)
                            </small>
                        </p>
                    </div>
                </div>
            @elseif($lecture->media_type=='mp4' || $lecture->media_type=='album')
                <input type="hidden" name="media_src" id="media_src" value="{{$lecture->media_src}}">
                <div id="player"></div>
                <div class="padding"></div>


                <div class="form-group">
                    <label class="control-label col-sm-2" for="quiz_title">ความยาวของวิดีโอ * </label>
                    <div class="col-sm-9">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio1" name="media_length" value="0" class="custom-control-input">
                            <label class="custom-control-label" for="customRadio1">ใช้ทั้งหมด</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio2" name="media_length" value="1" class="custom-control-input">
                            <label class="custom-control-label" for="customRadio2">ใช้บางส่วน</label>
                        </div>
                    </div>
                </div>

                <div id="mediaTrimmer" class="form-group">

                    <label class="control-label col-sm-3" for="start">เริ่ม * </label>
                    <div class="col-sm-3">
                        <div class="input-group bootstrap-timepicker timepicker">
                            <input id="timepickerStart" type="text" class="form-control input-small timepicker2"
                                   name="start">
                            <span class="input-group-addon">
                            <i class="glyphicon glyphicon-time"></i>
                        </span>
                        </div>
                    </div>
                    <label class="control-label col-sm-1" for="end">จบ * </label>
                    <div class="col-sm-3">
                        <div class="input-group bootstrap-timepicker timepicker">
                            <input id="timepickerEnd" type="text" class="form-control input-small timepicker2"
                                   name="end">
                            <span class="input-group-addon">
                            <i class="glyphicon glyphicon-time"></i>
                        </span>
                        </div>
                    </div>
                </div>

                {{--
                <label class="control-label col-sm-2">เลือกไฟล์ * </label>
                <div class="col-sm-9">
                    <div class="media_source">
                        <div id="video-preview" class="dropzone"></div>
                    </div>
                    <input type="hidden" id="media_src" name="media_src" value="{{$lecture->media_src}}">
                    @if($lecture->media_src!=null)

                    @endif
                </div>
                --}}
            @elseif($lecture->media_type=='albums')
                <style>
                    .inventory_related thead {
                        display: none;
                    }
                </style>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="quiz_title">เลือกวิดีโอ * </label>
                    <div class="col-sm-9">
                        <table class="table table-hover inventory_related" id="tb_list_video" width="100%">
                            <thead>
                            <th>header</th>
                            </thead>
                            <tbody>
                            @foreach($video_list as $list)
                                <tr>
                                    <td>
                                        <input type="checkbox" class="form-check-input"
                                               id="media_src[{{$list->id}}]"
                                               name="media_src" value="{{ $list->media_src }}"
                                                {{ ($list->media_src == $lecture->media_src)? 'checked' : '' }}>
                                        <label class="form-check-label" for="media_src[{{$list->id}}]">
                                            <i class="fa fa-file-video-o"></i> {{ $list->lecture_title }}
                                        </label>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="form-group">

                    <label class="control-label col-sm-3" for="start">เริ่ม * </label>
                    <div class="col-sm-3">
                        <div class="input-group bootstrap-timepicker timepicker">
                            <input id="start" type="text" class="form-control input-small timepicker2" name="start"
                                   value="{{$lecture->start_time}}">
                            <span class="input-group-addon">
                            <i class="glyphicon glyphicon-time"></i>
                        </span>
                        </div>
                    </div>
                    <label class="control-label col-sm-1" for="end">จบ * </label>
                    <div class="col-sm-3">
                        <div class="input-group bootstrap-timepicker timepicker">
                            <input id="end" type="text" class="form-control input-small timepicker2" name="end"
                                   value="{{$lecture->end_time}}">
                            <span class="input-group-addon">
                            <i class="glyphicon glyphicon-time"></i>
                        </span>
                        </div>
                    </div>
                </div>
            @endif
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="quiz_title">ชื่อบทเรียน * </label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="lecture_title" name="lecture_title"
                       placeholder="ชื่อบทเรียน" value="{{$lecture->lecture_title}}" required>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="quiz_title">คำนำ * </label>
            <div class="col-sm-9">
                <textarea rows="3" class="form-control" id="lecture_intro" name="lecture_intro"
                          placeholder="คำนำของบทเรียน ความยาวไม่เกิน 500 ตัวอักษร"
                          required>{{$lecture->lecture_intro}}</textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="quiz_title">รายละเอียด * </label>
            <div class="col-sm-9">
                <textarea rows="7" class="form-control" id="lecture_description" name="lecture_description"
                          placeholder="รายละเอียดของบทเรียน" required>{{$lecture->lecture_description}}</textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="file">ภาพปก * </label>
            <div class="col-sm-9">
                <input type="file" name="image" id="image" class="inputfile"/>
                <label for="avatar"><i class="fa fa-upload"></i> เลือกไฟล์ (รูปภาพ16:9) </label>
            </div>
        </div>

        <hr/>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-9">
                <button type="submit" class="btn btn-primary">บันทึก</button>
            </div>
        </div>
    </form>
</div>

<style type="text/css">
    .bootstrap-timepicker-widget{
        z-index: 9999 !important;
    }
</style>

<script type="text/javascript">

    Dropzone.autoDiscover = false;

    $(document).ready(function () {

        $('#lecture_description').summernote({
            height: 300, toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'hr']],
                ['view', ['fullscreen']],
                ['help', []]
            ]
        });

        /*
        $('input[type=radio][name=media_type]').change(function () {

            $('.media_source').hide();
            $('#split-time').hide();

            if (this.value == 'mp4') {
                $('#source-mp4').show();
                $('#split-time').show();

                var myDropzone = new Dropzone("div#video-preview",
                                              {
                    maxFilesize: 104857600,
                    url: "{{url('e-admin/course/upload')}}?type=preview&course_id=",
                    dictDefaultMessage: '<i class="fas fa-upload"></i> เลือกไฟล์ .mp4 จากเครื่องคอมพิวเตอร์ของคุณ',
                    acceptedFiles: ".mp4",
                    success: function (file, response) {
                        console.log(response)
                        if (response.success) {
                            location.reload();
                        }
                    }
                });
            }
            else if (this.value == 'youtube') {
                $('#source-youtube').show()
            }
            else if (this.value == 'link') {
                $('#source-link').show();
                $('#split-time').show();
            }
            else if (this.value == 'album') {
                $('#source-album').show();
                $('#split-time').show();
            }
        });
        */
        @if($lecture->media_type=='mp4'  || $lecture->media_type=='album')


        var timepicker_start = $('#timepickerStart').timepicker({
                showSeconds: true,
                showMeridian: false,
                format: 'hh:mm:ss',
                minuteStep: 1,
                secondStep: 1,
                autoclose: true,
                defaultTime: '00:00:00'
            });

        var timepicker_end = $('#timepickerEnd').timepicker({
            showSeconds: true,
            showMeridian: false,
            format: 'hh:mm:ss',
            minuteStep: 1,
            secondStep: 1,
            autoclose: true,
            defaultTime: '00:00:00'
        });

        var duration = '{{$lecture->media_duration}}';


        timepicker_start.timepicker().timepicker('setTime', '{{$lecture->start_time}}');
        timepicker_end.timepicker().timepicker('setTime', '{{$lecture->end_time}}');


        $("input[name='media_length']").on('change', function() {

            //use media in all length
            if($(this).val()==0){
                $('#mediaTrimmer').hide();
                timepicker_start.timepicker().timepicker('setTime', '{{$lecture->start_time}}');
                timepicker_end.timepicker().timepicker('setTime', '{{$lecture->end_time}}');
            }
            //use media in some length
            else if($(this).val()==1){
                $('#mediaTrimmer').show();
            }
        });


        /*
        var myDropzone = new Dropzone("div#video-preview",
                                      {
            maxFilesize: 104857600,
            url: "{{url('e-admin/lectures/upload')}}?lecture_id={{$lecture->lecture_id}}",
            dictDefaultMessage: '<i class="fas fa-upload"></i> เลือกไฟล์ .mp4 จากเครื่องคอมพิวเตอร์ของคุณ',
            acceptedFiles: ".mp4",
            success: function (file, response) {
                console.log(response)
                $('#media_src').val(response.url)
            }
        });
        */

        /*
        var playerInstance = jwplayer("player").setup({
            "file": "{{url($lecture->media_src)}}",
            image: "{{url($lecture->lecture_cover)}}",
            aspectratio: "16:9",
            controls: true, 
            width: "100%",
            autostart: "true",
            skin : {
                 "url":"{{url('frontend/medilab/css/skin-player.css')}}",
                 "name" : "setskin"
             }
        });
        */

        var playerInstance = jwplayer("player").setup({
            image: "{{url($lecture->lecture_cover)}}",
            aspectratio: "16:9",
            controls: true,
            width: "100%",
            autostart: "true",
            sources: [{
                file: "{{url($lecture->media_src)}}",
                label: "360p SD"
            }, {
                file: "{{url($lecture->media_src)}}",
                label: "720p HD",
                "default": "true"
            }, {
                file: "{{url($lecture->media_src)}}",
                label: "1280p HD"
            }]
        });

        /*
        playerInstance.on('time', function(e) {
            $.cookie('resumevideodata', Math.floor(e.position) + ':' + playerInstance.getDuration());
        });

        playerInstance.on('ready', function() {
            var cookieData = $.cookie('resumevideodata');
            if(cookieData) {
                var resumeAt = cookieData.split(':')[0],
                    videoDur = cookieData.split(':')[1];
                if(parseInt(resumeAt) < parseInt(videoDur)) {
                    playerInstance.seek(resumeAt);
                    logMessage('Resuming at ' + resumeAt); //for demo purposes
                }
                else if(cookieData && !(parseInt(resumeAt) < parseInt(videoDur))) {
                    logMessage('Video ended last time! Will skip resume behavior'); //for demo purposes
                }
            }
            else {
                logMessage('No video resume cookie detected. Refresh page.');
            }
        });
        */


        //start play only part of video
        @if($lecture->start_time=='00:00:00' && $lecture->end_time=='00:00:00')
            $('#customRadio1').attr('checked', true)
        @else
            $('#customRadio2').attr('checked', true)
            playerInstance.onBeforePlay(function(){
                playerInstance.seek({{$lecture->time_from_millisec/1000}});
            });

            playerInstance.onTime(function(event) {
                var currentSecond = parseInt(event.position)
                var timeFrom = {{$lecture->time_from_millisec/1000}}
                var timeTo = {{$lecture->time_to_millisec/1000}}
                //console.log(currentSecond)
                if(currentSecond > timeTo){
                    playerInstance.seek({{$lecture->time_from_millisec/1000}});
                }
                if(currentSecond < timeFrom){
                    playerInstance.seek({{$lecture->time_from_millisec/1000}});
                }
            });
        @endif

        @endif

        @if($lecture->media_type=='album')
        drawTableListVideo();

        $("input:checkbox").on('click', function () {
            // in the handler, 'this' refers to the box clicked on
            var box = $(this);
            if (box.is(":checked")) {
                // the name of the box is retrieved using the .attr() method
                // as it is assumed and expected to be immutable
                var group = "input:checkbox[name='" + box.attr("name") + "']";
                // the checked state of the group/box on the other hand will change
                // and the current value is retrieved using .prop() method
                $(group).prop("checked", false);
                box.prop("checked", true);
            } else {
                box.prop("checked", false);
            }
        });
        @endif

    })

    function drawTableListVideo() {
        var table_list_video = $('#tb_list_video').dataTable({
            pageLength: 5,
            "bLengthChange": false,
            "bInfo": false,
            "aaSorting": [],
            columnDefs: [
                {orderable: false, targets: -1}
            ],
            "searching": true,
            "scrollX": true,
            "responsive": true,
        });
    }

</script>