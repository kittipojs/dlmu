<style>
    .modal-body {
        padding: 0px;
    }
</style>
{{--{{ dd($courses) }}--}}
<div class="container-fluid">
    @foreach($courses as $course)
        <div class="col-sm-4">
            <div class="thumbnail">
                <h4>{{ $course->lecture_title }}</h4>
                <div class="padding">
                    {{ $course->lecture_intro }}
                </div>
                <video width="100%" height="300px" poster="{{asset($course->lecture_cover)}}" controls
                       style="padding-right: 10px; padding-left: 10px" data-toggle="modal" data-target="#preview"
                       onclick="videoPlayer('{{asset($course->media_src)}}')">
                    {{--<source src="{{asset($course->media_src)}}" type="video/mp4">--}}
                </video>
                <div class="caption" style="height:90px;">
                    {!! $course->lecture_description !!}
                </div>
            </div>
        </div>
    @endforeach
</div>


<!-- VIDEO PLAYER -->
<div id="preview" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="preview_area">

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#preview').on('hidden.bs.modal', function () {
            $(this).find($('#preview_area')).html('').end();
        });
    });

    function videoPlayer(src){
        var video_src = src;
        $('#player').html(``);
        $('#preview_area').html(`<video width="100%" controls id="player"><source id="_src" src="${video_src}"  type="video/mp4"></video>`);
    }
    // load remote content for BS modal.
    // $(document).on('click', '.load-modal', function(e) {
    //     e.preventDefault();
    //     $('#preview .modal-content')
    //         .removeData()
    //         .html('<div class="text-center loading-wrapper">\
		// 					<h1>\
		// 						<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>\
		// 						Loading.</h1></div>');
    //     $('#preview').modal({ show: true });
    // });
</script>