<ul class="nav nav-tabs tabs-left" style="min-height: 1000px;">
    <li class="active"><a data-toggle="tab" href="#tab_general"><i class="fa fa-edit"></i>
        รายละเอียดทั่วไป</a></li>
    <li><a data-toggle="tab" href="#tab_cover"><i class="fas fa-image"></i> ภาพปก</a></li>
    <li>
        <a data-toggle="tab" href="#tab_document">
            <i class="fas fa-file-alt"></i> เอกสารประกอบการเรียน
        </a>
    </li>
    <!-- <li><a data-toggle="tab" href="#tab_fee"><i class="fa fa-credit-card"></i> ค่าธรรมเนียม</a></li> -->
    <li>
        <a data-toggle="tab" href="#tab_video">
            <i class="fa fa-video"></i> 
            วิดีโอตัวอย่าง
        </a>
    </li>
    
    <li>
        <a data-toggle="tab" href="#tab_pretest">
            <i class="fa fa-pencil"></i> แบบทดสอบก่อนเรียน (Pre Test)
        </a>
    </li>
    
    <li>
        <a data-toggle="tab" href="#tab_structure">
            <i class="fa fa-share"></i> โครงสร้างหลักสูตร
        </a>
    </li>
    
    <li>
        <a data-toggle="tab" href="#tab_posttest">
            <i class="fa fa-pencil"></i> แบบทดสอบหลังเรียน (Post Test)
        </a>
    </li>
    
  {{--
    <li>
        <a data-toggle="tab" href="#tab_prerequisite">
            <i class="fa fa-copy"></i> หลักสูตรที่เกี่ยวข้อง
        </a>
    </li>
  --}}
    
    <li>
        <a data-toggle="tab" href="#tab_certification">
            <i class="fas fa-certificate"></i> การให้ประกาศณียบัตร
        </a>
    </li>
    
    <li>
        <a data-toggle="tab" href="#tab_statistic">
            <i class="fas fa-chart-bar"></i> สถิติ
        </a>
    </li>
    
    <hr />
    <div class="padding">
        @if($course->course_status==0)
        <a href="{{ url('/e-admin/course/status?value=1&course_id=').$course->course_id }}"
           class="btn btn-block btn-sm btn-success">เผยแพร่</a>
        @elseif($course->course_status==1)
        <a href="{{ url('/e-admin/course/status?value=0&course_id=').$course->course_id }}"
           class="btn btn-block btn-sm btn-danger">งดเผยแพร่</a>
        @endif
    </div>
    
</ul>