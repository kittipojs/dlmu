<div class="container">
    <div class="title">
        <h3>{{ $title }}</h3>
    </div>
    <div class="menu-control">
        <a href="{{url('/e-admin/course/add')}}" class="btn btn-sm btn-primary">เพิ่มหลักสูตรใหม่</a>
    </div>
    <div class="row padding">
        <table class="table table-striped table-hover" id="tb_course" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>สถานะ</th>
                    <th>รหัสคอร์ส</th>
                    <th>ชื่อคอร์ส</th>
                    <th>สร้างเมื่อ</th>
                </tr>
            </thead>
            <tbody>
                @foreach($courses as $course)
                <tr>
                    <td>
                        <div class="col-sm-12">
                            <button type="button" class="btn btn-sm btn-primary view-info-btn" onclick="viewCourse($(this))" data-id="{{ $course->course_id }}">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-danger" onclick="deleteCourse($(this))"
                                    data-id="{{ $course->course_id }}">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </td>
                    <td>
                        @if($course->course_status==1)
                        <span class="f-green"><i class="fas fa-eye"></i> เผยแพร่แล้ว</span>
                        @elseif($course->course_status==0)
                        <span class="f-red"><i class="fas fa-eye-slash"></i> ยังไม่เผยแพร่</span>
                        @endif
                    </td>
                    <td>{{ $course->course_code }}</td>
                    <td>{{ $course->course_title }}</td>
                    <td>{{ $course->created_at }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        drawTable();
        //Edit();

        setPaginageAction();
        $(".sorting_desc").click(setPaginageAction);
        $(".sorting_asc").click(setPaginageAction);
        $(".sorting").click(setPaginageAction);
    });

    function drawTable() {
        var table = $('#tb_course').dataTable({
            dom: 'Bfrtip',
            "aaSorting": [],
            columnDefs: [
                { orderable: false, targets: -1}
            ],
            buttons: [{
                extend: 'excel',
                title: 'รายการหลักสูตร',
                filename: 'ข้อมูลคอร์ส',
                exportOptions: {
                    columns: [1, 2, 3],
                    format: {
                        body: function (data, row, column, node) {
                            return column >= 3 ? data.split("  ")[0] : data;
                        }
                    }
                }
            }],
            "searching": true,
            "scrollX": true,
            "responsive": true,
        });
        $("#tb_course_filter").on("keyup", 'input', function() {
            setPaginageAction()
        });
    }

    function viewCourse(ele) {
        var course_id = $(ele).data('id'),
            url = '{{ url('/e-admin/course/manage') }}' + '/' + course_id;

        window.location.href = url;
    }

    function deleteCourse(ele) {
        var course_id = $(ele).data('id'),
            url = '{{ url('/e-admin/course/delete') }}' + '/' + course_id;

        $.ajax({
            url: url,
            type: 'GET',
            success: function (data) {
                $.toast({
                    heading: 'success',
                    text: data.message,
                    position: 'bottom-right',
                    icon: 'success',
                    hideAfter: 2500,
                    stack: 6
                });
                //location.reload();
            }, error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'error',
                    text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 2500,
                    stack: 6
                });
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    function setPaginageAction() {
        $(".paginate_button").click(setPaginageAction);

        $('.edit').unbind("click");

        //Edit()
    }
</script>