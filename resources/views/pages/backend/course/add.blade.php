<div class="container">
    <div class="sbox large-box">
        <div class="sbox-title">
            <h4 align="center">{{ $title }}</h4>
        </div>
        <div class="sbox-content">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab_general"><i class="fa fa-edit"></i> รายละเอียดทั่วไป</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab_general" class="tab-pane fade in active">

                    <div class="padding"></div>

                    <form class="form-horizontal" role="form" action="{{ url('e-admin/course/save') }}" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="course_code" class="col-sm-3 control-label">Code *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="course_code" id="course_code" placeholder="รหัสหลักสูตร เช่น STARTUP01" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="course_title" class="col-sm-3 control-label">ชื่อหลักสูตร *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="course_title" id="course_title" placeholder="ชื่อหลักสูตรเรียน" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="course_intro" class="col-sm-3 control-label">คำอธิบายหลักสูตร *</label>
                            <div class="col-sm-6">
                                <textarea rows="4" class="form-control" name="course_intro" id="course_intro" placeholder="ข้อความแนะนำหลักสูตรเรียนนี้ ความยาวไม่เกิน 500 ตัวอักษร" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="course_detail" class="col-sm-3 control-label">รายละเอียดหลักสูตรเพิ่มเติม *</label>
                            <div class="col-sm-6">
                                <textarea class="form-control" name="course_detail" id="course_detail" placeholder="รายละเอียดเกี่ยวกับหลักสูตรเรียน" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="course_detail" class="col-sm-3 control-label">หลักสูตรนี้เหมาะสำหรับ *</label>
                            <div class="col-sm-6">
                                <textarea rows="4" class="form-control" name="course_for_text" id="course_for_text" placeholder="" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                                <label for="course_detail" class="col-sm-3 control-label">ระดับของเนื้อหา *</label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="course_level" id="course_level">
                                        <option value="1">ระดับเริ่มต้น (Beginner)</option>
                                        <option value="2">ระดับกลาง (Intermediate)</option>
                                        <option value="3">ระดับสูง (Advance)</option>
                                    </select>
                                </div>
                            </div>
                        <div class="form-group">
                            <label for="course_detail" class="col-sm-3 control-label">ผู้สอน *</label>
                            <div class="col-sm-3">
                                <input class="form-control" name="course_instructor" id="course_instructor" placeholder="ชื่อ นามสกุล ผู้สอน" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="course_objective" class="col-sm-3 control-label">วัตถุประสงค์ของหลักสูตร (เป็นข้อๆ) *</label>
                            <div class="col-sm-6">
                                <div id="objective_area">
                                    <div class="input-group" id="addr0">
                                        <input type="text" class="form-control" name="course_objective[0]" id="course_objective[0]" placeholder="1. วัตถุประสงค์" required>
                                        <span class="input-group-btn">
                                            <button class="btn btn-secondary disable" type="button" disabled>
                                                <i class="fa fa-close"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="padding">
                                    <a id="add_row" class="btn btn-dark btn-sm pull-right">เพิ่มวัตถุประสงค์</a>
                                </div>
                            </div>
                        </div>

                        <!--
<div class="form-group">
<label for="image" class="col-sm-3 control-label">thumbnail *<br>
<small>(ไฟล์ .jpg, .png ขนาด 400x300 pixel เท่านั้น)</small>
</label>
<div class="col-sm-5">
<input type="file" name="image" id="image" class="form-control" data-preview-file-type="text" data-upload-url="#"/>
</div>
</div>
-->
                        <hr />    
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-8">
                                <button type="submit" class="btn btn-primary">บันทึก</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        $('#course_detail').summernote({ height:300, toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']],
            ['insert', ['link', 'hr']],
            ['view', ['fullscreen']],
            ['help', []]
        ]});

        var i = 0;

        $("#add_row").click(function () {
            $('#objective_area').append('<div id="addr${i + 1}" class="input-group" style="padding-top: 8px"><input type="text" class="form-control" name="course_objective[${i + 1}]" id="course_objective[${i + 1}]" placeholder="'+(i+2)+'. วัตถุประสงค์"  required><span onclick="removeRow($(this))" class="input-group-addon"><i class="fa fa-close"></i></span></div>');
            i++;
        });
    })

    function removeRow(row) {
        
        var countRow = $("#objective_area div.input-group").length;
        if(countRow <= 1){ return false }
        row.closest('div').remove();
        
    }
</script>