@section('content')
<div class="container">
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            <div class="sbox large-box">
                <div class="sbox-title">
                    <h4 align="center">{{ $pageTitle }}</h4>
                </div>
                <!-- START SBOX CONTENT -->
                <div class="sbox-content">
                    <div class="row">
                        <div class="col-sm-4 pull-left">
                            <a href="{{ url('e-admin/course/add') }}" class="btn btn-primary">
                                <i class="fa fa-plus"></i> เพิ่มคอร์สเรียนใหม่</a>
                        </div>
                        <div class="col-sm-4" align="center">

                        </div>
                        <div class="col-sm-4" align="right">
                            <div class="input-group">
                                <input type="text" name="searchInput" id="searchInput" class="form-control"
                                       placeholder="ค้นหาชื่อคอร์สเรียน">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                            </div>
                        </div>

                    </div>
                    <div class="row padding">
                        <table class="table table-striped table-hover" id="tb_course" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>รหัสคอร์ส</th>
                                    <th>ชื่อคอร์ส</th>
                                    <th>หัวข้อ</th>
                                    <th>แก้ไข</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($courses as $course)
                                <tr>
                                    <td>{{ $course->course_id }}</td>
                                    <td>{{ $course->course_code }}</td>
                                    <td>{{ $course->course_title }}</td>
                                    <td>{{ $course->course_intro }}</td>
                                    <!--
<td>{{ $course->course_detail }}</td>

<td id="obj_{{ $course->course_id }}"></td>
<script>
$(document).ready(function () {
var course_objective = JSON.parse('{{ $course->course_objective }}'.replace(/&quot;/g, '"'));
$.each(course_objective, function (index,value) {
$('#obj_{{ $course->course_id }}').append('<p id="addr${index}">${index + 1}.  ${value}</p>');
});
});

</script>
<td><img src="{{ url($course->course_thumbnail) }}" alt="..." class="img-thumbnail" width="200" height="200"></td>
-->
                                    <td data-values="action" data-key="{{ $course->question_id }}">
                                        <div class=" action ">
                                            <div class="dropdown">
                                                <button type="button" class="btn btn-primary edit"
                                                        {{--data-target="#nia-modal" data-toggle="modal"--}}
                                                        data-id="{{ $course->course_id }}">
                                                    <i class="fa fa-edit"></i> Edit
                                                </button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- END SBOX CONTENT -->
                </div>
            </div>
        </div>
    </div>
</div>
@include('modal.modal')
<script>

    $(document).ready(function () {
        drawTable();
        Edit();

        $("#searchInput").on('keyup', function (){
            $('#tb_course').dataTable().fnFilter(this.value);
        });

        setPaginageAction();
        $(".sorting_desc").click(setPaginageAction);
        $(".sorting_asc").click(setPaginageAction);
        $(".sorting").click(setPaginageAction);
        $('.edit').click(setPaginageAction);
    });

    function drawTable() {
        var table = $('#tb_course').dataTable({
            dom: 'Bfrtip',
            "aaSorting": [],
            columnDefs: [
                { orderable: false, targets: -1 }
            ],
            buttons: [{
                extend: 'excel',
                title: 'ข้อมูลคอร์ส',
                filename: 'ข้อมูลคอร์ส',
                exportOptions: {
                    columns: ':visible',
                    format: {
                        body: function (data, row, column, node) {
                            return column >= 3 ? data.split("  ")[0] : data;
                        }
                    }
                }
            }],
            "searching": true,
            "scrollX": true,
            "responsive": true,
        });
        $("#tb_course_filter").on("keyup", 'input', function() {
            setPaginageAction()
        });
    }

    function setPaginageAction() {
        $(".paginate_button").click(setPaginageAction);

        $('.edit').unbind("click");

        Edit()
    }

    function Edit() {
        $('.edit').click(function () {
            $('#modal-header').text('อัพเดทข้อมูล');
            var id = $(this).data('id');
            var data_area = $("#modal-info"),
                url;
            url = "{{ url('e-admin/course/update') }}" + "/" + id;
            window.location.href = url;
            // data_area.html('Loading . . .');
            // $.ajax({
            //     type: 'GET',
            //     url: url,
            //     success: function (data) {
            //         data_area.html(data);
            //     },
            //     error: function (jqXHR, textStatus, errorThrown) {
            //         data_area.html('Error Please try again');
            //     }
            // });
        })
    }
</script>

@endsection