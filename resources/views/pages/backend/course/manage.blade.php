<div class="container">
    <div class="sbox large-box">
        <div class="sbox-title">
            <div class="pull-left padding">
                <h4 align="center">ชื่อหลักสูตร : {{ $title }}</h4>
                <p style="margin-top:10px;">
                    <span>สถานะ : @if($course->course_status==0) ยังไม่เผยแพร่ @else เผยแพร่แล้ว @endif</span> &nbsp;&nbsp;|&nbsp;&nbsp;
                    <a target="_blank" href="{{ url("/c/$course->course_id") }}">
                        <i class="fas fa-eye"></i>&nbsp;ไปที่หลักสูตรนี้
                    </a>
                </p>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- START SBOX CONTENT -->
        <div class="row sbox-content">
            <div class="col-xs-4 col-md-3">
                @include('pages.backend.course.layout-tabs')
            </div>
            <div class="col-xs-6 col-md-7">

                <div class="tab-content">
                    <section id="tab_general" class="tab-pane fade in active">
                        <h3>รายละเอียดทั่วไป</h3>
                        <div class="padding-lg"></div>

                        <form class="form-horizontal" role="form" action="{{ url('e-admin/course/save') }}"
                              method="post"
                              enctype="multipart/form-data">
                            <input type="hidden" name="edit" value="true">
                            <input type="hidden" name="course_id" value="{{ $course->course_id }}">
                            <div class="form-group">
                                <label for="course_code" class="col-sm-3 control-label">รหัสวิชา *</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="course_code" id="course_code"
                                           placeholder="code" value="{{ $course->course_code }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="course_title" class="col-sm-3 control-label">ชื่อหลักสูตร *</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="course_title" id="course_title"
                                           placeholder="ชื่อคอร์ส" value="{{ $course->course_title }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="course_intro" class="col-sm-3 control-label">คำอธิบายหลักสูตร *</label>
                                <div class="col-sm-8">
                                    <textarea rows="4" class="form-control" name="course_intro" id="course_intro"
                                              placeholder="ข้อความแนะนำคอร์สเรียนนี้ ความยาวไม่เกิน 500 ตัวอักษร"
                                              required>{{ $course->course_intro }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="course_detail" class="col-sm-3 control-label">รายละเอียดหลักสูตรเพิ่มเติม
                                    *</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="course_detail" id="course_detail"
                                              placeholder="รายละเอียดเกี่ยวกับคอร์สเรียน"
                                              required>{{ $course->course_detail }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="course_detail" class="col-sm-3 control-label">หมวดหมู่ *</label>
                                <div class="col-sm-2">
                                    <select class="form-control" name="course_tags" id="course_tags" style="float:left;"
                                            required>
                                        <option value="">เลือกหมวดหมู่</option>
                                        @foreach($tags as $item)
                                            <option value="{{$item->tag_name}}"
                                            @if($course->course_tag!=null)
                                                {{ ($item->tag_name==$course->course_tag->tag_name)? 'selected' : '' }}
                                                    @endif >
                                                {{$item->tag_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-1">
                                    หรือ
                                </div>
                                <div class="col-sm-4">
                                    <button id="addTagBtn" type="button" class="btn btn-black"
                                            data-target="#sximo-modal" data-toggle="modal">เพิ่มหมวดหมู่ใหม่
                                    </button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="course_detail" class="col-sm-3 control-label">หลักสูตรนี้เหมาะสำหรับ
                                    *</label>
                                <div class="col-sm-6">
                                    <textarea rows="4" class="form-control" name="course_for_text" id="course_for_text"
                                              placeholder="" required>{{ $course->course_for_text }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="course_detail" class="col-sm-3 control-label">ระดับของเนื้อหา *</label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="course_level" id="course_level">
                                        <option value="1">ระดับเริ่มต้น (Beginner)</option>
                                        <option value="2">ระดับกลาง (Intermediate)</option>
                                        <option value="3">ระดับสูง (Advance)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="course_prerequisite_id" class="col-sm-3 control-label">หลักสูตรที่ต้องผ่าน
                                    *</label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="course_prerequisite_id"
                                            id="course_prerequisite_id">
                                        <option value="null">ไม่มี</option>
                                        @foreach($all_courses as $item)
                                            <option value="{{ $item->course_id }}"
                                                    @if($item->course_id==$course->course_prerequisite_id) selected @endif>{{ $item->course_title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="course_detail" class="col-sm-3 control-label">ผู้สอน *</label>
                                <div class="col-sm-3">
                                    <input class="form-control" name="course_instructor" id="course_instructor"
                                           placeholder="ผู้สอน" value="{{ $course->course_instructor }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="course_objective" class="col-sm-3 control-label">วัตถุประสงค์ของหลักสูตร
                                    (เป็นข้อๆ) *</label>
                                <div class="col-sm-8">
                                    <div id="objective_area">
                                    </div>
                                    <div class="padding">
                                        <a id="add_row" class="btn btn-dark btn-sm pull-right">เพิ่มวัตถุประสงค์</a>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-8">
                                    <hr class="divider">
                                    <button type="submit" class="btn btn-primary">บันทึก</button>
                                </div>
                            </div>
                        </form>
                    </section>

                    <section id="tab_document" class="tab-pane fade in">
                        <div class="row">
                            <div class="col-sm-4">
                                <h4>ไฟล์เอกสาร</h4>
                                <hr>
                                <div class="padding courseDocument">
                                    @if($course->course_document==null)
                                        คุณยังไม่ได้อัพโหลดไฟล์เอกสาร
                                    @else
                                        <a target="_blank" href="{{ url($course->course_document) }}"
                                           class="btn btn-dark">
                                            <i class="fas fa-eye"></i>&nbsp;ดูเอกสารประกอบการเรียน
                                        </a>
                                        <button type="button" class="btn btn-danger" onclick="deleteCourseDocument()"
                                                title="ลบเอกสารประกอบการเรียน">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <h4>อัพโหลดเอกสาร</h4>
                                <hr>
                                <div class="padding"></div>
                                <div id="document" class="dropzone"></div>
                                <p class="padding text-center">หรือใส่ url ในช่องด้านล่าง</p>
                                <form id="add-link-as-document" class="form-inline text-center">
                                    <label class="sr-only" for="document-link">URL</label>
                                    <input type="text" class="form-control mb-2 mr-sm-2" name="document-link"
                                           id="document-link" placeholder="https://drive.google.com"
                                           style="width:300px;">
                                    <button type="submit" class="btn btn-primary mb-2"> เพิ่มลิงค์</button>
                                </form>
                            </div>
                        </div>
                    </section>

                    <section id="tab_cover" class="tab-pane fade in">
                        <div class="row">
                            <div class="col-sm-4">
                                <h4>ภาพปก</h4>
                                <hr>
                                <div class="padding"></div>
                                <img src="{{ url($course->course_thumbnail) }}" class="img-responsive"/>
                            </div>
                            <div class="col-sm-8">
                                <h4>อัพโหลดภาพปกใหม่</h4>
                                <hr>
                                <div class="padding"></div>
                                <div id="cover" class="dropzone"></div>
                            </div>
                        </div>
                    </section>


                    <section id="tab_video" class="tab-pane fade in">
                        <div class="row">
                            <div class="col-sm-4">
                                <h4>วิดีโอตัวอย่าง</h4>
                                <hr>
                                <div class="padding"></div>
                                @if($course->course_preview_type=='mp4')
                                    {{--
                                    <video width="100%" controls>
                                        <source src="{{asset($course->course_preview)}}" type="video/mp4">
                                    </video>
                                    --}}
                                    <div id="player"></div>
                                @elseif($course->course_preview_type=='youtube')
                                    <iframe id="video" src="https://www.youtube.com/embed/{{ $course->course_preview }}"
                                            frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                @else
                                    คุณยังไม่ได้อัพโหลดวิดีโอตัวอย่าง
                                @endif
                            </div>
                            <div class="col-sm-8">
                                <h4>อัพโหลด</h4>
                                <hr>
                                <div class="padding"></div>
                                <div id="video-preview" class="dropzone"></div>
                                <p class="padding text-center">หรือใส่ url จาก Youtube ในช่องด้านล่าง</p>
                                <form id="add-youtube-as-preview" class="form-inline text-center">
                                    <label class="sr-only" for="youtube-link">URL</label>
                                    <input type="text" class="form-control mb-2 mr-sm-2" name="youtube-link"
                                           id="youtube-link" placeholder="https://youtu.be/nSOG99qClU8"
                                           style="width:300px;">
                                    <button type="submit" class="btn btn-primary mb-2"> เพิ่มลิงค์</button>
                                </form>
                            </div>
                        </div>
                    </section>


                    <section id="tab_pretest" class="tab-pane fade in">
                        <h3>แบบทดสอบก่อนเรียน (Pre Test)</h3>
                        @if($course->pre_test_id==null)
                            <p>ยังไม่มีแบบทดสอบก่อนเรียน</p>
                            <button type="button" class="btn btn-sm btn-primary add-quiz-btn" data-target="#sximo-modal"
                                    data-toggle="modal" data-quiz-type="pre_test">
                                <i class="fas fa-plus"></i> เพิ่มแบบทดสอบก่อนเรียน
                            </button>
                        @else
                            <div class="row">
                                <div class="col-sm-4">
                                    <h5>ชื่อ : {{ $pre_test['quiz']->quiz_title }}</h5>
                                </div>
                                <div class="col-sm-4">
                                    <a href="{{ url('e-admin/question/add?quiz_id='.$pre_test['quiz']->quiz_id) }}"
                                       class="btn btn-sm btn-primary">แก้ไข</a>
                                    <button id="delete-pre_test-btn" type="button" class="btn btn-sm btn-danger">ลบ
                                    </button>
                                </div>
                            </div>
                        @endif
                    </section>


                    <section id="tab_posttest" class="tab-pane fade in">
                        <h3>แบบทดสอบหลังเรียน (Post Test)</h3>
                        @if($course->post_test_id==null)
                            <p>ยังไม่มีแบบทดสอบหลังเรียน</p>
                            <button type="button" class="btn btn-sm btn-primary add-quiz-btn" data-target="#sximo-modal"
                                    data-toggle="modal" data-quiz-type="post_test">
                                <i class="fas fa-plus"></i> เพิ่มแบบทดสอบหลังเรียน
                            </button>
                        @else
                            <div class="row">
                                <div class="col-sm-4">
                                    <h5>ชื่อ : {{ $post_test['quiz']->quiz_title }}</h5>
                                </div>
                                <div class="col-sm-4">
                                    <a href="{{ url('e-admin/question/add?quiz_id='.$post_test['quiz']->quiz_id) }}"
                                       class="btn btn-sm btn-primary">แก้ไข</a>
                                    <button id="delete-post_test-btn" type="button" class="btn btn-sm btn-danger">ลบ
                                    </button>
                                </div>
                            </div>
                        @endif
                    </section>

                    <section id="tab_structure" class="tab-pane fade in">
                        <h3>โครงสร้างหลักสูตร</h3>
                        <p>
                            <small>จัดการโครงการหลักสูตรได้โดยการเพิ่มหรือลบบนเรียนที่ต้องการ หรือลากเพื่อจัดลำดับ
                                หลังจากนั้นกดปุ่ม "บันทึกโครงสร้างหลักสูตร"
                            </small>
                        </p>
                        <div class="padding"></div>
                        <div style="background-color:#ddd; padding:15px;">
                            <form action="{{ url('e-admin/course/reorder') }}" method="post">
                                <input type="hidden" name="course_id" value="{{ $course->course_id }}">
                                <div class="sortable">
                                    @if(count($curriculum)==0)
                                        <p class="text-center">
                                            <img src="{{asset('/custom/images/icon-owl-01.png')}}"/>
                                            <br>
                                            โครงสร้างหลักสูตรยังว่างเปล่า คลิกปุ่มด้านล่างเพื่อเพิ่มบทเรียนใหม่
                                        </p>
                                        <div class="padding"></div>
                                    @endif

                                    @foreach($curriculum as $item)

                                        <div class="padding" data-id="{{ $item['object_id'] }}"
                                             data-type="{{ $item['object_type'] }}"
                                             style="position:relative; margin: 10px 0; @if($item['type']=='lecture') background-color:#eee;  border:1px solid #aaa; @elseif($item['type']=='quiz') background-color:#ffeeb9;  border:1px solid #ffc924; @endif">
                                            <input type="hidden" name="object_order[]"
                                                   value="{{ $item['course_id'] }}:{{ $item['object_type'] }}">
                                            <input type="hidden" name="object_type[]" value="">
                                            <i class="fas fa-bars" style="position:absolute; left:10px; top: 10px;"></i>
                                            <div style="margin-left:30px;">
                                                @if($item['type']=='lecture')
                                                    <strong class="f-14">บทเรียน</strong>
                                                @elseif($item['type']=='quiz')
                                                    <strong class="f-14">แบบสอบถาม</strong>
                                                @endif
                                                <h3 class="check">{{ $item['title'] }}</h3>
                                                @if($item['media_type']=='mp4' || $item['media_type']=='album')
                                                    <p>ไฟล์วิดีโอ ตั้งแต่ {{$item['start']}} ถึง {{$item['end']}}</p>
                                                @endif
                                                <div class="padding"></div>
                                                <button type="button" class="btn btn-xs btn-dark"
                                                        onclick="editCurriculumItem($(this))" data-target="#sximo-modal"
                                                        data-toggle="modal">
                                                    <i class="far fa-edit"></i> แก้ไข
                                                </button>
                                                @if($item['type']=='lecture' && ($item['media_type']=='mp4' || $item['media_type']=='album'))
                                                    <a href="{{ url('e-admin/lectures/syncslide?lecture_id='.$item['object_id']) }}"
                                                       class="btn btn-xs btn-dark">
                                                        <i class="far fa-clone"></i> Sync Slide
                                                    </a>
                                                @endif

                                                {{--@if($item['media_type']=='mp4')
                                                <button type="button" class="btn btn-xs btn-dark" onclick="editSyncSlide($(this))" data-target="#sximo-modal" data-toggle="modal">
                                                    <i class="fas fa-file-powerpoint"></i> Sync Slide
                                                </button>
                                                @endif
                                                --}}
                                                <div class="btn-group dropdown">
                                                    <button class="btn btn-xs btn-danger dropdown-toggle" type="button"
                                                            data-toggle="dropdown">
                                                        <i class="far fa-trash-alt"></i> ลบ
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <button type="button"
                                                                    class="btn btn-xs btn-block btn-danger"
                                                                    onclick="removeCurriculum($(this))">
                                                                ยืนยัน
                                                            </button>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="padding"></div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="text-center">
                                    <button id="add-lecture-btn" type="button" class="btn btn-sm btn-default"
                                            data-target="#sximo-modal" data-toggle="modal">
                                        <i class="fas fa-plus"></i> เพิ่มบทเรียนใหม่
                                    </button>
                                    <button type="button" class="btn btn-sm btn-default add-quiz-btn"
                                            data-target="#sximo-modal" data-toggle="modal" data-quiz-type="quiz">
                                        <i class="fas fa-plus"></i> เพิ่มแบบทดสอบระหว่างเรียน
                                    </button>
                                </div>
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <hr class="divider-dark">
                                            <button type="submit" class="btn btn-success pull-right">
                                                บันทึกโครงสร้างหลักสูตร
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="padding"></div>
                    </section>

                    <section id="tab_prerequisite" class="tab-pane fade">
                        <h3>หลักสูตรที่เกี่ยวข้อง</h3>

                    </section>
                    <section id="tab_certification" class="tab-pane fade">
                        <h3>การให้ประกาศณียบัตร</h3>
                        <form class="form-horizontal" role="form" action="{{ url('e-admin/course/update-percentage') }}"
                              method="post" enctype="multipart/form-data">
                            <input type="hidden" name="course_id" value="{{ $course->course_id }}">
                            <div class="form-group">
                                <label for="course_code" class="col-xs-4 control-label">รูปแบบของประกาศณียบัตร
                                    * </label>
                                <div class="col-xs-4">
                                    <select name="cert_id" id="cert_id" class="select2 form-control" required></select>
                                </div>
                                <div class="col-xs-4">
                                    <a href="{{url('e-admin/certificates/add?course_id='.$course->course_id)}}"
                                       class="btn btn-sm btn-dark">
                                        สร้างใบประกาศณียบัตร
                                    </a>
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group">
                                <label for="course_code" class="col-xs-4 control-label">เข้าเรียนไม่น้อยกว่า (%)
                                    * </label>
                                <div class="col-xs-4">
                                    <input type="number" class="form-control" name="percentage_lecture"
                                           id="percentage_lecture" placeholder="50%"
                                           value="{{ $course->percentage_lecture }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="course_code" class="col-xs-4 control-label">คะแนนสอบหลังเรียนไม่น้อยกว่า (%)
                                    * </label>
                                <div class="col-xs-4">
                                    <input type="number" class="form-control" name="percentage_quiz"
                                           id="percentage_quiz" placeholder="50%" value="{{ $course->percentage_quiz }}"
                                           required>
                                </div>
                            </div>
                            <hr class="divider">
                            <div class="form-group">
                                <div class="col-xs-offset-4 col-sm-8">
                                    <button type="submit" class="btn btn-primary">บันทึก</button>
                                </div>
                            </div>
                        </form>
                    </section>

                    <section id="tab_statistic" class="tab-pane fade">

                        <div class="panel panel-default">
                            <div class="panel-heading" align="center">ภาพรวมของหลักสูตร</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-2 text-center">
                                        <small>นักเรียนทั้งหมด</small>
                                        <h2>{{ $stat['enroll_user'] }}</h2>
                                    </div>
                                    <div class="col-sm-2 text-center">
                                        <small>นักเรียนที่เรียนจบ</small>
                                        <h2>{{ $stat['graduated_users'] }}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="padding"></div>


                        @if($course->pre_test_id!=null)
                            <div class="panel panel-default">
                                <div class="panel-heading" align="center">การวัดผล Pre Test</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-2 text-center">
                                            <small>นักเรียนที่สอบ Pre Test</small>
                                            <h2>{{ $stat['pre_test']['total'] }}</h2>
                                        </div>
                                        <div class="col-sm-2 text-center">
                                            <small>นักเรียนที่สอบ Pre Test ผ่าน</small>
                                            <h2>{{ $stat['pre_test']['pass'] }}</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="padding"></div>
                        @endif

                        @if($course->post_test_id!=null)
                            <div class="panel panel-default">
                                <div class="panel-heading" align="center">การวัดผล Post Test</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-2 text-center">
                                            <small>นักเรียนที่สอบ Post Test</small>
                                            <h2>{{ $stat['post_test']['total'] }}</h2>
                                        </div>
                                        <div class="col-sm-2 text-center">
                                            <small>นักเรียนที่สอบ Post Test ผ่าน</small>
                                            <h2>{{ $stat['post_test']['pass'] }}</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="padding"></div>
                        @endif

                        <div class="panel panel-default">
                            <div class="panel-heading" align="center">ข้อมูลรายบุคคล</div>
                            <div class="panel-body">
                                <table width="100%">
                                    <thead style="border-bottom:2px solid #000; background-color:#efefef;">
                                    <td class="padding">
                                        #
                                    </td>
                                    <td class="padding">
                                        ชื่อ นามสกุล
                                    </td>
                                    <td class="padding">
                                        แบบทดสอบก่อนเรียน
                                    </td>
                                    <td class="padding">
                                        แบบทดสอบหลังเรียน
                                    </td>
                                    <td class="padding">
                                        วุฒิบัตร
                                    </td>
                                    </thead>
                                    @foreach($stat_list as $item)
                                        <tr style="border-bottom:1px solid #eee; height:75px;">
                                            <td class="padding">
                                                {{ $item['id'] }}
                                            </td>
                                            <td class="padding">
                                                {{ $item['name'] }}
                                            </td>
                                            <td class="padding">
                                                @if($item['pre_test']['status'])
                                                    <span>ผ่าน</span>
                                                @else
                                                    <span>ยังไม่ผ่าน</span>
                                                @endif
                                            </td>
                                            <td class="padding">
                                                @if($item['post_test']['status'])
                                                    <span>ผ่าน</span>
                                                @else
                                                    <span>ยังไม่ผ่าน</span>
                                                @endif
                                            </td>
                                            <td class="padding">
                                                @if($item['certificate']!=null)
                                                    <span>ได้แล้วเมื่อ<br>{{$item['certificate']->created_at}}</span>
                                                @else
                                                    <span>ยังไม่ได้วุฒิบัตร</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>


                    </section>
                </div>
            </div>
        </div>
        <!-- END SBOX CONTENT -->
    </div>
</div>
<script type="text/javascript">

    Dropzone.autoDiscover = false;

    $(document).ready(function () {

        $('#course_level').val({{$course->course_level}});


        $('#cert_id').jCombo("{!! url('select/certificates') !!}" @if($certificate!=null), {selected_value: '{{ $certificate->id }}'} @endif);


        $("div#video-preview").dropzone({
            maxFilesize: 104857600,
            timeout: 1800000,
            url: "{{url('e-admin/course/upload')}}?type=preview&course_id=" + "{{$course->course_id}}",
            dictDefaultMessage: '<i class="fas fa-upload"></i> เลือกไฟล์ .mp4 จากเครื่องคอมพิวเตอร์ของคุณ',
            success: function (file, response) {
                console.log(response);
                if (response.success) {
                    var r = confirm("อัพโหลดสำเร็จ");
                    location.reload();
                }
            },
            acceptedFiles: ".mp4"
        });

        $("div#document").dropzone({
            maxFilesize: 104857600,
            timeout: 1800000,
            url: "{{url('e-admin/course/upload')}}?type=document&course_id=" + "{{$course->course_id}}",
            dictDefaultMessage: '<i class="fas fa-upload"></i> เลือกเอกสาร .pdf, .doc, .docx จากเครื่องคอมพิวเตอร์ของคุณ',
            success: function (file, response) {
                console.log(response)
                if (response.success) {
                    var r = confirm("อัพโหลดสำเร็จ");
                    location.reload();
                }
            },
            acceptedFiles: ".pdf, .doc, .docx"
        });

        $("div#cover").dropzone({
            maxFilesize: 104857600,
            timeout: 1800000,
            url: "{{url('e-admin/course/upload')}}?type=cover&course_id=" + "{{$course->course_id}}",
            dictDefaultMessage: '<i class="fas fa-upload"></i> เลือกไฟล์ภาพ .jpg ขนาด 800x450pixel',
            success: function (file, response) {
                console.log(response)
                if (response.success) {
                    $.sweetModal({
                        content: 'This is a success.',
                        icon: $.sweetModal.ICON_SUCCESS
                    });
                    location.reload();
                }
            },
            acceptedFiles: "image/jpeg,image/png"
        });

        $('#course_detail').summernote({
            height: 300, toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'hr']],
                ['view', ['fullscreen']],
                ['help', []]
            ]
        });

        $('#add-youtube-as-preview').submit(function (e) {

            var youtube_link = $('#youtube-link').val();

            if (validateYouTubeUrl(youtube_link)) {

                var formData = {'youtube_link': youtube_parser(youtube_link)};
                var url = '{{url('e-admin/course/upload')}}?type=preview&course_id={{$course->course_id}}';

                $.ajax({
                    type: 'POST',
                    url: url,
                    data: formData,
                    dataType: 'json',
                    encode: true
                }).done(function (data) {
                    if (data.success) {
                        location.reload();
                    }
                });
            }

            e.preventDefault();
        });

        $('#add-link-as-document').submit(function (e) {

            var link = $('#document-link').val();

            var formData = { 'document_link': link };
            var url = '{{url('e-admin/course/upload')}}?type=document&course_id={{$course->course_id}}';

            $.ajax({
                type: 'POST',
                url: url,
                data: formData,
                dataType: 'json',
                encode: true
            }).done(function (data) {
                if (data.success) {
                    location.reload();
                }
            });

            e.preventDefault();
        });

        $('#addTagBtn').click(function () {

            var header_area = $("#sximo-modal-header"),
                data_area = $("#sximo-modal-content"),
                url = "{{ url('e-admin/course/tag/add?course_id='.$course->course_id) }}";

            header_area.html('เพิ่มหมวดหมู่หลักสูตรเรียน');
            data_area.html('Loading . . .');

            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });
        })

        $('.add-quiz-btn').click(function () {

            var header_area = $("#sximo-modal-header"),
                data_area = $("#sximo-modal-content"),
                url = "{{ url('e-admin/quiz/add') }}";

            var quiz_type = $(this).attr("data-quiz-type");

            header_area.html('เพิ่มแบบทดสอบสำหรับหลักสูตรนี้');
            data_area.html('Loading . . .');

            $.ajax({
                type: 'GET',
                url: url,
                data: {
                    course_id: '{{$course->course_id}}',
                    quiz_type: quiz_type
                },
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });

        });

        $('#add-lecture-btn').click(function () {

            var header_area = $("#sximo-modal-header"),
                data_area = $("#sximo-modal-content"),
                url = "{{ url('e-admin/lectures/selectMedia?course_id='.$course->course_id) }}";

            header_area.html('เลือกประเภทของสื่อ');
            data_area.html('Loading . . .');

            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });
        });

        $('#delete-pre_test-btn').click(function () {
            $.ajax({
                type: 'POST',
                url: "{{url('/e-admin/course/quiz/delete')}}",
                data: {course_id: '{{$course->course_id}}', quiz_type: 'pre_test'},
                dataType: 'json',
                encode: true
            }).done(function (data) {
                if (data.success) {
                    location.reload();
                }
            });
        });

        $('#delete-post_test-btn').click(function () {
            $.ajax({
                type: 'POST',
                url: "{{url('/e-admin/course/quiz/delete')}}",
                data: {course_id: '{{$course->course_id}}', quiz_type: 'post_test'},
                dataType: 'json',
                encode: true
            }).done(function (data) {
                if (data.success) {
                    location.reload();
                }
            });
        });


        var course_objective = JSON.parse('{{ $course->course_objective }}'.replace(/&quot;/g, '"')),
            objRow = course_objective.length;

        $.each(course_objective, function (index, value) {

            if (index > 0) {
                $('#objective_area').append(`<div class="input-group" id="addr${index}" style="padding-top: 8px"></div>`);
            } else {
                $('#objective_area').append(`<div class="input-group" id="addr${index}"></div>`);
            }

            $('#addr' + index).html(
                `<input type="text" class="form-control" name="course_objective[${index}]"
id="course_objective[${index}]"
placeholder="วัตถุประสงค์" required value="${value}"><span onclick='removeRow($(this))' class="input-group-addon"><i class="fa fa-close"></i></span></div>`
            );

        });

        var i = objRow;

        $("#add_row").click(function () {
            $('#objective_area').append(`<div id="addr${i + 1}" class="input-group" style="padding-top: 8px"><input type="text" class="form-control" name="course_objective[${i + 1}]" id="course_objective[${i + 1}]"
placeholder="วัตถุประสงค์"  required><span onclick='removeRow($(this))' class="input-group-addon"><i class="fa fa-close"></i></span></div>`);
            i++;
        });

        var playerInstance = jwplayer("player").setup({
            "file": "{{asset($course->course_preview)}}",
            "aspectratio": "16:9",
            "controls": true,
            "width": "100%",
            "autostart": "false",
            "skin": {
                "url": "/css/skin-player.css",
                "name": "setskin"
            }
        });

    });

    function removeRow(row) {
        var countRow = $("#objective_area div.input-group").length;
        if (countRow <= 1) {
            return false
        }
        row.closest('div').remove();
    }

    $(function () {
        @if(count($curriculum)==0)
        $(".sortable").sortable("disable")
        @else

        $(".sortable").sortable();
        $(".sortable").disableSelection();
        @endif
    });

    function validateYouTubeUrl(url) {
        if (url != undefined || url != '') {
            var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
            var match = url.match(regExp);
            if (match && match[2].length == 11) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    function youtube_parser(url) {
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        var match = url.match(regExp);
        return (match && match[7].length == 11) ? match[7] : false;
    }

    function editCurriculumItem(row) {

        var _this = $(row).parent().parent();
        var object_id = _this.attr("data-id");
        var object_type = _this.attr("data-type");

        if (object_type == 'lecture') {
            $('#sximo-modal-header').text('แก้ไขหลักสูตรเรียน');

            var id = $(this).data('id');
            var data_area = $("#sximo-modal-content"),
                url = "{{ url('e-admin/lectures/update') }}" + "/" + object_id;

            data_area.html('Loading . . .');

            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });
        }
        else if (object_type == 'quiz') {
            $('#sximo-modal-header').text('แก้ไขคำถาม');

            var id = $(this).data('id');
            var data_area = $("#sximo-modal-content"),
                url = "{{ url('e-admin/quiz/update') }}" + "/" + object_id;

            data_area.html('Loading . . .');

            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });
        }
    }

    function editSyncSlide(row) {

        var _this = $(row).parent().parent();
        var lecture_id = _this.attr("data-id");

        $('#sximo-modal-header').text('SYNC SLIDES');

        var id = $(this).data('id');
        var data_area = $("#sximo-modal-content"),
            url = "{{ url('e-admin/lectures/syncslide') }}" + "?lecture_id=" + lecture_id;

        data_area.html('Loading . . .');

        $.ajax({
            type: 'GET',
            url: url,
            success: function (data) {
                data_area.html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                data_area.html('Error Please try again');
            }
        });
    }

    function removeCurriculum(row) {
        $(row).closest('div').closest('div.padding').fadeOut("slow", function () {
            $(this).remove()
        });
    }

    function deleteCourseDocument() {
        var c = confirm('คุณต้องการลบเอกสารประกอบการเรียน ?');
        if (c === true) {
            $.ajax({
                type: 'POST',
                url: '{{ url('e-admin/course/delete/document') }}',
                data: {
                    'course_id': '{{ $course->course_id }}'
                },
                success: function (data) {
                    if (data.status === 'success') {
                        $('.courseDocument').html('คุณยังไม่ได้อัพโหลดไฟล์เอกสาร');
                    }
                    $.toast({
                        heading: data.status,
                        text: data.message,
                        position: 'bottom-right',
                        icon: data.status,
                        hideAfter: 2500,
                        stack: 6
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $.toast({
                        heading: 'เกิดข้อผิดพลาด',
                        text: 'โปรดลองใหม่อีกครั้ง',
                        position: 'bottom-right',
                        icon: 'error',
                        hideAfter: 2500,
                        stack: 6
                    });
                }
            });
        } else {
            return false;
        }
    }

</script>