<div class="container">
    <h4 align="center">{{ $title }}</h4>
    <div class="padding"></div>
    <form class="form-horizontal" action="{{ url('e-admin/info/save') }}" method="post">

        <div class="form-group">
            <label class="control-label col-sm-2" for="info_detail">ข้อมูล Info</label>
            <div class="col-sm-8">
                <textarea name="info_detail" rows="10" id="info_detail" class="form-control" required>
                    {{$info->info_detail}}
                </textarea>
            </div>
        </div>

        <hr class="divider">
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <button type="submit" class="btn btn-success">ตกลง</button>
            </div>
        </div>
    </form>
</div>
<script>

    $('#info_detail').summernote({
        placeholder: 'ใส่ข้อมูล Info ตรงนี้...',
        height: 200,
        disableResizeEditor: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['insert', ['link', 'picture']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ],
        onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable);
        }
    });

    function sendFile(file, editor, welEditable) {
        data = new FormData();
        data.append("file", file);
        $.ajax({
            data: data,
            type: "POST",
            url: "{{url('e-admin/info/upload')}}",
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                alert('upload image success!')
                var image = $('<img>').attr('src', data.file);
                $('#answer').summernote("insertNode", image[0]);
            }
        });
    }
</script>