<style>
    .modal {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        overflow: hidden;
    }

    .modal-dialog {
        position: fixed;
        margin: 0;
        width: 100%;
        height: 100%;
        padding: 0;
    }

    .modal-content {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        border: 2px solid #1ab394;
        border-radius: 0;
        box-shadow: none;
    }

    .modal-header {
        position: absolute;
        top: 0;
        right: 0;
        left: 0;
        height: 50px;
        padding: 10px;
        background: #1ab394;
        border: 0;
    }

    .modal-title {
        font-weight: 300;
        font-size: 2em;
        color: #fff;
        line-height: 30px;
    }

    .modal-body {
        position: absolute;
        top: 50px;
        bottom: 60px;
        width: 100%;
        font-weight: 300;
        overflow: auto;
    }

    .modal-footer {
        position: absolute;
        right: 0;
        bottom: 0;
        left: 0;
        height: 60px;
        padding: 10px;
        background: #f1f3f5;
    }

    .btn-modal {
        position: absolute;
        top: 50%;
        left: 50%;
        margin-top: -20px;
        margin-left: -100px;
        width: 200px;
    }

    ::-webkit-scrollbar {
        -webkit-appearance: none;
        width: 10px;
        background: #f1f3f5;
        border-left: 1px solid darken(#f1f3f5, 10%);
    }

    ::-webkit-scrollbar-thumb {
        background: darken(#f1f3f5, 20%);
    }
</style>

<div class="container">
    <div class="col-xs-offset-2 col-xs-8">
        <div class="row">
            <div class="col-xs-6">
                <label>Certificates All</label>
            </div>
            <div class="col-xs-6">
                <div class="pull-right">
                    <a href="{{ url('e-admin/certificates/add') }}" class="btn btn-primary btn-xs"> Add </a>
                </div>
            </div>
        </div>
        <table id="tb_cert" class="table table-striped table-hover nowrap" width="100%">
            <thead>
            <tr>
                <th>ใบประกาศนียบัตร</th>
                <th>รูปแบบ</th>
                <th></th>
            </tr>
            </thead>
            <tobody>
                @foreach( $certs as $cert)
                    <tr>
                        <td>{{ $cert->cert_name }}</td>
                        <td>{{ $cert->cert_type }}</td>
                        <td>
                            <button type="button" class="btn btn-primary btn-xs preview"
                                    data-cert_id="{{ $cert->id }}"
                                    data-toggle="modal" data-target="#preview">preview
                            </button>
                            <button type="button" class="btn btn-primary btn-xs edit"
                                    data-cert_id="{{ $cert->id }}"
                                    data-toggle="modal" data-target="#preview">edit</button>
                        </td>
                    </tr>
                @endforeach
            </tobody>
        </table>
    </div>
</div>
<!-- Modal -->
<div id="preview" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Certificate Preview</h4>
            </div>
            <div class="modal-body" id="preview_area">
                <p>Loading...</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-modal btn-default" data-dismiss="modal">ปิด</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        drawTable();
        setPaginageAction();
        $(".sorting_desc").click(setPaginageAction);
        $(".sorting_asc").click(setPaginageAction);
        $(".sorting").click(setPaginageAction);
        $('.preview').click(setPaginageAction);
        preview();
        edit();
    });

    function drawTable() {
        var table = $('#tb_cert').dataTable({
            scrollX: true,
            "bLengthChange": false,
            "bInfo": false,
            columnDefs: [
                { orderable: false, targets: -1 }
            ],
        });
        $("#tb_cert_filter").on("keyup", 'input', function () {
            setPaginageAction()
        });
    }

    function setPaginageAction() {
        $(".paginate_button").click(setPaginageAction);

        $('.preview').unbind("click");
        $('.edit').unbind("click");

        preview();
        edit();
    }

    function preview() {
        $('.preview').click(function (e) {
            var data = {"cert_id": $(this).data('cert_id')},
                url = "{{url('e-admin/certificate/preview/index')}}";
            $('#preview_area').html(`<p>Loading...</p>`);
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                success: function (data) {
                    $('#preview_area').html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#preview').modal('hide');
                    $.toast({
                        heading: 'error',
                        text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                        position: 'bottom-right',
                        icon: 'error',
                        hideAfter: 2500,
                        stack: 6
                    });
                }
            });
        })
    }

    function edit() {
        $('.edit').click(function (e) {
            var data = {"cert_id": $(this).data('cert_id')},
                url = "{{url('e-admin/certificate/form')}}";
            $('#preview .modal-title').html(`แก้ไข Certificate`);
            $('#preview_area').html(`<p>Loading...</p>`);
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                success: function (data) {
                    $('#preview_area').html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#preview').modal('hide');
                    $.toast({
                        heading: 'error',
                        text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                        position: 'bottom-right',
                        icon: 'error',
                        hideAfter: 2500,
                        stack: 6
                    });
                }
            });
        })
    }
</script>