<style>

    @font-face {
        font-family: 'THSarabun';
        font-style: normal;
        font-weight: normal;
        src: url("{{ asset('custom/font/THSarabunNew/THSarabunNew.ttf') }}") format('truetype');
    }

    @font-face {
        font-family: 'THSarabunBold';
        font-style: normal;
        font-weight: bold;
        src: url("{{ asset('custom/font/THSarabunNew/THSarabunNew Bold.ttf') }}") format('truetype');
    }

    @font-face {
        font-family: 'THSarabunBoldItalic';
        font-style: italic;
        font-weight: normal;
        src: url("{{ asset('custom/font/THSarabunNew/THSarabunNew BoldItalic.ttf') }}") format('truetype');
    }

    @font-face {
        font-family: 'THSarabunItalic';
        font-style: italic;
        font-weight: bold;
        src: url("{{ asset('custom/font/THSarabunNew/THSarabunNew Italic.ttf') }}") format('truetype');
    }

    .th-sarabun {
        font-family: 'THSarabun';
    }

    .a4{
        font-size:  80px !important;
        position: relative; 
        width:3508px; 
        height:2280px; 
        zoom:30%; 
        margin:auto; 
        left:auto; 
        right:auto;
    }


</style>

<div class="th-sarabun a4">

    <div class="text-center">
        <img src="{{url('frontend/medilab/img/logo.png')}}" width="400px" />
        <h1 style="font-size: 100px; margin-top:120px;">วุฒิบัตรฉบับนี้ให้เพื่อแสดงว่า</h1>

        <h1 style="font-size: 150px; margin-top:120px;">{{$user->name_title}} {{$user->first_name}} {{$user->last_name}}</h1>

        <h1 style="font-size: 80px; margin-top:80px;">ผ่านการเข้าอบรม U League {{ DataHelpers::year() }} ครบถ้วนตามหลักสูตร ดังนี้</h1>

        <div class="row" style="margin-top:100px;">
            @foreach($my_certs as $item)
            <div class="col-xs-{{$col}}" style="height:380px;">
                <p style="font-weight:bold;">{{$item['name']}}</p>
                <p style="font-size:60px; margin-top:30px;">ณ {{ $item['venue']->name }}</p>
                <p style="font-size:60px; margin-top:30px;">เข้าร่วมวันที่ {{ $item['end_date'] }}</p>
            </div>
            @endforeach
        </div>

        <h1 style="font-size: 90px; margin-top:80px;"></h1>

    </div>


    <div style="position:absolute; width:100%; top:1840px;">
        <div class="text-center">
            นาย...........................................
        </div>
    </div>
    
    <div style="position:absolute; width:100%; top:2000px;">
        <div class="text-center">
            ตำแหน่ง...........................................
        </div>
    </div>

    <div style="position:absolute; width:100%; top:2200px;">
        <div class="text-center">
            วุฒิบัติฉบับนี้ถูกสร้างขึ้นโดยระบบอัตโนมัติ Startup Thailand
        </div>
    </div>

    <div style="position:absolute; width:100%; top:2300px;">
        <div class="text-center">
            สอบถามข้อมูลเพิ่มเติม กรุณาติดต่อ คุณพัชรีนาถ โทร 02 017 5555 ต่อ 209  อีเมล : startup@nia@or.th
        </div>
    </div>