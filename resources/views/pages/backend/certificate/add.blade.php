<div class="padding">
<div class="row">
    <form action="{{ url('e-admin/certificate/form') }}" method="post" class="form-inline" id="create-cer">
        <div class="panel panel-default">
            <div class="panel-heading" align="center">ออกใบประกาศณียบัตร</div>
            <div class="panel-body">
                <div class="row text-center">
                    <input type="hidden" name="course_id" id="course_id" value="{{$course->course_id}}" />
                    <div class="form-group">
                        <label for="cer_name">ชื่อ</label>
                        <input type="text" @if(isset($course)) value="ประกาศณียบัตรของ {{$course->course_title}}" @endif name="cer_name" id="cer_name" class="form-control" placeholder="ชื่อใบ Certificate" style="width:300px;" required>
                    </div>
                    <div class="form-group">
                        <label for="cer_style">&nbsp;&nbsp;เลือกรูปแบบ&nbsp;&nbsp;</label>
                        <select name="cer_style" id="cer_style" class="form-control" style="width:100px;">
                            <option value="A">รูปแบบ A</option>
                            <option value="B">รูปแบบ B</option>
                            {{-- <option value="B">รูปแบบ B</option> --}}
                        </select>
                    </div>
                    <div class="form-group">
                        &nbsp;<button class="btn btn-primary">ตกลง</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="certificate_form"></div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        createForm()
    });

    function createForm() {
        $('#create-cer').submit(function (event) {
            event.preventDefault();
            
            var data_area = $('#certificate_form');
            
            $.ajax({
                type: 'POST',
                data: $(this).serialize(),
                cache: false,
                url: $(this).attr('action'),
                success: function (data) {
                    data_area.html(data);
                    $('#search').click();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $.toast({
                        heading: 'error',
                        text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                        position: 'bottom-right',
                        icon: 'error',
                        hideAfter: 2500,
                        stack: 6
                    });
                }
            });
        })
    }
</script>