{{--{{ dd($users) }}--}}
<div class="container-fluid">
    <div class="row padding">
        <div class="col-sm-4 pull-left">
            <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#preview"><i class="fa fa-plus"></i> เพิ่ม
                Admin
            </button>
        </div>
        <div class="col-sm-4" align="center">
        </div>
        <div class="col-sm-4" align="right">
        </div>
    </div>
    <div class="row ">
        <table class="table table-striped table-hover" id="tb_users" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ชื่อ - นามสกุล</th>
                    <th>อีเมล</th>
                    <th>สถานะ</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <td>
                        <button type="button" class="btn btn-sm btn-danger" onclick="deleteAdmin($(this))"
                                data-id="{{ $user->email }}">
                            <i class="fa fa-trash"></i>
                        </button>
                    </td>
                    <td>{{ $user->first_name }} {{ $user->last_name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ ($user->group_id == 1)? 'Superadmin' : ($user->group_id == 2)? 'Admin' : 'User' }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<!-- modal -->
<div id="preview" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>เพิ่ม Admin</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" id="add-admin" action="{{ url('e-admin/admin/add') }}"
                      method="post">
                    <div class="form-group">
                        <label class="col-sm-2 control-label text-right" style="padding-top: 7px"
                               for="email">Email : </label>
                        <div class="col-sm-6">
                            <input type="email" class="form-control"
                                   id="email" placeholder="Email" name="email"/>
                        </div>
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-default">
                                <i class="fa fa-plus"></i> เพิ่ม
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        drawTable();
        setPaginageAction();
        $(".sorting_desc").click(setPaginageAction);
        $(".sorting_asc").click(setPaginageAction);
        $(".sorting").click(setPaginageAction);

        $("#add-admin").submit(function (e) {
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                success: function (data) {
                    $.toast({
                        heading: data.status,
                        text: data.message,
                        position: 'bottom-right',
                        icon: data.status,
                        hideAfter: 2500,
                        stack: 6
                    });
                    $('#preview').modal('hide');
                    location.reload();
                },
                cache: false,
                contentType: false,
                processData: false
            });
            return false;
        });



    });

    function drawTable() {
        var table = $('#tb_users').dataTable({
            dom: 'Bfrtip',
            "aaSorting": [],
            columnDefs: [
                {orderable: false, targets: 0}
            ],
            buttons: [{
                extend: 'excel',
                title: 'ข้อมูลแอดมิน',
                filename: 'ข้อมูลแอดมิน',
                exportOptions: {
                    columns: [1, 2, 3],
                    format: {
                        body: function (data, row, column, node) {
                            return column >= 3 ? data.split("  ")[0] : data;
                        }
                    }
                }
            }],
            "searching": true,
            "scrollX": true,
            "responsive": true,
        });
        $("#tb_users_filter").on("keyup", 'input', function() {
            setPaginageAction()
        });
    }

    function setPaginageAction() {
        $(".paginate_button").click(setPaginageAction);
    }

    function deleteAdmin(ele) {
        var email = $(ele).data('id'),
            url = '{{ url('/e-admin/admin/delete') }}?email='+email;
        
        $.ajax({
            url: url,
            type: 'POST',
            success: function (data) {
                $.toast({
                    heading: 'success',
                    text: 'ลบแอดมินสำเร็จ',
                    position: 'bottom-right',
                    icon: 'success',
                    hideAfter: 2500,
                    stack: 6
                });
                location.reload();
            }, error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'error',
                    text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 2500,
                    stack: 6
                });
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
</script>