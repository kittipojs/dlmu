{{-- <h5>เลือก hightlight event ได้สูงสุด 3 events</h5> --}}
<div class="row events-wrapper">
    @foreach($events as $key => $item)
    @if($key%4==0 && $key>0)
</div>
<div class="row events-wrapper">
    @endif
    <div class="col-sm-3">
        <div class="event-item">
            <div class="text-center">
                <img src="{{ $item['cover'] }}" class="img-responsive" style="max-height:100px;" />
            </div>
            <div class="event-content padding">
                <a href="{{ $item['url'] }}" target="_blank"><h4>{{ $item['name'] }}</h4></a>
                <p>
                    <i class="fa fa-calendar"></i> วันที่ : {{ $item['start_date'] }} - {{ $item['end_date'] }}
                </p>
                <p>
                    <i class="fa fa-map-marker"></i> สถานที่ : {{ $item['venue']->name }}
                </p>
            </div>
            <div class="padding">
                <div class="btn-group" role="group">
                    @if(!$item['is_highlight'])
                    <button type="button" class="btn btn-sm btn-success" data-id="{{ $item['id'] }}">
                        <i class="fa fa-star"></i> ตั้งเป็น Hightlight
                    </button>
                    @else
                    <button type="button" class="btn btn-sm btn-danger" data-id="{{ $item['id'] }}">
                        <i class="fa fa-close"></i> ลบออกจาก highlight
                    </button>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
<script type="text/javascript">
    $('.btn-success').click(function(){

        var b = $(this);
        var event_id = b.data("id");

        var formData = { event_id:event_id }

        $.ajax({
            type        : 'POST',
            url         : '{{ url('/e-admin/event') }}',
            data        : formData,
            dataType    : 'json',
            encode      : true
        }).done(function(data) {
            if(data.success){
                refreshDataArea();
            }
            else{
                alert(data.message)
            }
        });
    });

    $('.btn-danger').click(function(){

        var b = $(this);
        var event_id = b.data("id");

        var formData = { event_id:event_id }

        $.ajax({
            type        : 'DELETE',
            url         : '{{ url('/e-admin/event') }}',
            data        : formData,
            dataType    : 'json',
            encode      : true
        }).done(function(data) {
            if(data.success){
                refreshDataArea();
            }
        });
    });

    function param() {

        var start = $('#start').val(),
            end = $('#end').val();

        var params = { start: start, end: end };

        return params;
    }


    function refreshDataArea(){
        var data_area = $("#details"),
            url,
            params = param();

        url = "{{url('e-admin/events/result')}}" + "?" + $.param(params);

        //data_area.html('กำลังโหลดข้อมูล . . .');

        $.ajax({
            type: 'GET',
            url: url,
            success: function (data) {
                data_area.html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(JSON.stringify(jqXHR))
                data_area.html('เกิดข้อผิดพลาด');
            }
        });
    }
</script>