<style>
    .modal {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        overflow: hidden;
    }

    .modal-dialog {
        position: fixed;
        margin: 0;
        width: 100%;
        height: 100%;
        padding: 0;
    }

    .modal-content {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        border: 2px solid #1ab394;
        border-radius: 0;
        box-shadow: none;
    }

    .modal-header {
        position: absolute;
        top: 0;
        right: 0;
        left: 0;
        height: 50px;
        padding: 10px;
        background: #1ab394;
        border: 0;
    }

    .modal-title {
        font-weight: 300;
        font-size: 2em;
        color: #fff;
        line-height: 30px;
    }

    .modal-body {
        position: absolute;
        top: 50px;
        bottom: 60px;
        width: 100%;
        font-weight: 300;
        overflow: auto;
    }

    .modal-footer {
        position: absolute;
        right: 0;
        bottom: 0;
        left: 0;
        height: 60px;
        padding: 10px;
        background: #f1f3f5;
    }

    .btn-modal {
        position: absolute;
        top: 50%;
        left: 50%;
        margin-top: -20px;
        margin-left: -100px;
        width: 200px;
    }

    ::-webkit-scrollbar {
        -webkit-appearance: none;
        width: 10px;
        background: #f1f3f5;
        border-left: 1px solid darken(#f1f3f5, 10%);
    }

    ::-webkit-scrollbar-thumb {
        background: darken(#f1f3f5, 20%);
    }
</style>
<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading" align="center"> Certificate Style {{ $style }}</div>
        <div class="panel-body">
            <form action="{{ url('e-admin/event/certificate/preview') }}" method="post" class="form-horizontal" role="form" enctype="multipart/form-data" id="cerForm">
                <input type="hidden" name="style" id="style" value="{{ $style }}">
                <input type="hidden" name="name" id="name" value="{{ $name }}">
                <input type="hidden" name="event_id" id="event_id" value="{{$event_id}}">
                @if(isset($edit))
                    <input type="hidden" name="edit" id="edit" value="{{$edit}}">
                @endif
                @foreach( $elements as $element)
                <div class="form-group">
                    <label for="{{ $element->title }}" class="control-label col-sm-4">
                        {{ $element->title }}
                    </label>
                    <div class="col-sm-6">
                        <input type="{{ ($element->type == 'image')? 'file' : $element->type }}" name="{{ $element->name }}"
                               id="{{ $element->name }}" class="form-control"
                        value="{{ ($element->type == 'image')? asset($element->value) : $element->value }}">
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                @endforeach
                <div class="form-group">
                    <div class="col-sm-offset-6 col-sm-6">
                        <button class="btn btn-default" type="button" id="btn_submit">บันทึก</button>
                        @if(!isset($edit))<button class="btn btn-primary" type="submit" id="btn_preview" data-toggle="modal" data-target="#preview">ตัวอย่าง</button>@endif
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="preview" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Certificate Style {{ $style }} Preview</h4>
            </div>
            <div class="modal-body" id="preview_area">
                <p>Loading...</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-modal btn-default" data-dismiss="modal">ปิด</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $("#cerForm").submit(function(e) {
            e.preventDefault();

            var formData = new FormData(this);
            $('#preview_area').html('');
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#preview_area').html(data);
                }, error: function (jqXHR, textStatus, errorThrown) {
                    $('#preview').modal('hide');
                    $.toast({
                        heading: 'error',
                        text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                        position: 'bottom-right',
                        icon: 'error',
                        hideAfter: 2500,
                        stack: 6
                    });
                },
                cache: false,
                contentType: false,
                processData: false
            });
            return false;
        });

        $('#btn_submit').click(function(e){

            var formData = new FormData(document.getElementById('cerForm'));
            $('body').loadingModal({text: 'Saving...'});
            var url = "{{url('/e-admin/event/certificate/save')}}";
            $.ajax({
                url: url,
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('body').loadingModal('destroy');
                    @isset($edit)$('#preview').modal('hide');@endif
                    $.toast({
                        heading: 'success',
                        text: 'สร้าง Certificate สำเร็จ',
                        position: 'bottom-right',
                        icon: 'success',
                        hideAfter: 2500,
                        stack: 6
                    });
                    $('#preview_area').html(data);
                    
                    window.location = "{{url('/e-admin/event/certificates')}}"
                }, error: function (jqXHR, textStatus, errorThrown) {
                    $('body').loadingModal('destroy');
                    $.toast({
                        heading: 'error',
                        text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                        position: 'bottom-right',
                        icon: 'error',
                        hideAfter: 2500,
                        stack: 6
                    });
                },
                cache: false,
                contentType: false,
                processData: false
            });
            // return false;
        })
    });
</script>