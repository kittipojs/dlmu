<div class="container">

    <div class="">
        <h3>{{ $title }}</h3>
    </div>
    
    <div class="menu-control">
        <a href="{{url('/e-admin/event/certificates/add')}}" class="btn btn-sm btn-primary">สร้างใบประกาศณียบัตรใหม่</a>
    </div>
    <div class="row padding">
        <table id="tb_data" class="table table-hover nowrap" width="100%">
            <thead>
            <tr>
                <th></th>
                <th>ชื่อใบประกาศนียบัตร</th>
                {{-- <th>รูปแบบ</th> --}}
            </tr>
            </thead>
            <tobody>
                @foreach( $certs as $cert)
                    <tr>
                        <td>
                            <button type="button" class="btn btn-xs btn-primary view-info-btn"
                                    onclick="viewCertificate($(this))" data-id="{{ $cert->id }}">
                                <i class="fa fa-eye"></i> แก้ไข
                            </button>
                            {{--
                            <button type="button" class="btn btn-primary btn-xs preview"
                                    data-cert_id="{{ $cert->id }}"
                                    data-toggle="modal" data-target="#preview">preview
                            </button>
                            <button type="button" class="btn btn-primary btn-xs edit"
                                    data-cert_id="{{ $cert->id }}"
                                    data-toggle="modal" data-target="#preview">edit</button>
                            --}}
                        </td>
                        <td>{{ $cert->cert_name }}</td>
                        {{-- <td>{{ $cert->cert_type }}</td>--}}
                    </tr>
                @endforeach
            </tobody>
        </table>
    </div>
</div>
@include('modal.modal')
<script type="text/javascript">

    $(document).ready(function () {
        drawTable();
        //Edit();

        setPaginageAction();
        $(".sorting_desc").click(setPaginageAction);
        $(".sorting_asc").click(setPaginageAction);
        $(".sorting").click(setPaginageAction);
    });

    function drawTable() {
        var table = $('#tb_data').dataTable({
            dom: 'Bfrtip',
            "aaSorting": [],
            columnDefs: [
                { orderable: false, targets: -1}
            ],
            buttons: [{
                extend: 'excel',
                title: 'รายการหลักสูตร',
                filename: 'ข้อมูลคอร์ส',
                exportOptions: {
                    columns: ':visible',
                    format: {
                        body: function (data, row, column, node) {
                            return column >= 3 ? data.split("  ")[0] : data;
                        }
                    }
                }
            }],
            "searching": true,
            "scrollX": true,
            "responsive": true,
        });
        $("#tb_course_filter").on("keyup", 'input', function() {
            setPaginageAction()
        });
    }

    function viewCertificate(ele) {
        var certificates_id = $(ele).data('id'),
            url = '{{ url('/e-admin/event/certificates/edit') }}' + '/' + certificates_id;

        window.location.href = url;
    }

    function deleteCourse(ele) {
        var course_id = $(ele).data('id'),
            url = '{{ url('/e-admin/course/delete') }}' + '/' + course_id;

        $.ajax({
            url: url,
            type: 'GET',
            success: function (data) {
                $.toast({
                    heading: 'success',
                    text: data.message,
                    position: 'bottom-right',
                    icon: 'success',
                    hideAfter: 2500,
                    stack: 6
                });
                //location.reload();
            }, error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'error',
                    text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 2500,
                    stack: 6
                });
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    function setPaginageAction() {
        $(".paginate_button").click(setPaginageAction);
        $('.edit').unbind("click");
    }
</script>