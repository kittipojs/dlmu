<style>
    @font-face {
        font-family: 'THSarabun';
        font-style: normal;
        font-weight: normal;
        src: url("{{ asset('custom/font/THSarabunNew/THSarabunNew.ttf') }}") format('truetype');
    }

    @font-face {
        font-family: 'THSarabunBold';
        font-style: normal;
        font-weight: bold;
        src: url("{{ asset('custom/font/THSarabunNew/THSarabunNew Bold.ttf') }}") format('truetype');
    }

    @font-face {
        font-family: 'THSarabunBoldItalic';
        font-style: italic;
        font-weight: normal;
        src: url("{{ asset('custom/font/THSarabunNew/THSarabunNew BoldItalic.ttf') }}") format('truetype');
    }

    @font-face {
        font-family: 'THSarabunItalic';
        font-style: italic;
        font-weight: bold;
        src: url("{{ asset('custom/font/THSarabunNew/THSarabunNew Italic.ttf') }}") format('truetype');
    }

    .th-sarabun {
        font-family: 'THSarabun';
    }
</style>

<div style="position: relative; width:3508px; height:2480px; zoom:30%; margin:auto; left:auto; right:auto;" class="th-sarabun">
    @foreach( $elements as $element)
        <div style="position: absolute;left :{{ $element->position_x }}; top: {{ $element->position_y }}; {{ (isset($element->cssStyle))? $element->cssStyle : '' }}"
                {{ (isset($element->width))? $element->width : '' }}>
            @if($element->type === 'image')
                <img src="{{ asset($element->value) }}" alt="{{ $element->name }}" style="width:{{ $element->size_w }}; height:{{ $element->size_h }};">
            @else
                {{ $element->value }}
            @endif
        </div>
    @endforeach
</div>