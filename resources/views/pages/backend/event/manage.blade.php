<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-warning">
                <div class="panel-heading" align="center">ค้นหาอีเว้นท์</div>
                <div class="panel-body">
                    <div class="form-inline">
                        <label for="team_name">เริ่ม</label>
                        <input name="start" id="start" class="form-control date" />

                        <label for="email">&nbsp;&nbsp;สิ้นสุด&nbsp;&nbsp;</label>
                        <input name="end" id="end" class="form-control date" />
                        <button id="search" type="submit" class="btn btn-primary mb-2">ค้นหา</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <!-- Data Area -->
    <div id="details"></div>
    <!--/Data Area -->
</div>
<script type="text/javascript">

    $(document).ready(function(){

        $('.date').datepicker( {container:'#myDatetimeId', format: 'yyyy-mm-dd', autoclose:true, changeMonth: true, changeYear: true} ).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });

        function param() {

            var start = $('#start').val(),
                end = $('#end').val();

            var params = { start: start, end: end };

            return params;
        }

        $('#search').click(function () {
            var data_area = $("#details"),
                url,
                params = param();
            
            url = "{{url('e-admin/events/result')}}" + "?" + $.param(params);
            
            data_area.html('กำลังโหลดข้อมูล . . .');
            
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(JSON.stringify(jqXHR))
                    data_area.html('เกิดข้อผิดพลาด');
                }
            });
        })

    })
</script>