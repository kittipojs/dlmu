<!-- search -->
<div class="container">
    <div class="row">
        <div class="col-sm-offset-6 col-sm-4">
            <select name="event" id="event" class="form-control"></select>
        </div>
        <div class="col-sm-2">
            <button type="button" id="filter" class="btn btn-default btn-block">
                ดูรายชื่อ
            </button>
        </div>
    </div>

<script>
    
    $(document).ready(function () {
        $('#event').jCombo("{!! url('select/event') !!}");
        $("#filter").click(function () {
            loadList()
        });
        loadList();
    });
    
    function param() {

        var round = $('#event').val();

        var params = {
            round: round
        };

        return params;

    }
</script>
<!--/search-->
<div class="padding"></div>
<div class="padding"></div>
<!-- report list -->
<div id="list">
</div>
    <script>
        function loadList() {
            var data_area = $("#list"),
                url,
                params = param();
            url = "{{url('e-admin/report/event/details')."?"}}" + $.param(params);
            data_area.html('Loading . . .');
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });
        }
    </script>
<!--/report list -->
</div>