<table class="table table-striped table-hover" id="tb_quiz" width="100%">
    <thead>
        <tr>
            <th>ชื่อแบบทดสอบ</th>
            <th>รายละเอียด</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($quiz as $item)
        <tr>
            <td>{{ $item->quiz_title }}</td>
            <td>{{ $item->quiz_description }}</td>
            <td data-values="action" data-key="{{ $item->quiz_id }}">
                <div class=" action ">
                    <div class="dropdown">
                        <button type="button" data-target="#nia-modal" data-toggle="modal" class="btn btn-xs btn-dark edit append" data-id="{{ $item->quiz_id }}">
                            <i class="fa fa-plus"></i> เพิ่ม
                        </button>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="padding text-center">
    <p>หรือ</p>
    <a href="{{url('/e-admin/quiz/add')}}" class="btn btn-primary btn-md">สร้างแบบทดสอบใหม่</a>
</div>
<script>

    $(document).ready(function () {
        drawTable()
        addCurriculum()
    });

    function drawTable() {

        var table = $('#tb_quiz').dataTable({
            searching: true,
            scrollX: true,
            destroy: true,
            responsive: true,
        });
        $("#tb_quiz_filter").on("keyup", 'input', function() {
            setPaginageAction()
        });
    }

    function Edit() {
        $('.edit').click(function () {
            $('#modal-header').text('อัพเดทข้อมูล');
            var id = $(this).data('id');
            var data_area = $("#modal-info"),
                url;
            url = "{{ url('e-admin/quiz/update') }}" + "/" + id;
            data_area.html('Loading . . .');
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });
        })
    }

    function addCurriculum() {
        $('.append').click(function () {
            var id = $(this).data('id'),
                row = $(this).closest("tr"),
                title = $(row).children('td').eq(0).text(),
                alert = $(row).children('td').eq(2).find('button'),
                checkList = $('span.check'),
                duplicate = false;
            if($(this).hasClass('disabled')){
                $.toast({
                    heading: 'แบบทดสอบซ้ำ',
                    text: `เพิ่ม ${title} อยู่ในหลักสูตรนี้แล้ว`,
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 2500,
                    stack: 6
                });
                return false;
            }
            checkList.each(function (index,object) {
                if($(object).text() === title){
                    duplicate = true;
                }
            });

            if(duplicate){
                alert.removeClass('btn-dark');
                alert.addClass('btn-warning disabled');
                alert.html('<i class="fa fa-warning"></i> เพิ่มซ้ำ');
                $.toast({
                    heading: 'แบบทดสอบซ้ำ',
                    text: `เพิ่ม ${title} อยู่ในหลักสูตรนี้แล้ว`,
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 2500,
                    stack: 6
                });
            }
            else {
                alert.removeClass('btn-dark');
                alert.addClass('btn-primary disabled');
                alert.html('<i class="fa fa-check"></i> เพิ่มสำเร็จ');
                $('div.sortable').append(`<div class="padding" style="margin: 10px 0; background-color:#eee; border:1px solid #aaa;">
                <input type="hidden" name="object_order[]" value="${id}">
                <i class="fas fa-bars"></i>
                <div style="height:50px; margin-left:30px;">
                    quiz - <span class="check">${title}</span>
                    </div>
                    <button type="button" class="btn btn-xs btn-danger" onclick="removeCurriculum($(this))"><i class="far fa-trash-alt"></i> ลบออก </button>
                    </div>`)
            }
        })
    }
</script>