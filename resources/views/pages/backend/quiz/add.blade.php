<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="sbox">
                <div class="sbox-title">
                    <h4 align="center">{{ $title }}</h4>
                </div>
                <!-- START SBOX CONTENT -->
                <div class="sbox-content">
                    <form id="add-quiz-form" class="form-horizontal" action="{{ url('e-admin/quiz/save') }}" method="post">

                        <input type="hidden" name="course_id" value="{{ $course_id }}">
                        <input type="hidden" name="quiz_type" value="{{ $quiz_type }}">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="quiz_description">ประเภทของแบบทดสอบ</label>
                            <div class="col-sm-8">
                                <select id="quiz_type" name="quiz_type" class="form-control" disabled>
                                    <option value="1" @if($quiz_type=='pre_test') selected @endif>แบบทดสอบก่อนเรียน (Pre Test)</option>
                                    <option value="2" @if($quiz_type=='quiz') selected @endif>แบบทดสอบระหว่างเรียน (Quiz)</option>
                                    <option value="3" @if($quiz_type=='post_test') selected @endif>แบบทดสอบหลังเรียน (Post Test)</option>
                                </select>
                            </div>
                        </div>
                        {{--
                        <div class="form-group" style="display:none;">
                            <label class="control-label col-sm-4" for="course_id">คอร์สเรียน </label>
                            <div class="col-sm-8">
                                <select name="course_id" id="course_id" class="form-control">
                                </select>
                            </div>
                        </div>
                        --}}
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="quiz_title">ชื่อแบบทดสอบ * </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="quiz_title" name="quiz_title" placeholder="ชื่อแบบทดสอบ" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="quiz_description">รายละเอียด * </label>
                            <div class="col-sm-8">
                                <textarea rows="7" class="form-control" id="quiz_description" name="quiz_description" placeholder="รายละเอียดแบบทดสอบ" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="quiz_timer">เวลาในการทำแบบทดสอบ * </label>
                            <div class="col-sm-8">
                                <input class="form-control" type="number" id="quiz_timer" name="quiz_timer" placeholder="ระยะเวลา (นาที)" value="0" required />
                                <small>หากไม่จำกัดเวลาในการทำข้อสอบให้ใส่ 0</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="quiz_score_to_pass">เกณฑ์การสอบผ่าน (%) * </label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" id="quiz_score_to_pass" name="quiz_score_to_pass" placeholder="เกณฑ์การสอบผ่าน(%)" value="0" required>
                                <small>หากไม่มีเกณฑ์ผ่านให้ใส่ 0</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="quiz_is_random_question">สอบไม่ผ่านให้ทำซ้ำ</label>
                            <div class="col-sm-8">
                                <label class="radio-inline"><input type="radio" name="require_pass" value="1">ใช่</label>
                                <label class="radio-inline"><input type="radio" name="require_pass" value="0" checked>ไม่ใช่</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="quiz_is_random_question">การสุ่มคำถาม</label>
                            <div class="col-sm-8">
                                <label class="radio-inline"><input type="radio" name="quiz_is_random_question" value="1">ใช่</label>
                                <label class="radio-inline"><input type="radio" name="quiz_is_random_question" value="0" checked>ไม่ใช่</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="quiz_is_random_answer">การสุ่มคำตอบ</label>
                            <div class="col-sm-8">
                                <label class="radio-inline"><input type="radio" name="quiz_is_random_answer" value="1" required>ใช่</label>
                                <label class="radio-inline"><input type="radio" name="quiz_is_random_answer" value="0" required checked>ไม่ใช่</label>
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-primary">บันทึก</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- END SBOX CONTENT -->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#course_id').jCombo("{!! url('select/course') !!}");

    $('#add-quiz-form').submit(function(e){

        e.preventDefault();

        $.ajax({
            type: 'POST',
            data: $(this).serialize(),
            cache: false,
            url: $(this).attr('action'),
            success: function (data) {
                console.log(JSON.stringify(data))

                var quiz_id = data.quiz_id;

                if(data.success){
                    openQuestionModal(quiz_id)
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'error',
                    text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 2500,
                    stack: 6
                });
            }
        });
    });

    function openQuestionModal(quiz_id){

        window.location = "{{url('e-admin/question/add?quiz_id=')}}"+quiz_id;
        {{--

        var header_area = $("#sximo-modal-header"),
            data_area = $("#sximo-modal-content"),
            url = "{{ url('e-admin/question/addTo') }}";

          header_area.html('เพิ่มแบบทดสอบสำหรับหลักสูตรนี้');
          data_area.html('Loading . . .');

          alert(quiz_id)

          $.ajax({
              type: 'GET',
              url: url,
              data: { 
                  quiz_id: quiz_id
              },
              success: function (data) {
                  data_area.html(data);
              },
              error: function (jqXHR, textStatus, errorThrown) {
                  data_area.html('Error Please try again');
              }
          });
          --}}
        }
</script>