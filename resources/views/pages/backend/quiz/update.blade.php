<div class="container">
    <h4 align="center">แก้ไขแบบทดสอบ</h4>
    <div class="padding"></div>

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#info">ข้อมูลทั่วไป</a></li>
        <li><a data-toggle="tab" href="#questions">รายการคำถาม</a></li>
    </ul>

    <div class="tab-content">

        <div class="padding"></div>
        <div id="info" class="tab-pane fade in active">

            <form class="form-horizontal" action="{{ url('e-admin/quiz/save') }}" method="post">
                <input type="hidden" name="edit" value="true">
                <input type="hidden" name="quiz_id" value="{{ $quiz->quiz_id }}">
                {{--
                <div class="form-group">
                    <label class="control-label col-sm-4" for="course_id">ครอส </label>
                    <div class="col-sm-8">
                        <select name="course_id" id="course_id" class="form-control" required></select>
                    </div>
                </div>
                --}}
                <div class="form-group">
                    <label class="control-label col-sm-2" for="quiz_title">ชื่อ</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="quiz_title" name="quiz_title" placeholder="ชื่อควิส"
                               value="{{ $quiz->quiz_title }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="quiz_description">รายละเอียด</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="quiz_description" name="quiz_description"
                               placeholder="รายละเอียดควิส" value="{{ $quiz->quiz_description }}" required>
                    </div>
                </div>
                <div class=" form-group">
                    <label class="control-label col-sm-2" for="quiz_timer">เวลา</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="number" id="quiz_timer" name="quiz_timer" value="{{$quiz->quiz_timer}}" placeholder="ระยะเวลา (นาที)" required />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="quiz_score_to_pass">คะแนนผ่าน</label>
                    <div class="col-sm-8">
                        <input type="number" class="form-control" id="quiz_score_to_pass" name="quiz_score_to_pass"
                               placeholder="คะแนนผ่าน" value="{{ $quiz->quiz_score_to_pass }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="quiz_is_random_question">สุ่มคำถาม</label>
                    <div class="col-sm-8">
                        <label class="radio-inline">
                            <input type="radio" name="quiz_is_random_question"
                                   {{ ($quiz->quiz_is_random_question == 1) ? "checked":"" }} value="1">ใช่</label>
                        <label class="radio-inline">
                            <input type="radio" name="quiz_is_random_question"
                                   {{ ($quiz->quiz_is_random_question == 0) ? "checked":"" }} value="0">ไม่ใช่</label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="quiz_is_random_answer">สุ่มคำตอบ</label>
                    <div class="col-sm-8">
                        <label class="radio-inline">
                            <input type="radio" name="quiz_is_random_answer"
                                   {{ ($quiz->quiz_is_random_answer == 1) ? "checked":"" }} value="1" required>ใช่</label>
                        <label class="radio-inline">
                            <input type="radio" name="quiz_is_random_answer"
                                   {{ ($quiz->quiz_is_random_answer == 0) ? "checked":"" }} value="0" required>ไม่ใช่</label>
                    </div>
                </div>
                {{--
                <div class="form-group">
                    <label class="control-label col-sm-2" for="quiz_is_random_answer">รายการคำถาม</label>
                    <div class="col-sm-8">
                        <button onclick="open" class="btn btn-sm btn-warning">ดูรายการคำถาม</button>
                    </div>
                </div>
                --}}
                <hr />
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-8">
                        <button type="submit" class="btn btn-primary">
                            แก้ไข
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div id="questions" class="tab-pane fade in">
            @include('pages.backend.quizquestion.list')
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#course_id').jCombo("{!! url('select/course') !!}",
                                   {selected_value: '{{ $quiz->course_id }}'});
        });
    </script>