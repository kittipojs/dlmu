@section('content')
<div class="container">
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            <div class="sbox large-box">
                <div class="sbox-title">
                    <h4 align="center">{{ $pageTitle }}</h4>
                </div>
                <!-- START SBOX CONTENT -->
                <div class="sbox-content">
                    <table class="table table-hover" id="tb_quiz" width="100%">
                        <thead>
                            <tr>
                                <th>quiz_id</th>
                                {{-- <th>course_title</th> --}}
                                <th>quiz_title</th>
                                <th>quiz_description</th>
                                <th>quiz_timer</th>
                                <th>quiz_score_to_pass</th>
                                <th>quiz_is_random_question</th>
                                <th>quiz_is_random_answer</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($quizs as $quiz)
                            <tr>
                                <td>{{ $quiz->quiz_id }}</td>
                                {{-- <td>{{ $quiz->course_title }}</td> --}}
                                <td>{{ $quiz->quiz_title }}</td>
                                <td>{{ $quiz->quiz_description }}</td>
                                <td>{{ $quiz->quiz_timer }}</td>
                                <td>{{ $quiz->quiz_score_to_pass }}</td>
                                <td>{{ $quiz->quiz_is_random_question == "1" ? "ใช่" : "ไม่ใช่" }}</td>
                                <td>{{ $quiz->quiz_is_random_answer == "1" ? "ใช่" : "ไม่ใช่" }}</td>
                                <td data-values="action" data-key="{{ $quiz->quiz_id }}">
                                    <div class=" action ">
                                        <div class="dropdown">
                                            <button type="button" data-target="#nia-modal" data-toggle="modal"
                                                    class="btn btn-primary edit" data-id="{{ $quiz->quiz_id }}">
                                                <i class="fa fa-edit"></i> Edit
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @include('modal.modal')

                </div>
            </div>
            <!-- END SBOX CONTENT -->
        </div>
    </div>
</div>
<script>
    
    $(document).ready(function () {
        drawTable()
        Edit()
    });

    function drawTable() {
        var table = $('#tb_quiz').dataTable({
            dom: 'Bfrtip',
            "searching": true,
            "scrollX": true,
            "responsive": true,
            "aaSorting": [],
            columnDefs: [
                { orderable: false, targets: -1 }
            ],
            buttons: [{
                extend: 'excel',
                title: 'ข้อมูลคำถาม',
                filename: 'ข้อมูลคำถาม',
                exportOptions: {
                    columns: ':visible',
                    format: {
                        body: function (data, row, column, node) {
                            return column >= 3 ? data.split("  ")[0] : data;
                        }
                    }
                }
            }],

        });

        $("#tb_quiz_filter").on("keyup", 'input', function() {
            setPaginageAction()
        });
    }

    function Edit() {
        $('.edit').click(function () {
            $('#modal-header').text('อัพเดทข้อมูล');
            var id = $(this).data('id');
            var data_area = $("#modal-info"),
                url;
            url = "{{ url('e-admin/quiz/update') }}" + "/" + id;
            data_area.html('Loading . . .');
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });
        })

    }
</script>
@endsection