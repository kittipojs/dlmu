<style type="text/css">
    .vcenter {
        display: inline-block;
        vertical-align: middle;
        float: none;
    }
</style>
<h3>
    ทีม {{ $team->team_name }}
</h3>
<div class="padding"></div>
<ul class="nav nav-tabs">
    <li class="active"><a href="#user_info" data-toggle="tab">ข้อมูลทั่วไป</a></li>
    <li><a href="#pitch_deck" data-toggle="tab"><i class="fas fa-upload"></i> อัพโหลด Proposal</a></li>
    <li><a href="#members" data-toggle="tab">สมาชิก</a></li>
    <li><a href="#pitching" data-toggle="tab">สนามแข่งขัน</a></li>
    {{-- <li><a href="#approve" data-toggle="tab">สถานะทีม</a></li> --}}
    @if($show_delete)
    <li><a href="#delete" data-toggle="tab"><i class="far fa-trash-alt"></i> ลบทีม</a></li>
    @endif
</ul>
<div class="tab-content">

    <div class="tab-pane active m-t" id="user_info">
        <form action="{{ url('e-admin/team/edit') }}" class="form-horizontal" id="edit-team-info" method="post"
              role="form" enctype="multipart/form-data">
            <input type="hidden" name="team_id" id="team_id" value="{{ $team->team_id }}">
            <div class="form-group">
                <label for="team_name" class="control-label col-sm-3"> ชื่อทีม </label>
                <div class="col-sm-8">
                    <input name="team_name" type="text" id="team_name" class="form-control input-sm"
                           value="{{ $team->team_name }}" placeholder="ชื่อทีม"/>
                </div>
            </div>

            <div class="form-group">
                <label for="team_info" class="col-sm-3 control-label">ข้อมูลเกี่ยวกับโครงการ *</label>
                <div class="col-sm-8">
                    <textarea type="text" class="form-control" name="team_info" id="team_info"
                              placeholder="อธิบายข้อมูลโครงการเริ่มต้น และจุดประสงค์ของโครงการ"
                              rows="6">{{ $team->team_info }}</textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="university" class="col-sm-3 control-label">ส่งในนามมหาวิทยาลัย</label>
                <div class="col-sm-8">
                    <select class="form-control" id="university" name="university"></select>
                </div>
            </div>

            <div class="form-group">
                <label for="sector" class="col-sm-3 control-label">Startup Sector</label>
                <div class="col-sm-8">
                    <select name="startup_sector" id="startup_sector" class="form-control">
                        <option value="1"> การแพทย์และสาธารณสุข (MedTech/ Health Tech)</option>
                        <option value="2"> เกษตรและอาหาร (AgriTech/ Food Startup)</option>
                        <option value="3"> อสังหาริมทรัพย์ (Property Tech)</option>
                        <option value="4"> การเงินและการธนาคาร (FinTech)</option>
                        <option value="5"> การศึกษา (EdTech)</option>
                        <option value="6"> การท่องเที่ยว (TravelTech)</option>
                        <option value="7"> ไลฟ์สไตล์ (LifeStyle)</option>
                        <option value="8"> พาณิชยกรรมอิเล็กทรอนิกส์ (E-Commerce)</option>
                        <option value="9"> ภาครัฐ (GovTech)</option>
                        <option value="14"> อุตสาหกรรม (Industrial Tech)</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="sector" class="col-sm-3 control-label">Proposal</label>
                <div class="col-sm-9">
                    <a href="{{ asset($team->file_url) }}?cache={{date('Y-m-d H:i:s')}}" class="btn-link" target="_blank">
                        <i class="fa fa-download"></i> ดาวน์โหลด
                    </a>
                </div>
            </div>

            <hr class="divider">

            <div class="form-group">
                <div class="form-group">
                    <label class=" control-label col-sm-3">&nbsp;</label>
                    <div class="col-sm-8">
                        <button class="btn btn-success" type="submit">
                            บันทึก
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="tab-pane m-t" id="pitch_deck">
        <div id="file-preview" class="dropzone"></div>
    </div>
    <div class="tab-pane m-t" id="members">

        @foreach($members as $item)
        <div id="member_{{$item->user_id}}" class="row padding">
            <div class="col col-xs-3 vcenter">
                <a class="btn-link" href="{{ asset($item->photo_src) }}?cache={{date('Y-m-d H:i:s')}}" data-lightbox="image-1"
                   data-title="{{ $item->first_name }} {{ $item->last_name }}">
                    {{ $item->first_name }} {{ $item->last_name }}
                </a>
            </div>
            <div class="col col-xs-4 vcenter">
                @if($item->pass_camp)
                (ผ่าน camp แล้ว)
                @else
                (ยังไม่ผ่าน camp)
                @endif

                @if($team->user_id != $item->user_id)
                <button data-user_id="{{$item->user_id}}" type="button" class="btn btn-sm remove-member-btn">
                    <i class="far fa-trash-alt"></i> ลบออกจากทีม
                </button>
                @else
                <button type="button" class="btn btn-sm" disabled>หัวหน้าทีม</button>
                @endif
            </div>
        </div>
        @endforeach
    </div>
    <div class="tab-pane m-t" id="pitching">
        @if($pitching_event!=null)

        @foreach($pitching_event as $item)
        <div class="row" style="border-bottom:1px solid #eee; padding-bottom:5px;">
            <div class="col-sm-4">
                <div class="event-item">
                    <div class="text-center">
                        <img src="{{ $item['event']->cover }}" class="img-responsive" style="max-height:100px;"/>
                    </div>
                    <div class="event-content" style="height:50px; font-weight:bold;">
                        <a href="{{ $item['event']->url }}" target="_blank">{{ $item['event']->name }}</a>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <small>เปลี่ยนสถานะ</small>
                            <select class="custom-select form-control team_status" name="team_status"  data-id="{{ $item['apply']->id }}">
                                <option value="0" @if($item['apply']->status==0) selected @endif>ยังไม่สมบูรณ์</option>
                                <option value="1" @if($item['apply']->status==1) selected @endif>รออนุมัติ</option>
                                <option value="2" @if($item['apply']->status==2) selected @endif>อนุมัติแล้ว</option>
                                <option value="-99" @if($item['apply']->status==-99) selected @endif>เอกสารไม่ถูกต้อง/ครบถ้วน</option>
                                <option value="-98" @if($item['apply']->status==-98) selected @endif>สมาชิกภายในทีมขาดคุณสมบัติ</option>
                            </select>
                        </div>
                    </div>
                    <div class="padding"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <small>ผลการแข่งขัน</small>
                            <select class="custom-select form-control team_result" name="team_result"  data-id="{{ $item['apply']->id }}">
                                <option value="0" @if($item['apply']->result==0) selected @endif>รอ</option>
                                <option value="0" @if($item['apply']->result==-1) selected @endif>ไม่ผ่าน</option>
                                <option value="1" @if($item['apply']->result==1) selected @endif>ผ่าน</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 text-center">
                <strong>สถานะการแข่งขัน</strong>
                <p>
                    @if($item['apply']->result==0)
                    รอ
                    @elseif($item['apply']->result==-1)
                    ไม่ผ่าน
                    @elseif($item['apply']->result==1)
                    ผ่าน
                    @endif
                </p>
            </div>
            <div class="col-sm-4 text-center">
                @if($item['event']->end_date > $now)
                <strong>กิจกรรมนี้ยังไม่เริ่ม </strong>
                @else
                <strong>กิจกรรมนี้ผ่านมาแล้ว</strong>
                @endif
                <p>
                    {{date('d-m-Y', $item['event']->start_date)}} ถึง {{date('d-m-Y', $item['event']->end_date)}}
                </p>
            </div>
        </div>
        @endforeach
        @else
        <p class="text-center">ยังไม่ได้เลือกรอบ pitching</p>
        @endif
    </div>
    {{--
    <div class="tab-pane m-t" id="approve">
        <div class="padding"></div>
        <form action="{{ url('e-admin/team/edit-status') }}" class="form-horizontal" id="edit-team-status" method="post"
              role="form" enctype="multipart/form-data">
            <input type="hidden" name="team_id" id="team_id" value="{{ $team->team_id }}">
            <div class="padding form-group row">
                <label for="colFormLabelSm"
                       class="col-sm-2 offset-sm-1 col-form-label col-form-label-sm">สถานะทีม</label>
                <div class="col-sm-9">
                    <select class="custom-select form-control" id="team_status" name="team_status">
                        <option value="0">ยังไม่สมบูรณ์</option>
                        <option value="1">รออนุมัติ</option>
                        <option value="2">อนุมัติ</option>
                        <option value="-99">เอกสารไม่ถูกต้อง/ครบถ้วน</option>
                        <option value="-98">สมาชิกภายในทีมขาดคุณสมบัติ</option>
                    </select>
                </div>
            </div>
            <div class="padding form-group row">
                <div class="col-sm-2"></div>
                <div class="col-sm-9">
                    <button class="btn btn-success" type="submit">
                        บันทึก
                    </button>
                </div>
            </div>
        </form>
    </div>
    --}}
    @if($show_delete)
    <div class="tab-pane m-t" id="delete">
        <form action="{{ url('e-admin/team/delete') }}" method="post" class="form-horizontal" id="delete-team">
            <input type="hidden" name="team_id" id="team_id" value="{{ $team->team_id }}">
            <input type="hidden" name="user_id" id="user_id" value="{{ $team->user_id }}">
            <div class="col-sm-12">
                <p>คุณต้องการลบทีม {{ $team->team_name }} ?</p>

                <small>นักศึกษาในทีม {{ $team->team_name }} จะต้องเริ่มสร้างทีมใหม่</small>
            </div>
            <div class="col-sm-12">
                <div class="padding"></div>
                <div class="btn-group">
                    <button class="btn btn-danger" type="button" data-toggle="dropdown">
                        <i class="far fa-trash-alt"></i> ลบ
                    </button>
                    <ul class="dropdown-menu">
                        <li>
                            <button type="submit" class="btn btn-xs btn-block btn-danger">
                                ยืนยัน
                            </button>
                        </li>
                    </ul>
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">
                        ยกเลิก
                    </button>
                </div>
            </div>
        </form>
    </div>
    @endif
</div>

<script type="text/javascript">
    $(document).ready(function () {


        //$('#team_status').val({{ $team->status }})

        $('select.team_status').on('change', function() {

            var id = $(this).data('id');

            $('body').loadingModal({text: 'กำลังเปลี่ยนสถานะทีม...'});
            allowSubmit = false;

            var params = { id:id, status:this.value }
            $.ajax({
                type: 'POST',
                data: params,
                cache: false,
                url: '{{ url('e-admin/team/edit-apply-status') }}',
                success: function(data) {

                $('#side_modal').modal('hide');

                $.toast({
                heading: 'success',
                text: 'บันทึกสำเร็จ',
                position: 'bottom-right',
                icon: 'success',
                hideAfter: 2500,
                stack: 6
            });

            $('body').loadingModal('destroy');
            allowSubmit = true;

            $('#search').click();
        },error: function (jqXHR, textStatus, errorThrown) {

            $('body').loadingModal('destroy');
            allowSubmit = true;

            $.toast({
                heading: 'error',
                text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                position: 'bottom-right',
                icon: 'error',
                hideAfter: 2500,
                stack: 6
            });
        }
    });
    });

    $('select.team_result').on('change', function() {
        var id = $(this).data('id');

        var params = { id:id, result:this.value }
        $.ajax({
            type: 'POST',
            data: params,
            cache: false,
            url: '{{ url('e-admin/team/edit-apply-result') }}',
            success: function(data) {

            $('#side_modal').modal('hide');

            $.toast({
            heading: 'success',
            text: 'บันทึกสำเร็จ',
            position: 'bottom-right',
            icon: 'success',
            hideAfter: 2500,
            stack: 6
        });

        $('body').loadingModal('destroy');
        allowSubmit = true;

        $('#search').click();
    },error: function (jqXHR, textStatus, errorThrown) {

        $('body').loadingModal('destroy');
        allowSubmit = true;

        $.toast({
            heading: 'error',
            text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
            position: 'bottom-right',
            icon: 'error',
            hideAfter: 2500,
            stack: 6
        });
    }
    });
    });

    $("div#file-preview").dropzone({
        maxFiles: 100000,
        url: "{{url('e-admin/team/upload?team_id='.$team->team_id)}}",
        acceptedFiles: "image/*,application/pdf,.pdf",
        dictDefaultMessage: '<i class="fas fa-upload"></i> เลือกไฟล์ .pdf จากเครื่องคอมพิวเตอร์ของคุณ',
        success: function (file, response) {
            if (response.success) {
                $('#side_modal').modal('hide');
                $.toast({
                    heading: 'success',
                    text: 'บันทึกสำเร็จ',
                    position: 'bottom-right',
                    icon: 'success',
                    hideAfter: 2500,
                    stack: 6
                });
                $('#search').click();
            }
        }
    });

    $("#university").jCombo("{!! url('select/university') !!}", {selected_value: '{{ $team->university_id }}'});
    $('#startup_sector').val('{{ $team->startup_sector }}');

    submitForm();
    deleteTeam();


    $('.remove-member-btn').click(function(){

        var user_id = $(this).data('user_id');

        var txt;
        var r = confirm("ยืนยันการลบสมาชิกออกจากทีม?");
        if (r == true) {

            $.ajax({
                type: 'POST',
                data: {team_id:'{{$team->team_id}}', user_id:user_id},
                url: '{{url('/e-admin/team/remove-member')}}',
                success: function (data) {
                $('#member_'+user_id).css({ 'display': "none" })
        },
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'error',
                    text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 2500,
                    stack: 6
                });
            }
    });
    }
    })
    });

    function submitForm() {

        var allowSubmit = true;

        $('#edit-team-info').submit(function (event) {

            $.ajax({
                type: 'POST',
                data: $(this).serialize(),
                cache: false,
                url: $(this).attr('action'),
                success: function (data) {
                    $('#side_modal').modal('hide');
                    $.toast({
                        heading: 'success',
                        text: 'บันทึกสำเร็จ',
                        position: 'bottom-right',
                        icon: 'success',
                        hideAfter: 2500,
                        stack: 6
                    });
                    $('#search').click();

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $.toast({
                        heading: 'error',
                        text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                        position: 'bottom-right',
                        icon: 'error',
                        hideAfter: 2500,
                        stack: 6
                    });
                }
            });
            event.preventDefault();
        });

    }

    function deleteTeam() {
        $('#delete-team').submit(function (event) {

            $.ajax({
                type: 'POST',
                data: $(this).serialize(),
                url: $(this).attr('action'),
                success: function (data) {
                    $('#side_modal').modal('hide');
                    $.toast({
                        heading: 'success',
                        text: 'ลบทีมสำเร็จ',
                        position: 'bottom-right',
                        icon: 'success',
                        hideAfter: 2500,
                        stack: 6
                    });
                    $('#search').click();

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $.toast({
                        heading: 'error',
                        text: 'เกิดข้อผิดพลาด โปรดลองใหม่',
                        position: 'bottom-right',
                        icon: 'error',
                        hideAfter: 2500,
                        stack: 6
                    });
                }
            });

            event.preventDefault();
        });
    }
</script>