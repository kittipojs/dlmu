<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading" align="center">ภาพรวมของทีม</div>
                <div class="panel-body">
                    <div class="row text-center">
                        <div class="col-sm-3">
                            <h3> {{ number_format($all_team) }} </h3>
                            <small>
                                ทีมในระบบทั้งหมด
                            </small>
                        </div>
                        {{--
                        <div class="col-sm-3">
                            <h3> {{ number_format($all_apply_team) }} </h3>
                            <small>
                                ส่งเข้าแข่งขันแล้ว
                            </small>
                        </div>
                        <div class="col-sm-3">
                            <h3> {{ number_format($all_waiting_team) }} </h3>
                            <small>
                                รออนุมัติ
                            </small>
                        </div>
                        <div class="col-sm-3">
                            <h3> {{ number_format($all_verify_team)}} </h3>
                            <small>
                                ผ่านการอนุมัติ
                            </small>
                        </div>
                        --}}
                    </div> 
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-warning">
                <div class="panel-heading" align="center">ค้นหาข้อมูล</div>
                <div class="panel-body">
                    <p>ค้นหาทีมด้วยข้อมูลดังต่อไปนี้</p>
                    <div class="form-inline">
                        <select id="key" name="key" class="custom-select form-control">
                            <option value="team_name">ชื่อทีม</option>
                            <option value="email">อีเมลหัวหน้าทีม</option>
                            {{-- <option value="pitching_events">สนามแข่งขัน</option> --}}
                        </select>

                        <input type="text" name="value" id="value" class="form-control" placeholder="ค่าที่ต้องการค้นหา" />
                        {{-- 
                        <select class="form-control" name="event_id" id="event_id" style="display:none; width:320px;">
                            <option>--- เลือกกิจกรรม Pitching ---</option>
                            @foreach($pitching_events as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        --}}

                        <button id="search" type="submit" class="btn btn-primary mb-2">ค้นหา</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <!-- Data Area -->
    <div id="details"></div>
    <!--/Data Area -->
</div>
<script type="text/javascript">

    $(document).ready(function () {
        getLastestSubmit();
    });

    function getLastestSubmit(){

        var data_area = $("#details"),
            url = "{{url('e-admin/team/results')}}?key=team_name&value=";

        data_area.html('กำลังโหลดข้อมูล . . .');

        $.ajax({
            type: 'GET',
            url: url,
            success: function (data){
                data_area.html(data);
            },
            error: function (jqXHR, textStatus, errorThrown){
                data_area.html('เกิดข้อผิดพลาด');
            }
        });
    }

    function param() {

        var email       = $('#email').val();
        var team_name   = $('#team_name').val();

        var params = {
            key: $('#key').val(),
            value: $('#value').val()
        };
        return params;
    }

    $('select#key').on('change', function() {
        if(this.value=='pitching_events'){
            $('#value').hide();
            $('#event_id').show();
        }
        else{
            $('#value').show();
            $('#event_id').hide();
        }
    })

    $('#search').click(function () {

        var data_area = $("#details"),
            params = param(),
            url ='';

        if($('#key').val()=='pitching_events'){

            params = { event_id: $('#event_id').val() };
            url = "{{url('e-admin/report/uleague/pitching')}}" + "?" + $.param(params);

            data_area.html('กำลังโหลดข้อมูล . . .');

            $.ajax({
                type: 'GET',
                url: url,
                success: function (data){
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    data_area.html('เกิดข้อผิดพลาด');
                }
            });
        }
        else{
            if(params.value==''){
                getLastestSubmit();
            }
            else{
                url = "{{url('e-admin/team/results')}}" + "?" + $.param(params);

                data_area.html('กำลังโหลดข้อมูล . . .');

                $.ajax({
                    type: 'GET',
                    url: url,
                    success: function (data) {
                        data_area.html(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //alert(JSON.stringify(jqXHR))
                        data_area.html('เกิดข้อผิดพลาด');
                    }
                });
            }
        }

    });
</script>