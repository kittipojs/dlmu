<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading" align="center">ภาพรวมของการสมัคร Pitching</div>
                <div class="panel-body">
                    <div class="row text-center">
                        <div class="col-sm-6">
                            <h3> {{ number_format(count($pitching_events)) }} </h3>
                            <small>
                                สนามแข่งขันทั้งหมด
                            </small>
                        </div>
                        <div class="col-sm-6">
                            <h3> {{ number_format($all_apply_team) }} </h3>
                            <small>
                                ทีมทั้งหมด
                            </small>
                        </div>
                    </div>
                    {{--
                    <hr>
                    <div class="row text-center">
                        <div class="col-sm-3">
                            <h3> {{ number_format($all_waiting_team) }} </h3>
                            <small>
                                รออนุมัติ
                            </small>
                        </div>
                        <div class="col-sm-3">
                            <h3> {{ number_format($all_approve_team)}} </h3>
                            <small>
                                อนุมัติแล้ว
                            </small>
                        </div>
                        <div class="col-sm-3">
                            <h3> {{ number_format($all_fail_team)}} </h3>
                            <small>
                                สมาชิกภายในทีมขาดคุณสมบัติ
                            </small>
                        </div>
                        <div class="col-sm-3">
                            <h3> {{ number_format($fails)}} </h3>
                            <small>
                                ไม่ผ่านการอนุมัติ
                            </small>
                        </div>
                    </div>
                    --}}
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-warning">
                <div class="panel-heading" align="center">ค้นหาข้อมูล</div>
                <div class="panel-body text-center">
                    <p >ค้นหาทีมด้วยข้อมูลดังต่อไปนี้</p>
                    <div class="form-inline text-center">
                        <select id="key" name="key" class="custom-select form-control">
                            <option value="pitching_events">สนามแข่งขัน</option>
                        </select>
                        <select class="form-control" name="event_id" id="event_id" style="width:320px;">
                            <option value="-1">--- เลือกกิจกรรม Pitching ---</option>
                            <option value="-1">ทั้งหมด</option>
                            @foreach($pitching_events as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        <button id="search" type="submit" class="btn btn-primary mb-2">ค้นหา</button>
                    </div>
                    <p id="remark-text" class="text-center">
                        <small>หมายเหตุ : การเลือกดูทีมจากสนามแข่งขันทั้งหมดจะไม่สามารถดาวน์โหลด Proposal ได้</small>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <!-- Data Area -->
    <div id="details"></div>
    <!--/Data Area -->
</div>
<script type="text/javascript">

    $(document).ready(function () {
        getLastestApply();
        
        $('#event_id').val(-1)
    });

    function getLastestApply(){

        var data_area = $("#details"),
            url = "{{url('e-admin/team/apply-results')}}?key=team_name&value=";

        data_area.html('กำลังโหลดข้อมูล . . .');

        $.ajax({
            type: 'GET',
            url: url,
            success: function (data){
                data_area.html(data);
            },
            error: function (jqXHR, textStatus, errorThrown){
                data_area.html('เกิดข้อผิดพลาด');
            }
        });
    }

    function param() {

        var email       = $('#email').val();
        var team_name   = $('#team_name').val();

        var params = {
            key: $('#key').val(),
            value: $('#value').val()
        };
        return params;
    }

    $('select#key').on('change', function() {
        if(this.value=='pitching_events'){
            $('#value').hide();
            $('#event_id').show();
        }
        else{
            $('#value').show();
            $('#event_id').hide();
        }
    })
    
    $('select#event_id').on('change', function() {
        if(this.value==-1){
            $('#remark-text').show();
        }
        else{
            $('#remark-text').hide();
        }
    })


    $('#search').click(function () {

        var data_area = $("#details"),
            params = param(),
            url ='';

        if($('#key').val()=='pitching_events'){
            
            params = { event_id: $('#event_id').val() };
            url = "{{url('e-admin/team/apply-results')}}" + "?" + $.param(params);

            data_area.html('กำลังโหลดข้อมูล . . .');

            $.ajax({
                type: 'GET',
                url: url,
                success: function (data){
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    data_area.html('เกิดข้อผิดพลาด');
                }
            });
            
        }
        else{
            if(params.value==''){
                getLastestSubmit();
            }
            else{
                url = "{{url('e-admin/team/results')}}" + "?" + $.param(params);

                data_area.html('กำลังโหลดข้อมูล . . .');

                $.ajax({
                    type: 'GET',
                    url: url,
                    success: function (data) {
                        data_area.html(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        data_area.html('เกิดข้อผิดพลาด');
                    }
                });
            }
        }

    });
</script>