<div class="row">
    <h3><i class="fa fa-search"></i> ผลการค้นหา</h3>
    <table id="tb_user" class="table table-striped table-hover nowrap" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>#</th>
                <th>ชื่อทีม</th>
                <th>หัวหน้าทีม</th>
                <th>มหาวิทยาลัย</th>
                <th>Startup Sector</th>
                <th>สร้างทีมเมื่อ</th>
                {{-- <th>สถานะ</th> --}}
            </tr>
        </thead>
        <tobody>
            @foreach( $teams as $item )
            <tr>
                <td>
                    <button type="button" class="btn btn-sm btn-primary view-info-btn" data-target="#side_modal" data-toggle="modal" data-id="{{ $item->user_id }}">
                        <i class="fa fa-eye"></i>
                    </button>
                </td>
                <td>{{ $item->team_id }}</td>
                <td>{{ $item->team_name }}</td>
                <td>{{ $item->first_name }} {{ $item->last_name }}</td>
                <td>{{ $item->name }}</td>
                <td>{!! DataHelpers::startupLookup($item->startup_sector) !!}</td>
                <td>{{ $item->created_at }}</td>
                {{--
                <td data-order="{{ $item->data_status }}">
                    @if($item->status===0)
                    <i class="fas fa-exclamation"></i> ยังไม่สมบูรณ์
                    @elseif($item->status===1)
                    <i class="far fa-clock"></i> รออนุมัติ 
                    @elseif($item->status===2)
                    <i class="far fa-smile"></i> อนุมัติแล้ว 
                    @elseif($item->status===-99)
                    <i class="far fa-frown"></i>  เอกสารไม่ถูกต้อง/ครบถ้วน 
                    @elseif($item->status===-98)
                    <i class="far fa-frown"></i> สมาชิกภายในทีมขาดคุณสมบัติ 
                    @endif
                </td>
                --}}
            </tr>
            @endforeach
        </tobody>
    </table>
</div>

<script>
    $(document).ready(function () {
        
        drawTable();
        viewInfo();

        setPaginageAction();
        $(".sorting_desc").click(setPaginageAction);
        $(".sorting_asc").click(setPaginageAction);
        $(".sorting").click(setPaginageAction);
        $('.view-info-btn').click(setPaginageAction);

    });

    function drawTable() {
        var table = $('#tb_user').dataTable({
            dom: 'Bfrtip',
            searching: true,
            scrollX: true,
            order: [[ 6, "desc" ]],
            pageLength: 50,
            buttons: [{
                extend: 'excel',
                text: 'Export รายงาน',
                title: 'รายชื่อทีม',
                filename: 'รายชื่อทีม',
                exportOptions: {
                    columns: [1,2,3,4,5,6],
                    format: {
                        body: function (data, row, column, node) {
                            return column >= 3 ? data.split("  ")[0] : data;
                        }
                    }
                }
            }],
            "oLanguage": {
                "sSearch": "ค้นหา :"
            }
        });
        $("#tb_user_filter").on("keyup", 'input', function() {
            setPaginageAction()
        });
    }

    function viewInfo(){
        $('.view-info-btn').click(function () {
            var id = $(this).data('id');

            var header_area = $("#modal-header"),
                data_area = $("#modal-info"),
                url;
            url = "{{ url('e-admin/team/results/info') }}" + "/" + id;
            header_area.html('ข้อมูลทีม');
            data_area.html('Loading . . .');
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    data_area.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    data_area.html('Error Please try again');
                }
            });
        });
    }

    function setPaginageAction() {
        $(".paginate_button").click(setPaginageAction);

        $('.view-info-btn').unbind("click");

        viewInfo();
    }
</script>