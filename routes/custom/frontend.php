<?php

Route::get('login', 'CustomLoginController@getLogin');
Route::get('register', 'CustomLoginController@getRegister');
Route::get('events', 'frontend\EventController@index');
Route::get('events/all', 'frontend\EventController@allEvents');
Route::get('course-group/{tag_name}', 'HomeController@getAllCourse');

Route::get('download', 'frontend\AboutController@getDownloadPage');

Route::group(['prefix' => 'user'], function () {

    Route::get('profile','frontend\ProfileController@getProfile');
    Route::get('certificates','frontend\ProfileController@getCertificates');
    Route::post('certificates/preview','frontend\ProfileController@getCertificatesPreview');
    Route::post('certificates/preview-all','frontend\ProfileController@getCertificatesPreviewAll');
    Route::post('event-certificates/preview','frontend\ProfileController@getEventCertificatesPreview');

    Route::get('uleague','frontend\ProfileController@getULeague');
    Route::get('mycourses','frontend\ProfileController@getMyCourses');
    //Route::get('myevents','frontend\ProfileController@getMyEvents');
    Route::get('myresume','PdfController@usage');
    Route::get('uleague/join', 'frontend\ProfileController@ULeagueJoinPage');
    Route::post('uleague/join', 'frontend\UleagueController@joinULeague');
    Route::post('uleague/update', 'frontend\UleagueController@updateInfo');
    Route::post('uleague/upload', 'frontend\UleagueController@uploadStudentCard');

    Route::get('uleague/camp', 'frontend\ProfileController@getApplyCamp');
    Route::get('uleague/pitching', 'frontend\ProfileController@getApplyPitching');
    Route::get('uleague/team', 'frontend\ProfileController@getTeam');
    Route::get('uleague/team/accept/{invite_code}', 'frontend\ProfileController@acceptInvite');

    Route::post('uleague/camp/apply', 'frontend\UleagueController@applyCamp');
    Route::get('uleague/pitching/apply', 'frontend\UleagueController@applyPitching');
    Route::get('uleague/pitching/reapply', 'frontend\UleagueController@reApplyPitching');

    //camping
    Route::post('camp/register', 'frontend\CampingController@getRegister');
    Route::get('camp/details', 'frontend\CampingController@getCampDetails');

    //pitching
    Route::get('pitch/details', 'frontend\PitchingController@getPitchDetails');
    Route::post('pitch/register', 'frontend\PitchingController@getRegister');
    Route::post('pitch/update/team', 'frontend\PitchingController@updateTeam');
    Route::get('pitch/invite-friends', 'frontend\PitchingController@getInviteFriends');
    Route::post('pitch/invite-friends', 'frontend\PitchingController@inviteFriends');
    Route::post('pitch/apply', 'frontend\PitchingController@applyRound');

    //pdf
    Route::get('myresume','PdfController@usage');

    //team
    Route::post('team/create', 'frontend\TeamController@createTeam');
    Route::post('team/update', 'frontend\TeamController@updateTeam');
    Route::post('team/member/invite', 'frontend\TeamController@inviteFriends');
    Route::post('team/member/rminvite', 'frontend\TeamController@removeInvite');
    Route::post('team/member/rpinvite', 'frontend\TeamController@replyInvite'); 

});

Route::group(['prefix' => 'uleague'], function () {
    Route::get('', 'frontend\UleagueController@WelcomePage');
    //camping
    Route::get('camping', 'frontend\CampingController@FormRegisterCamping');

    //pitching
    Route::get('pitching', 'frontend\PitchingController@FormRegisterPitching');
    //Route::get('team/invite/{invite_code}', 'frontend\TeamController@acceptInvite');

    //team
    Route::get('team/{team_id}', 'frontend\TeamController@TeamPage');

});

Route::get('ajax/pitch/member', 'frontend\PitchingController@checkMember');

//Route::get('course/{id}', 'frontend\CourseController@courseInfo');
Route::get('course/{id}/pre-test', 'frontend\CourseController@coursePretest');
Route::get('course/{id}/post-test', 'frontend\CourseController@coursePosttest');
Route::get('course/{id}/learn', 'frontend\CourseController@courseLearn');
Route::get('course/{id}/result', 'frontend\CourseController@courseResult');
Route::get('course/{id}/lecture', 'frontend\CourseController@courseLecture');

//Route::get('course/{course_id}/quiz', 'frontend\CourseController@quizInstruction');
//Route::get('quiz/view/{id}', 'frontend\CourseController@quizInfo');

//mock up
Route::get('c/{id}', 'frontend\CourseController@courseInfo');
Route::get('c/{id}/learn', 'frontend\CourseController@loadCourseLearn');
Route::get('c/{id}/result', 'frontend\CourseController@loadCourseResult');
Route::get('c/quiz/{id}/question', 'frontend\QuizController@mockupQuiz');
Route::get('c/quiz/{id}/do', 'frontend\QuizController@question');
Route::post('c/quiz/save', 'frontend\QuizController@save');
Route::post('c/pretest/save', 'frontend\QuizController@savePretest');


Route::get('csession/check/{course_id}', 'frontend\CourseSessionController@checkSession');
Route::post('csession/create', 'frontend\CourseSessionController@create');
Route::post('api/csession/update-progress', 'api\CourseSessionController@updateProgress');

Route::get('curriculum', 'frontend\CurriculumController@getContent');

Route::get('question-and-answer', 'frontend\QaController@index');
Route::get('about', 'frontend\AboutController@index');