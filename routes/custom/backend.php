<?php


Route::group(['prefix' => 'api'], function () {
    Route::get('zipevent/search', 'api\ZipEventController@search');
    Route::get('zipevent/highlight', 'api\ZipEventController@highlight');
});

Route::group(['prefix' => 'e-admin'], function () {

    Route::get('', 'backend\AdminController@index');
    Route::get('login', 'UserController@getLogin');
    Route::get('profile', 'UserController@getProfile');

    //Admin
    Route::get('admin', 'backend\AdminController@userList');
    Route::post('admin/add', 'backend\AdminController@addAdmin');
    Route::post('admin/delete', 'backend\AdminController@deleteAdmin');

    //Info
    Route::get('info/view', 'backend\InfoController@index');
    Route::post('info/save', 'backend\InfoController@saveInfo');
    Route::post('info/upload', 'backend\InfoController@upload');

    //User
    Route::get('user/report', 'backend\UserController@index');
    Route::get('user/search', 'backend\UserController@search');
    Route::get('user/{user_id}', 'backend\UserController@getInfo');
    Route::post('user/{user_id}', 'backend\UserController@updateInfo');

    Route::get('report/user/result', 'backend\UserController@result');
    Route::get('report/user/today', 'backend\UserController@today');
    Route::get('report/user/detailsDelete', 'backend\UserController@userDetails');
    Route::get('report/user/delete/{id}', 'backend\UserController@delete');

    //Route::get('user/reportDelete', 'backend\UserController@deleteIndex');

    //Uleague
    Route::get('report/uleague/pitching', 'backend\UleagueController@pitchingDetail');
    Route::get('report/uleague/apply', 'backend\UleagueController@pitchingApplyDetail');
    Route::get('report/uleague/details/team/{user_id}', 'backend\UleagueEventController@teamInfo');
    Route::post('uleague/card/upload', 'backend\UleagueController@uleagueInfoUpload');
    Route::post('uleague/{user_id}', 'backend\UleagueController@updateInfo');

    //Event
    //Route::get('report/event', 'backend\UleagueEventController@index');
    //Route::get('report/uleague/pitching', 'backend\UleagueController@pitchingDetail');

    //Camping
    //Route::get('report/camping', 'backend\ReportCampingController@index');
    //Route::get('report/camping/details', 'backend\ReportCampingController@details');

    //Pitching
    Route::get('pitching/report', 'backend\ReportPitchingController@index');

    //Quiz
    Route::get('quiz', 'backend\QuizController@index');
    Route::get('quiz/list', 'backend\QuizController@getList');
    Route::get('quiz/add', 'backend\QuizController@addQuizPage');
    Route::get('quiz/update/{id}', 'backend\QuizController@updateQuiz');
    Route::post('quiz/save', 'backend\QuizController@saveQuiz');

    //Question
    Route::get('question/view', 'backend\QuizQuestionController@index');
    Route::get('question/view/{quiz_id}', 'backend\QuizQuestionController@getQuestionsPage');
    Route::get('question/add', 'backend\QuizQuestionController@addQuizQuestion');
    Route::get('question/add-to', 'backend\QuizQuestionController@addQuestionPage');
    Route::get('question/update/{id}', 'backend\QuizQuestionController@updateQuizQuestion');
    Route::post('question/save', 'backend\QuizQuestionController@saveQuizQuestion');

    //Course
    Route::get('courses', 'backend\CourseController@allCourse');
    //Route::get('course/view', 'backend\CourseController@index');
    Route::get('course/add', 'backend\CourseController@addCourse');
    Route::get('course/manage/{id}', 'backend\CourseController@manageCourse');
    Route::get('course/delete/{id}', 'backend\CourseController@deleteCourse');
    Route::post('course/delete/document', 'backend\CourseController@deleteCourseDocument');
    Route::post('course/reorder', 'backend\CourseController@reorder');
    Route::post('course/save', 'backend\CourseController@saveCourse');
    Route::get('course/status', 'backend\CourseController@updateStatus');
    Route::post('course/update-percentage', 'backend\CourseController@updatePercentage');
    Route::post('course/upload', 'backend\CourseController@uploadMedia');
    Route::get('course/tag/add', 'backend\CourseController@getAddTag');
    Route::post('course/tag/save', 'backend\CourseController@saveTag');
    Route::post('course/quiz/delete', 'backend\CourseController@deleteQuiz');


    Route::post('course-session/add', 'backend\CourseSessionController@addSession');
    Route::post('course-session/update', 'backend\CourseSessionController@updateSession');

    //Record
    Route::get('record/view', 'backend\RecordController@index');
    Route::get('record/calendar', 'backend\RecordController@calendar');

    //Lectures
    Route::get('lectures/add', 'backend\CourseLectureController@addLecturePage');
    Route::get('lectures/selectMedia', 'backend\CourseLectureController@selectMediaPage');
    Route::get('lectures/list', 'backend\CourseLectureController@getList');
    Route::post('lectures/save', 'backend\CourseLectureController@save');
    Route::get('lectures/update/{id}', 'backend\CourseLectureController@updateLecturePage');
    Route::post('lectures/update/save', 'backend\CourseLectureController@updateLectureSave');
    Route::post('lectures/upload', 'backend\CourseLectureController@upload');
    Route::get('lectures/syncslide', 'backend\CourseLectureController@syncSlidePage');
    Route::post('lectures/syncslide/save', 'backend\CourseLectureController@syncSlideSave');

    //Question & Answer
    Route::get('qa/view', 'backend\QaController@index');
    Route::get('qa/view/table', 'backend\QaController@viewTable');
    Route::get('qa/add', 'backend\QaController@addQa');
    Route::get('qa/update/{id}', 'backend\QaController@updateQa');
    Route::post('qa/save', 'backend\QaController@saveQa');
    Route::post('qa/sort', 'backend\QaController@sortQa');
    Route::post('qa/upload', 'backend\QaController@upload');

    //Team
    Route::post('team/verify', 'backend\TeamController@verifyTeam');
    Route::get('team/search', 'backend\TeamController@searchPage');
    Route::get('team/approve', 'backend\TeamController@approvePage');
    Route::get('team/results', 'backend\TeamController@results');
    Route::get('team/results/info/{user_id}', 'backend\TeamController@teamInfo');

    Route::get('team/apply-results', 'backend\TeamController@applyResults');
    Route::get('team/apply-info/{id}', 'backend\TeamController@applyInfo');
    Route::post('team/edit-apply-status', 'backend\TeamController@teamApplyStatusEdit');
    Route::post('team/edit-apply-result', 'backend\TeamController@teamApplyResultEdit');
    Route::post('team/remove-member', 'backend\TeamController@removeMember');

    Route::post('team/edit', 'backend\TeamController@teamInfoEdit');
    Route::post('team/edit-status', 'backend\TeamController@teamStatusEdit');
    Route::post('team/upload', 'backend\TeamController@teamInfoUpload');
    Route::post('team/delete', 'backend\TeamController@deleteTeam');
    Route::get('team/download/pitch_deck/{event_id}', 'backend\TeamController@downloadZip');

    //Events
    Route::get('events', 'backend\EventController@index');
    Route::get('events/result', 'backend\EventController@details'); 
    Route::post('event', 'backend\EventController@addEvent'); 
    Route::get('event/certificates', 'backend\EventController@certificates');
    Route::get('event/certificates/edit/{certificates_id}', 'backend\EventController@getEditPage');
    Route::get('event/certificates/add', 'backend\EventController@getAddPage');
    Route::post('event/certificate/form', 'backend\EventController@getForm');
    Route::post('event/certificate/save', 'backend\EventController@save');
    Route::post('event/certificate/preview/index', 'backend\EventController@indexPreview');
    Route::post('event/certificate/preview', 'backend\EventController@addPreview');
    Route::delete('event', 'backend\EventController@deleteEvent');

    //Certificates
    Route::get('certificates', 'backend\CertificateController@index');
    Route::get('certificates/add', 'backend\CertificateController@getAddPage');
    Route::post('certificate/form', 'backend\CertificateController@getForm');
    Route::post('certificate/save', 'backend\CertificateController@save');
    Route::post('certificate/preview/index', 'backend\CertificateController@indexPreview');
    Route::post('certificate/preview', 'backend\CertificateController@preview');

    //Videos
    Route::get('videos', 'backend\CourseLectureController@getMP4');

    
    Route::get('documents', 'backend\DocumentController@index');
    Route::get('document/add', 'backend\DocumentController@getAdd');
    Route::post('document/upload', 'backend\DocumentController@upload');
    Route::post('document/save', 'backend\DocumentController@addDocument');
    Route::post('document/delete', 'backend\DocumentController@deleteDocument');
});

Route::post('test', function (){
    $input = \Illuminate\Support\Facades\Input::all();
});


/*
Route::get('team', function (){
    $apply = \App\Models\uleagueapply::join('uleague_team', 'uleague_team.user_id', 'uleague_apply.user_id')->get();
    foreach ($apply as $item){
        $insert = \App\Models\uleagueapply::where('user_id', $item->user_id)->first();
        $insert->team_id = $item->team_id;
        $insert->save();
        echo $item->team_id.'<br>';
    }
});
*/


Route::get('rename/pitchdeck', function (){
    $decks= \App\Models\uleagueteam::get();
    foreach ($decks as $deck){
        $team_id = (basename($deck->file_url,'.pdf'));
        $bug_team_id = explode('.', $team_id);
        if(count($bug_team_id) > 1)continue;
        $team = \App\Models\uleagueteam::where('team_id', $team_id)->first();
        $newName = DataHelpers::fileName($team->team_name);
        $new_file_name = $team_id . '_' . $newName . '.pdf';
        rename(public_path() . $deck->file_url, public_path(). '/uploads/uleague/pitch_deck/' . $new_file_name);
        $team->file_url = '/uploads/uleague/pitch_deck/' . $new_file_name;
        $team->save();
        echo 'File Name => ' . $new_file_name . '<br>';
        echo 'File Path => ' . $team->file_url . '<br>' . '----------------' . '<br>';
    }
});
