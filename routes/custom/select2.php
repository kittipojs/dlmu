<?php
/**
 * Created by PhpStorm.
 * User: kitti
 * Date: 29/1/2561
 * Time: 17:39
 */
Route::group(['prefix' => 'select'], function () {
    Route::get('/university', 'Select2Controller@GetUniversity');
    Route::get('/faculty', 'Select2Controller@GetFaculty');
    Route::get('/province', 'Select2Controller@GetProvince');
    Route::get('/pitch/member', 'Select2Controller@SelectPitchMember');
    Route::get('/camp/round', 'Select2Controller@getCampingRound');
    Route::get('/pitch/round', 'Select2Controller@getPitchingRound');
    Route::get('/course', 'Select2Controller@getCourse');
    Route::get('/quiz', 'Select2Controller@getQuiz');
    Route::get('/event', 'Select2Controller@getEvent');
    Route::get('/email', 'Select2Controller@getEmail');
    Route::get('/citizen_id', 'Select2Controller@getCitizenId');
    Route::get('/certificates', 'Select2Controller@getCertificates');
});