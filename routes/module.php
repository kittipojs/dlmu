<?php
        
// Start Routes for university 
Route::resource('e-admin/university','UniversityController');
// End Routes for university 

                    
// Start Routes for faculty 
Route::resource('e-admin/faculty','FacultyController');
// End Routes for faculty 

                    
// Start Routes for courses 
Route::resource('courses','CoursesController');
// End Routes for courses 

                    
// Start Routes for quiz 
Route::resource('quiz','QuizController');
// End Routes for quiz 

                    
// Start Routes for quizequestions 
Route::resource('quizequestions','QuizequestionsController');
// End Routes for quizequestions 

                    
// Start Routes for usernia 
Route::resource('usernia','UserniaController');
// -- Post Method --

// End Routes for usernia 

                    
// Start Routes for province 
Route::resource('province','ProvinceController');
// End Routes for province 

                    
// Start Routes for pitching 
Route::resource('pitching','PitchingController');
// End Routes for pitching 

                    
// Start Routes for pitchinground 
Route::resource('pitchinground','PitchingroundController');
// End Routes for pitchinground 

                    
// Start Routes for camping 
Route::resource('camping','CampingController');
// End Routes for camping 

                    
// Start Routes for campinground 
Route::resource('campinground','CampingroundController');
// End Routes for campinground 

                    
// Start Routes for records 
Route::resource('records','RecordsController');
// End Routes for records 

                    
// Start Routes for quizscore 
Route::resource('quizscore','QuizscoreController');
// End Routes for quizscore 

                    
// Start Routes for certificates 
Route::resource('certificates','CertificatesController');
// End Routes for certificates 

                    
// Start Routes for usercertificates 
Route::resource('usercertificates','UsercertificatesController');
// End Routes for usercertificates 

                    ?>