<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'tb_users';

    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];
    
    public function nia()
    {
        return $this->hasOne('App\Models\Usernia');
    }
    
    public function faculty()
    {
        return $this->hasOne('App\Models\Faculty');
    }

    
    public function uleague()
    {
        return $this->hasOne('App\Models\Uleagueuser');
    }
}
