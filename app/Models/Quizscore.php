<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class quizscore extends Sximo  {

    protected $table = 'quiz_score';
    protected $primaryKey = 'id';

    public function __construct() {
        parent::__construct();

    }

    public static function querySelect(  ){

        return "  SELECT quiz_score.* FROM quiz_score  ";
    }	

    public static function queryWhere(  ){

        return "  WHERE quiz_score.id IS NOT NULL ";
    }

    public static function queryGroup(){
        return "  ";
    }
    
    public static function getStat($quiz_id){
        
        $response = array();
        
        $response['total'] = quizscore::where('quiz_id', $quiz_id)->count();
        $response['pass'] = quizscore::where('quiz_id', $quiz_id)->where('is_pass', 1)->count();
        
        return $response;
    }


}
