<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class info extends Sximo  {
	
	protected $table = 'info';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT info.* FROM info  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE info.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
