<?php 

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\Models\Coursesession;

class coursetags extends Sximo  {

    protected $table = 'course_tags';
    protected $primaryKey = 'id';

    public function __construct() {
        parent::__construct();

    }

    public static function querySelect(  ){

        return "  SELECT course_tags.* FROM qa  ";
    }	

    public static function queryWhere(  ){

        return "  WHERE course_tags.id IS NOT NULL ";
    }

    public static function queryGroup(){
        return "  ";
    }
    
    public static function getByTag($tag_name){
    
        $coursetags = coursetags::select('*')
            ->join('courses', 'courses.course_id', 'course_tags.course_id')
            ->where('course_tags.tag_name', $tag_name)
            ->where('courses.course_status', 1)
            ->orderBy('courses.created_at', 'DESC')
            ->get();
        
        foreach($coursetags as $key => $value){
            
            $coursetags[$key]->enroll = 0;
                
            $session = Coursesession::where('course_id', $value->course_id)->groupBy('user_id')->get();
            
            $coursetags[$key]->enroll = count($session);
            
        }
        
        return $coursetags;
        
    }


}
