<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class lectures extends Sximo  {

    protected $table = 'course_lectures';
    protected $primaryKey = 'lecture_id';
    protected $casts = [
        'course_id' => 'string',
        'lecture_id' => 'string',
    ];

    public function __construct() {
        parent::__construct();

    }

    public static function querySelect(  ){

        return "  SELECT course_lectures.* FROM course_lectures  ";
    }	

    public static function queryWhere(){

        return "  WHERE course_lectures.course_id IS NOT NULL ";
    }

    public static function getLectureInfo($lecture_id){
        return lectures::join('courses', 'courses.course_id', 'course_lectures.course_id')
            ->where('course_lectures.lecture_id', $lecture_id)
            ->first();
    }

    public static function queryGroup(){
        return "  ";
    }




}
