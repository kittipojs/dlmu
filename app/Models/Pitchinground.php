<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class pitchinground extends Sximo  {
	
	protected $table = 'startup_pitching_round';
	protected $primaryKey = 'round_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT startup_pitching_round.* FROM startup_pitching_round  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE startup_pitching_round.round_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
