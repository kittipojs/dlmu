<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class coursecurriculum extends Sximo
{

    protected $table = 'course_curriculum';
    protected $primaryKey = 'id';
    protected $casts = [
        'course_id' => 'string',
        'object_id' => 'string',
    ];

    public function __construct()
    {
        parent::__construct();

    }

    public static function querySelect()
    {

        return " SELECT course_curriculum.* FROM course_curriculum  ";
    }

    public static function queryWhere()
    {

        return " WHERE course_curriculum.id IS NOT NULL ";
    }

    public static function queryGroup()
    {
        return "";
    }

    public static function getDetail($id){
        
        $curriculum = coursecurriculum::leftjoin('course_lectures', 'course_lectures.lecture_id', 'course_curriculum.object_id')
            ->leftjoin('quiz', 'quiz.quiz_id', 'course_curriculum.object_id')
            ->where('course_curriculum.course_id', $id)
            ->orderBy('object_order', 'ASC')
            ->get();

        //dd($curriculum);

        $response = array();

        foreach ($curriculum as $key => $value) {

            $r = array();

            $r['id'] = $curriculum[$key]['curriculum_id'];
            $r['course_id'] = $curriculum[$key]['object_id'];

            //lecture
            if ($curriculum[$key]['lecture_id'] != null && $curriculum[$key]['quiz_id'] == null) {
                $r['title'] = $curriculum[$key]['lecture_title'];
                $r['type'] = 'lecture';
                $r['media_type'] = $curriculum[$key]['media_type'];

                if($r['media_type']=='mp4' || $r['media_type']=='album'){
                    $r['start'] = $curriculum[$key]['start_time'];
                    $r['end'] = $curriculum[$key]['end_time'];
            }

            //quiz
            } else {
                $r['title'] = $curriculum[$key]['quiz_title'];
                $r['type'] = 'quiz';
            }

            $r['object_order'] = $curriculum[$key]['object_order'];
            $r['object_id'] = $curriculum[$key]['object_id'];
            $r['object_type'] = $curriculum[$key]['object_type'];


            $response[] = $r;
        }
        //dd($response);

        return $response;
    }
    
    public static function getDetailById($id){
        $curriculum = coursecurriculum::select('course_curriculum.*', 'course_lectures.*', 'quiz.quiz_title', 'quiz.quiz_id', 'quiz.quiz_description', 'quiz.quiz_type', 'quiz.quiz_timer', 'quiz.quiz_score_to_pass', 'quiz.quiz_is_random_question', 'quiz.quiz_is_random_answer')
            ->leftjoin('course_lectures', 'course_lectures.lecture_id', 'course_curriculum.object_id')
            ->leftjoin('quiz', 'quiz.quiz_id', 'course_curriculum.object_id')
            ->where('course_curriculum.curriculum_id', $id)
            ->orderBy('object_order', 'ASC')
            ->first();

        return $curriculum;
    }
    
    
    public static function getDetailByCourseId($course_id){
        $curriculum = coursecurriculum::select('course_curriculum.*', 'course_lectures.*', 'quiz.quiz_title', 'quiz.quiz_id', 'quiz.quiz_description', 'quiz.quiz_type', 'quiz.quiz_timer', 'quiz.quiz_score_to_pass', 'quiz.quiz_is_random_question', 'quiz.quiz_is_random_answer')
            ->leftjoin('course_lectures', 'course_lectures.lecture_id', 'course_curriculum.object_id')
            ->leftjoin('quiz', 'quiz.quiz_id', 'course_curriculum.object_id')
            ->where('course_curriculum.course_id', $course_id)
            ->orderBy('object_order', 'ASC')
            ->first();

        return $curriculum;
    }
    
    
    public static function getDetailByObjectId($id){
        $curriculum = coursecurriculum::select('course_curriculum.*', 'course_lectures.*', 'quiz.quiz_title', 'quiz.quiz_id', 'quiz.quiz_description', 'quiz.quiz_type', 'quiz.quiz_timer', 'quiz.quiz_score_to_pass', 'quiz.quiz_is_random_question', 'quiz.quiz_is_random_answer')
            ->leftjoin('course_lectures', 'course_lectures.lecture_id', 'course_curriculum.object_id')
            ->leftjoin('quiz', 'quiz.quiz_id', 'course_curriculum.object_id')
            ->where('course_curriculum.object_id', $id)
            ->orderBy('object_order', 'ASC')
            ->first();

        return $curriculum;
    }

    public static function reorder($course_id, $object_order){
        
        coursecurriculum::where('course_id', $course_id)->delete();
        
        foreach($object_order as $order => $object) {
            
            $courseCurriculum = new coursecurriculum();
            $courseCurriculum->course_id = $course_id;
            $courseCurriculum->object_id = explode(':', $object)[0];
            $courseCurriculum->object_type = explode(':', $object)[1];
            $courseCurriculum->object_order = $order + 1;
            
            $courseCurriculum->save();
        }
        
    }


}
