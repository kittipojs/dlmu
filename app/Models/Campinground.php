<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class campinground extends Sximo  {
	
	protected $table = 'startup_camping_round';
	protected $primaryKey = 'round_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT startup_camping_round.* FROM startup_camping_round  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE startup_camping_round.round_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
