<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class uleagueteam extends Sximo  {

    protected $table = 'uleague_team';
    protected $primaryKey = 'team_id';

    public function __construct() {
        parent::__construct();

    }

    public static function querySelect(  ){

        return "  SELECT uleague_team.* FROM uleague_team  ";
    }	

    public static function queryWhere(  ){

        return "  WHERE uleague_team.id IS NOT NULL ";
    }

    public static function queryGroup(){
        return "  ";
    }

    public static function getTeamDetailByID($user_id){
        return uleagueteam::join('tb_users', 'uleague_team.user_id', 'tb_users.id')
            ->where('uleague_team.user_id', $user_id)
            ->first();
    }

}
