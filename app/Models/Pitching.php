<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class pitching extends Sximo  {
	
	protected $table = 'startup_pitching';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT startup_pitching.* FROM startup_pitching  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE startup_pitching.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}

    public static function getPitchDetailByID($id){
        return pitching::join('tb_users', 'startup_pitching.user_id', 'tb_users.id')
            ->join('university', 'university.university_id', 'startup_pitching.university_id')
            ->join('faculty', 'faculty.faculty_id', 'startup_pitching.faculty_id')
            ->where('startup_pitching.user_id', $id)
            ->first();
    }

}
