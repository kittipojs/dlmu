<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class records extends Sximo  {
	
	protected $table = 'records';
	protected $primaryKey = 'record_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT records.* FROM records  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE records.record_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
