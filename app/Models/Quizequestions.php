<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class quizequestions extends Sximo  {
	
	protected $table = 'quiz_questions';
	protected $primaryKey = 'question_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT quiz_questions.* FROM quiz_questions  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE quiz_questions.question_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
