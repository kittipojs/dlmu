<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class qa extends Sximo  {
	
	protected $table = 'qa';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT qa.* FROM qa  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE qa.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
