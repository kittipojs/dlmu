<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class camping extends Sximo  {

    protected $table = 'startup_camping';
    protected $primaryKey = 'id';

    public function __construct() {
        parent::__construct();

    }

    public static function querySelect(  ){

        return "  SELECT startup_camping.* FROM startup_camping  ";
    }	

    public static function queryWhere(  ){

        return "  WHERE startup_camping.id IS NOT NULL ";
    }

    public static function queryGroup(){
        return "  ";
    }

    public static function getCampDetailByID($id){
        return camping::join('tb_users', 'startup_camping.user_id', 'tb_users.id')
            ->join('startup_camping_round', 'startup_camping_round.round_id', 'startup_camping.round_id')
            ->join('university', 'university.university_id', 'startup_camping.university_id')
            ->join('faculty', 'faculty.faculty_id', 'startup_camping.faculty_id')
            ->where('startup_camping.user_id', $id)
            ->first();
    }
}
