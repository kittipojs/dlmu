<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class eventcertificates extends Sximo  {
	
	protected $table = 'event_certificates';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT events.* FROM event_certificates  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE event_certificates.event_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "";
	}
	

}
