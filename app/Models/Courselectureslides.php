<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class courselectureslides extends Sximo
{

    protected $table = 'course_lecture_slides';
    protected $primaryKey = 'id';
    protected $casts = [
        'lecture_id' => 'string',
    ];

    public function __construct()
    {
        parent::__construct();

    }

    public static function querySelect()
    {

        return " SELECT course_lecture_slides.* FROM course_lecture_slides  ";
    }

    public static function queryWhere()
    {

        return " WHERE course_lecture_slides.id IS NOT NULL ";
    }

    public static function queryGroup()
    {
        return "";
    }

}
