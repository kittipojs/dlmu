<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon as Carbon;

class courses extends Sximo  {

    protected $table = 'courses';
    protected $primaryKey = 'course_id';

    protected $casts = [
        'course_id' => 'string',
    ];

    public function __construct() {
        parent::__construct();

    }

    public static function querySelect(  ){

        return "  SELECT courses.* FROM courses  ";
    }	

    public static function queryWhere(  ){

        return "  WHERE courses.course_id IS NOT NULL ";
    }

    public static function queryGroup(){
        return "  ";
    }


    public function getCreatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i');
    }

    public function getUpdatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i');
    }

}
