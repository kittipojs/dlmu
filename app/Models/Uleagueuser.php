<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class uleagueuser extends Sximo  {

    protected $table = 'uleague_user';
    protected $primaryKey = 'id';

    public function __construct() {
        parent::__construct();

    }

    public static function querySelect(  ){

        return "  SELECT uleague_user.* FROM uleague_user  ";
    }	

    public static function queryWhere(  ){

        return "  WHERE uleague_user.id IS NOT NULL ";
    }

    public static function queryGroup(){
        return "  ";
    }

    public static function getCampDetailByID($id){
        return uleagueuser::join('tb_users', 'uleague_user.user_id', 'tb_users.id')
            ->join('startup_camping_round', 'startup_camping_round.round_id', 'startup_camping.round_id')
            ->join('university', 'university.university_id', 'startup_camping.university_id')
            ->join('faculty', 'faculty.faculty_id', 'startup_camping.faculty_id')
            ->where('startup_camping.user_id', $id)
            ->first();
    }


}
