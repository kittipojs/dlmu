<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class uleagueapply extends Sximo  {

    protected $table = 'uleague_apply';
    protected $primaryKey = 'id';

    public function __construct() {
        parent::__construct();

    }

    public static function querySelect(  ){

        return "  SELECT uleague_apply.* FROM uleague_apply  ";
    }	

    public static function queryWhere(  ){

        return "  WHERE uleague_apply.id IS NOT NULL ";
    }

    public static function queryGroup(){
        return "  ";
    }

    public static function countParticipants($event_id){
        return uleagueapply::where('reference_id', $event_id)
                ->where('status', '>=', 1)
                ->count();
    }
    
    public static function hasJoinCampEvent($user_id){
        return uleagueapply::where('user_id', $user_id)
                ->where('status', '>=', 2)
                ->where('apply_for', 'camp')
                ->get()->first();
    }


}
