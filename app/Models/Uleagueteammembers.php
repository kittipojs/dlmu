<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class uleagueteammembers extends Sximo  {

    protected $table = 'uleague_team_members';
    protected $primaryKey = 'member_id';

    public function __construct() {
        parent::__construct();

    }

    public static function getTeamMembers($team_id){ 
        return uleagueteammembers::where('uleague_team_members.team_id', $team_id)
                                ->join('tb_users', 'tb_users.id', 'uleague_team_members.user_id')
                                ->join('uleague_user', 'uleague_user.user_id', 'uleague_team_members.user_id')
                                ->join('faculty', 'faculty.faculty_id', 'uleague_user.faculty_id')
            /* ->leftJoin('uleague_apply', 'uleague_apply.user_id', 'uleague_team_members.user_id') */
            ->get();
    } 

    public static function getTeamDetailByUserId($user_id){ 
        return uleagueteammembers::join('uleague_team', 'uleague_team_members.team_id', 'uleague_team.team_id')
            ->where('uleague_team_members.user_id', $user_id) 
            ->first(); 
    } 

}
