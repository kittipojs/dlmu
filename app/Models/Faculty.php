<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class faculty extends Sximo  {
	
	protected $table = 'faculty';
	protected $primaryKey = 'faculty_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT faculty.* FROM faculty  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE faculty.faculty_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
