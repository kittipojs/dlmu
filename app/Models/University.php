<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class university extends Sximo  {
	
	protected $table = 'university';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT university.* FROM university  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE university.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
