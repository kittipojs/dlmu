<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class usercertificates extends Sximo  {
	
	protected $table = 'user_certificates';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT user_certificates.* FROM user_certificates  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE user_certificates.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
