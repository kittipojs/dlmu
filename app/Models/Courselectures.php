<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class courselectures extends Sximo  {

    protected $table = 'course_lectures';
    protected $primaryKey = 'id';
    protected $casts = [
        'course_id' => 'string',
        'lecture_id' => 'string',
    ];

    public function __construct() {
        parent::__construct();

    }

    public static function querySelect(  ){

        return " SELECT course_lectures.* FROM course_lectures  ";
    }	

    public static function queryWhere(  ){

        return " WHERE course_lectures.id IS NOT NULL ";
    }

    public static function queryGroup(){
        return "";
    }

    /*
    public static function getDetail($id){
        $curriculum = coursecurriculum::leftjoin('course_lectures', 'course_lectures.lecture_id', 'course_curriculum.object_id')
            ->leftjoin('quiz', 'quiz.quiz_id', 'course_curriculum.object_id')
            ->where('course_curriculum.course_id', $id)
            ->orderBy('object_order', 'ASC')
            ->get();

        $response = array();

        foreach($curriculum as $key => $value){

            $r = array();
            
            $r['id'] = $curriculum[$key]['curriculum_id'];

            if($curriculum[$key]['lecture_id']!=null && $curriculum[$key]['quiz_id']==null){
                
                $r['title'] = $curriculum[$key]['lecture_title'];
                $r['type'] = 'lecture';
            }
            else{
    
                $r['title'] = $curriculum[$key]['quiz_title'];
                $r['type'] = 'quiz';
            }
            
            $r['object_order'] = $curriculum[$key]['object_order'];

            $response[] = $r;
        }

        return $response;
    }
    */


}
