<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class quiz extends Sximo  {
	
	protected $table = 'quiz';
	protected $primaryKey = 'quiz_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT quiz.* FROM quiz  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE quiz.quiz_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
