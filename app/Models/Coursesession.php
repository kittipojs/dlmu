<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class coursesession extends Sximo{

    protected $table = 'course_session';
    protected $primaryKey = 'id';
    protected $casts = [
        'course_id' => 'string',
    ];

    public function __construct(){
        parent::__construct();

    }

    public static function querySelect(){

        return " SELECT course_session.* FROM course_session ";
    }

    public static function queryWhere(){
        return " WHERE course_session.id IS NOT NULL ";
    }

    public static function countStudent($course_id){
        return coursesession::where('course_id', $course_id)
                ->groupBy('user_id')->get()->count();
    }

    public static function create($user_id, $course_id, $curriculum_id, $object_id, $progress, $is_pass){
        
        $course_session = new coursesession();

        $course_session->user_id = $user_id;        
        $course_session->course_id = $course_id;    
        $course_session->curriculum_id = $curriculum_id; 
        $course_session->object_id = $object_id;
        $course_session->progress = $progress;
        $course_session->is_pass = $is_pass;
        
        
        $course_session->save();
    }

}
