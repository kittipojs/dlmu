<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class uleagueevent extends Sximo  {

    protected $table = 'uleague_events';
    protected $primaryKey = 'id';

    public function __construct() {
        
        parent::__construct();

    }

    public static function querySelect(  ){

        return "  SELECT uleague_events.* FROM uleague_events ";
        
    }	

    public static function queryWhere(  ){

        return "  WHERE uleague_events.id IS NOT NULL ";
        
    }

    public static function queryGroup(){
        return "  ";
    }


    public static function getCampEvents(){
        return uleagueevent::where('event_type', 'camp')->get();
    }
    

    public static function getPitchingEvents(){
        return uleagueevent::where('event_type', 'pitching')->get();
    }
    
}
