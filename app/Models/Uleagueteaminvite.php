<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class uleagueteaminvite extends Sximo  {

    protected $table = 'uleague_team_invite';
    protected $primaryKey = 'invite_id';

    public function __construct() {
        parent::__construct();

    }

    public static function getInvitationByEmail($email){ 
        return uleagueteaminvite::join('uleague_team', 'uleague_team.team_id', 'uleague_team_invite.team_id') 
            ->join('tb_users', 'uleague_team.user_id', 'tb_users.id') 
            ->where('invite_to', $email) 
            ->where('uleague_team_invite.status', 0) 
            ->select('invitation_code', 'uleague_team.team_id', 'uleague_team.team_name', 'first_name', 'last_name') 
            ->get() 
            ->first(); 
    } 

}
