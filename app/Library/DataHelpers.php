<?php

class DataHelpers
{

    static function Occupation($index)
    {

        $occupation["C03"] = "ธุรกิจส่วนตัว";
        $occupation["C09"] = "อาชีพอิสระ";
        $occupation["C07"] = "ข้าราชการ/พนักงานรัฐวิสาหกิจ";
        $occupation["C11"] = "นักลงทุนในหลักทรัพย์มืออาชีพ";
        $occupation["C02"] = "ผู้แนะนำการลงทุนในหลักทรัพย์";
        $occupation["C08"] = "พนักงานบริษัทในธุรกิจการเงิน";
        $occupation["C12"] = "บุคลากรทางการแพทย์";
        $occupation["C13"] = "วิศวกร";
        $occupation["C14"] = "บุคลากรทางการศึกษาและวิจัย";
        $occupation["C05"] = "พนักงานบริษัทเอกชนอื่นๆ";
        $occupation["C04"] = "นักเรียน/นักศึกษา";
        $occupation["C01"] = "เกษียณ";
        $occupation["C06"] = "แม่บ้าน/พ่อบ้าน";
        $occupation["C10"] = "อื่นๆ";
        $occupation[""] = "ไม่ระบุ";

        return $occupation[$index];
    }

    static function Age($birthday)
    {
        $birthDate = explode("-", $birthday);

        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[2], $birthDate[1], $birthDate[0]))) > date("md")
            ? ((date("Y") - $birthDate[0]) - 1)
            : (date("Y") - $birthDate[0]));

        if ($age > 99) {
            return '-';
        } else {
            return $age;
        }
    }

    static function startupLookup($index)
    {
        $startupLookup["0"] = "";
        $startupLookup["1"] = "การแพทย์และสาธารณสุข (MedTech/ Health Tech)";
        $startupLookup["2"] = "เกษตรและอาหาร (AgriTech/ Food Startup)";
        $startupLookup["3"] = "อสังหาริมทรัพย์ (Property Tech)";
        $startupLookup["4"] = "การเงินและการธนาคาร (FinTech)";
        $startupLookup["5"] = "การศึกษา (EdTech)";
        $startupLookup["6"] = "การท่องเที่ยว (TravelTech)";
        $startupLookup["7"] = "ไลฟ์สไตล์ (LifeStyle) ";
        $startupLookup["8"] = "พาณิชยกรรมอิเล็กทรอนิกส์ (E-Commerce)";
        $startupLookup["9"] = "ภาครัฐ/การศึกษา (GovTech/EdTech) ";
        $startupLookup["10"] = "แหล่งเงินทุน (Venture Capital/Coperate Venture Capital)";
        $startupLookup["11"] = "พันธมิตร (Partners)";
        $startupLookup["12"] = "การสนับสนุนจากรัฐบาล (Government Support)";
        $startupLookup["13"] = "คอร์สเรียนออนไลน์ (Online Learning Courses)";
        $startupLookup["14"] = "อุตสาหกรรม (Industrial Tech)";

        $index_arr = explode(',', $index);
        $startupLookup_text = '';

        foreach ($index_arr as $key => $value) {
            $startupLookup_text .= $startupLookup[$value] . "  <br>";
        }

        return $startupLookup_text;
    }

    static function fileName($fileName)
    {
        $replace = [' ', '  ', '	', '\\', '/', ':', '*', '?', '"', '<', '>', '|'];
        $name = trim($fileName);
        foreach ($replace as $search) {
            $name = str_replace($search, "_", $name);
        }
        $pattern = '[\\\/\:\*\?\"\<\>\|\s+]';
        $replacement = '_';
        return preg_replace($pattern, $replacement, $name);
    }

    static function year()
    {
        return date('Y');
    }

}