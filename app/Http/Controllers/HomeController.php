<?php  namespace App\Http\Controllers;

use Mail;
use App\Models\Courses;
use App\Models\Coursetags;
use App\Models\Events;
use App\Models\Lectures;
use App\Models\Post;
use App\Models\Uleagueuser;
use App\Models\Uleagueteaminvite;
use App\Library\Markdown;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 

class HomeController extends Controller {

    public function __construct(){
        parent::__construct();

        $this->data['pageLang'] = 'en';
        if(\Session::get('lang') != '')
        {
            $this->data['pageLang'] = \Session::get('lang');
        }	
    }

    public function index( Request $request){

        \App::setLocale(\Session::get('lang'));

        if(config('sximo.cnf_front') =='false' && $request->segment(1) =='' ) :
        return redirect('e-admin');
        endif;

        $page = $request->segment(1);

        \DB::table('tb_pages')->where('alias',$page)->update(array('views'=> \DB::raw('views+1')));

        if($page !='') {

            $sql = \DB::table('tb_pages')->where('alias','=',$page)->where('status','=','enable')->get();
            $row = $sql[0];

            if(file_exists(base_path().'/resources/views/layouts/'.config('sximo.cnf_theme').'/template/'.$row->filename.'.blade.php') && $row->filename !=''){
                $page_template = 'layouts.'.config('sximo.cnf_theme').'.template.'.$row->filename;
            } else {
                $page_template = 'layouts.'.config('sximo.cnf_theme').'.template.page';
            }

            if($row->access !=''){
                $access = json_decode($row->access,true)	;
            } else {
                $access = array();
            }

            // If guest not allowed
            if($row->allow_guest !=1){
                $group_id = \Session::get('gid');
                $isValid =  (isset($access[$group_id]) && $access[$group_id] == 1 ? 1 : 0 );
                if($isValid ==0){
                    return redirect('')->with(['message' => __('core.note_restric') ,'status'=>'error']);
                }
            }
            //custom page cms
            if (strpos($row->note, 'pages.') === 0) {
                $this->data['content'] = view($row->note, $this->data);
            } else {
                $this->data['content'] = \PostHelpers::formatContent($row->note);
            }

            $this->data['pages'] = $page_template;
            $this->data['title'] = $row->title ;
            $this->data['subtitle'] = $row->sinopsis ;
            $this->data['pageID'] = $row->pageID ;
            $this->data['content'] = \PostHelpers::formatContent($row->note);
            $this->data['note'] = $row->note;

            if($row->template =='frontend'){
                $page = 'layouts.'.config('sximo.cnf_theme').'.index';
            }
            else {
                return view($page_template, $this->data);
            }

            return view( $page, $this->data);
        }
        else {

            $sql = \DB::table('tb_pages')->where('default','1')->get();
            if(count($sql)>=1)
            {
                $row = $sql[0];
                $this->data['title'] = $row->title;
                $this->data['subtitle'] = $row->sinopsis;

                if(file_exists(base_path().'/resources/views/layouts/'.config('sximo.cnf_theme').'/template/'.$row->filename.'.blade.php') && $row->filename !=''){
                    $page_template = 'layouts.'.config('sximo.cnf_theme').'.template.'.$row->filename;
                } else {
                    $page_template = 'layouts.'.config('sximo.cnf_theme').'.template.page';
                }

                $this->data['pages'] = $page_template;
                $this->data['pageID'] = $row->pageID ;
                $this->data['note'] = $row->note;

                $page = 'layouts.'.config('sximo.cnf_theme').'.index';

                //custom page cms
                if (strpos($row->note, 'pages.') === 0) {
                    $this->data['content'] = $row->note;
                } else {
                    $this->data['content'] = \PostHelpers::formatContent($row->note);
                }

                $this->data['has_notification'] = false;
                $this->data['show_uleague_btn'] = true;
                //$hl_events = self::getUpcomingEvents(5);

                
                //$hl_events = Events::orderBy('created_at', 'DESC')->get();

                /*
                foreach($hl_events as $event){
                    //$item = self::getEvent($event->event_id);

                    $e = array();

                    $e['id'] = $event->id;
                    $e['name'] = $event->name;
                    $e['venue'] = $event->venue;
                    $e['type'] = $event->type;
                    $e['start_date'] = self::dateThai( date('Y-m-d H:i:s', $event->start_date) );
                    $e['end_date'] = self::dateThai( date('Y-m-d H:i:s', $event->end_date) );
                    $e['registration'] = $event->registration;
                    $e['remain'] = $event->registration - $event->total_registration;
                    $e['cover'] = asset('/frontend/medilab/img/event-banner.jpg');
                    if(isset($event->cover)){
                        $e['cover'] = $event->cover;
                    }

                    $e['url'] = $event->url;
                    $e['is_register'] = $event->is_register;
                    $e['is_checkin'] = $event->is_checkin;

                    if($event->type=='Camp'){
                        $e['type_text'] = 'Camp  กลาง';
                    }
                    else if($event->type=='UCamp'){
                        $e['type_text'] = 'Camp  ของมหาวิทยาลัย';
                    }

                    $this->data['events'][] = $e;
                }
                */
                

                $this->data['invitation'] = null;
                if(Auth::check()){

                    $user_id = Auth::user()->id;
                    $email = Auth::user()->email;

                    $invites = uleagueteaminvite::where('invite_to', $email)->where('status', 0)->get()->first();

                    $this->data['uleague'] = self::checkULeagueUser();

                    if($invites){
                        $this->data['invitation'] = $invites;
                        $this->data['has_notification'] = true;
                    }

                }
                else{
                    //$events = self::getEvents('General', '');
                }
                $this->data['courses_101'] = array();
                $this->data['courses_warrior'] = array();

                $this->data['courses_101']      = Coursetags::getByTag('startup101')->take(4);
                $this->data['all_courses_101']  = (count(Coursetags::getByTag('startup101')) > 4 ? true :false);


                $this->data['courses_warrior']      = Coursetags::getByTag('warrior')->take(4);
                $this->data['all_courses_warrior']  = (count(Coursetags::getByTag('warrior')) > 4 ? true :false);

                /*
                $this->data['videos_coaching'] = lectures::where('course_id', '1518413832816')->orderBy('lecture_id', 'desc')->limit(4)->get();
                $this->data['video_online_courses'] = lectures::where('course_id', '1518413832819')->orderBy('lecture_id', 'desc')->limit(4)->get();
                */

                return view( $page, $this->data);

            } else {
                return 'Please Set Default Page';
            }
        }
    }

    public function  getLang( Request $request , $lang='en')
    {
        $request->session()->put('lang', $lang);
        return  Redirect::back();
    }

    public function  getSkin($skin='sximo')
    {
        \Session::put('themes', $skin);
        return  Redirect::back();
    }		

    public  function  postContact( Request $request)
    {

        $this->beforeFilter('csrf', array('on'=>'post'));
        $rules = array(
            'name'		=>'required',
            'subject'	=>'required',
            'message'	=>'required|min:20',
            'sender'	=>'required|email'			
        );
        $validator = Validator::make(Input::all(), $rules);	
        if ($validator->passes()) 
        {

            $data = array('name'=>$request->input('name'),'sender'=>$request->input('sender'),'subject'=>$request->input('subject'),'notes'=>$request->input('message')); 
            $message = view('emails.contact', $data); 		
            $data['to'] = $this->config['cnf_email'];			
            if($this->config['cnf_mail'] =='swift')
            { 
                Mail::send('user.emails.contact', $data, function ($message) use ($data) {
                    $message->to($data['to'])->subject($data['subject']);
                });	

            }  else {

                $headers  	= 'MIME-Version: 1.0' . "\r\n";
                $headers 	.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers 	.= 'From: '.$request->input('name').' <'.$request->input('sender').'>' . "\r\n";
                mail($data['to'],$data['subject'], $message, $headers);		
            }

            return Redirect::to($request->input('redirect'))->with('message', \SiteHelpers::alert('success','Thank You , Your message has been sent !'));	

        } else {
            return Redirect::to($request->input('redirect'))->with('message', \SiteHelpers::alert('error','The following errors occurred'))
                ->withErrors($validator)->withInput();
        }		
    }	

    public function submit( Request $request )
    {
        $formID = $request->input('form_builder_id');

        $rows = \DB::table('tb_forms')->where('formID',$formID)->get();
        if(count($rows))
        {
            $row = $rows[0];
            $forms = json_decode($row->configuration,true);
            $content = array();
            $validation = array();
            foreach($forms as $key=>$val)
            {
                $content[$key] = (isset($_POST[$key]) ? $_POST[$key] : ''); 
                if($val['validation'] !='')
                {
                    $validation[$key] = $val['validation'];
                }
            }

            $validator = Validator::make($request->all(), $validation);	
            if (!$validator->passes()) 
                return redirect()->back()->with(['status'=>'error','message'=>'Please fill required input !'])
                ->withErrors($validator)->withInput();


            if($row->method =='email')
            {
                // Send To Email
                $data = array(
                    'email'		=> $row->email ,
                    'content'	=> $content ,
                    'subject'	=> "[ " .config('sximo.cnf_appname')." ] New Submited Form ",
                    'title'		=> $row->name 			
                );

                if( config('sximo.cnf_mail') =='swift' )
                { 				
                    \Mail::send('sximo.form.email', $data, function ( $message ) use ( $data ) {
                        $message->to($data['email'])->subject($data['subject']);
                    });		

                }  else {

                    $message 	 = view('sximo.form.email', $data);
                    $headers  	 = 'MIME-Version: 1.0' . "\r\n";
                    $headers 	.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers 	.= 'From: '. config('sximo.cnf_appname'). ' <'.config('sximo.cnf_email').'>' . "\r\n";
                    mail($data['email'], $data['subject'], $message, $headers);	
                }

                return redirect()->back()->with(['status'=>'success','message'=> $row->success ]);

            } else {
                // Insert into database 
                \DB::table($row->tablename)->insert($content);
                return redirect()->back()->with(['status'=>'success','message'=>  $row->success  ]);

            }
        } else {

            return redirect()->back()->with(['status'=>'error','message'=>'Cant process the form !']);
        }


    }

    public function getLoad()
    {	
        $result = \DB::table('tb_notification')->where('userid',\Session::get('uid'))->where('is_read','0')->orderBy('created','desc')->limit(5)->get();

        $data = array();
        $i = 0;
        foreach($result as $row)
        {
            if(++$i <=10 )
            {
                if($row->postedBy =='' or $row->postedBy == 0)
                {
                    $image = '<img src="'.asset('uploads/images/system.png').'" border="0" width="30" class="img-circle" />';
                } 
                else {
                    $image = \SiteHelpers::avatar('30', $row->postedBy);
                }
                $data[] = array(
                    'url'	=> $row->url,
                    'title'	=> $row->title ,
                    'icon'	=> $row->icon,
                    'image'	=> $image,
                    'text'	=> substr($row->note,0,100),
                    'date'	=> date("d/m/y",strtotime($row->created))
                );
            }	
        }

        $data = array(
            'total'	=> count($result) ,
            'note'	=> $data
        );	
        return response()->json($data);	
    }

    public function posts( Request $request , $read = '') 
    {
        $posts = \DB::table('tb_pages')
            ->select('tb_pages.*','tb_users.username',\DB::raw('COUNT(commentID) AS comments'))
            ->leftJoin('tb_users','tb_users.id','tb_pages.userid')
            ->leftJoin('tb_comments','tb_comments.pageID','tb_pages.pageID')
            ->groupBy('tb_pages.pageID')
            ->where('pagetype','post');
        if($read !='') {
            $posts = $posts->where('alias', $read )->get(); 
        } 
        else {

            $posts = $posts->paginate(12);
        }					

        $this->data['title']		= 'Post Articles';
        $this->data['posts']		= $posts;
        $this->data['pages']		= 'secure.posts.posts';
        base_path().'/resources/views/layouts/'.config('sximo.cnf_theme').'/blog/index.blade.php';

        if(file_exists(base_path().'/resources/views/layouts/'.config('sximo.cnf_theme').'/blog/index.blade.php'))
        {
            $this->data['pages'] = 'layouts.'.config('sximo.cnf_theme').'.blog.index';
        }	

        if($read !=''){
            if(file_exists(base_path().'/resources/views/layouts/'.config('sximo.cnf_theme').'/blog/view.blade.php'))
            {
                if(count($posts))
                {
                    $this->data['posts'] = $posts[0];
                    $this->data['comments']	= \DB::table('tb_comments')
                        ->select('tb_comments.*','username','avatar','email')
                        ->leftJoin('tb_users','tb_users.id','tb_comments.UserID')
                        ->where('PageID',$this->data['posts']->pageID)
                        ->get();
                    \DB::table('tb_pages')->where('pageID',$this->data['posts']->pageID)->update(array('views'=> \DB::raw('views+1')));						
                } else {
                    return redirect('posts');
                }	
                $this->data['title']		= $this->data['posts']->title;
                $this->data['pages'] = 'layouts.'.config('sximo.cnf_theme').'.blog.view';

            }	
        }	
        $page = 'layouts.'.config('sximo.cnf_theme').'.index';
        return view( $page , $this->data);	
    }

    public function comment( Request $request)
    {
        $rules = array(
            'comments'	=> 'required'
        );
        $validator = Validator::make($request->all(), $rules);	
        if ($validator->passes()) {

            $data = array(
                'userID'		=> \Session::get('uid'),
                'posted'		=> date('Y-m-d H:i:s') ,
                'comments'		=> $request->input('comments'),
                'pageID'		=> $request->input('pageID')
            );

            \DB::table('tb_comments')->insert($data);
            return redirect('posts/'.$request->input('alias'))
                ->with(['message'=>'Thank You , Your comment has been sent !','status'=>'success']);
        } else {
            return redirect('posts/'.$request->input('alias'))
                ->with(['message'=>'The following errors occurred','status'=>'error']);	
        }
    }

    public function remove( Request $request, $pageID , $alias , $commentID )
    {
        if($commentID !='')
        {
            \DB::table('tb_comments')->where('commentID',$commentID)->delete();
            return redirect('posts/'.$alias)
                ->with(['message'=>'Comment has been deleted !','status'=>'success']);

        } else {
            return redirect('posts/'.$alias)
                ->with(['message'=>'Failed to remove comment !','status'=>'error']);
        }
    }

    public function set_theme( $id ){
        session(['set_theme'=> $id ]);
        return response()->json(['status'=>'success']);
    }
    
    private function getUpcomingEvents($pagesize){

        $curl = new \anlutro\cURL\cURL;

        $rq = $curl->newRequest('get', config('app.zipevent_url').'/api/nia/event/search?pagesize='.$pagesize.'&page=1&isPast=false&type=&start=&end=')
            ->setHeader('Content-Type', 'application/json')
            ->setHeader('Host', config('app.zipevent_host'))
            ->setHeader('client_id', config('app.zipevent_client_id'))
            ->setHeader('client_secret', config('app.zipevent_client_secret'))
            ->setHeader('device_id', '1')
            ->setHeader('Content-Length', '0');

        $ev = $rq->send();

        return json_decode($ev->body);
    }

    private function getEvent($event_id){

        $curl = new \anlutro\cURL\cURL;

        $rq = $curl->newRequest('get', config('app.zipevent_url').'/api/nia/Event/Info?id='.$event_id)
            ->setHeader('Content-Type', 'application/json')
            ->setHeader('Host', config('app.zipevent_host'))
            ->setHeader('client_id', config('app.zipevent_client_id'))
            ->setHeader('client_secret', config('app.zipevent_client_secret'))
            ->setHeader('device_id', '1')
            ->setHeader('Content-Length', '0');

        $ev = $rq->send();

        return json_decode($ev->body);
    }

    private function getEvents($type, $email){

        $curl = new \anlutro\cURL\cURL;

        $rq = $curl->newRequest('get', config('app.zipevent_url').'/api/nia/event/uleague?type='.$type.'&email='.$email)
            ->setHeader('Content-Type', 'application/json')
            ->setHeader('Host', config('app.zipevent_host'))
            ->setHeader('client_id', config('app.zipevent_client_id'))
            ->setHeader('client_secret', config('app.zipevent_client_secret'))
            ->setHeader('device_id', '1')
            ->setHeader('Content-Length', '0');

        $ev = $rq->send();

        return json_decode($ev->body);
    }

    function getAllCourse($tag_name){

        if(Auth::check()){

            $user_id = Auth::user()->id;
            $email = Auth::user()->email;

            $data['uleague'] = self::checkULeagueUser();
        }

        $data['has_notification'] = self::hasInvitation();

        if($data['has_notification']){
            $data['invitation'] = $this->data['has_notification'];
        }


        $data['courses'] = Coursetags::getByTag($tag_name);
        $data['content'] = 'pages.frontend.main.all-course';

        $data['title'] = '';
        if($tag_name=='warrior'){
            $data['title'] = 'หลักสูตร Startup Warrior';
        }
        else if($tag_name=='startup101'){
            $data['title'] = 'หลักสูตร Startup 101';
        }

        $data['show_uleague_btn'] = true;

        $page_template = 'layouts.medilab.index';

        return view($page_template,$data);
    }


    private function checkULeagueUser(){

        $uleague = uleagueuser::join('tb_users', 'tb_users.id', 'uleague_user.user_id')
            ->join('university', 'university.university_id', 'uleague_user.university_id')
            ->join('faculty', 'faculty.faculty_id', 'uleague_user.faculty_id')
            ->where('user_id', \Auth::user()->id)->first();

        return $uleague;
    }


    private function hasInvitation(){
        $has_notification = false;

        if(Auth::check()){

            $user_id = Auth::user()->id;
            $email = Auth::user()->email;

            $invites = uleagueteaminvite::where('invite_to', $email)->where('status', 0)->get()->first();

            if($invites){
                $has_notification = $invites;
            }
        }

        return $has_notification;

    } 

    private function dateThai($strDate){
        
        $strYear = date("Y", strtotime($strDate)) + 543;
        $strMonth = date("n", strtotime($strDate));
        $strDay = date("j", strtotime($strDate));
        $strHour = date("H", strtotime($strDate));
        $strMinute = date("i", strtotime($strDate));
        $strSeconds = date("s", strtotime($strDate));

        $strMonthCut = Array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");

        $strMonthThai = $strMonthCut[$strMonth];

        return "$strDay $strMonthThai $strYear, $strHour:$strMinute";
        
    }

}
