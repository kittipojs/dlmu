<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Vsmoraes\Pdf\Pdf;


class PdfController extends Controller
{
    protected $data = array();
    private $pdf;

    public function __construct(Pdf $pdf)
    {
        $this->pdf = $pdf;
    }

    public function usage()
    {
        if(!Auth::check()){
            return redirect('')->with('message', 'Please Login')->with('status','error');
        }
        $user = User::leftjoin('user_nia', 'tb_users.id', 'user_nia.user_id')
            ->leftJoin('faculty', 'faculty.faculty_id', 'user_nia.faculty_id')
            ->where('user_id', Auth::user()->id)
            ->first();
//dd($user);
        if(!$user){
            return redirect('user/profile')->with('message', 'ไม่พบข้อมูล')->with('status','error');
        }
        $this->data['user'] = $user;
        $html = view('pdf.resume', $this->data)->render();

        return $this->pdf->load($html)->filename('resume')->show();
    }
}