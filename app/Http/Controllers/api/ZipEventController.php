<?php

namespace App\Http\Controllers\api;

use App\User;
use App\Models\Events;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class ZipEventController extends Controller{

    protected $data = array();
    protected $template = array();

    public function __construct(){
        parent::__construct();
    }

    public function search( Request $request ){

        $input = $request->input();

        $type = $input['type'];

        $curl = new \anlutro\cURL\cURL;

        $rq = $curl->newRequest('get', config('app.zipevent_url').'/api/nia/event/search?type='.$type)
            ->setHeader('Content-Type', 'application/json')
            ->setHeader('Host', config('app.zipevent_host'))
            ->setHeader('client_id', config('app.zipevent_client_id'))
            ->setHeader('client_secret', config('app.zipevent_client_secret'))
            ->setHeader('device_id', '1')
            ->setHeader('Content-Length', '0');

        $ev = $rq->send();

        return response()->json(array('success' => true, 'data' => json_decode($ev->body)), 200);
    }

    public function highlight( Request $request ){

        //$hl_events = Events::orderBy('created_at', 'DESC')->get();
        $hl_events = self::getUpcomingEvents(5);
        
        foreach($hl_events as $event){
            //$item = self::getEvent($event->event_id);

            $item = $event;
            $e = array();

            $e['id'] = $item->id;
            $e['name'] = $item->name;
            $e['venue'] = $item->venue;
            $e['type'] = $item->type;
            $e['start_date'] = self::dateThai( date('Y-m-d H:i:s', $item->start_date) );
            $e['end_date'] = self::dateThai( date('Y-m-d H:i:s', $item->end_date) );
            $e['registration'] = $item->registration;
            $e['remain'] = $item->registration-$item->total_registration;
            $e['cover'] = asset('/frontend/medilab/img/event-banner.jpg');
            if(isset($item->cover)){
                $e['cover'] = $item->cover;
            }

            $e['url'] = $item->url;
            $e['is_register'] = $item->is_register;
            $e['is_checkin'] = $item->is_checkin;

            if($item->type=='Camp'){
                $e['type_text'] = 'Camp  กลาง';
            }
            else if($item->type=='UCamp'){
                $e['type_text'] = 'Camp  ของมหาวิทยาลัย';
            }

            $this->data['events'][] = $e;
        }
        
        return view('pages.frontend.event.highlight', $this->data);
    }
    
    private function getUpcomingEvents($pagesize){

        $curl = new \anlutro\cURL\cURL;

        $rq = $curl->newRequest('get', config('app.zipevent_url').'/api/nia/event/search?pagesize='.$pagesize.'&page=1&isPast=false&type=&start=&end=')
            ->setHeader('Content-Type', 'application/json')
            ->setHeader('Host', config('app.zipevent_host'))
            ->setHeader('client_id', config('app.zipevent_client_id'))
            ->setHeader('client_secret', config('app.zipevent_client_secret'))
            ->setHeader('device_id', '1')
            ->setHeader('Content-Length', '0');

        $ev = $rq->send();

        return json_decode($ev->body);
    }


    private function getEvent($event_id){

        $curl = new \anlutro\cURL\cURL;

        $rq = $curl->newRequest('get', config('app.zipevent_url').'/api/nia/Event/Info?id='.$event_id)
            ->setHeader('Content-Type', 'application/json')
            ->setHeader('Host', config('app.zipevent_host'))
            ->setHeader('client_id', config('app.zipevent_client_id'))
            ->setHeader('client_secret', config('app.zipevent_client_secret'))
            ->setHeader('device_id', '1')
            ->setHeader('Content-Length', '0');

        $ev = $rq->send();

        return json_decode($ev->body);
    }

    private function dateThai($strDate){
        $strYear = date("Y", strtotime($strDate)) + 543;
        $strMonth = date("n", strtotime($strDate));
        $strDay = date("j", strtotime($strDate));
        $strHour = date("H", strtotime($strDate));
        $strMinute = date("i", strtotime($strDate));
        $strSeconds = date("s", strtotime($strDate));

        $strMonthCut = Array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");

        $strMonthThai = $strMonthCut[$strMonth];

        return "$strDay $strMonthThai $strYear, $strHour:$strMinute";
    }

}