<?php

namespace App\Http\Controllers\api;

use App\Models\Courses;
use App\Models\Coursecurriculum;
use App\Models\Coursesession;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class CourseSessionController extends Controller{

    protected $data = array();
    protected $template = array();

    public function __construct(){

        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Pitching',
            'pageNote' => 'Register',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );
    }
    
    public function updateProgress(Request $request){
        
        $input = $request->input();
        
        
        return response()->json(array('success' => false, 'message' => $input), 200);
    }
    
}