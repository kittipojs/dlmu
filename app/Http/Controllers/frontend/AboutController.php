<?php

namespace App\Http\Controllers\frontend;

use App\Models\Info;
use App\Models\Uleagueteam;
use App\Models\Uleagueuser;
use App\Models\Uleagueteammembers;
use App\Models\Uleagueteaminvite;
use App\Models\Documents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    protected $data = array();
    protected $template = array();

    public function __construct(){

        parent::__construct();

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );
    }

    function index(){

        //$this->data['qa'] = qa::where('status', 1)->orderBy('order', 'asc')->get();

        $this->data['content'] = "pages.frontend.main.about";
        $this->data['title'] = 'เกี่ยวกับเรา';

        if(Auth::check()){
            $this->data['uleague'] = self::checkULeagueUser();
            $this->data['has_notification'] = self::hasInvitation();
        }

        $page_template = $this->template['frontend'];

        $info = Info::find(1);
        $this->data['info'] = $info;

        return view($page_template, $this->data);
    }


    public function getDownloadPage(){

        $this->data['content'] = "pages.frontend.main.download";
        $this->data['title'] = 'เกี่ยวกับเรา';

        if(Auth::check()){
            $this->data['uleague'] = self::checkULeagueUser();
            $this->data['has_notification'] = self::hasInvitation();
        }

        $docs = documents::get();

        $this->data['docs'] = $docs;

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);
    }


    private function hasInvitation(){
        $has_notification = false;

        if(Auth::check()){

            $user_id = Auth::user()->id;
            $email = Auth::user()->email;

            $invites = uleagueteaminvite::where('invite_to', $email)->where('status', 0)->get()->first();

            if($invites){
                $has_notification = $invites;
            }
        }

        return $has_notification;
    } 

    private function checkULeagueTeamMember(){

        $team_member = uleagueteammembers::getTeamDetailByUserId(\Auth::user()->id);

        return $team_member;
    }

    private function checkULeagueTeamLeader(){

        $team_leader = Uleagueteam::where('user_id', \Auth::user()->id)->get()->first();

        return $team_leader;
    }

    private function checkULeagueUser(){

        if(Auth::check()){
            $uleague = uleagueuser::join('tb_users', 'tb_users.id', 'uleague_user.user_id')
                ->join('university', 'university.university_id', 'uleague_user.university_id')
                ->join('faculty', 'faculty.faculty_id', 'uleague_user.faculty_id')
                ->where('user_id', \Auth::user()->id)->first();

            return $uleague;
        }
        else{
            return false;
        }
    }
}

