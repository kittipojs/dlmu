<?php

namespace App\Http\Controllers\frontend;

use App\Models\Camping;
use App\Models\Campinground;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class CampingController extends Controller
{
    protected $data = array();
    protected $camping = 'pages.frontend.camping';
    protected $pitching = 'pages.frontend.pitching';
    protected $template = array();

    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Camping',
            'pageNote' => 'Register',
        );
        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );
    }

    function FormRegisterCamping()
    {
        if(!Auth::check()){
            return redirect('/user/login')->with(['message' => 'กรุณาสมัครสมาชิกก่อน','status'=>'error']);
        }

        $user = User::where('id', Auth::user()->id)->first();
        $rounds = campinground::get();

        $camping = camping::where('user_id', Auth::user()->id)->get()->first();

        if($camping){
            return redirect('/user/camp/details');
        }

        $this->data['user'] = $user;
        $this->data['rounds'] = $rounds;
        $this->data['round_count'] = self::CountRound();
        $this->data['content'] = $this->camping.".form";
        $this->data['title'] = 'ลงทะเบียน - Startup Camping';

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);
    }

    function getRegister(Request $request)
    {
        
        $input = Input::all();
        $round_count = self::CountRound();
        $user_id = $input['user_id'];

        if(!isset($input['round'])){
            return back()->with('message', 'โปรดเลือกรอบการสมัคร')->with('status', 'error');
        }

        if (camping::where('user_id', $user_id)->count() > 0) {
            return back()->with('message', 'คุณมีสิทธิ์สมัครได้เพียง 1 ครั้งเท่านั้น')->with('status', 'error');
        }

        if ($round_count[$input['round']] > 150) {
            return back()->with('message', 'ผู้สมัครเกินจำนวนที่จำกัด')->with('status', 'error');
        }

        // Get the UploadedFile object
        if($request->hasFile('image')){

            $files = $request->file('image');
            $fileImage = substr($files->getMimeType(), 0, 5);

            if($fileImage != 'image'){
                return back()->with('message', 'ไฟล์อัพโหลดผิดประเภท')->with('status', 'error');
            }

            //Path for save and display image
            $urlPath = 'uploads/stl2018/camping/';
            $filename = $urlPath.time()."_".$user_id.".jpg";
            //Path for save image
            $destinationPath = public_path(). "/uploads/stl2018/camping/";
            //Store image to /public/uploads/images/profile
            $files->move($destinationPath, $filename);
            $url = $urlPath.time()."_".$user_id.".jpg";

        } else {
            return back()->with('message', 'โปรดอัพโหลดรูป')->with('status', 'error');
        }

        $camping = new camping();
        $camping->user_id = $user_id;
        $camping->nickname = $input['nickname'];
        $camping->university_id = $input['university'];
        $camping->faculty_id = $input['faculty'];
        $camping->round_id = $input['round'];
        $camping->college_year = $input['year'];
        $camping->need_hotel = $input['room'];
        $camping->photo_src = $url;
        $camping->save();

        $user = User::where('id', '=', $user_id);
        if ($user->count() >= 1) {
            $user = $user->get();
            $user = $user[0];
            $data = array();
            $to = $user->email;
            $subject = "ตอบกลับ: สมัคร Startup Thailand League 2018 (Camp)";
            $data['subject'] = $subject;
            $data['email'] = $to;

            if (config('sximo.cnf_mail') == 'swift') {

                \Mail::send('emails.camping-confirm-register', $data, function ($message) use ($data) {
                    $message->to($data['email'])->subject($data['subject']);
                });

            } else {

                $message = view('emails.camping-confirm-register', $data);
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: ' . config('sximo.cnf_appname') . ' <' . config('sximo.cnf_email') . '>' . "\r\n";
                mail($to, $subject, $message, $headers);
            }

            return redirect('')->with('message', 'ลงทะเบียนเสร็จสิ้น')->with('status', 'success');
        }
    }

    static function CountRound()
    {
        $rounds = campinground::get();
        $count = array();
        foreach($rounds as $round){
            $count[$round->round_id] = camping::where('round_id',$round->round_id)->count();
        }
        return $count;
    }

    function getCampDetails()
    {
        if(!Auth::check()){
            return redirect('')->with(['message' => 'กรุณาสมัครสมาชิกก่อน','status'=>'error']);
        }

        $camp_detail = camping::getCampDetailByID(Auth::user()->id);
        $this->data['row'] = $camp_detail;
        $this->data['content'] = $this->camping.".details";
        $this->data['title'] = 'สถานะการสมัคร Camping';
        $this->data['pageNote'] = 'สถานะการสมัคร';

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);

    }


}
