<?php

namespace App\Http\Controllers\frontend;

use App\Models\Coursecurriculum;
use App\Models\Courselectures;
use App\Models\Courselectureslides;
use App\Models\Coursesession;
use App\User;
use App\Models\Courses;
use App\Models\Lectures;
use App\Models\Quiz;
use App\Models\Quizscore;
use App\Models\Quizequestions;
use App\Models\Usercertificates;
use App\Models\Uleagueuser;
use App\Models\Uleagueteaminvite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class CourseController extends Controller{


    protected $data = array();
    protected $course_dir = 'pages.frontend.course';
    protected $template = array();

    public function __construct(){
        parent::__construct();

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );
    }

    function courseInfo($id){

        //if (!\Auth::check()) return redirect('user/login');

        $course = courses::where('course_id', $id)->first();

        if(!$course){
            return redirect('/')->with(['message' => 'ไม่พบหลักสูตรเรียน','status'=>'error']);
        }

        if($course->course_status==0){

            $group_id = \Auth::user()->group_id;

            if($group_id >= 3){
                return redirect('/')->with(['message' => 'ไม่พบหลักสูตรเรียน','status'=>'error']);
            }
        }
        
        if($course->course_prerequisite_id!=null){
           $this->data['course_prerequisite'] = courses::where('course_id', $course->course_prerequisite_id)->first();
        }
        
        $this->data['free_course'] = false;

        //increment course view
        courses::where('course_id', $id)->increment('course_view');
        
        if($course->percentage_lecture==0 && $course->percentage_quiz==0){
            $this->data['free_course'] = true;
        }

        $this->data['course']  = $course;
        $this->data['has_session'] = false;

        $curriculum = coursecurriculum::getDetail($id);

        if (\Auth::check()){
            $session = coursesession::where('user_id', Auth::user()->id)->where('course_id', $id)->first();

            if($session){
                $this->data['has_session'] = $session;
            }
        }

        $this->data['curriculum'] = $curriculum;

        $this->data['content'] = $this->course_dir.".index";
        $this->data['title']   = $course->course_title;
        $this->data['uleague'] = self::checkULeagueUser();
        $this->data['has_notification'] = self::hasInvitation();

        if($this->data['has_notification']){
            $this->data['invitation'] = $this->data['has_notification'];
        }

        $this->data['show_uleague_btn'] = true;
        
        $this->data['og']['title'] = $course->course_title;
        $this->data['og']['description'] = 'หลักสูตรเรียนออนไลน์ที่เหมาะกับ '.$course->course_for_text;
        $this->data['og']['image'] = url($course->course_thumbnail);
        $this->data['og']['url'] = url('/c/'.$course->course_id);

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);
    }

    function courseLearn($course_id, Request $request){

        if (!\Auth::check()) return redirect('user/login');

        if(!$request->session){
            if (!\Auth::check()) return redirect('user/login');
        }

        $user_id = \Auth::user()->id;
        $lecture_id = null;

        if(!$request->curriculum){
            //$cur_id = $curriculum[0]['id'];
            return redirect('/course/'.$course_id.'/learn?curriculum=welcome');
        }
        else{
            $cur_id = $request->curriculum;

            $current_session = coursesession::where('curriculum_id', $cur_id)->where('user_id', $user_id)->get();

            //$lecture_id = $current_session[0]->object_id;

        }

        $course = courses::where('course_id', $course_id)->first();
        if(!$course){
            return redirect('/')->with(['message' => 'ไม่พบหลักสูตรเรียน','status'=>'error']);
        }
        
        if($course->course_prerequisite_id!=null){
            $uc = usercertificates::where('user_id', Auth::user()->id)->where('issue_for', 'course')->where('object_id', $course->course_prerequisite_id)->first();
            
            if(!$uc){
                 return redirect('/c/'.$course->course_id)->with(['message' => 'คุณไม่สามารถเรียนหลักสูตรนี้ได้ เนื่องจากขาดคุณสมบัติ','status'=>'error']);
            }
        }

        if($course->course_status==0){

            $group_id = \Auth::user()->group_id;

            if($group_id >= 3){
                return redirect('/')->with(['message' => 'ไม่พบหลักสูตรเรียน','status'=>'error']);
            }
        }
        
        $this->data['free_course'] = false;
        if($course->percentage_lecture==0 && $course->percentage_quiz==0){
            $this->data['free_course'] = true;
        }

        $curriculum = coursecurriculum::getDetail($course_id);

        $user_sessions = coursesession::where('course_id', $course_id)->where('user_id', Auth::user()->id)->get();
        $session_array = array();
        //$current_session = null;

        /*
        if(count($user_sessions)>0){

            foreach($user_sessions as $item){
                $session_array[] = $item->object_id;
            }

            $current_session = $user_sessions[count($user_sessions)-1];
        }
        */

        //START curriculum builder
        foreach($curriculum as $key => $value){

            if(!$course->can_skip){
                $found_id = null;

                $curriculum[$key]['session'] = null;
                $curriculum[$key]['active'] = false;
                foreach($user_sessions as $k => $v) {
                    if ($curriculum[$key]['object_id'] == $v->object_id){
                        $found_id = $k;
                        $curriculum[$key]['active'] = true;
                        $curriculum[$key]['session'] = $v;
                        break;
                    }
                }

            }
            else{
                $curriculum[$key]['active'] = true;
            }

            //MATCH WITH USER'S CURRENT SESSION

            $curriculum[$key]['is_current'] = false;
            if($current_session!=null){
                /*
                if($curriculum[$key]['object_id'] == $current_session->object_id){
                    $curriculum[$key]['is_current'] = true;
                }
                */
            }

        }
        //END curriculum builder

        if($cur_id=='welcome'){

            $this->data['welcome'] = true;

            $content_object = coursecurriculum::getDetailById($cur_id);
        }
        else{

            $this->data['welcome'] = false;

            $content_object = coursecurriculum::getDetailById($cur_id);

            $content_object->time_from_millisec = self::timeToMillisec($content_object->start_time);
            $content_object->time_to_millisec = self::timeToMillisec($content_object->end_time);


            $lecture_id = $content_object->object_id;

            $hours = floor($content_object->media_duration / 3600);
            $mins = floor($content_object->media_duration / 60 % 60);
            $secs = floor($content_object->media_duration % 60);

            $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);

            $content_object->time_format = $timeFormat;


            $is_eligible = false;
            if($course->can_skip==false){

                $last_session = $user_sessions[count($user_sessions)-1];

                //elegible if curriculum match any session
                foreach($user_sessions as $k => $v) {
                    if ($content_object->object_id == $v->object_id){
                        $is_eligible = true;
                        break;
                    }
                }

                if(!$is_eligible){
                    if($last_session->is_pass){
                        coursesession::create(Auth::user()->id, $course_id, $content_object->object_id, 0, false);
                        $is_eligible = true;
                    }
                }
            }
            else{
                $is_eligible = true;
                $has_session = false;
                //chech if course_session exist or not
                foreach($user_sessions as $k => $v) {
                    if ($content_object->object_id == $v->object_id){
                        $has_session = true;
                        break;
                    }
                }

                if(!$has_session){
                    coursesession::create(Auth::user()->id, $course_id, $request->curriculum, $content_object->object_id, 0, false);
                }
            }

            $this->data['content_object'] = $content_object;

            if($content_object->quiz_id==null && $content_object->lecture_id!=null){
                $this->data['content_type'] = 'lecture';
                if($content_object->media_type=='youtube'){
                    $content_object->enable_progress = false;
                }
                else{
                    $content_object->enable_progress = true;
                }
            }
            else{
                $this->data['content_type'] = 'quiz';

                $quiz_id = $content_object->quiz_id;

                $quiz = quiz::where('quiz_id', $quiz_id)->first();
                $questions = quizequestions::where('quiz_id', $quiz_id);
                if($quiz->quiz_is_random_question){
                    $questions = $questions->inRandomOrder();
                }
                $questions = $questions->get();
                $isFirst = true;
                foreach ($questions as $question){
                    $choices = (array)json_decode($question->question_choices);
                    if($quiz->quiz_is_random_answer){
                        shuffle($choices);
                    }
                    $question->choices = $choices;

                    if($isFirst){
                        $question->active = true;
                        $isFirst = false;
                    }
                    else{
                        $question->active = false;
                    }
                }
                $this->data['quiz'] = $quiz;
                $this->data['questions'] = $questions;

            }

            $this->data['is_eligible']  = $is_eligible;


            $slides = courselectureslides::where('lecture_id', $lecture_id)->get();

            $this->data['has_slide']  = false;
            if(count($slides)>0){

                $slide_transition = array();

                foreach($slides as $item){
                    $slide_transition[] = self::timeToSecond($item->slide_time);
                }
                $this->data['slide_transition'] = $slide_transition;

                $this->data['has_slide']  = true;
                $this->data['slides']  = $slides;
            }
        }

        $this->data['course']  = $course;
        $this->data['curriculum_id'] = $cur_id;
        $this->data['curriculum'] = $curriculum;
        $this->data['current_session'] = $current_session;

        $this->data['content'] = $this->course_dir.".course-learn";

        $this->data['title']   = 'เรียน :: '.$course->course_title;
        $this->data['uleague'] = self::checkULeagueUser();
        $this->data['has_notification'] = self::hasInvitation();
        if($this->data['has_notification']){
            $this->data['invitation'] = $this->data['has_notification'];
        }
        $this->data['show_uleague_btn'] = true;

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);

    }

    function courseLecture($id){

        if (!\Auth::check()) return redirect('user/login');

        $course_id = explode('_', $id)[0];
        $lecture_id = explode('_', $id)[1];
        
        $course = courses::where('course_id', $course_id)->first();
        if(!$course){
            return redirect('/')->with(['message' => 'ไม่พบหลักสูตรเรียน','status'=>'error']);
        }
        
        $this->data['free_course'] = false;
        if($course->percentage_lecture==0 && $course->percentage_quiz==0){
            $this->data['free_course'] = true;
        }

        $this->data['course'] = $course;
        $this->data['lecture'] = lectures::getLectureInfo($lecture_id);

        $this->data['related_lectures'] = lectures::where('course_id', $course_id)->get();
        $this->data['content'] = $this->course_dir.".course-lecture";
        $this->data['title']   = 'เรียน :: '.$this->data['lecture']->lecture_title;
        $this->data['uleague'] = self::checkULeagueUser();
        $this->data['has_notification'] = self::hasInvitation();
        if($this->data['has_notification']){
            $this->data['invitation'] = $this->data['has_notification'];
        }
        $this->data['show_uleague_btn'] = true;

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);
    }

    function coursePretest($course_id){

        if (!\Auth::check()) return redirect('user/login');

        $user_id = Auth::user()->id;

        $course = courses::where('course_id', $course_id)->first();
        if(!$course){
            return redirect('/')->with(['message' => 'ไม่พบหลักสูตรเรียน','status'=>'error']);
        }
        
        $this->data['free_course'] = false;
        
        if($course->percentage_lecture==0 && $course->percentage_quiz==0){
            $this->data['free_course'] = true;
        }

        $quiz = quiz::where('quiz_id', $course->pre_test_id)->first();

        $questions = quizequestions::where('quiz_id', $course->pre_test_id);

        if($quiz->quiz_is_random_question){
            $questions = $questions->inRandomOrder();
        }

        $questions = $questions->get();
        $isFirst = true;

        foreach ($questions as $question){
            $choices = (array)json_decode($question->question_choices);
            if($quiz->quiz_is_random_answer){
                shuffle($choices);
            }
            $question->choices = $choices;

            if($isFirst){
                $question->active = true;
                $isFirst = false;
            }
            else{
                $question->active = false;
            }
        }

        $this->data['quiz'] = $quiz;
        $this->data['questions'] = $questions;

        $this->data['course']  = $course;

        $this->data['content'] = $this->course_dir.".course-pretest";
        $this->data['title']   = 'แบบทดสอบก่อนเรียน :: '.$course->course_title;
        $this->data['uleague'] = self::checkULeagueUser();
        $this->data['has_notification'] = self::hasInvitation();
        if($this->data['has_notification']){
            $this->data['invitation'] = $this->data['has_notification'];
        }
        $this->data['show_uleague_btn'] = true;

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);

    }

    function coursePosttest($course_id){

        if (!\Auth::check()) return redirect('user/login');

        $user_id = Auth::user()->id;

        $course = courses::where('course_id', $course_id)->first();
        if(!$course){
            return redirect('/')->with(['message' => 'ไม่พบหลักสูตรเรียน','status'=>'error']);
        }
        
        $this->data['free_course'] = false;
        
        if($course->percentage_lecture==0 && $course->percentage_quiz==0){
            $this->data['free_course'] = true;
        }

        $quiz = quiz::where('quiz_id', $course->post_test_id)->first();

        $questions = quizequestions::where('quiz_id', $course->post_test_id);

        if($quiz->quiz_is_random_question){
            $questions = $questions->inRandomOrder();
        }

        $questions = $questions->get();
        $isFirst = true;

        foreach ($questions as $question){
            $choices = (array)json_decode($question->question_choices);
            if($quiz->quiz_is_random_answer){
                shuffle($choices);
            }
            $question->choices = $choices;

            if($isFirst){
                $question->active = true;
                $isFirst = false;
            }
            else{
                $question->active = false;
            }
        }

        $this->data['quiz'] = $quiz;
        $this->data['questions'] = $questions;


        $this->data['course']  = $course;

        $this->data['content'] = $this->course_dir.".course-pretest";
        $this->data['title']   = 'แบบทดสอบก่อนเรียน :: '.$course->course_title;
        $this->data['uleague'] = self::checkULeagueUser();
        $this->data['has_notification'] = self::hasInvitation();
        if($this->data['has_notification']){
            $this->data['invitation'] = $this->data['has_notification'];
        }
        $this->data['show_uleague_btn'] = true;

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);

    }

    function courseResult($course_id){

        if (!\Auth::check()) return redirect('user/login');

        $user_id = Auth::user()->id;

        $course = courses::where('course_id', $course_id)->first();
        
        if(!$course){
            return redirect('/')->with(['message' => 'ไม่พบหลักสูตรเรียน','status'=>'error']);
        }
        
        if($course->percentage_lecture==0 && $course->percentage_quiz==0){
            return redirect('/')->with(['message' => 'ไม่มีผลการเรียนสำหรับหลักสูตรนี้','status'=>'error']);
        }

        if($course->course_status==0){

            $group_id = \Auth::user()->group_id;

            if($group_id >= 3){
                return redirect('/')->with(['message' => 'ไม่พบหลักสูตรเรียน','status'=>'error']);
            }
        }
        
        if($course->percentage_lecture==0 && $course->percentage_quiz==0){
            return redirect('/c/'.$course_id)->with(['message' => 'หลักสูตรเรียนฟรี ไม่สามารถออกใบประกาศได้','status'=>'error']);
        }

        $curriculum = coursecurriculum::getDetail($course_id);
        $course_session = coursesession::where('course_id', $course_id)->where('user_id', $user_id)->get();


        $curriculum_arr = array();

        foreach($curriculum as $item){
            $curriculum_arr[$item['object_id']] = $item['object_id'];
        }

        $course_total_number = count($curriculum)*100;
        $user_progress = 0;

        foreach($course_session as $key => $value ){

            $user_progress += $value->progress;

            foreach($curriculum as $k => $v){

                if( $value->curriculum_id==$v['id'] ){
                    $curriculum[$k]['session'] = $value;
                }
            }
        }

        /* START CHECK PRE TEST */
        $this->data['pre_test'] = null;
        if($course->pre_test_id!=null){
            $pre_test = quizscore::where('quiz_id', $course->pre_test_id)
                ->where('user_id', $user_id)->first();

            $this->data['pre_test'] = $pre_test;
        }


        /* START CHECK POST TEST */
        $this->data['post_test'] = null;

        if($course->post_test_id!=null){
            $post_test = quizscore::where('quiz_id', $course->post_test_id)
                ->where('user_id', $user_id)->first();

            $this->data['post_test'] = $post_test;
        }

        $this->data['course_total_number'] = $course_total_number;
        $this->data['user_progress'] = $user_progress;
        $this->data['user_progress_percentage'] = ($user_progress / $course_total_number)*100;

        /* START CHECK PASS */
        $this->data['graduated'] = array('lecture'=>false, 'quiz'=>false);

        $percentage_lecture = $course->percentage_lecture;
        $percentage_quiz = $course->percentage_quiz;

        if($percentage_lecture<=0){
            $this->data['graduated']['lecture'] = true;
        }

        if($percentage_quiz<=0){
            $this->data['graduated']['quiz'] = true;
        }

        if($this->data['user_progress_percentage'] >= $percentage_lecture ){
            $this->data['graduated']['lecture'] = true;
        }

        if($this->data['post_test']!=null){
            if($this->data['post_test']->percentage >= $percentage_quiz){
                $this->data['graduated']['quiz'] = true;
            }
        }

        //Issue certificate if pass
        if($this->data['graduated']['quiz'] && $this->data['graduated']['lecture']){
            $uc = usercertificates::where('issue_for', 'course')->where('object_id', $course->course_id)->where('user_id', $user_id)->first();

            if(!$uc){
                $uc = new usercertificates();
                $uc->certification_id = $course->course_certificate_id;
                $uc->user_id = $user_id;
                $uc->issue_for = 'course';
                $uc->object_id = $course->course_id;
                $uc->save();
            }
        }

        $this->data['curriculum']  = $curriculum;
        $this->data['course']  = $course;
        $this->data['content'] = $this->course_dir.".course-result";
        $this->data['title']   = 'ผลการเรียน :: '.$course->course_title;
        $this->data['uleague'] = self::checkULeagueUser();
        $this->data['has_notification'] = self::hasInvitation();
        if($this->data['has_notification']){
            $this->data['invitation'] = $this->data['has_notification'];
        }
        $this->data['show_uleague_btn'] = true;

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);
    }

    /*
    function quizInstruction($course_id, $quiz_id){

        if (!\Auth::check()) return redirect('user/login');

        $quiz = quiz::where('quiz_id', $quiz_id)->first();
        $questions = quizequestions::where('quiz_id', $quiz_id)->get();
        $isFirst = true;
        foreach ($questions as $question){

            $question->choices = json_decode($question->question_choices);
            if($isFirst){
                $question->active = true;
                $isFirst = false;
            }
            else{
                $question->active = false;
            }
        }

        $this->data['quiz'] = $quiz;
        $this->data['questions'] = $questions;

        $this->data['content']  = $this->course_dir.".course-quiz";
        $this->data['title']    = $quiz->quiz_title;
        $this->data['uleague'] = self::checkULeagueUser();

        $this->data['has_notification'] = self::hasInvitation();
        if($this->data['has_notification']){
            $this->data['invitation'] = $this->data['has_notification'];
        }

        $page_template = $this->template['frontend'];


        return view($page_template, $this->data);
    }
    */

    private function timeToMillisec($input){
        $time   = explode(":", $input);

        $hour   = $time[0] * 60 * 60 * 1000;
        $minute = $time[1] * 60 * 1000;

        $second = explode(",", $time[2]);
        $sec    = $second[0] * 1000;
        //$milisec= $second[1];

        $result = $hour + $minute + $sec;

        return $result;
    }

    private function hasInvitation(){

        $has_notification = false;

        if(Auth::check()){

            $user_id = Auth::user()->id;
            $email = Auth::user()->email;

            $invites = uleagueteaminvite::where('invite_to', $email)->where('status', 0)->get()->first();

            if($invites){
                $has_notification = $invites;
            }
        }

        return $has_notification;

    } 

    private function checkULeagueUser(){

        if(Auth::check()){
            $uleague = uleagueuser::join('tb_users', 'tb_users.id', 'uleague_user.user_id')
                ->join('university', 'university.university_id', 'uleague_user.university_id')
                ->join('faculty', 'faculty.faculty_id', 'uleague_user.faculty_id')
                ->where('user_id', \Auth::user()->id)->first();

            return $uleague;
        }
        else{
            return false;
        }
    }

    private function timeToSecond($str_time){

        $str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);

        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);

        $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;

        return $time_seconds;
    }

    /*
    function loadCourseLearn($id){

        $course = courses::where('course_id', $id)->first();

        $this->data['course']  = $course;

        $course_lecture = courselectures::where('course_id', $id)->get();

        $this->data['course_lecture']  = $course_lecture;

        $course_curriculum = coursecurriculum::getDetail($id);

        $this->data['course_curriculum']  = $course_curriculum;

        $this->data['uleague'] = self::checkULeagueUser();
        $this->data['has_notification'] = self::hasInvitation();

        if($this->data['has_notification']){
            $this->data['invitation'] = $this->data['has_notification'];
        }

        $this->data['show_uleague_btn'] = true;

        return view($this->course_dir.".courseLearn", $this->data);
    }

    function loadCourseResult($id){

        $course = courses::where('course_id', $id)->first();

        $this->data['course']  = $course;
        $this->data['uleague'] = self::checkULeagueUser();
        $this->data['has_notification'] = self::hasInvitation();

        if($this->data['has_notification']){
            $this->data['invitation'] = $this->data['has_notification'];
        }

        $this->data['show_uleague_btn'] = true;

        return view($this->course_dir.".courseResult", $this->data);
    }
    */

}
