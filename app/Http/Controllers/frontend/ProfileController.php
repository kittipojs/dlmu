<?php

namespace App\Http\Controllers\frontend;

use App;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Coursesession;
use App\Models\Uleagueapply;
use App\Models\Uleagueevent;
use App\Models\Uleagueteam;
use App\Models\Uleagueteammembers;
use App\Models\Uleagueteaminvite;
use App\Models\Uleagueuser;
use App\Models\Usercertificates;
use App\Models\Eventcertificates;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;

class ProfileController extends Controller{

    protected $content = 'layouts.default.profile-menus';

    public function __construct(){
        parent::__construct();

        $this->data = array(
            'pageTitle' => 'Pitching',
            'pageNote' => 'Register',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend' => 'layouts.default.template.backend'
        );
    }

    public function getProfile(){

        if (!\Auth::check()) return redirect('user/login');

        $user_info = User::where('tb_users.id', \Auth::user()->id)
            ->leftJoin('user_nia', 'tb_users.id', 'user_nia.user_id')
            ->leftJoin('thailand_provinces', 'thailand_provinces.code', 'user_nia.province_id')
            ->leftJoin('faculty', 'faculty.faculty_id', 'user_nia.faculty_id')
            ->get()
            ->first();

        if (is_null($user_info)) {
            return \redirect('uleague')->with(['message' => 'คุณต้องสมัคร U League 2018 ก่อน จึงสามารถดูข้อมูลนี้ได้', 'status' => 'error']);
        }

        $this->data['title'] = $user_info->first_name . ' ' . $user_info->last_name;
        $this->data['content'] = $this->content;
        $this->data['sub_content'] = 'pages.frontend.user.profile';
        $this->data['user'] = $user_info;
        $this->data['uleague'] = self::checkULeagueUser();
        $this->data['team_member'] = self::checkULeagueTeamMember();
        $this->data['team_leader'] = self::checkULeagueTeamLeader();
        $this->data['has_notification'] = self::hasInvitation();
        if ($this->data['has_notification']) {
            $this->data['invitation'] = $this->data['has_notification'];
        }
        $this->data['show_uleague_btn'] = true;



        //GET MY EVENTS
        $this->data['events'] = [];

        $my_orders = self::getMyOrders(\Auth::user()->email);

        foreach ($my_orders as $order) {

            $item = $order->event;

            $e = array();

            $e['id'] = $item->id;
            $e['name'] = $item->name;
            $e['venue'] = $item->venue;
            $e['type'] = $item->type;
            $e['start_date'] = self::dateThai(date('Y-m-d H:i:s', $item->start_date));
            $e['end_date'] = self::dateThai(date('Y-m-d H:i:s', $item->end_date));
            $e['registration'] = $item->registration;
            $e['remain'] = $item->registration - $item->total_registration;
            $e['cover'] = asset('/frontend/medilab/img/event-banner.jpg');
            if (isset($item->cover)) {
                $e['cover'] = $item->cover;
            }

            $e['url'] = $item->url;
            $e['is_register'] = $item->is_register;
            $e['is_checkin'] = $item->is_checkin;
            $e['type_text'] = $item->type;

            if ($item->type == 'Camp') {
                $e['type_text'] = 'Camp  กลาง';
            } else if ($item->type == 'UCamp') {
                $e['type_text'] = 'Camp  ของมหาวิทยาลัย';
            }

            if($e['is_register'] && $e['is_checkin']){

            }

            $this->data['events'][] = $e;
        }

        //GET COURSES
        $this->data['apply_courses'] = array();

        $coursesession  = coursesession::where('course_session.user_id', \Auth::user()->id)
            ->groupBy('course_session.course_id')
            ->join('courses', 'course_session.course_id', 'courses.course_id')->get();

        foreach($coursesession as $key => $value){
            if($value->percentage_lecture!=0 || $value->percentage_quiz!=0){

                $c = array();
                $c['course_id'] = $value->course_id;
                $c['course_code'] = $value->course_code;
                $c['course_title'] = $value->course_title;
                $c['course_thumbnail'] = $value->course_thumbnail;

                $session = coursesession::where('course_session.user_id', \Auth::user()->id)
                    ->where('course_session.course_id', $value->course_id)
                    ->orderBy('course_session.created_at', 'asc')->get();

                $c['first'] = self::dateThai($session[0]->created_at);
                $c['last'] = self::dateThai($session[count($session)-1]->created_at);

                $cert = usercertificates::where('user_id', \Auth::user()->id)->where('object_id', $value->course_id)->first();

                if(!$cert){
                    $c['pass'] = array('status'=>false);
                }
                else{
                    $c['pass'] = array('status'=>true, 'datetime'=>self::dateThai($cert->created_at));
                }

                $this->data['apply_courses'][] = $c;
            }
        }

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);

    }

    public function ULeagueJoinPage()
    {

        if (!\Auth::check()) return redirect('user/login');

        $uleague = self::checkULeagueUser();
        $this->data['user_info'] = User::where('tb_users.id', \Auth::user()->id)
            ->leftJoin('user_nia', 'tb_users.id', 'user_nia.user_id')
            ->leftJoin('thailand_provinces', 'thailand_provinces.code', 'user_nia.province_id')
            ->leftJoin('faculty', 'faculty.faculty_id', 'user_nia.faculty_id')
            ->get()
            ->first();

        if (!$uleague) {
            $this->data['title'] = 'ลงทะเบียน U League - 201';
            $this->data['content'] = $this->content;
            $this->data['sub_content'] = 'pages.frontend.user.join-uleague';
            $this->data['uleague'] = $uleague;
            $this->data['team_member'] = self::checkULeagueTeamMember();
            $this->data['team_leader'] = self::checkULeagueTeamLeader();
            $this->data['has_notification'] = self::hasInvitation();
            if ($this->data['has_notification']) {
                $this->data['invitation'] = $this->data['has_notification'];
            }

        } else {
            return redirect('/user/uleague');
        }

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);
    }

    public function getULeague(){

        if (!\Auth::check()) return redirect('user/login');

        $uleague = self::checkULeagueUser();

        if (!$uleague) return redirect('user/uleague/join');

        $this->data['uleague_apply'] = false;

        /*
        $this->data['events'] = [];

        $my_orders = self::getMyOrders(\Auth::user()->email);

        foreach ($my_orders as $order) {

            $item = $order->event;

            $e = array();

            $e['id'] = $item->id;
            $e['name'] = $item->name;
            $e['venue'] = $item->venue;
            $e['type'] = $item->type;
            $e['start_date'] = self::dateThai(date('Y-m-d H:i:s', $item->start_date));
            $e['end_date'] = self::dateThai(date('Y-m-d H:i:s', $item->end_date));
            $e['registration'] = $item->registration;
            $e['remain'] = $item->registration - $item->total_registration;
            $e['cover'] = asset('/frontend/medilab/img/event-banner.jpg');
            if (isset($item->cover)) {
                $e['cover'] = $item->cover;
            }

            $e['url'] = $item->url;
            $e['is_register'] = $item->is_register;
            $e['is_checkin'] = $item->is_checkin;
            $e['type_text'] = $item->type;

            if ($item->type == 'Camp') {
                $e['type_text'] = 'Camp  กลาง';
            } else if ($item->type == 'UCamp') {
                $e['type_text'] = 'Camp  ของมหาวิทยาลัย';
            }

            if($e['is_register'] && $e['is_checkin']){

            }

            $this->data['events'][] = $e;
        }
        */

        $this->data['title'] = 'สถานะ U League';
        $this->data['content'] = $this->content;
        $this->data['sub_content'] = 'pages.frontend.user.uleague';
        $this->data['uleague'] = self::checkULeagueUser();
        $this->data['team_member'] = self::checkULeagueTeamMember();
        $this->data['team_leader'] = self::checkULeagueTeamLeader();
        $this->data['has_notification'] = self::hasInvitation();
        if ($this->data['has_notification']) {
            $this->data['invitation'] = $this->data['has_notification'];
        }

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);
    }

    public function getMyCourses()
    {

        if (!\Auth::check()) return redirect('user/login');

        $this->data['title'] = 'หลักสูตรที่ลงเรียน';
        $this->data['content'] = $this->content;
        $this->data['sub_content'] = 'pages.frontend.user.courses';
        $this->data['uleague'] = self::checkULeagueUser();
        $this->data['team_member'] = self::checkULeagueTeamMember();
        $this->data['team_leader'] = self::checkULeagueTeamLeader();
        $this->data['has_notification'] = self::hasInvitation();
        if ($this->data['has_notification']) {
            $this->data['invitation'] = $this->data['has_notification'];
        }

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);
    }


    /*
    public function getMyEvents()
    {

        if (!\Auth::check()) return redirect('user/login');

        $this->data['title'] = 'อีเว้นท์ที่เข้าร่วม';
        $this->data['content'] = $this->content;
        $this->data['sub_content'] = 'pages.frontend.user.courses';
        $this->data['uleague'] = self::checkULeagueUser();
        $this->data['team_member'] = self::checkULeagueTeamMember();
        $this->data['team_leader'] = self::checkULeagueTeamLeader();
        $this->data['has_notification'] = self::hasInvitation();
        if($this->data['has_notification']){
            $this->data['invitation'] = $this->data['has_notification'];
        }

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);
    }
    */

    public function getApplyCamp()
    {

        if (!\Auth::check()) return redirect('user/login');

        //$events = uleagueevent::getCampEvents();

        $camp = array();

        $this->data['title'] = 'เลือกรอบ U League - Camp';
        $this->data['content'] = $this->content;
        $this->data['sub_content'] = 'pages.frontend.user.apply-camp';
        $this->data['uleague'] = self::checkULeagueUser();
        $this->data['team_member'] = self::checkULeagueTeamMember();
        $this->data['team_leader'] = self::checkULeagueTeamLeader();
        $this->data['has_notification'] = self::hasInvitation();
        if ($this->data['has_notification']) {
            $this->data['invitation'] = $this->data['has_notification'];
        }

        $this->data['has_join_camp'] = false;
        $this->data['events'] = array();
        $this->data['user'] = \Auth::user()->find(\Auth::user()->id)->nia;

        $events = self::getEvents('Camp', \Auth::user()->email);

        foreach ($events as $item) {

            $e = array();

            $e['id'] = $item->id;
            $e['name'] = $item->name;
            $e['venue'] = $item->venue;
            $e['type'] = $item->type;
            $e['start_date'] = self::dateThai(date('Y-m-d H:i:s', $item->start_date));
            $e['end_date'] = self::dateThai(date('Y-m-d H:i:s', $item->end_date));
            $e['registration'] = $item->registration;
            $e['remain'] = $item->registration - $item->total_registration;
            $e['cover'] = asset('/frontend/medilab/img/event-banner.jpg');
            if (isset($item->cover)) {
                $e['cover'] = $item->cover;
            }
            /*
            if(isset($item->logo)){
                $e['logo'] = $item->logo;
            }
            */
            $e['url'] = $item->url;
            $e['is_register'] = $item->is_register;
            $e['is_checkin'] = $item->is_checkin;

            if ($item->type == 'Camp') {
                if ($item->is_register) {
                    $this->data['has_join_camp'] = true;
                }
            }

            if ($item->type == 'Camp') {
                $e['type_text'] = 'Camp  กลาง';
            } else if ($item->type == 'UCamp') {
                $e['type_text'] = 'Camp  ของมหาวิทยาลัย';
            }

            $this->data['events'][] = $e;
        }

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);

    }

    public function getTeam(Request $request)
    {

        if (!\Auth::check()) return redirect('user/login');

        $uleague = self::checkULeagueUser();
        $my_team = uleagueteam::where('user_id', Auth::user()->id)->get()->first();

        $this->data['sub_content'] = 'pages.frontend.user.create_team.step_1';
        $this->data['step'] = 1;
        $this->data['title'] = 'ทีม';
        $this->data['content'] = $this->content;
        $this->data['uleague'] = $uleague;
        $this->data['has_join_camp'] = false;
        $this->data['has_notification'] = self::hasInvitation();

        if ($this->data['has_notification']) {
            $this->data['invitation'] = $this->data['has_notification'];
        }

        //$camp_events = self::getEvents('Camp', \Auth::user()->email);
        $camp_events = self::getMyOrders(\Auth::user()->email);

        //ALL TEAM MEMBERS MUST CHECK IN AT LEAST ONE CAMP
        foreach ($camp_events as $order) {
            $item = $order->event;
            if ($item->type == 'Camp' || $item->type == 'UCamp') {
                if ($item->is_checkin) {
                    $this->data['has_join_camp'] = true;
                }
            }
        }

        if ($this->data['has_join_camp']) {

            $this->data['team_member'] = self::checkULeagueTeamMember();
            $this->data['team_leader'] = self::checkULeagueTeamLeader();

            $this->data['team_member_num'] = false;
            $this->data['team_is_eligible'] = false;
            $this->data['team_is_apply'] = false;
            $this->data['team_status'] = 0;

            $has_apply = uleagueapply::where('user_id', \Auth::user()->id)->get()->first();

            if ($has_apply) {
                $this->data['team_is_apply'] = true;
            }

            if (!$uleague) {
                $this->data['sub_content'] = 'pages.frontend.user.create_team.step_0';
                $this->data['step'] = 0;
            } else {

                $team_members = array();

                if ($this->data['team_leader']) {

                    $team_members = uleagueteammembers::getTeamMembers($my_team->team_id);
                    $waiting_list = uleagueteaminvite::where('team_id', $my_team->team_id)->where('status', 0)->get();

                    $this->data['team'] = $my_team;
                    $this->data['waiting_list'] = $waiting_list;
                    $this->data['sub_content'] = 'pages.frontend.user.team-as-leader';
                    $this->data['step'] = 1;


                } elseif ($this->data['team_member']) {

                    $this->data['team'] = uleagueteammembers::getTeamDetailByUserId(Auth::user()->id);
                    $t = uleagueteam::where('team_id', $this->data['team']->team_id)->get()->first();

                    if ($t) {
                        $this->data['team_status'] = uleagueapply::where('user_id', $t->user_id)->where('apply_for', 'pitching')->join('uleague_events', 'uleague_apply.reference_id', 'uleague_events.event_id')->get()->first();
                    }

                    $team_members = uleagueteammembers::getTeamMembers($this->data['team']->team_id);

                    $this->data['sub_content'] = 'pages.frontend.user.team-as-member';
                    $this->data['step'] = 1;
                }

                $count_member = 0;
                $pass_member = 0;

                $num_faculty = [];
                $num_subfaculty = [];

                foreach ($team_members as $item => $field) {

                    if (!in_array($team_members[$item]['faculty_id'], $num_faculty)) {
                        $num_faculty[] = $team_members[$item]['faculty_id'];
                    }

                    if (!in_array($team_members[$item]['sub_faculty'], $num_subfaculty)) {
                        $num_subfaculty[] = $team_members[$item]['sub_faculty'];
                    }
                    // TODO :: check from zip event
                    $z = self::getMyOrders($team_members[$item]['email']);

                    $team_members[$item]['pass_camp'] = false;
                    foreach ($z as $o) {
                        $i = $o->event;
                        if ($i->is_checkin) {
                            $team_members[$item]['pass_camp'] = true;
                            $pass_member++;
                            break;
                        }
                    }
                    $count_member++;
                }

                //echo $pass_member.' '.$count_member;  //DEDUG

                if ($count_member >= 3 && $count_member <= 5) {
                    $this->data['team_member_num'] = true;
                    if ($pass_member == $count_member) {
                        if (count($num_faculty) >= 2 && count($num_subfaculty) >= 3) {
                            $this->data['team_is_eligible'] = true;
                        }
                    }
                }
                $this->data['team_members'] = $team_members;
            }

            //DEBUG
            //$this->data['team_is_eligible'] = true;

            $this->data['pitching_events'] = array();
            //if($this->data['team_is_eligible'] && isset($request->step)){

            if ($this->data['team_is_eligible']) {
                $this->data['step'] = 2;

                $pitchings = array();
                $pitching_events = self::getEvents('Pitching', \Auth::user()->email);

                foreach ($pitching_events as $item) {

                    $e = array();

                    $e['id'] = $item->id;
                    $e['name'] = $item->name;
                    $e['venue'] = $item->venue;
                    $e['type'] = $item->type;
                    $e['start_date'] = self::dateThai(date('Y-m-d H:i:s', $item->start_date));
                    $e['end_date'] = self::dateThai(date('Y-m-d H:i:s', $item->end_date));
                    $e['registration'] = $item->registration;
                    $e['remain'] = $item->registration - $item->total_registration;

                    $e['cover'] = asset('/frontend/medilab/img/event-banner.jpg');
                    $e['logo'] = asset('/frontend/medilab/img/event-banner.jpg');

                    if (isset($item->cover)) {
                        $e['cover'] = $item->cover;
                    }
                    if (isset($item->logo)) {
                        $e['logo'] = $item->logo;
                    }

                    $e['url'] = $item->url;
                    $e['is_register'] = $item->is_register;
                    $e['is_checkin'] = $item->is_checkin;

                    $pitchings[] = $e;

                }

                $this->data['pitching_events'] = $pitchings;

            } else {
                $this->data['step'] = 1;
            }

            if (isset($request->step)) {

                $s = $request->step - 1;

                if ($s <= $this->data['step']) {
                    if ($s <= 0) {
                        $s = 1;
                    }
                    $this->data['step'] = $s;
                }
            }

            //LOCK IF TEAM ALREADY APPLY
            if ($this->data['team_is_apply']) {
                $this->data['step'] = 3;

                $this->data['has_reject'] = false;
                $this->data['reject_reason'] = '';

                if ($my_team->status == -99) {
                    $this->data['has_reject'] = true;
                    $this->data['reject'] = array('reason' => 'เอกสารไม่ถูกต้อง/ครบถ้วน', 'code' => -99);
                }

                if ($my_team->status == -98) {
                    $this->data['has_reject'] = true;
                    $this->data['reject'] = array('reason' => 'สมาชิกภายในทีมขาดคุณสมบัติ', 'code' => -98);
                }

                if ($has_apply->status == 2) {
                    $this->data['step'] = 4;
                }
            }
        } else {
            $this->data['sub_content'] = 'pages.frontend.user.create_team.step_0';
        }

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);

    }

    public function getApplyPitching(){

        if (!\Auth::check()) return redirect('user/login');

        $this->data['title'] = 'U League 2018 - Pitching';
        $this->data['content'] = $this->content;
        $this->data['sub_content'] = 'pages.frontend.user.apply-pitching';
        $this->data['has_join_camp'] = false;
        $this->data['uleague'] = self::checkULeagueUser();
        $this->data['team_member'] = self::checkULeagueTeamMember();
        $this->data['team_leader'] = self::checkULeagueTeamLeader();
        $this->data['has_notification'] = self::hasInvitation();
        if ($this->data['has_notification']) {
            $this->data['invitation'] = $this->data['has_notification'];
        }

        $camp_events = self::getMyOrders(\Auth::user()->email);


        //ALL TEAM MEMBERS MUST CHECK IN AT LEAST ONE CAMP
        foreach($camp_events as $order) {
            $item = $order->event;
            if ($item->type == 'Camp' || $item->type == 'UCamp') {
                if ($item->is_checkin==true) {
                    $this->data['has_join_camp'] = true;
                }
            }
        }

        //$this->data['has_join_camp'] = uleagueapply::hasJoinCampEvent(Auth::user()->id);

        $page_template = $this->template['frontend'];

        if ($this->data['team_leader'] || $this->data['team_member']) {
            return redirect('user/uleague/team');
        }

        return view($page_template, $this->data);

    }

    public function acceptInvite(Request $request){

        if (!\Auth::check()) return redirect('user/login')->with(['message' => 'กรุณาเข้าสู่ระบบก่อน', 'status' => 'error']);

        $email = Auth::user()->email;

        $this->data['title'] = 'ตอบรับคำเชิญเข้าร่วมทีม';
        $this->data['content'] = $this->content;

        $code = explode('$', $request->invite_code);

        $this->data['team_id'] = $code[0];
        $this->data['invite_code'] = $code[1];

        $this->data['sub_content'] = 'pages.frontend.user.accept-invite';
        $this->data['uleague'] = self::checkULeagueUser();
        $this->data['team_member'] = self::checkULeagueTeamMember();
        $this->data['team_leader'] = self::checkULeagueTeamLeader();
        $this->data['has_notification'] = self::hasInvitation();
        if ($this->data['has_notification']) {
            $this->data['invitation'] = $this->data['has_notification'];
        }

        $invite = uleagueteaminvite::where('invitation_code', $this->data['invite_code'])
            ->where('team_id', $this->data['team_id'])
            ->where('status', 0)
            ->get()->first();

        if ($invite) {
            //CHECK IF EMAIL MISSMATCH
            if ($invite->invite_to != $email) {
                return redirect('/user/profile')->with(['message' => 'Code คำเชิญไม่ถูกต้อง', 'status' => 'error']);
            }
        }

        //CHECK IS VALID INVITATION CODE
        if (!$invite) {
            return redirect('/user/profile')->with(['message' => 'Code คำเชิญหมดอายุหรือถูกใช้ไปแล้ว', 'status' => 'error']);
        }

        $team = uleagueteam::where('team_id', $this->data['team_id'])->get()->first();

        $this->data['team'] = uleagueteam::getTeamDetailByID($team->user_id);

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);

    }

    private function getEvents($type, $email){

        $curl = new \anlutro\cURL\cURL;

        $rq = $curl->newRequest('get', config('app.zipevent_url') . '/api/nia/event/uleague?type=' . $type . '&email=' . $email)
            ->setHeader('Content-Type', 'application/json')
            ->setHeader('Host', config('app.zipevent_host'))
            ->setHeader('client_id', config('app.zipevent_client_id'))
            ->setHeader('client_secret', config('app.zipevent_client_secret'))
            ->setHeader('device_id', '1')
            ->setHeader('Content-Length', '0');

        $ev = $rq->send();

        return json_decode($ev->body);
    }


    public function getCertificates(){

        if (!\Auth::check()) return redirect('user/login');

        $this->data['event_certs'] = array();
        $this->data['course_certs'] = array();

        $user_info = User::where('tb_users.id', \Auth::user()->id)
            ->leftJoin('user_nia', 'tb_users.id', 'user_nia.user_id')
            ->leftJoin('thailand_provinces', 'thailand_provinces.code', 'user_nia.province_id')
            ->leftJoin('faculty', 'faculty.faculty_id', 'user_nia.faculty_id')
            ->first();


        $course_cert = usercertificates::select('*', 'user_certificates.created_at')
            ->where('user_certificates.user_id', Auth::user()->id)
            ->join('courses', 'courses.course_id', 'user_certificates.object_id')
            ->join('certificates', 'certificates.id', 'user_certificates.certification_id')
            ->get();

        foreach ($course_cert as $cert) {
            $cert->created_cert = self::dateThai(date('Y-m-d H:i:s', strtotime($cert->created_at)));
            $this->data['course_certs'][] = $cert;
        }

        $my_orders = self::getMyOrders(\Auth::user()->email);

        foreach ($my_orders as $order) {

            $item = $order->event;

            $e = array();

            $e['id'] = $item->id;
            $e['name'] = $item->name;
            $e['venue'] = $item->venue;
            $e['type'] = $item->type;
            $e['start_date'] = self::dateThai(date('Y-m-d H:i:s', $item->start_date));
            $e['end_date'] = self::dateThai(date('Y-m-d H:i:s', $item->end_date));
            $e['registration'] = $item->registration;
            $e['remain'] = $item->registration - $item->total_registration;
            $e['cover'] = asset('/frontend/medilab/img/event-banner.jpg');
            if (isset($item->cover)) {
                $e['cover'] = $item->cover;
            }

            $e['url'] = $item->url;
            $e['is_register'] = $item->is_register;
            $e['is_checkin'] = $item->is_checkin;
            $e['type_text'] = $item->type;

            if ($item->type == 'Camp') {
                $e['type_text'] = 'Camp  กลาง';
            } else if ($item->type == 'UCamp') {
                $e['type_text'] = 'Camp  ของมหาวิทยาลัย';
            }

            if($e['is_register'] && $e['is_checkin']){
                $this->data['event_certs'][] = $e;
            }

        }

        $this->data['title'] = $user_info->first_name . ' ' . $user_info->last_name;
        $this->data['content'] = $this->content;
        $this->data['sub_content'] = 'pages.frontend.user.certificates';
        $this->data['user'] = $course_cert;
        //$this->data['certs'] = $course_cert;
        $this->data['uleague'] = self::checkULeagueUser();
        $this->data['team_member'] = self::checkULeagueTeamMember();
        $this->data['team_leader'] = self::checkULeagueTeamLeader();
        $this->data['has_notification'] = self::hasInvitation();
        if ($this->data['has_notification']) {
            $this->data['invitation'] = $this->data['has_notification'];
        }
        $this->data['show_uleague_btn'] = true;

        //if(\Auth::user()->group_id > 2) return view('user.profile', $this->data);

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);

    }

    function getCertificatesPreview(Request $request){


        $cert_id = $request->input('cert_id');


        if($cert_id=='camp'){

            $this->data['my_certs'] = array();

            $my_orders = self::getMyOrders(\Auth::user()->email);

            foreach ($my_orders as $order) {

                $item = $order->event;

                $e = array();

                $e['id'] = $item->id;
                $e['name'] = $item->name;
                $e['venue'] = $item->venue;
                $e['type'] = $item->type;
                $e['start_date'] = self::dateThai(date('Y-m-d H:i:s', $item->start_date));
                $e['end_date'] = self::dateThai(date('Y-m-d H:i:s', $item->end_date));
                $e['registration'] = $item->registration;
                $e['remain'] = $item->registration - $item->total_registration;
                $e['cover'] = asset('/frontend/medilab/img/event-banner.jpg');
                if (isset($item->cover)) {
                    $e['cover'] = $item->cover;
                }

                $e['url'] = $item->url;
                $e['is_register'] = $item->is_register;
                $e['is_checkin'] = $item->is_checkin;
                $e['type_text'] = $item->type;

                if ($item->type == 'Camp') {
                    $e['type_text'] = 'Camp  กลาง';
                } else if ($item->type == 'UCamp') {
                    $e['type_text'] = 'Camp  ของมหาวิทยาลัย';
                }

                if($e['is_register'] && $e['is_checkin']){
                    $this->data['my_certs'][] = $e;
                }

            }

            if(count($this->data['my_certs'])%2==0 && count($this->data['my_certs'])<6){
                $this->data['col'] = 6;
            }
            else{
                $this->data['col'] = 4;
            }

            $this->data['user'] = User::join('user_nia', 'user_nia.user_id', 'tb_users.id')->find(Auth::user()->id);

            return view('pages.backend.certificate.preview-event', $this->data);

        }
        else{

            $user_cert = usercertificates::select('*', 'user_certificates.created_at')
                ->where('user_certificates.user_id', Auth::user()->id)
                ->where('user_certificates.certification_id', $cert_id)
                ->join('courses', 'courses.course_id', 'user_certificates.object_id')
                ->join('certificates', 'certificates.id', 'user_certificates.certification_id')
                ->first();
            $user_info = User::where('tb_users.id', \Auth::user()->id)
                ->leftJoin('user_nia', 'tb_users.id', 'user_nia.user_id')
                ->leftJoin('thailand_provinces', 'thailand_provinces.code', 'user_nia.province_id')
                ->leftJoin('faculty', 'faculty.faculty_id', 'user_nia.faculty_id')
                ->first();
            $created_cert = self::dateThai(date('Y-m-d H:i:s', strtotime($user_cert->created_at)));

            $elements = json_decode($user_cert->cert_elements);
            foreach ($elements as $element) {
                $element->value = str_replace("%pre_name%", $user_info->name_title, $element->value);
                $element->value = str_replace("%first_name%", $user_info->first_name, $element->value);
                $element->value = str_replace("%last_name%", $user_info->last_name, $element->value);
                $element->value = str_replace("%date%", $created_cert, $element->value);
                $element->value = str_replace("%course_title%", $user_cert->course_title, $element->value);
            }
            $this->data['elements'] = $elements;

            return view('pages.backend.certificate.preview', $this->data);

        }
    }

    function getCertificatesPreviewAll(Request $request){

        $this->data['certs'] = array();

        $my_orders = self::getMyOrders(\Auth::user()->email);


        foreach ($my_orders as $order) {

            $item = $order->event;

            $e = array();

            $e['item_type'] = 'event';
            $e['name'] = $item->name;
            $e['type'] = $item->type;
            $e['start_date'] = self::dateThai(date('Y-m-d H:i:s', $item->start_date));
            $e['end_date'] = self::dateThai(date('Y-m-d H:i:s', $item->end_date));

            if($item->is_register && $item->is_checkin){
                $this->data['certs'][] = $e;
            }
        }

        $this->data['user'] = User::join('user_nia', 'user_nia.user_id', 'tb_users.id')->find(Auth::user()->id);

        $course_certs = usercertificates::select('*', 'user_certificates.created_at')
            ->where('user_certificates.user_id', Auth::user()->id)
            ->join('courses', 'courses.course_id', 'user_certificates.object_id')
            ->orderBy('courses.course_title')
            ->get();

        foreach ($course_certs as $course) {
            $e = array();

            $e['item_type'] = 'course';
            $e['name'] = $course->course_title;
            $timestamp = strtotime($course->created_at);
            $e['end_date'] = self::dateThai(date('Y-m-d H:i:s', $timestamp));

            if($course->percentage_lecture!=0 || $course->percentage_quiz!=0){
                $this->data['certs'][] = $e;   
            }
        }


        $user_info = User::where('tb_users.id', \Auth::user()->id)
            ->leftJoin('user_nia', 'tb_users.id', 'user_nia.user_id')
            ->leftJoin('thailand_provinces', 'thailand_provinces.code', 'user_nia.province_id')
            ->leftJoin('faculty', 'faculty.faculty_id', 'user_nia.faculty_id')
            ->first();

        $this->data['user_info'] = $user_info;

        return view('pages.frontend.certificate.preview', $this->data);

    }


    function getEventCertificatesPreview(Request $request){

        $event_id = $request->input('event_id');
        //check
        $my_orders = self::getMyOrders(\Auth::user()->email);

        $is_pass = false;

        foreach ($my_orders as $order) {

            $item = $order->event;
            if($item->is_register && $item->is_checkin){
                $is_pass = true;
            }
        }

        if($is_pass){

            $user_cert = eventcertificates::where('event_certificates.event_id', $event_id)
                ->join('certificates', 'certificates.id', 'event_certificates.certificates_id')->first();

            $user_info = User::where('tb_users.id', \Auth::user()->id)
                ->leftJoin('user_nia', 'tb_users.id', 'user_nia.user_id')
                ->leftJoin('thailand_provinces', 'thailand_provinces.code', 'user_nia.province_id')
                ->leftJoin('faculty', 'faculty.faculty_id', 'user_nia.faculty_id')
                ->first();


            $created_cert = self::dateThai(date('Y-m-d H:i:s', strtotime($user_cert->created_at)));

            if($user_cert){

                $elements = json_decode($user_cert->cert_elements);

                foreach ($elements as $element) {
                    $element->value = str_replace("%pre_name%", $user_info->name_title, $element->value);
                    $element->value = str_replace("%first_name%", $user_info->first_name, $element->value);
                    $element->value = str_replace("%last_name%", $user_info->last_name, $element->value);
                    $element->value = str_replace("%date%", $created_cert, $element->value);
                    $element->value = str_replace("%course_title%", $user_cert->course_title, $element->value);
                }
                $this->data['elements'] = $elements;

                return view('pages.backend.certificate.preview', $this->data);
            }
        }

        /*
        $user_cert = usercertificates::select('*', 'user_certificates.created_at')
            ->where('user_certificates.user_id', Auth::user()->id)
            ->where('user_certificates.certification_id', $cert_id)
            ->join('courses', 'courses.course_id', 'user_certificates.object_id')
            ->join('certificates', 'certificates.id', 'user_certificates.certification_id')
            ->first();

        $user_info = User::where('tb_users.id', \Auth::user()->id)
            ->leftJoin('user_nia', 'tb_users.id', 'user_nia.user_id')
            ->leftJoin('thailand_provinces', 'thailand_provinces.code', 'user_nia.province_id')
            ->leftJoin('faculty', 'faculty.faculty_id', 'user_nia.faculty_id')
            ->first();
        $created_cert = self::dateThai(date('Y-m-d H:i:s', strtotime($user_cert->created_at)));

        $elements = json_decode($user_cert->cert_elements);
        foreach ($elements as $element) {
            $element->value = str_replace("%pre_name%", $user_info->name_title, $element->value);
            $element->value = str_replace("%first_name%", $user_info->first_name, $element->value);
            $element->value = str_replace("%last_name%", $user_info->last_name, $element->value);
            $element->value = str_replace("%date%", $created_cert, $element->value);
            $element->value = str_replace("%course_title%", $user_cert->course_title, $element->value);
        }
        $this->data['elements'] = $elements;

        return view('pages.backend.certificate.preview', $this->data);
        */
    }


    private function getMyOrders($email){

        $curl = new \anlutro\cURL\cURL;

        $rq = $curl->newRequest('get', config('app.zipevent_url') . '/api/nia/ticket/list?email=' . $email)
            ->setHeader('Content-Type', 'application/json')
            ->setHeader('Host', config('app.zipevent_host'))
            ->setHeader('client_id', config('app.zipevent_client_id'))
            ->setHeader('client_secret', config('app.zipevent_client_secret'))
            ->setHeader('device_id', '1')
            ->setHeader('Content-Length', '0');

        $ev = $rq->send();

        return json_decode($ev->body);
    }

    private function hasInvitation(){
        $has_notification = false;

        if (Auth::check()) {

            $user_id = Auth::user()->id;
            $email = Auth::user()->email;

            $invites = uleagueteaminvite::where('invite_to', $email)->where('status', 0)->get()->first();

            if ($invites) {
                $has_notification = $invites;
            }
        }

        return $has_notification;

    }

    private function checkULeagueTeamMember(){

        $team_member = uleagueteammembers::getTeamDetailByUserId(\Auth::user()->id);

        return $team_member;
    }

    private function checkULeagueTeamLeader(){

        $team_leader = Uleagueteam::where('user_id', \Auth::user()->id)->get()->first();

        return $team_leader;
    }

    private function checkULeagueUser(){

        if (Auth::check()) {
            $uleague = uleagueuser::join('tb_users', 'tb_users.id', 'uleague_user.user_id')
                ->join('university', 'university.university_id', 'uleague_user.university_id')
                ->join('faculty', 'faculty.faculty_id', 'uleague_user.faculty_id')
                ->where('user_id', \Auth::user()->id)->first();

            return $uleague;
        } else {
            return false;
        }
    }

    private function dateThai($strDate){

        $strYear = date("Y", strtotime($strDate)) + 543;
        $strMonth = date("n", strtotime($strDate));
        $strDay = date("j", strtotime($strDate));
        $strHour = date("H", strtotime($strDate));
        $strMinute = date("i", strtotime($strDate));
        $strSeconds = date("s", strtotime($strDate));

        $strMonthCut = Array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");

        $strMonthThai = $strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }

    private function statusEvent($status){
        $status_text[0] = 'ไม่เข้าร่วม';
        $status_text[1] = 'สมัครสำเร็จ';
        $status_text[2] = 'เข้าร่วมแล้ว';

        return $status_text[$status];
    }
}