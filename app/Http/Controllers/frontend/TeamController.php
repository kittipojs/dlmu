<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Pitching;
use App\Models\Pitchinground;
use App\Models\Uleagueuser;
use App\Models\Uleagueteam;
use App\Models\Uleagueteammembers;
use App\Models\Uleagueteaminvite;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class TeamController extends Controller
{
  public function __construct(){

    parent::__construct();

    $this->data = array(
      'pageTitle' => 'Pitching',
      'pageNote' => 'Register',
    );

    $this->template = array(
      'frontend' => 'layouts.medilab.index',
      'backend'  => 'layouts.default.template.backend'
    );


    $this->zipevent = array('HOST'=>'test.zipeventapp.com', 'URL'=>'https://test.zipeventapp.com', 'CLIENT_ID'=>'VqCzcz6q7IHa09jb', 'CLIENT_SECRET'=>'aefc7402-e5de-4cdd-b77e-e698b3cfe358');

  }

  function TeamPage(Request $request){


    if(!Auth::check()){
      return redirect('/user/login')->with(['message' => 'กรุณาเข้าสู่ระบบก่อน', 'status'=>'error']);
    }

    $team_id = $request->team_id;
    $user_id = Auth::user()->id;

    $member = pitchingteammember::where('user_id', $user_id)->get()->first();

    if(!$member){
      return redirect('/uleague/pitching')->with(['message' => 'คุณไม่มีสิทธิดูข้อมูลของทีมนี้', 'status'=>'error']);
    }

    $team = pitchingteam::where('team_id', $team_id)->get()->first();

    if(!$team){
      return redirect('')->with(['message' => 'ไม่พบทีมที่ต้องการ', 'status'=>'error']);
    }

    $this->data['team'] = $team;

    $this->data['title'] = 'ทีม - Startup Thailand League 2018'; 
    $this->data['content'] = 'pages.frontend.team'; 

    $page_template = $this->template['frontend'];

    return view($page_template, $this->data);

  }

  function createTeam(Request $request){

    if(!Auth::check()){
      return redirect('/user/login')->with(['message' => 'กรุณาเข้าสู่ระบบก่อน', 'status'=>'error']);
    }


    $team_id = time().''.rand(1000, 9999);

    $input = Input::all();
    $user_id = $input['user_id'];
    $team_name = $input['team_name'];
    $team_info = $input['team_info'];
    $university_id = $input['university'];
    $startup_sector = $input['startup_sector'];

    // Get the UploadedFile object
    if($request->hasFile('file')){
      $files = $request->file('file');

      $fileType = $files->getMimeType();

      if($fileType !== 'application/pdf' && $fileType !== 'image/jpeg' && $fileType !== 'image/png'){
        return back()->with('message', 'อัพโหลดไฟล์ผิดประเภท')->with('status', 'error');
      }

      if($fileType == 'application/pdf'){
        $ext = '.pdf';
      }
      else if($fileType !== 'image/jpeg'){
        $ext = '.jpg';
      }
      else if($fileType !== 'image/png'){
        $ext = '.png';
      }

      //Path for save and display image
      $urlPath = '/uploads/uleague/pitch_deck/';
      $filename = $urlPath.$team_id.'_'.\DataHelpers::fileName($team_name).''.$ext;

      //Path for save image
      $destinationPath = public_path(). '/uploads/uleague/pitch_deck/';

      //Store image to /public/uploads/files/pitching
      $files->move($destinationPath, $filename);

      $url = $urlPath.$team_id.'_'.\DataHelpers::fileName($team_name).''.$ext;

    } else {
      return back()->with('message', 'โปรดเลือกไฟล์สำหรับอัพโหลด')->with('status', 'error');
    }

    $team = new uleagueteam();

    $team->team_id = $team_id;
    $team->user_id = $user_id;
    $team->team_name = $team_name;
    $team->team_info = $team_info;
    $team->file_url  = $url;
    $team->university_id  = $university_id;
    $team->startup_sector = $startup_sector;
    $team->save();

    $member = new uleagueteammembers();
    $member->user_id = $user_id;
    $member->team_id = $team_id;
    $member->save();


    return redirect('/user/uleague/team')->with(['message' => 'สร้างทีม '.$team_name.' สำเร็จ', 'status'=>'success']);
  }

  function updateTeam(Request $request){

    if (!\Auth::check()) return redirect('user/login');

    $input = Input::all();

    $team_id = $input['team_id'];

    $team = uleagueteam::where('team_id', $team_id)->where('user_id', \Auth::user()->id)->first();

    // Get the UploadedFile object
    if($request->hasFile('file')){
      $files = $request->file('file');

      $fileType = $files->getMimeType();

      if($fileType !== 'application/pdf' && $fileType !== 'image/jpeg' && $fileType !== 'image/png'){
        return back()->with('message', 'อัพโหลดไฟล์ผิดประเภท')->with('status', 'error');
      }

      if($fileType == 'application/pdf'){
        $ext = '.pdf';
      }
      else if($fileType !== 'image/jpeg'){
        $ext = '.jpg';
      }
      else if($fileType !== 'image/png'){
        $ext = '.png';
      }

      //Path for save and display image
      $urlPath = '/uploads/uleague/pitch_deck/';
      $filename = $urlPath.$team_id.'_'.$team->team_name.''.$ext;

      //Path for save image
      $destinationPath = public_path(). '/uploads/uleague/pitch_deck/';

      //Store image to /public/uploads/files/pitching
      $files->move($destinationPath, $filename);

      $url = $urlPath.$team_id.'_'.$team->team_name.''.$ext;

    } else {
      return back()->with('message', 'โปรดเลือกไฟล์สำหรับอัพโหลด')->with('status', 'error');
    }


    $team->team_name = $request->input('team_name');
    $team->team_info = $request->input('team_info');
    $team->file_url = $url;
    $team->startup_sector = $request->input('startup_sector');
    $team->university_id = $request->input('university');
    $team->save();

    return redirect('user/uleague/team')->with('message', 'อัพเดททีมสำเร็จ')->with('status', 'success');

  }

  function inviteFriends(Request $request){

    if (!\Auth::check()) return response()->json(['success' => false, 'message'=>'Not Authorized'], 503);

    $data = array();

    $input = $request->all();

    $invite_to = $input['invite_to'];
    $team_id = $input['team_id'];

    $u = user::where('email', $invite_to)->get()->first();

    if($u){

      $uleague_user = uleagueuser::where('user_id', $u->id)->get()->first();

      if(!$uleague_user){
        return response()->json(['success' => false, 'message'=>'อีเมลนี้ยังไม่ได้สมัคร U League 2018'], 200);
      }

      $join_camp = false;
      $orders = self::getMyOrders($invite_to);
      foreach ($orders as $item) {

        $event = $item->event;

        if($event->is_checkin){
          $join_camp = true;
        }
      }

      if(!$join_camp){
        return response()->json(['success' => false, 'message'=>'อีเมลนี้ยังไม่เคยเข้าร่วม U League Camp มาก่อน'], 200);
      }

      $is_member = uleagueteammembers::where('user_id', $u->id)->get()->first();

      if($is_member){
        return response()->json(['success' => false, 'message'=>'สมาชิกนี้เข้าร่วมทีมอื่นไปแล้ว'], 200);
      }

    }
    else{
      return response()->json(['success' => false, 'message'=>'ไม่พบอีเมลนี้ในระบบ'], 200);
    }


    $inv = uleagueteaminvite::where('invite_to', $invite_to)->where('status', '>=', '0')->get();

    if( count($inv)<1 ){

      $invite = new uleagueteaminvite();
      $invite->team_id = $team_id;
      $invite->invite_to = $invite_to;
      $invite->invitation_code = rand('100000', '999999');
      $invite->status = 0;

      $invite->save();

      //self::sendInviteEmail($invite->invite_to, $invite->invitation_code, $team_id);
    }
    else{
      return response()->json(['success' => false, 'message'=>'คุณเคยส่งคำเชิญไปยังอีเมลนี้แล้ว'], 200);
    }

    return response()->json(['success' => true, 'data'=>$data], 200);
  }

  function removeInvite(Request $request){

    if (!\Auth::check()) return response()->json(['success' => false, 'message'=>'Not Authorized'], 503);

    $data = array();

    $input = $request->all();

    $email = $input['email'];
    $team_id = $input['team_id'];

    $affectedRows = Uleagueteaminvite::where('team_id', $team_id)->where('invite_to', $email)->update( array('status' => -99) );

    return response()->json(['success' => true, 'data'=>$data], 200);

  }

  function replyInvite(Request $request){ 

    $data = array();

    if(!Auth::check()){
      return response()->json(['success' => false, 'message' => 'เกิดข้อผิดพลาด! Code คำเชิญไม่ถูกต้อง'], 200);
    }

    $input = $request->all(); 

    $email = Auth::user()->email; 
    $user_id = Auth::user()->id; 

    $invitation_code = $input['invitation_code']; 
    $status = $input['status']; 
    $team_id = $input['team_id'];

    $invite = uleagueteaminvite::where('invitation_code', $invitation_code)->where('uleague_team_invite.team_id', $team_id)->join('uleague_team', 'uleague_team.team_id', 'uleague_team_invite.team_id')->join('tb_users', 'tb_users.id', 'uleague_team.user_id')->get()->first();

    $uleague = uleagueuser::where('user_id', $user_id)->get()->first();
    $is_member = uleagueteammembers::where('user_id', $user_id)->get()->first();

    //CHECK IF EMAIL MISSMATCH
    if($invite->invite_to!=$email){
      return response()->json(['success' => false, 'message' => 'เกิดข้อผิดพลาด! Code คำเชิญไม่ถูกต้อง'], 200);
    }

    //CHECK IF USER ALREADY REGISTER U LEAGUE 2018
    if(!$uleague && $status==1){
      return response()->json(['success' => false, 'message' => 'คุณยังไม่ได้ลงทะเบียน U League 2018'], 200);
    }

    //CHECK IF USER ALREADY JON ANOTHER TEAM
    if($is_member && $status==1){
      return response()->json(['success' => false, 'message' => 'คุณเคยเข้าร่วมทีมอื่นแล้ว'], 200);
    }

    //update invitation status
    $p = uleagueteaminvite::where('invitation_code', $invitation_code)->where('team_id', $team_id); 
    $p->update(['status' => $status]);

    //add this user to team member
    if($status==1){ 

      $data['message'] = 'คุณตอบรับคำเชิญแล้ว';
      $data['redirect'] = '/user/uleague/team';

      $member = new uleagueteammembers(); 
      $member->user_id = $user_id; 
      $member->team_id = $team_id; 
      $member->save();

      /*
            //confirm email for apply invite
            $to = $invite->email;
            $subject = "ตอบรับการเข้าร่วมทีม U League Pitching 2018";
            $data['subject'] = $subject;
            $data['email'] = $to;

            if (config('sximo.cnf_mail') == 'swift') {

                \Mail::send('emails.team-confirm-apply', $data, function ($message) use ($data) {
                    $message->to($data['email'])->subject($data['subject']);
                });

            } else {

                $message = view('emails.team-confirm-apply', $data);
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: ' . config('sximo.cnf_appname') . ' <' . config('sximo.cnf_email') . '>' . "\r\n";
                mail($to, $subject, $message, $headers);
            }
            */
    }else{

      $data['message'] = 'คุณได้ปฏิเสธคำเชิญแล้ว';
      $data['redirect'] = '/user/profile';

      /*
            //confirm email for reject invite
            $to = $invite->email;
            $subject = "ปฎิเสธการเข้าร่วมทีม U League Pitching 2018";
            $data['subject'] = $subject;
            $data['email'] = $to;

            if (config('sximo.cnf_mail') == 'swift') {

                \Mail::send('emails.team-confirm-reject', $data, function ($message) use ($data) {
                    $message->to($data['email'])->subject($data['subject']);
                });

            } else {

                $message = view('emails.team-confirm-reject', $data);
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: ' . config('sximo.cnf_appname') . ' <' . config('sximo.cnf_email') . '>' . "\r\n";
                mail($to, $subject, $message, $headers);
            }
            */
    }

    return response()->json(['success' => true, 'data' => $data, 'd'=>$invite], 200); 
  }

  private function getEvents($type, $email){
    $curl = new \anlutro\cURL\cURL;

    $rq = $curl->newRequest('get', config('app.zipevent_url').'/api/nia/event/uleague?type='.$type.'&email='.$email)
      ->setHeader('Content-Type', 'application/json')
      ->setHeader('Host', config('app.zipevent_host'))
      ->setHeader('client_id', config('app.zipevent_client_id'))
      ->setHeader('client_secret', config('app.zipevent_client_secret'))
      ->setHeader('device_id', '1')
      ->setHeader('Content-Length', '0');

    $ev = $rq->send();

    return json_decode($ev->body);
  }

  private function getMyOrders($email){

    $curl = new \anlutro\cURL\cURL;

    $rq = $curl->newRequest('get', config('app.zipevent_url').'/api/nia/ticket/list?email='.$email)
      ->setHeader('Content-Type', 'application/json')
      ->setHeader('Host', config('app.zipevent_host'))
      ->setHeader('client_id', config('app.zipevent_client_id'))
      ->setHeader('client_secret', config('app.zipevent_client_secret'))
      ->setHeader('device_id', '1')
      ->setHeader('Content-Length', '0');

    $ev = $rq->send();

    return json_decode($ev->body);
  }

  private function sendInviteEmail($to, $invitation_code, $team_id){

    $data['subject'] = "ตอบกลับ: คำเชิญเข้าร่วมทีม U - Leauge Pitching 2018";

    $data['email'] = $to;
    $data['invitation_code'] = $invitation_code;
    $data['invitation_url'] = url('/').'/user/uleague/team/accept/'.$team_id.'$'.$invitation_code;

    if (config('sximo.cnf_mail') == 'swift') {

      \Mail::send('emails.pitching_invitation', $data, function ($message) use ($data) {
        $message->to($data['email'])->subject($data['subject']);
      });

      $data['sent_to'][] = $data['email'];

    } else {

      $message = view('emails.pitching_invitation', $data);
      $headers = 'MIME-Version: 1.0' . "\r\n";
      $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
      $headers .= 'From: ' . config('sximo.cnf_appname') . ' <' . config('sximo.cnf_email') . '>' . "\r\n";
      mail($data['email'], $data['subject'], $message, $headers);
    }
  }
}