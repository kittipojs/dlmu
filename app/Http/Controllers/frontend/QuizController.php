<?php

namespace App\Http\Controllers\frontend;

use App\Models\Coursesession;
use App\Models\Quiz;
use App\Models\Quizequestions;
use App\Models\Quizscore;
use App\Models\Uleagueteam;
use App\Models\Uleagueteaminvite;
use App\Models\Uleagueteammembers;
use App\Models\Uleagueuser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class QuizController extends Controller
{
    protected $data = array();
    protected $template = array();

    public function __construct()
    {

        parent::__construct();

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend' => 'layouts.default.template.backend'
        );
    }

    function question($quiz_id){

        if (!Auth::check()) {
            return redirect('/user/login')->with(['message' => 'กรุณาสมัครสมาชิกก่อน', 'status' => 'error']);
        }

        $quiz = quiz::where('quiz_id', $quiz_id)->first();
        $questions = quizequestions::where('quiz_id', $quiz_id)->get();
        $isFirst = true;
        foreach ($questions as $question) {

            $choices = json_decode($question->question_choices);

            $question->choices = $choices;

            if ($isFirst) {
                $question->active = true;
                $isFirst = false;
            } else {
                $question->active = false;
            }
        }

        $this->data['quiz'] = $quiz;
        $this->data['questions'] = $questions;

        $this->data['content'] = "pages.frontend.quiz.question";
        $this->data['title'] = 'Quiz';
        $this->data['invitation'] = null;

        if (Auth::check()) {
            $this->data['uleague'] = self::checkULeagueUser();
            $this->data['has_notification'] = self::hasInvitation();
            if($this->data['has_notification']){
                $this->data['invitation'] = $this->data['has_notification'];
            }
        }

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);
    }

    function mockupQuiz($quiz_id){

        if (!\Auth::check()) return redirect('user/login');

        $quiz = quiz::where('quiz_id', $quiz_id)->first();
        $quiz->question = quizequestions::where('quiz_id', $quiz_id)->count();
        $this->data['quiz'] = $quiz;

        $this->data['content'] = "pages.frontend.quiz.instruction";
        $this->data['title'] = 'Quiz';
        $this->data['invitation'] = null;

        if(Auth::check()){
            $this->data['uleague'] = self::checkULeagueUser();
            $this->data['has_notification'] = self::hasInvitation();
            if($this->data['has_notification']){
                $this->data['invitation'] = $this->data['has_notification'];
            }
        }

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);
    }

    function save(Request $request){

        if (!Auth::check()) {
            return redirect('/user/login')->with(['message' => 'เกิดข้อผิดพลาด โปรดเข้าสู่ระบบ', 'status' => 'error']);
        }

        $quiz_id = $request->quiz_id;
        $quiz = quiz::where('quiz_id', $quiz_id)->first();
        $answers = $request->choice;
        $course_id = $request->course_id;
        $user_id = Auth::user()->id;
        $curriculum_id = $request->curriculum_id;
        $object_id = $request->object_id;
        $score = 0;

        foreach ($answers as $question_id => $answer){
            $question = quizequestions::where('question_id', $question_id)->first();
            $choices = json_decode($question->question_choices);
            foreach ($choices as $choice) {
                if($choice->name === $answer && $choice->correct === "true"){
                    $score++;
                    continue;
                }
            }
        }

        $percentage = ($score/count($answers))*100;

        if($percentage >= $quiz->quiz_score_to_pass){
            $is_pass = 1;
            $is_pass_status = "ผ่านการทดสอบ";
            $is_pass_class = "success";

            $session = coursesession::where('user_id', $user_id)->where('curriculum_id', $curriculum_id)->first();

            if(!$session){
                $s = new coursesession();
                $s->course_id = $course_id;
                $s->user_id = $user_id;
                $s->curriculum_id = $curriculum_id;
                $s->object_id = $object_id;
                $s->progress = 0;
                $s->is_pass = 0;
                $s->save();
            }
            else{
                $data['progress'] = 100;
                $data['is_pass'] = 1;

                $session = coursesession::where('user_id', $user_id)->where('curriculum_id', $curriculum_id)->update($data);
            }

        }
        else {
            $is_pass = 0;
            $is_pass_status = "ไม่ผ่านการทดสอบ";
            $is_pass_class = "danger";
        }

        $quiz_score = new quizscore();
        $quiz_score->quiz_id    = $quiz_id;
        $quiz_score->user_id    = Auth::user()->id;
        $quiz_score->score      = $score;
        $quiz_score->is_pass    = $is_pass;
        $quiz_score->percentage = $percentage;
        $quiz_score->save();

        return response()->json(
            [
                'success' =>'success',
                'message'=>'บันทึกคำตอบสำเร็จ',
                'score' => $score,
                'percentage' => $percentage,
                'is_past' => $is_pass,
                'result_status' => $is_pass_status,
                'result_class' => $is_pass_class,
            ], 200);
    }

    function savePretest(Request $request){

        if (!Auth::check()) {
            return redirect('/user/login')->with(['message' => 'เกิดข้อผิดพลาด โปรดเข้าสู่ระบบ', 'status' => 'error']);
        }

        $quiz_id = $request->quiz_id;
        $quiz = quiz::where('quiz_id', $quiz_id)->first();
        $answers = $request->choice;
        $course_id = $request->course_id;
        $user_id = Auth::user()->id;
        //$curriculum_id = $request->curriculum_id;
        //$object_id = $request->object_id;
        $score = 0;


        foreach ($answers as $question_id => $answer){
            $question = quizequestions::where('question_id', $question_id)->first();
            $choices = json_decode($question->question_choices);
            foreach ($choices as $choice) {
                if($choice->name === $answer && $choice->correct === "true"){
                    $score++;
                    continue;
                }
            }
        }

        $percentage = ($score/count($answers))*100;
        

        if($percentage >= $quiz->quiz_score_to_pass){
            $is_pass = 1;
            $is_pass_status = "ผ่านการทดสอบ";
            $is_pass_class = "success";
        }
        else {
            $is_pass = 0;
            $is_pass_status = "ไม่ผ่านการทดสอบ";
            $is_pass_class = "danger";
        }

        $quiz_score = quizscore::where('user_id', $user_id)->where('quiz_id', $quiz_id)->first();

        if(is_null($quiz_score)){
            $quiz_score = new quizscore();
            $quiz_score->quiz_id    = $quiz_id;
            $quiz_score->user_id    = Auth::user()->id;
            $quiz_score->score      = $score;
            $quiz_score->is_pass    = $is_pass;
            $quiz_score->percentage = $percentage;
        }
        else{ 
            $quiz_score->score      = $score;
            $quiz_score->is_pass    = $is_pass;
            $quiz_score->percentage = $percentage;
        }

        $quiz_score->save();

        /*
        $quiz_score = new quizscore();
        $quiz_score->quiz_id    = $quiz_id;
        $quiz_score->user_id    = Auth::user()->id;
        $quiz_score->score      = $score;
        $quiz_score->is_pass    = $is_pass;
        $quiz_score->percentage = $percentage;
        $quiz_score->save();
        */

        return response()->json(
            [
                'success' =>'success',
                'message'=>'บันทึกคำตอบสำเร็จ',
                'score' => $score,
                'percentage' => $percentage,
                'is_past' => $is_pass,
                'result_status' => $is_pass_status,
                'result_class' => $is_pass_class,
            ], 200);

    }


    private function hasInvitation(){
        $has_notification = false;

        if (Auth::check()) {

            $user_id = Auth::user()->id;
            $email = Auth::user()->email;

            $invites = uleagueteaminvite::where('invite_to', $email)->where('status', 0)->get()->first();

            if ($invites) {
                $has_notification = $invites;
            }
        }

        return $has_notification;
    }

    private function checkULeagueTeamMember()
    {

        $team_member = uleagueteammembers::getTeamDetailByUserId(Auth::user()->id);

        return $team_member;
    }

    private function checkULeagueTeamLeader()
    {

        $team_leader = uleagueteam::where('user_id', Auth::user()->id)->get()->first();

        return $team_leader;
    }

    private function checkULeagueUser()
    {

        if (Auth::check()) {
            $uleague = uleagueuser::join('tb_users', 'tb_users.id', 'uleague_user.user_id')
                ->join('university', 'university.university_id', 'uleague_user.university_id')
                ->join('faculty', 'faculty.faculty_id', 'uleague_user.faculty_id')
                ->where('user_id', Auth::user()->id)->first();

            return $uleague;
        } else {
            return false;
        }
    }
}
