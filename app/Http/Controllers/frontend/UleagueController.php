<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Uleagueapply;
use App\Models\Uleagueevent;
use App\Models\Uleagueteam;
use App\Models\Uleagueteaminvite;
use App\Models\Uleagueuser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class UleagueController extends Controller
{
    public function __construct(){
        parent::__construct();

        $this->data = array(
            'pageTitle' => 'Pitching',
            'pageNote' => 'Register',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );

        $this->zipevent = array('HOST'=>'test.zipeventapp.com', 'URL'=>'https://test.zipeventapp.com', 'CLIENT_ID'=>'VqCzcz6q7IHa09jb', 'CLIENT_SECRET'=>'aefc7402-e5de-4cdd-b77e-e698b3cfe358');
    }

    function WelcomePage(Request $request){

        $this->data['title'] = 'ลงทะเบียน - Startup Thailand League 2018'; 
        $this->data['content'] = 'pages.frontend.uleague';
        $this->data['invitation'] = null;

        if(Auth::check()){
            $this->data['uleague'] = self::checkULeagueUser();
            $this->data['has_notification'] = self::hasInvitation();
            if($this->data['has_notification']){
                $this->data['invitation'] = $this->data['has_notification'];
            }
        }

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);
    }

    function uploadStudentCard(Request $request){

        $input = Input::all();
        $user_id = \Auth::user()->id;

        // Get the UploadedFile object
        if($request->hasFile('image')){

            $files = $request->file('image');
            $fileImage = substr($files->getMimeType(), 0, 5);

            if($fileImage != 'image'){
                return back()->with('message', 'ไฟล์อัพโหลดผิดประเภท')->with('status', 'error');
            }

            //Path for save and display image
            $urlPath = '/uploads/uleague/photo_ids/';
            $filename = $urlPath.time()."_".$user_id.".jpg";

            //Path for save image
            $destinationPath = public_path(). "/uploads/uleague/photo_ids/";

            //Store image to /public/uploads/images/profile
            $files->move($destinationPath, $filename);
            $photo_src = $urlPath.time()."_".$user_id.".jpg";

            $uleagueuser = uleagueuser::where('user_id', $user_id)->first();
            $uleagueuser->photo_src = $photo_src;
            $uleagueuser->save();


        } else {
            return back()->with('message', 'โปรดอัพโหลดรูป')->with('status', 'error');
        }

        return redirect('user/uleague')->with('message', 'อัพเดทข้อมูล U League 2018 เรียบร้อยแล้ว')->with('status', 'success');

    }

    function updateInfo(Request $request){

        $input = Input::all();

        $user_id = \Auth::user()->id;

        foreach ($input as $key => $value){
            if($value === ""){
                return back()->with('message', 'โปรดใส่ข้อมูลให้ครบ')->with('status', 'error');
            }
        }

        $uleagueuser = uleagueuser::where('user_id', $user_id)->first();

        $uleagueuser->nickname = $input['nickname'];
        $uleagueuser->university_id = $input['university_id'];
        $uleagueuser->faculty_id = $input['faculty_id'];
        $uleagueuser->student_id = $input['student_id'];
        $uleagueuser->sub_faculty = $input['sub_faculty'];
        $uleagueuser->college_year = $input['college_year'];

        $uleagueuser->save();

        return redirect('user/uleague')->with('message', 'อัพเดทข้อมูล U League 2018 เรียบร้อยแล้ว')->with('status', 'success');

    }

    function joinULeague(Request $request){

        $input = Input::all();
        $user_id = \Auth::user()->id;

        //Check reCaptcha
        if (config('sximo.cnf_recaptcha') == 'true') {
            $return = $this->reCaptcha($request->all());
            if ($return !== false) {
                if ($return['success'] != 'true') {
                    return back()->with('status','error')->with('message','ข้อมูล Recapcha ไม่ถูกต้อง');
                }
            }
        }

        foreach ($input as $key => $value){
            if($value === ""){
                return back()->with('message', 'โปรดใส่ข้อมูลให้ครบ')->with('status', 'error');
            }
        }

        // Get the UploadedFile object
        if($request->hasFile('image')){

            $files = $request->file('image');
            $fileImage = substr($files->getMimeType(), 0, 5);

            if($fileImage != 'image'){
                return back()->with('message', 'ไฟล์อัพโหลดผิดประเภท')->with('status', 'error');
            }

            //Path for save and display image
            $urlPath = '/uploads/uleague/photo_ids/';
            $filename = $urlPath.time()."_".$user_id.".jpg";

            //Path for save image
            $destinationPath = public_path(). "/uploads/uleague/photo_ids/";

            //Store image to /public/uploads/images/profile
            $files->move($destinationPath, $filename);
            $photo_src = $urlPath.time()."_".$user_id.".jpg";

        } else {
            return back()->with('message', 'โปรดอัพโหลดรูป')->with('status', 'error');
        }

        $uleagueuser = new uleagueuser();

        $uleagueuser->user_id       = $user_id;
        $uleagueuser->status        = 0;
        $uleagueuser->nickname      = $input['nickname'];
        $uleagueuser->university_id = $input['university'];
        $uleagueuser->faculty_id    = $input['faculty'];
        $uleagueuser->student_id    = $input['student_id'];
        $uleagueuser->sub_faculty   = $input['sub_faculty'];
        $uleagueuser->college_year  = $input['year'];
        $uleagueuser->photo_src     = $photo_src;

        $uleagueuser->save();

        $user = User::where('id', '=', $user_id);

        if ($user->count() >= 1) {
            $user = $user->get();
            $user = $user[0];
            $data = array();
            $to = $user->email;
            $subject = "ตอบกลับ: สมัคร Startup Thailand League 2018";
            $data['subject'] = $subject;
            $data['email'] = $to;

            if (config('sximo.cnf_mail') == 'swift') {

                \Mail::send('emails.uleague-confirm-register', $data, function ($message) use ($data) {
                    $message->to($data['email'])->subject($data['subject']);
                });

            } else {

                $message = view('emails.uleague-confirm-register', $data);
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: ' . config('sximo.cnf_appname') . ' <' . config('sximo.cnf_email') . '>' . "\r\n";
                mail($to, $subject, $message, $headers);
            }

            return redirect('user/uleague')->with('message', 'ลงทะเบียน U League 2018 เรียบร้อยแล้ว')->with('status', 'success');
        }
    }

    //POST : RETURN JSON
    function applyCamp(Request $request){

        if (!\Auth::check()) return redirect('user/login');

        $input = $request->all();

        $user_id = Auth::user()->id;
        $event_id = $input['event_id'];

        $nia_user = User::where('tb_users.id', \Auth::user()->id)->join('user_nia', 'user_nia.user_id', 'tb_users.id')->join('uleague_user', 'tb_users.id', 'uleague_user.user_id')->join('faculty', 'faculty.faculty_id', 'user_nia.faculty_id')->join('university', 'university.university_id', 'uleague_user.university_id')->get()->first();
        //$uleague_user = \Auth::user()->find( \Auth::user()->id )->nia;

        if(!$nia_user){
            return response()->json(['success' => false, 'message'=>'เกิดข้อผิดพลาดในการลงทะเบียน' ], 200);
        }

        $user = array('email'=>Auth::user()->email, 
                      'firstname'=>Auth::user()->first_name, 
                      'lastname'=>Auth::user()->last_name,
                      'nickname'=>$nia_user->nickname,
                      'citizen_id'=>$nia_user->citizen_id,
                      'student_id'=>$nia_user->student_id,
                      'birthday'=>'20/12/1987',
                      'college_year'=>$nia_user->college_year,
                      'university'=>$nia_user->name, 
                      'faculty'=>$nia_user->faculty_name,
                      'subfaculty'=>$nia_user->sub_faculty,
                      'occupation'=>'นักศึกษา',
                      'mobile'=>$nia_user->mobile_number, 
                      'gender'=>$nia_user->gender,
                      'address'=>$nia_user->address,
                      'province'=>$nia_user->province_id,
                      'team'=>'');

        $post_params = ['event_id'=>$event_id, 'user'=>$user];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => config('app.zipevent_url')."/api/nia/event/uleague/register",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($post_params),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Host: ".config('app.zipevent_host'),
                "client_id: ".config('app.zipevent_client_id'),
                "client_secret: ".config('app.zipevent_client_secret'),
                "device_id: ".$user_id
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return response()->json(['success' => false, 'message'=>'เกิดข้อผิดพลาดในการลงทะเบียน' ], 200);
        }


        return response()->json(['success' => true, 'data'=>json_decode($response), 'user'=>$user ], 200);
        //return response()->json(['success' => true, 'data'=>$post_params ], 200);
    }

    function reApplyPitching(Request $request){

        if (!\Auth::check()) return redirect('user/login');

        $user_id = Auth::user()->id;
        $team_id = $request->team_id;


        Uleagueteam::where('team_id', $team_id)->update(array('status' => 1));
        Uleagueapply::where('user_id', $user_id)->update(array('status' => 1));

        return redirect('/user/uleague/team')->with('message', 'สมัคร U League 2018 Pitching เรียบร้อย')->with('status', 'success');

    }

    function applyPitching(Request $request){

        if (!\Auth::check()) return redirect('user/login');

        $user_id = Auth::user()->id;
        $event_id = $request->event_id;
        $team_name = $request->team_name;
        $nia_user = User::where('tb_users.id', \Auth::user()->id)->join('user_nia', 'user_nia.user_id', 'tb_users.id')->join('uleague_user', 'tb_users.id', 'uleague_user.user_id')->join('faculty', 'faculty.faculty_id', 'user_nia.faculty_id')->join('university', 'university.university_id', 'uleague_user.university_id')->get()->first();

        if(!$nia_user){
            return response()->json(['success' => false, 'message'=>'เกิดข้อผิดพลาดในการลงทะเบียน' ], 200);
        }

        $user = array('email'=>Auth::user()->email, 
                      'firstname'=>Auth::user()->first_name, 
                      'lastname'=>Auth::user()->last_name,
                      'nickname'=>$nia_user->nickname,
                      'citizen_id'=>$nia_user->citizen_id,
                      'student_id'=>$nia_user->student_id,
                      'birthday'=>'20/12/1987',
                      'college_year'=>$nia_user->college_year,
                      'university'=>$nia_user->name, 
                      'faculty'=>$nia_user->faculty_name,
                      'subfaculty'=>$nia_user->sub_faculty,
                      'occupation'=>'นักศึกษา',
                      'mobile'=>$nia_user->mobile_number, 
                      'gender'=>$nia_user->gender,
                      'address'=>$nia_user->address,
                      'province'=>$nia_user->province_id,
                      'team'=>$team_name);

        $post_params = ['event_id'=>$event_id, 'user'=>$user];


        $post_params = ['event_id'=>$event_id, 'user'=>$user];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => config('app.zipevent_url')."/api/nia/event/uleague/register",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($post_params),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Host: ".config('app.zipevent_host'),
                "client_id: ".config('app.zipevent_client_id'),
                "client_secret: ".config('app.zipevent_client_secret'),
                "device_id: ".$user_id
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if($err) {
            return response()->json(['success' => false, 'message'=>'เกิดข้อผิดพลาดในการลงทะเบียน' ], 200);
        }

        $apply = new uleagueapply();

        $apply->user_id = $user_id;
        $apply->status = 1;
        $apply->apply_for = 'pitching';
        $apply->reference_id = $event_id;
        $apply->reason_reject = '';
        $apply->save();

        Uleagueteam::where('user_id', $user_id)->update(array('status' => 1));

        return redirect('/user/uleague/team?step=3')->with('message', 'ลงทะเบียนกิจกรรมเรียบร้อยแล้ว')->with('status', 'success');

    }

    public function reCaptcha($request){
        if (!is_null($request['g-recaptcha-response'])) {
            $api_url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . config('sximo.cnf_recaptchaprivatekey') . '&response=' . $request['g-recaptcha-response'];
            $response = @file_get_contents($api_url);
            $data = json_decode($response, true);

            return $data;
        } else {
            return false;
        }

    }

    private function hasInvitation(){
        $has_notification = false;

        if(Auth::check()){

            $user_id = Auth::user()->id;
            $email = Auth::user()->email;

            $invites = uleagueteaminvite::where('invite_to', $email)->where('status', 0)->get()->first();

            if($invites){
                $has_notification = $invites;
            }
        }

        return $has_notification;
    } 

    private function checkULeagueUser(){

        if(Auth::check()){
            $uleague = uleagueuser::join('tb_users', 'tb_users.id', 'uleague_user.user_id')
                ->join('university', 'university.university_id', 'uleague_user.university_id')
                ->join('faculty', 'faculty.faculty_id', 'uleague_user.faculty_id')
                ->where('user_id', \Auth::user()->id)->first();

            return $uleague;
        }
        else{
            return false;
        }
    }

}