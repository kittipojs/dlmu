<?php

namespace App\Http\Controllers\frontend;

use App\Models\Coursesession;
use App\Models\Coursecurriculum;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class CurriculumController extends Controller{

    protected $data = array();
    protected $template = array();

    public function __construct(){

        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Pitching',
            'pageNote' => 'Register',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );
    }

    public function getContent(Request $request){

        if (!\Auth::check()){ return response()->json(['success' => false, 'code'=>100, 'message'=>'Not Authorized'], 200); }

        $cir_id = $request->id;
        $course_id = $request->course_id;
        $session_value = $request->session;

        $curriculum = coursecurriculum::getDetailByCourseId($course_id);

        if ($curriculum['lecture_id'] != null && $curriculum['quiz_id'] == null) {
            $curriculum['title'] = $curriculum['lecture_title'];
            $curriculum['type'] = 'lecture';
        } else {
            $curriculum['title'] = $curriculum['quiz_title'];
            $curriculum['type'] = 'quiz';
        }
        //dd($curriculum);
        
        $page_template = 'pages.frontend.course.content-'.$curriculum['type'];
        
        $session = coursesession::where('session_value', $session_value)->first();
        
        $session->current_state = $curriculum->curriculum_id;
        $session->current_object_id = $curriculum->object_id;
        $session->save();
        
        $this->data['course_content'] = $curriculum;
        
        return view($page_template, $this->data);

    }

    private function checkSession($course_id){

        if (!\Auth::check()){ return response()->json(['success' => false, 'code'=>100, 'message'=>'Not Authorized'], 200); }

        $user_id = Auth::user()->id; 

        $session = coursesession::where('user_id', $user_id)->where('course_id', $course_id)->first();

        if(!$session){
            return response()->json(['success' => false, 'code'=>101, 'message'=>'ยังไม่ได้ลงทะเบียน'], 200);
        }

        return response()->json(['success' => true, 'session'=>$session->session_value, 'message'=>$session], 200);

    }

}