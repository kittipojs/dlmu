<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Pitching;
use App\Models\Pitchinground;
use App\Models\Pitchingteam;
use App\Models\Pitchingteammember;
use App\Models\Pitchinginvite;
use App\Models\Camping;
use App\Models\Campinground;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class PitchingController extends Controller
{
    protected $data = array();
    protected $camping = 'pages.frontend.camping';
    protected $pitching = 'pages.frontend.pitching';
    protected $invitation = 'pages.frontend.pitching.invitation';
    protected $template = array();

    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Pitching',
            'pageNote' => 'Register',
        );
        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );
    }

    function FormRegisterPitching(){

        $this->data['enable_ng'] = true; 
        $this->data['title'] = 'ลงทะเบียน - Startup Pitching'; 
        $this->data['content'] = $this->pitching.".form"; 

        $this->data['invite_accepted'] = false; 
        $this->data['is_team_leader'] = false; 
        $this->data['is_team_member'] = false; 
        $this->data['has_team'] = false; 
        $this->data['has_invitation'] = false; 

        if(!Auth::check()){
            return redirect('/user/login')->with(['message' => 'กรุณาสมัครสมาชิกก่อน','status'=>'error']);
        }

        $step_to_go = 0;
        $user_id = Auth::user()->id;

        $user = User::where('id', Auth::user()->id)->first();
        $pitch_detail = pitching::getPitchDetailByID($user_id);
        $pitch_team_detail = pitchingteam::getPitchTeamDetailByID($user_id);

        $this->data['team'] = (object) array('team_id'=>null);

        $invitation = pitchinginvite::getInvitationByEmail($user->email); 

        if($invitation){
            $this->data['has_invitation'] = true;
            $this->data['invitation'] = $invitation;
        }


        //CHECK IF USER HAS REGISTER STL2018
        if($pitch_detail){

            $step_to_go = 1;

            if($pitch_team_detail){
                $this->data['has_team'] = true;
            }

            $this->data['team'] = (object)  array('team_name'=>'', 'team_info'=>'', 'file_url'=>'', 'university_id'=>0, 'sector'=>'');

            if($pitch_team_detail){
                $this->data['team'] = $pitch_team_detail;
            }

            $is_team_member = pitchingteammember::getPitchTeamDetailByID($user_id);

            //CHECK IF USER JOIN TEAM
            if($is_team_member){
                $step_to_go = 2;

                $this->data['is_team_member'] = $is_team_member;
            }

            //CHECK IF USER HAS CREATE TEAM
            if($pitch_team_detail){

                $step_to_go = 2;

                $this->data['is_team_leader'] = true;

                $team_id = $pitch_team_detail['team_id'];

                $invite_list = pitchinginvite::where('team_id', $team_id)->get();

                $this->data['pitch_detail'] = $pitch_detail->first();

                if($invite_list){
                    $this->data['invite_list'] = $invite_list;
                }

                $this->data['invite_accepted'] = false;

                $accepted = pitchinginvite::where('team_id', $team_id)->where('status', 1)->get();


                //CHECK IF ALL FREINDS ALREADY ACCEPTED
                if(count($accepted)>=2 && count($accepted)<=4 && count($invite_list)==count($accepted)){

                    $t = pitchingteammember::where('team_id', $team_id)->get();
                    $number_member = count($t);

                    if($number_member == count($accepted)+1){
                        $this->data['invite_accepted'] = true;

                        $rounds = pitchinground::get();
                        $this->data['rounds'] = $rounds;
                        $this->data['round_count'] = self::CountRound();
                    }
                }

                //CHECK IF USER HAS SELECT PITCHING ROUND
                if($pitch_team_detail['status'] > 0 && $pitch_team_detail['round_id'] != 0){
                    $step_to_go = 3;
                }

            }

        }
        else{

            $is_team_member = pitchingteammember::getPitchTeamDetailByID($user_id);

            $this->data['is_team_member'] = $is_team_member;

        }

        $this->data['step_to_go'] = $step_to_go; 
        $this->data['user'] = $user;

        $page_template = $this->template['frontend'];

        return view($page_template, $this->data);
    }

    function getRegister(Request $request)
    {
        $input = Input::all();
        $user_id = $input['user_id'];

        foreach ( $input as $key => $value){
            if($value === ""){
                return back()->with('message', 'โปรดใส่ข้อมูลให้ครบ')->with('status', 'error');
            }
        }

        // Get the UploadedFile object
        if($request->hasFile('image')){

            $files = $request->file('image');
            $fileImage = substr($files->getMimeType(), 0, 5);

            if($fileImage != 'image'){
                return back()->with('message', 'ไฟล์อัพโหลดผิดประเภท')->with('status', 'error');
            }

            //Path for save and display image
            $urlPath = '/uploads/stl2018/pitching/';
            $filename = $urlPath.time()."_".$user_id.".jpg";
            //Path for save image
            $destinationPath = public_path(). "/uploads/stl2018/pitching/";
            //Store image to /public/uploads/images/profile
            $files->move($destinationPath, $filename);
            $url = $urlPath.time()."_".$user_id.".jpg";

        } else {
            return back()->with('message', 'โปรดอัพโหลดรูป')->with('status', 'error');
        }

        $pitching = new pitching();
        $pitching->user_id = $user_id;
        $pitching->nickname = $input['nickname'];
        $pitching->university_id = $input['university'];
        $pitching->faculty_id = $input['faculty'];
        $pitching->college_year = $input['year'];
        $pitching->photo_src = $url;
        $pitching->save();

        $user = User::where('id', '=', $user_id);
        if ($user->count() >= 1) {
            $user = $user->get();
            $user = $user[0];
            $data = array();
            $to = $user->email;
            $subject = "ตอบกลับ: สมัคร Startup Thailand League 2018 (Pitching) ";
            $data['subject'] = $subject;
            $data['email'] = $to;

            if (config('sximo.cnf_mail') == 'swift') {

                \Mail::send('emails.pitching-confirm-register', $data, function ($message) use ($data) {
                    $message->to($data['email'])->subject($data['subject']);
                });

            } else {

                $message = view('emails.pitching-confirm-register', $data);
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: ' . config('sximo.cnf_appname') . ' <' . config('sximo.cnf_email') . '>' . "\r\n";
                mail($to, $subject, $message, $headers);
            }

            return redirect('/uleague/pitching')->with('message', 'ลงทะเบียน Pitching เสร็จสิ้น')->with('status', 'success');
        }
    }

    static function CountRound()
    {
        $rounds = pitchinground::get();
        $count = array();

        foreach($rounds as $round){
            $count[$round->round_id] = pitchingteam::where('round_id', $round->round_id)->count();
        }

        return $count;
    }

    function getPitchDetails()
    {

        $this->data['team'] = (object)  array('team_name'=>'', 'team_info'=>'', 'file_url'=>'', 'university_id'=>0, 'sector'=>'');

        if(!Auth::check()){
            return redirect('')->with(['message' => 'กรุณาสมัครสมาชิกก่อน','status'=>'error']);
        }

        $user_id = Auth::user()->id;
        $pitch_detail = pitching::getPitchDetailByID($user_id);
        $pitch_team_detail = pitchingteam::getPitchTeamDetailByID($user_id);


        $has_team = false;
        $has_register = true;
        if(!$pitch_detail){
            $has_register = false;
        }

        $this->data['row'] = $pitch_detail;

        if($pitch_team_detail){
            $this->data['team'] = $pitch_team_detail;
            $has_team = true;
        }

        $this->data['content'] = $this->pitching.".details";
        $this->data['step'] = 1;
        $this->data['has_register'] = $has_register;
        $this->data['has_team'] = $has_team;

        $this->data['team_submit_url'] = 'user/pitch/create/team';
        if($has_team){
            $this->data['team_submit_url'] = 'user/pitch/update/team';
        }

        $this->data['title'] = 'สถานะการสมัคร Pitching';
        $this->data['pageNote'] = 'สถานะการสมัคร';

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);

    }

    function createTeam(Request $request)
    {
        $input = Input::all();
        $user_id = $input['user_id'];
        $teamName = $input['team_name'];
        $teamInfo = $input['team_info'];
        $university_id = $input['university'];
        $sector = $input['sector'];

        // Get the UploadedFile object
        if($request->hasFile('file')){
            $files = $request->file('file');

            $fileType = $files->getMimeType();

            if($fileType !== 'application/pdf' && $fileType !== 'image/jpeg' && $fileType !== 'image/png'){
                return back()->with('message', 'อัพโหลดไฟล์ผิดประเภท')->with('status', 'error');
            }

            if($fileType == 'application/pdf'){
                $ext = '.pdf';
            }
            else if($fileType !== 'image/jpeg'){
                $ext = '.jpg';
            }
            else if($fileType !== 'image/png'){
                $ext = '.png';
            }

            //Path for save and display image
            $urlPath = '/uploads/stl2018/team/';
            $filename = $urlPath.time()."_".$user_id.''.$ext;

            //Path for save image
            $destinationPath = public_path(). "/uploads/stl2018/team/";

            //Store image to /public/uploads/files/pitching
            $files->move($destinationPath, $filename);

            $url = $urlPath.time()."_".$user_id.''.$ext;

        } else {
            return back()->with('message', 'โปรดเลือกไฟล์สำหรับอัพโหลด')->with('status', 'error');
        }

        $team = new pitchingteam();
        $team_id = time().''.rand(1000, 9999);
        $team->team_id = $team_id;
        //$team->pitching_team_id = pitchingteam::max('pitching_team_id') + 1;
        $team->user_id = $user_id;
        $team->team_name = $teamName;
        $team->team_info = $teamInfo;
        $team->file_url  = $url;
        $team->university_id  = $university_id;
        $team->sector = $sector;
        $team->save();

        $member = new pitchingteammember();
        $member->user_id = $user_id;
        $member->team_id = $team_id;
        $member->save();

        //Get details
        $pitch_detail = pitching::getPitchDetailByID($user_id);
        $pitch_team_detail = pitchingteam::getPitchTeamDetailByID($user_id);

        $has_team = false;
        $has_register = true;
        if(!$pitch_detail){
            $has_register = false;
        }

        $this->data['row'] = $pitch_detail;

        if($pitch_team_detail){
            $this->data['team'] = $pitch_team_detail;
            $has_team = true;
        }

        $this->data['row'] = $pitch_detail;
        $this->data['team'] = $pitch_team_detail;
        $this->data['content'] = $this->pitching.".details";
        $this->data['has_register'] = $has_register;
        $this->data['has_team'] = $has_team;

        $this->data['team_submit_url'] = 'user/pitch/create/team';
        if($has_team){
            $this->data['team_submit_url'] = 'user/pitch/update/team';
        }

        $this->data['title'] = 'สถานะการสมัคร Pitching';
        $this->data['pageNote'] = 'สถานะการสมัคร';

        $page_template = $this->template['backend'];

        return redirect('/uleague/pitching')->with(['message' => 'สร้างทีม '.$teamName.' สำเร็จ','status'=>'success']);
    }

    function updateTeam(Request $request)
    {
        $input = Input::all();

        $team_id = $input['team_id'];

        $data['user_id'] = $input['user_id'];
        $data['team_name'] = $input['team_name'];
        $data['team_info'] = $input['team_info'];
        $data['university_id'] = $input['university'];
        $data['sector'] = $input['sector'];

        // Get the UploadedFile object
        if($request->hasFile('file')){
            $files = $request->file('file');

            $fileType = $files->getMimeType();

            if($fileType !== 'application/pdf' && $fileType !== 'image/jpeg' && $fileType !== 'image/png'){
                return back()->with('message', 'อัพโหลดไฟล์ผิดประเภท')->with('status', 'error');
            }

            if($fileType == 'application/pdf'){
                $ext = '.pdf';
            }
            else if($fileType !== 'image/jpeg'){
                $ext = '.jpg';
            }
            else if($fileType !== 'image/png'){
                $ext = '.png';
            }

            //Path for save and display image
            $urlPath = '/uploads/stl2018/team/';
            $filename = $urlPath.time()."_".$user_id.''.$ext;

            //Path for save image
            $destinationPath = public_path(). "/uploads/stl2018/team/";

            //Store image to /public/uploads/files/pitching
            $files->move($destinationPath, $filename);

            $url = $urlPath.time()."_".$user_id.''.$ext;

        } else {
            return back()->with('message', 'โปรดเลือกไฟล์สำหรับอัพโหลด')->with('status', 'error');
        }

        pitchingteam::where('team_id', '=', $team_id)->update($data);

        //$page_template = $this->template['backend'];

        return self::getPitchDetails();
    }

    function inviteFriends(Request $request){

        $input = $request->all();

        $team_members = $input['team_members'];
        $team_id = $input['team_id'];

        $data['count'] = 0;
        $data['sent_to'] = array();

        foreach($team_members as $item){

            if($data['count']>0 && $item['status']==-99){

                $invite = new pitchinginvite();
                $invite->team_id = $team_id;
                $invite->invite_to = $item['invite_to'];
                $invite->invitation_code = rand('100000', '999999');
                $invite->status = 0;
                $invite->save();

                $subject = "คุณมี 1 คำเชิญเข้าร่วมทีมดเพื่อแข่งขัน Startup Thailand League 2018";

                $data['subject'] = $subject;
                $data['email'] = $invite->invite_to;
                $data['invitation_code'] = $invite->invitation_code;
                $data['invitation_url'] = url('/').'/uleague/team/invite/'.$team_id.''.$data['invitation_code'];

                if (config('sximo.cnf_mail') == 'swift') {

                    \Mail::send('emails.pitching_invitation', $data, function ($message) use ($data) {
                        $message->to($data['email'])->subject($data['subject']);
                    });

                    $data['sent_to'][] = $data['email'];

                } else {

                    $message = view('emails.pitching_invitation', $data);
                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers .= 'From: ' . config('sximo.cnf_appname') . ' <' . config('sximo.cnf_email') . '>' . "\r\n";
                    mail($invite->invite_to, $subject, $message, $headers);
                }
            }
            $data['count']++;
        }

        return response()->json(['success' => true, 'data'=>$data], 200);
    }

    function getInviteFriends(Request $request){


        $invite_list = null;

        if(!Auth::check()){
            return response()->json(['success' => false], 403);
        }

        $user_id = Auth::user()->id;

        $team = pitchingteam::where('user_id', '=', $user_id)->get()->first();
        if($team){
            $invite_list = pitchinginvite::where('team_id', '=', $team->team_id)->get();
        }

        return response()->json(['success' => true, 'data' => $invite_list], 200);
    }

    //CALLED FROM ANGULAR
    function replyInvitation(Request $request){ 

        $data = array();

        if(!Auth::check()){
            $data['message'] = 'เกิดข้อผิดพลาด! Code คำเชิญไม่ถูกต้อง';
            return response()->json(['success' => false, 'data' => $data], 200);
        }

        $email = Auth::user()->email; 
        $user_id = Auth::user()->id; 

        $input = $request->all(); 

        $invitation_code = $input['invitation_code']; 
        $status = $input['status']; 
        $team_id = $input['team_id'];

        $p = pitchinginvite::where('invitation_code', $invitation_code) 
            ->where('team_id', $team_id);

        $invite = $p->get()->first();

        //CHECK IF EMAIL MISSMATCH
        if($invite->invite_to!=$email){
            $data['message'] = 'เกิดข้อผิดพลาด! Code คำเชิญไม่ถูกต้อง';
            return response()->json(['success' => false, 'data' => $data], 200);
        }

        $is_member = pitchingteammember::where('user_id', $user_id)->get()->first();

        if($is_member){
            $data['message'] = 'คุณเคยเข้าร่วมทีมอื่นแล้ว';
            return response()->json(['success' => false, 'data' => $data], 200);
        }

        //update invitation status
        $p->update(['status' => $status]);


        //add user to team member
        if($status==1){ 
            $member = new pitchingteammember(); 
            $member->user_id = $user_id; 
            $member->team_id = $team_id; 
            $member->save();
        }

        return response()->json(['success' => true, 'data' => $data], 200); 
    }


    function applyRound(Request $request){

        $input = Input::all();

        $user_id = $input['user_id'];
        $team_id = $input['team_id'];
        $round_id = $input['round'];

        pitchingteam::where('user_id', $user_id)
            ->where('team_id', $team_id)
            ->update(['round_id' => $round_id, 'status' => 1]);

        return redirect('/uleague/pitching')->with(['message' => 'ลงทะเบียนเรียนร้อยแล้ว','status'=>'sucsess']);
    }

}
