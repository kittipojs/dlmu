<?php

namespace App\Http\Controllers\frontend;

use App\Models\Courses;
use App\Models\Coursecurriculum;
use App\Models\Coursesession;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class CourseSessionController extends Controller{

    protected $data = array();
    protected $template = array();

    public function __construct(){

        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Pitching',
            'pageNote' => 'Register',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );
    }

    public function create(Request $request){

        $input = Input::all();

        if (!\Auth::check()){ return response()->json(['success' => false, 'code'=>100, 'message'=>'Not Authorized'], 200); }

        //$cs_id = md5($course_id.'.'.$user_id.'.'.time());
        $course_id = $input['course_id'];
        $previous = $input['previous'];
        $user_id = Auth::user()->id;
        $object_id = null;

        /*
        if($previous==null){
            $object_id = $curriculum[0]->object_id;
        }
        else{
            $i = 0;
            foreach($curriculum as $item){
                if($item['object_id']==$previous){
                    if($i+1 < count($curriculum)){
                        $object_id = $curriculum[$i+1]->object_id;
                        break;
                    }
                }
                $i++;
            }
        }

        if($object_id==null){
             $object_id = $curriculum[count($curriculum)-1]->object_id;
        }
        */

        $course = Courses::where('course_id', $course_id)->first();

        if($course->course_status!=1){
            return response()->json(['success' => false, 'code'=>102, 'message'=>'หลักสูตรนี้ยังไม่เปิดให้เข้าเรียน'], 200);
        }

        $curriculum = Coursecurriculum::where('course_id', $course_id)->get();

        $session = coursesession::where('user_id', $user_id)->where('course_id', $course_id)->first();

        if($session){


            dd($session);

            //return response()->json(['success' => true, 'url'=>url('/course/'.$course_id.'/learn?session='.$session->id.'&v=lecture'), 'message'=>'เคยลงทะเบียนหลักสูตรนี้ไว้แล้ว'], 200);
        }
        else{

            $course_session = new Coursesession();

            $course_session->user_id = $user_id;        
            $course_session->course_id = $course_id;  
            $course_session->object_id = $object_id;
            $course_session->progress = 0;
            $course_session->is_pass = false;
            $course_session->object_id = $curriculum[0]->object_id;
            $course_session->save();

            return response()->json(['success' => true, 'url'=>url('/course/'.$course_id.'/learn'), 'message'=>'ลงทะเบียนเรียบร้อยแล้ว'], 200);
        }

    }

    public function checkSession($course_id){

        if (!\Auth::check()){ return response()->json(['success' => false, 'code'=>100, 'message'=>'Not Authorized'], 200); }

        $user_id = Auth::user()->id; 

        $session = coursesession::where('user_id', $user_id)->where('course_id', $course_id)->first();

        if(!$session){
            return response()->json(['success' => false, 'code'=>101, 'message'=>'ยังไม่ได้ลงทะเบียน'], 200);
        }

        return response()->json(['success' => true, 'session'=>$session->session_value, 'message'=>$session], 200);
    }

}