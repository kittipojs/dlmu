<?php

namespace App\Http\Controllers\frontend;

use App\Models\Uleagueteaminvite;
use App\Models\Uleagueuser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class EventController extends Controller
{
    protected $layout = "layouts.main";
    protected $event_dir = "pages.frontend.event";
    public function __construct()
    {
        parent::__construct();
        $this->data = array();

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );

    }

    public function index(){

        if(Auth::check()){
            $email = Auth::user()->email;
            
            $this->data['uleague'] = self::checkULeagueUser();
            
        } else {
            $email = '';
        }

        $this->data['has_notification'] = self::hasInvitation();
        if($this->data['has_notification']){
            $this->data['invitation'] = $this->data['has_notification'];
        }
        $this->data['show_uleague_btn'] = true;

        $events_camp                   = self::getEvents('Camp', $email);
        $this->data['events_camp']     = array_slice(self::convertData($events_camp), -3, 3, true);
        $this->data['all_camp']        = (count($events_camp) > 3) ? true : false;
        $events_pitch                  = self::getEvents('Pitching', $email);
        $this->data['events_pitch']    = array_slice(self::convertData($events_pitch), -3, 3, true);
        $this->data['all_pitch']        = (count($events_pitch) > 3) ? true : false;
        $events_general                = self::getEvents('General', $email);
        $this->data['events_general']  = array_slice(self::convertData($events_general), -3, 3, true);
        $this->data['all_general']        = (count($events_general) > 3) ? true : false;

        $this->data['content'] = $this->event_dir.".index";
        $this->data['title']   = 'กิจกรรมทั้งหมด';
        $page_template = $this->template['frontend'];

        return view($page_template,$this->data);
    }

    public function allEvents(){

        if(Auth::check()){
            $email = Auth::user()->email;
        } else {
            $email = '';
        }

        $this->data['uleague'] = self::checkULeagueUser();
        $this->data['has_notification'] = self::hasInvitation();

        if($this->data['has_notification']){
            $this->data['invitation'] = $this->data['has_notification'];
        }

        $this->data['show_uleague_btn'] = true;

        $param = Input::get('type');
        $events = self::getEvents($param, $email);
        
        $allEvents  = self::convertData($events);

        $this->data['content'] = $this->event_dir.".all_event";
        $this->data['title']   = 'กิจกรรม '.$param;
        $this->data['type']    = $param;
        $this->data['events']  = $allEvents;
        $page_template = $this->template['frontend'];

        return view($page_template,$this->data);
    }

    private function convertData($events){

        foreach ( $events as $key => $value ){
            if(!isset($value->cover)){
                $value->cover = asset('/frontend/medilab/img/event-banner.jpg');
            }
            $value->start_date   =   $this->dateThai(date('Y-m-d H:i:s', $value->start_date));
            $value->end_date     =   $this->dateThai(date('Y-m-d H:i:s', $value->end_date));
        }
        return $events;
    }

    private function dateThai($strDate){

        $strYear = date("Y", strtotime($strDate)) + 543;
        $strMonth = date("n", strtotime($strDate));
        $strDay = date("j", strtotime($strDate));
        $strHour = date("H", strtotime($strDate));
        $strMinute = date("i", strtotime($strDate));
        $strSeconds = date("s", strtotime($strDate));

        $strMonthCut = Array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");

        $strMonthThai = $strMonthCut[$strMonth];

        return "$strDay $strMonthThai $strYear";
    }

    private function getEvents($type, $email){

        $curl = new \anlutro\cURL\cURL;

        $rq = $curl->newRequest('get', config('app.zipevent_url').'/api/nia/event/uleague?type='.$type.'&email='.$email)
            ->setHeader('Content-Type', 'application/json')
            ->setHeader('Host', config('app.zipevent_host'))
            ->setHeader('client_id', config('app.zipevent_client_id'))
            ->setHeader('client_secret', config('app.zipevent_client_secret'))
            ->setHeader('device_id', '1')
            ->setHeader('Content-Length', '0');

        $ev = $rq->send();

        return json_decode($ev->body);
    }

    private function hasInvitation(){

        $has_notification = false;

        if(Auth::check()){

            $email = Auth::user()->email;

            $invites = uleagueteaminvite::where('invite_to', $email)->where('status', 0)->get()->first();

            if($invites){
                $has_notification = $invites;
            }
        }

        return $has_notification;

    }

    private function checkULeagueUser(){

        if(Auth::check()){
            $uleague = uleagueuser::join('tb_users', 'tb_users.id', 'uleague_user.user_id')
                ->join('university', 'university.university_id', 'uleague_user.university_id')
                ->join('faculty', 'faculty.faculty_id', 'uleague_user.faculty_id')
                ->where('user_id', \Auth::user()->id)->first();

            return $uleague;
        }
        else{
            return false;
        }
    }
}
