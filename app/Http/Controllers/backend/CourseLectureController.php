<?php

namespace App\Http\Controllers\backend;

use App\Models\Coursecurriculum;
use App\Models\Courselectureslides;
use App\Models\Courses;
use App\Models\Courselectures;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\UploadedFile;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;

class CourseLectureController extends Controller{

    protected $data = array();
    protected $layout = "layouts.main";
    protected $backend = 'pages.backend';
    protected $template = array();

    public function __construct(){
        parent::__construct();
        $this->access = array();

        $this->data = array(
            'pageTitle' => 'Startup Thailand Academy',
            'pageNote' => 'Course',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend' => 'layouts.default.template.backend'
        );
    }

    function index(){
        if (Auth::user()->group_id > 2 || !Auth::check()) {
            return redirect('/e-admin/login')->with('message', 'You don\'t have permission')->with('status', 'error');
        }

        $courses = courses::get();

        $this->data['courses'] = $courses;
        $this->data['content'] = $this->backend . ".course.index";
        $this->data['title'] = 'รายการคอร์สเรียน';

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    function addLecturePage(Request $request){

        if (Auth::user()->group_id > 2 || !Auth::check()) {
            return redirect('/e-admin/login')->with('message', 'You don\'t have permission')->with('status', 'error');
        }

        $course_id = $request->to;
        $media_type = $request->media_type;

        if ($media_type == 'album') {
            $this->data['video_list'] = courselectures::where('media_type', 'mp4')->orderBy('created_at', 'DESC')->groupBy('media_src')->get();
        }

        $this->data['lecture_id'] = time() . '' . rand(100, 999);

        $this->data['to'] = $course_id;
        $this->data['media_type'] = $request->media_type;
        $this->data['course'] = courses::where('course_id', $course_id)->first();


        $this->data['content'] = $this->backend . ".lectures.add";
        $this->data['title'] = 'สร้างบทเรียนใหม่';
        $this->data['file_name'] = $this->data['lecture_id'].'_'.md5(time());

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);

    }

    function getList(Request $request){

        if (!Auth::check() || Auth::user()->group_id > 2) {
            return redirect('/e-admin/login')->with('message', 'You don\'t have permission')->with('status', 'error');
        }

        $lectures = courselectures::join('courses', 'courses.course_id', 'course_lectures.course_id')->get();

        $this->data['course_id'] = $request->input('course_id');
        $this->data['lectures'] = $lectures;
        $this->data['title'] = 'รายการบทเรียน';

        return view($this->backend . ".lectures.list", $this->data);
    }

    function selectMediaPage(Request $request)
    {

        if (!Auth::check() || Auth::user()->group_id > 2) {
            return redirect('/e-admin/login')->with('message', 'You don\'t have permission')->with('status', 'error');
        }

        $this->data['course_id'] = $request->input('course_id');
        $this->data['title'] = 'รายการบทเรียน';

        return view($this->backend . ".lectures.select-media", $this->data);
    }

    function save(Request $request){

        $input = Input::all();


        $course_id = $input['course_id'];
        $lecture_id = $input['lecture_id'];
        $lecture_title = $input['lecture_title'];
        $lecture_intro = $input['lecture_intro'];
        $lecture_description = $input['lecture_description'];
        $media_type = $input['media_type'];
        $media_src = $input['media_src'];
        $media_duration = 0;
        if(isset($input['media_duration'])){
            $media_duration = $input['media_duration'];
        }
        $url = '';

        if ($request->hasFile('image')) {
            $files = $request->file('image');

            $fileType = $files->getMimeType();

            if ($fileType !== 'image/jpeg' && $fileType !== 'image/png') {
                return response()->json(['success' => false, 'อัพโหลดไฟล์ผิดประเภท'], 200);
            }

            if ($fileType !== 'image/jpeg') {
                $ext = '.jpg';
            } else if ($fileType !== 'image/png') {
                $ext = '.png';
            }

            //Path for save and display image
            $urlPath = 'uploads/cover/';
            $filename = $urlPath . $lecture_id . '' . $ext;

            //Path for save image
            $destinationPath = public_path() . '/uploads/cover/';

            //Store image to /public/uploads/files/pitching
            $files->move($destinationPath, $filename);

            $url = $urlPath . $lecture_id . '' . $ext;
        }

        $courseLecture = new courselectures();

        $courseLecture->lecture_id = $lecture_id;
        $courseLecture->lecture_title = $lecture_title;
        $courseLecture->lecture_intro = $lecture_intro;
        $courseLecture->lecture_description = $lecture_description;
        $courseLecture->lecture_cover = $url;
        $courseLecture->media_type = $media_type;
        $courseLecture->media_src = $media_src;
        $courseLecture->media_duration = $media_duration;

        if ($courseLecture->media_type == 'youtube') {
            $vid = self::get_url_id($courseLecture->media_src);

            $courseLecture->media_src = self::get_url_embed($courseLecture->media_src);
            $courseLecture->media_duration = self::getYoutubeDuration($vid);
        }

        if ($courseLecture->media_type == 'mp4') {
            $courseLecture->start_time = '00:00:00';
            $courseLecture->end_time = '00:00:00';
        }

        if ($courseLecture->media_type == 'album') {
            $courseLecture->start_time = $input['start'];
            $courseLecture->end_time = $input['end'];
        }

        $courseLecture->save();

        /* SAVE NEW CURRICULUM */
        $courseCurriculum = new coursecurriculum();
        $courseCurriculum->course_id = $course_id;
        $courseCurriculum->object_id = $courseLecture->lecture_id;
        $courseCurriculum->object_type = 'lecture';
        $courseCurriculum->object_order = coursecurriculum::where('course_id', $course_id)->max('object_order') + 1;
        $courseCurriculum->save();


        return redirect('e-admin/course/manage/' . $course_id)->with('message', 'บันทึกสำเร็จ')->with('status', 'success');
    }

    function updateLecturePage($id){

        if (!Auth::check() || Auth::user()->group_id > 2) {
            return redirect('')->with('message', 'You don\'t have permission')->with('status', 'error');
        }

        $lecture = coursecurriculum::getDetailByObjectId($id);

        $lecture->time_from_millisec = self::timeToMillisec($lecture->start_time);
        $lecture->time_to_millisec = self::timeToMillisec($lecture->end_time);

        $this->data['video_list'] = courselectures::get();

        $this->data['lecture'] = $lecture;

        return view($this->backend . ".lectures.update", $this->data);
    }

    function updateLectureSave(Request $request){

        if (!Auth::check() || Auth::user()->group_id > 2) {
            return redirect('')->with('message', 'You don\'t have permission')->with('status', 'error');
        }

        $input = Input::all();

        $course_id = $input['course_id'];
        $lecture_id = $input['lecture_id'];
        $lecture_title = $input['lecture_title'];
        $lecture_intro = $input['lecture_intro'];
        $lecture_description = $input['lecture_description'];
        $media_type = $input['media_type'];
        $media_src = $input['media_src'];

        $lecture = courselectures::where('lecture_id', $lecture_id)->first();

        $lecture->lecture_title = $lecture_title;
        $lecture->lecture_intro = $lecture_intro;
        $lecture->lecture_description = $lecture_description;


        if ($request->hasFile('image')) {
            $files = $request->file('image');

            $fileType = $files->getMimeType();

            if ($fileType !== 'image/jpeg' && $fileType !== 'image/png') {
                return response()->json(['success' => false, 'อัพโหลดไฟล์ผิดประเภท'], 200);
            }

            if ($fileType !== 'image/jpeg') {
                $ext = '.jpg';
            } else if ($fileType !== 'image/png') {
                $ext = '.png';
            }

            //Path for save and display image
            $urlPath = 'uploads/cover/';
            $filename = $urlPath . $lecture_id . '' . $ext;

            //Path for save image
            $destinationPath = public_path() . 'uploads/cover/';

            //Store image to /public/uploads/files/pitching
            $files->move($destinationPath, $filename);

            $url = $urlPath . $lecture_id . '' . $ext;

            $lecture->lecture_cover = $url;
        }
        $lecture->media_type = $media_type;
        $lecture->media_src = $media_src;

        if ($lecture->media_type == 'youtube') {
            $vid = self::get_url_id($lecture->media_src);

            $lecture->media_src = self::get_url_embed($lecture->media_src);
            $lecture->media_duration = self::getYoutubeDuration($vid);
        }

        if ($lecture->media_type == 'album' || $lecture->media_type == 'mp4') {
            $lecture->start_time = $input['start'];
            $lecture->end_time = $input['end'];
        }

        $lecture->save();

        return redirect('e-admin/course/manage/' . $course_id)->with('message', 'บันทึกสำเร็จ')->with('status', 'success');
    }

    /*
    function upload(Request $request)
    {

        if (Auth::user()->group_id > 2 || !Auth::check()) {
            return redirect('/e-admin/login')->with('message', 'You don\'t have permission')->with('status', 'error');
        }

        $input = Input::all();

        $lecture_id = $input['lecture_id'];

        if ($request->hasFile('file')) {


            $files = $request->file('file');

            //Path for save and display image
            $urlPath = 'uploads/courses/videos/';
            $filename = $urlPath . $lecture_id . ".mp4";

            //Path for save image
            $destinationPath = public_path() . "/uploads/courses/videos";

            //Store image to /public/uploads/images/profile
            $files->move($destinationPath, $filename);
            $url = $urlPath . $lecture_id . ".mp4";
            $full_url = \URL::to('/') . '/' . $url;


            return response()->json(['success' => true, 'message' => 'อัพโหลดไฟล์สำเร็จ', 'url' => $url, 'full_url' => $full_url], 200);

        } else {

            if ($request->input('youtube_link')) {
                $course = courses::where('course_id', $course_id)->first();
                $course->course_preview = $request->input('youtube_link');
                $course->course_preview_type = 'youtube';
                $course->save();

                return response()->json(['success' => true, 'message' => 'เพิ่มลิงค์สำเร็จ'], 200);
            }

            return response()->json(['success' => false, 'message' => 'อัพโหลดไฟล์ไม่สำเร็จ'], 200);
        }

    }
    */

    public function upload(Request $request) {

        if (Auth::user()->group_id > 2 || !Auth::check()) {
            return redirect('/e-admin/login')->with('message', 'You don\'t have permission')->with('status', 'error');
        }

        // create the file receiver
        $receiver = new FileReceiver("file", $request, HandlerFactory::classFromRequest($request));

        // check if the upload is success, throw exception or return response you need
        if ($receiver->isUploaded() === false) {
            throw new UploadMissingFileException();
        }

        // receive the file
        $save = $receiver->receive();

        // check if the upload has finished (in chunk mode it will send smaller files)
        if ($save->isFinished()) {
            // save the file and return any response you need, current example uses `move` function. If you are
            // not using move, you need to manually delete the file by unlink($save->getFile()->getPathname())
            return $this->saveFile($save->getFile());
        }

        // we are in chunk mode, lets send the current progress
        /** @var AbstractHandler $handler */
        $handler = $save->handler();

        return response()->json([
            "done" => $handler->getPercentageDone(),
        ]);
    }

    protected function saveFile(UploadedFile $file)
    {

        $input = Input::all();

        $lecture_id = $input['lecture_id'];
        $file_name = $input['filename'];

        $fileName = $this->createFilename($file, $file_name);
        // Group files by mime type
        $mime = str_replace('/', '-', $file->getMimeType());
        // Group files by the date (week
        //$dateFolder = date("Y-m-W");

        // Build the file path
        $path = "/uploads/courses/videos";
        $filePath = public_path() . $path;
        //$finalPath = storage_path("app/".$filePath);

        // move the file name
        $file->move($filePath, $fileName);

        $full_url = \URL::to('/') . '/' . $path .'/' . $fileName;

        return response()->json(['success' => true, 'mime_type' => $mime, 'message' => 'อัพโหลดไฟล์สำเร็จ', 'video_url' => $full_url], 200);

    }

    protected function createFilename(UploadedFile $file, $file_name)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = $file_name; // Filename without extension

        // Add timestamp hash to name of the file
        //$filename .= "_" . md5(time()) . "." . $extension;
        $filename .=  "." . $extension;

        return $filename;
    }

    function syncSlidePage(Request $request)
    {
        if (Auth::user()->group_id > 2 || !Auth::check()) {
            return redirect('/e-admin/login')->with('message', 'You don\'t have permission')->with('status', 'error');
        }

        $input = Input::all();

        $lecture_id = $input['lecture_id'];

        $lecture = coursecurriculum::getDetailByObjectId($lecture_id);
        $lecture->time_from_millisec = self::timeToMillisec($lecture->start_time);
        $lecture->time_to_millisec = self::timeToMillisec($lecture->end_time);
        
        $lectureSlide = courselectureslides::where('lecture_id',$lecture_id)->get();

        $this->data['lecture_id'] = $lecture_id;
        $this->data['lecture'] = $lecture;
        $this->data['lecture_slide'] = $lectureSlide;
        $this->data['title'] = 'Sync Slide';
        $this->data['content'] = $this->backend . ".lectures.syncslide";

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }


    function syncSlideSave(Request $request){

        if (Auth::user()->group_id > 2 || !Auth::check()) {
            return redirect('/e-admin/login')->with('message', 'You don\'t have permission')->with('status', 'error');
        }

        $input = Input::all();
        $lecture_id = $input['lecture_id'];
        
        if(!isset($input['slide_time'])){
            return redirect('e-admin/lectures/syncslide?lecture_id='.$lecture_id)->with('message', 'เกิดข้อผิดพลาด')->with('status', 'error');;
        }
        
        $slide_time = $input['slide_time'];
        $slide_image = Input::file('slide_image');
        

        courselectureslides::where('lecture_id', $lecture_id)->delete();  


        foreach ($slide_image as $key => $value) {

            $fileType = $value->getMimeType();

            if ($fileType !== 'image/jpeg' && $fileType !== 'image/png') {
                return response()->json(['success' => false, 'อัพโหลดไฟล์ผิดประเภท'], 200);
            }

            if ($fileType !== 'image/jpeg') {
                $ext = '.jpg';
            } else if ($fileType !== 'image/png') {
                $ext = '.png';
            }

            //Path for save and display image
            $urlPath = '/uploads/syncslide/';
            $filename = $urlPath . uniqid() . '' . $ext;

            //Path for save image
            $destinationPath = public_path().'/uploads/syncslide/';

            //Store image to /public/uploads/files/pitching
            $value->move($destinationPath, $filename);

            $url = $filename;


            $lecture_slide = new courselectureslides();
            $lecture_slide->lecture_id = $lecture_id;
            $lecture_slide->slide_time = $slide_time[$key];
            $lecture_slide->slide_image = $url;
            $lecture_slide->save();

        }

        return redirect('e-admin/lectures/syncslide?lecture_id='.$lecture_id)->with('message', 'บันทึกสำเร็จ')->with('status', 'success');;
    }


    private function timeToMillisec($input){
        $time   = explode(":", $input);

        $hour   = $time[0] * 60 * 60 * 1000;
        $minute = $time[1] * 60 * 1000;

        $second = explode(",", $time[2]);
        $sec    = $second[0] * 1000;
        //$milisec= $second[1];

        $result = $hour + $minute + $sec;

        return $result;
    }

    private function getYoutubeDuration($vid)
    {

        $apikey = "AIzaSyCFNrJcu5u5bMdKImBXgDYhtrRdqBeK9bM";

        $videoDetails = file_get_contents("https://www.googleapis.com/youtube/v3/videos?id=" . $vid . "&part=contentDetails,statistics&key=$apikey");

        $VidDuration = json_decode($videoDetails, true);
        foreach ($VidDuration['items'] as $vidTime) {
            $VidDuration = $vidTime['contentDetails']['duration'];
        }

        $pattern = '/PT(\d+)M(\d+)S/';

        preg_match($pattern, $VidDuration, $matches);

        $seconds = $matches[1] * 60 + $matches[2];

        return $seconds;
    }

    private function identify_service($url)
    {
        if (preg_match('%youtube|youtu\.be%i', $url)) {
            return 'youtube';
        } elseif (preg_match('%vimeo%i', $url)) {
            return 'vimeo';
        }
        return null;
    }

    private function get_url_id($url)
    {
        $service = self::identify_service($url);
        if ($service == 'youtube') {
            return self::get_youtube_id($url);
        } elseif ($service == 'vimeo') {
            return self::get_vimeo_id($url);
        }
        return null;
    }

    private function get_url_embed($url)
    {
        $service = self::identify_service($url);
        $id = self::get_url_id($url);
        if ($service == 'youtube') {
            return self::get_youtube_embed($id);
        } elseif ($service == 'vimeo') {
            return self::get_vimeo_embed($id);
        }
        return null;
    }

    private function get_youtube_id($url)
    {
        $youtube_url_keys = array('v', 'vi');
        // Try to get ID from url parameters
        $key_from_params = self::parse_url_for_params($url, $youtube_url_keys);
        if ($key_from_params) return $key_from_params;
        // Try to get ID from last portion of url
        return self::parse_url_for_last_element($url);
    }

    private function parse_url_for_params($url, $target_params)
    {
        parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_params);
        foreach ($target_params as $target) {
            if (array_key_exists($target, $my_array_of_params)) {
                return $my_array_of_params[$target];
            }
        }
        return null;
    }

    private function get_youtube_embed($youtube_video_id, $autoplay = 1)
    {
        $embed = "http://youtube.com/embed/$youtube_video_id?autoplay=$autoplay";
        return $embed;
    }

    private function parse_url_for_last_element($url)
    {
        $url_parts = explode("/", $url);
        $prospect = end($url_parts);
        $prospect_and_params = preg_split("/(\?|\=|\&)/", $prospect);
        if ($prospect_and_params) {
            return $prospect_and_params[0];
        } else {
            return $prospect;
        }
        return $url;
    }

    function getMP4()
    {
        if (Auth::user()->group_id > 2 || !Auth::check()) {
            return redirect('/e-admin/login')->with('message', 'You don\'t have permission')->with('status', 'error');
        }

        $courses = courselectures::where('media_type', 'mp4')->get();

        $this->data['courses'] = $courses;
        $this->data['content'] = $this->backend . ".course.videos";
        $this->data['title'] = 'วิดีโอคอร์สเรียน .mp4';

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }


}
