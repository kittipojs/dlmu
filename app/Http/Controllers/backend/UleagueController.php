<?php

namespace App\Http\Controllers\backend;

use App\Models\Uleagueuser;
use App\Models\Uleagueapply;
use App\Models\Uleagueevent;
use App\Models\Uleagueteam;
use App\Models\Uleagueteammembers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

class UleagueController extends Controller
{
    protected $data = array();
    protected $layout = "layouts.main";
    protected $backend = 'pages.backend';
    protected $template = array();

    public function __construct(){
        parent::__construct();

        $this->access = array();

        $this->data = array(
            'pageTitle' => 'Startup Thailand Academy',
            'pageNote' => 'รายงาน STL Event',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend' => 'layouts.default.template.backend'
        );

    }

    public function pitchingDetail(Request $request){

        if (!Auth::check() || Auth::user()->group_id > 2) {
            return redirect('')->with('message', 'You don\'t have permission')->with('status', 'error');
        }

        if(\Illuminate\Support\Facades\Input::get("event_id")){

            $this->data['report_title'] = 'ผลการค้นหาตามสนามแข่งขัน';

            $event_id = \Illuminate\Support\Facades\Input::get("event_id");
            $teams = uleagueapply::join('uleague_team', 'uleague_team.user_id', 'uleague_apply.user_id')
                ->join('tb_users', 'tb_users.id', 'uleague_apply.user_id')
                ->join('university', 'university.university_id', 'uleague_team.university_id')
                ->select('uleague_team.team_id', 'team_name', 'uleague_team.user_id', 'first_name', 'last_name', 'university.name', 'uleague_team.startup_sector', 'uleague_team.created_at', 'uleague_team.status')->where('uleague_apply.reference_id', $event_id)->get();
        }
        else{

            $this->data['report_title'] = 'ทีมทั้งหมด';

            $teams = uleagueapply::join('uleague_team', 'uleague_team.user_id', 'uleague_apply.user_id')
                ->join('tb_users', 'tb_users.id', 'uleague_apply.user_id')
                ->join('university', 'university.university_id', 'uleague_team.university_id')
                ->select('uleague_team.team_id', 'team_name', 'uleague_team.user_id', 'first_name', 'last_name', 'university.name', 'uleague_team.startup_sector', 'uleague_team.status')->where('uleague_apply.status', 1)->orderBy('uleague_apply.created_at', 'asc')->get();
        }

        $this->data['teams'] = $teams; 

        return view($this->backend.".uleague.details", $this->data);

    }

    public function pitchingApplyDetail(Request $request){
        if (!Auth::check() || Auth::user()->group_id > 2) {
            return redirect('')->with('message', 'You don\'t have permission')->with('status', 'error');
        }

        if(\Illuminate\Support\Facades\Input::get("event_id")){
            
            $event_id = \Illuminate\Support\Facades\Input::get("event_id");
            $apply = uleagueapply::join('uleague_team', 'uleague_team.user_id', 'uleague_apply.user_id')
                ->join('tb_users', 'tb_users.id', 'uleague_apply.user_id')
                ->join('university', 'university.university_id', 'uleague_team.university_id')
                ->select('uleague_team.team_id', 'team_name', 'uleague_team.user_id', 'first_name', 'last_name', 'university.name', 'uleague_team.startup_sector', 'uleague_team.created_at', 'uleague_team.status')->where('uleague_apply.reference_id', $event_id)->get();
            
        }


        $this->data['apply'] = $apply; 

        return view($this->backend.".team-apply.details", $this->data);
    }

    public function updateInfo($user_id, Request $request){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return response()->json(['success' => false, 'data'=>'You don\'t have permission'], 200);
        }

        $uleagueuser = Uleagueuser::where('user_id', $user_id)->first();

        $uleagueuser->nickname = $request->input('nickname');
        $uleagueuser->university_id = $request->input('university');
        $uleagueuser->faculty_id = $request->input('faculty');
        $uleagueuser->student_id = $request->input('student_id');
        $uleagueuser->sub_faculty = $request->input('sub_faculty');
        $uleagueuser->college_year = $request->input('college_year');

        $uleagueuser->save();

        return response()->json(['success' => true, 'data'=>'อัพเดทสำเร็จ'], 200);

    }

    public function uleagueInfoUpload(Request $request){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return response()->json(['success' => false, 'You don\'t have permission'], 200);
        }

        $input  = Input::all();

        $user_id  = $input['user_id'];

        if($request->hasFile('file')){
            $files = $request->file('file');

            $fileType = $files->getMimeType();

            if($fileType !== 'image/jpeg' && $fileType !== 'image/png'){
                return response()->json(['success' => false, 'อัพโหลดไฟล์ผิดประเภท'], 200);
            }

            if($fileType !== 'image/jpeg'){
                $ext = '.jpg';
            }
            else if($fileType !== 'image/png'){
                $ext = '.png';
            }

            //Path for save and display image
            $urlPath = '/uploads/uleague/photo_ids/';
            $filename = $urlPath.time()."_".$user_id.".jpg";

            //Path for save image
            $destinationPath = public_path(). "/uploads/uleague/photo_ids/";

            //Store image to /public/uploads/images/profile
            $files->move($destinationPath, $filename);
            $photo_src = $urlPath.time()."_".$user_id.".jpg";

            $uleagueuser = Uleagueuser::where('user_id', $user_id)->first();
            $uleagueuser->photo_src = $photo_src;
            $uleagueuser->save();

            return response()->json(['success' => true, 'message'=>'อัพโหลดไฟล์สำเร็จ'], 200);

        } else {
            return response()->json(['success' => false, 'message'=>'อัพโหลดไฟล์ไม่สำเร็จ'], 200);
        }

    }


}