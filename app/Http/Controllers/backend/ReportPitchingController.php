<?php

namespace App\Http\Controllers\backend;

use App\Models\Uleagueapply;
use App\Models\Uleagueteam;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReportPitchingController extends Controller
{
    protected $data = array();
    protected $layout = "layouts.main";
    protected $backend = 'pages.backend';
    protected $template = array();

    public function __construct(){
        parent::__construct();
        $this->access = array();

        $this->data = array(
            'pageTitle' => 'Startup Thailand Academy',
            'pageNote' => 'รายงาน STL Pitching',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );
    }

    public function index(){
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }
        
        $this->data['content'] = $this->backend.".uleague.search";
        $this->data['title'] = 'รายงาน U League Pitching';
        $page_template = $this->template['backend'];
        
        $this->data['all_team'] = uleagueteam::count();
        $this->data['all_apply_team'] = uleagueapply::where('status', '>=', -99)->count();
        $this->data['all_verify_team'] = uleagueapply::where('status', '>', 1)->count();
        $this->data['pitching_events'] = self::getEvents('Pitching', '');

        return view($page_template, $this->data);
    }

    public function details(){
        $param = \SiteHelpers::GetParam();

        $list = pitchingteam::join('tb_users', 'tb_users.id', 'startup_pitching_team.user_id');
        if($param->round !== ""){
            $list->where('startup_pitching_team.round_id',$param->round);
        }

        $list = $list->get();

        $this->data['row'] = $list;
        return view($this->backend.".pitching.details",$this->data);
    }

    private function getEvents($type, $email){

        $curl = new \anlutro\cURL\cURL;

        $rq = $curl->newRequest('get', config('app.zipevent_url').'/api/nia/event/uleague?type='.$type.'&email='.$email)
            ->setHeader('Content-Type', 'application/json')
            ->setHeader('Host', config('app.zipevent_host'))
            ->setHeader('client_id', config('app.zipevent_client_id'))
            ->setHeader('client_secret', config('app.zipevent_client_secret'))
            ->setHeader('device_id', '1')
            ->setHeader('Content-Length', '0');

        $ev = $rq->send();

        return json_decode($ev->body);
    }
}
