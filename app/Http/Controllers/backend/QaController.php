<?php

namespace App\Http\Controllers\backend;

use App\Models\Qa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class QaController extends Controller
{
    protected $data = array();
    protected $layout = "layouts.main";
    protected $backend = 'pages.backend';
    protected $template = array();

    public function __construct(){

        parent::__construct();

        $this->access = array();

        $this->data = array(
            'pageTitle' => 'Startup Thailand Academy',
            'pageNote' => 'Question & Answer',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );
    }

    function index(){
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $qa = qa::orderBy('order', 'asc')->get();

        $this->data['qa'] = $qa;
        $this->data['content']  = $this->backend.".qa.index";
        $this->data['title']    = 'คำถาม-คำตอบ ทั้งหมด';
        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    function addQa(){
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }
        $this->data['content'] = $this->backend.".qa.add";
        $this->data['title'] = 'เพิ่ม คำถาม-คำตอบ';
        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    function saveQa(){
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $input = Input::all();
        $question   = $input['question'];
        $answer     = $input['answer'];
        if(isset($input['status'])){
            $status = 1;
        } else {
            $status = 0;
        }
        if(isset($input['edit'])){
            $qa = qa::where('id', $input['id'])->first();
        } else {
            $qa = new qa();
            $qa->order  = qa::max('order') + 1;
        }
        $qa->question   = $question;
        $qa->answer     = $answer;
        $qa->status     = $status;
        $qa->modify_by  = Auth::user()->id;
        $qa->save();

        return redirect('e-admin/qa/view')->with(['message'=>'บันทึกสำเร็จ' ,'status'=>'success']);
    }

    function sortQa(){
        $input = Input::all();
        $sorts = $input['sort'];
        foreach ( $sorts as $key => $value){
            $qa         = qa::where('id', $value)->first();
            $qa->order  = $key + 1;
            $qa->save();
        }
        return redirect('e-admin/qa/view')->with(['message'=>'Sort success' ,'status'=>'success']);
    }

    function updateQa($id){
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $qa = qa::where('id', $id)->first();

        $this->data['qa'] = $qa;
        return view($this->backend.".qa.update", $this->data);
    }

    function upload(Request $request){
        
        $files = $request->file('file');
        
        
        //Path for save and display image
        $urlPath = '/uploads/qa/';
        $name = md5(rand(10000, 90000));
        $filename = $urlPath.$name.".jpg";

        //Path for save image
        $destinationPath = public_path(). "/uploads/qa/";

        //Store image to /public/uploads/images/profile
        $files->move($destinationPath, $filename);
        $photo_src = $urlPath.$name.".jpg";

        return response()->json(['success' => true, 'file'=>url($photo_src), 'message'=>'อัพโหลดไฟล์สำเร็จ'], 200);

    }
}
