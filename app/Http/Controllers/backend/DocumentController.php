<?php

namespace App\Http\Controllers\backend;

use App\Models\Documents;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    protected $data = array();
    protected $layout = "layouts.main";
    protected $backend = 'pages.backend';
    protected $template = array();

    public function __construct(){

        parent::__construct();

        $this->access = array();

        $this->data = array(
            'pageTitle' => 'Startup Thailand Academy',
            'pageNote' => 'Question & Answer',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );
    }

    function index(){
        
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $documents = documents::get();

        $this->data['documents'] = $documents;
        $this->data['content']  = $this->backend.".documents.index";
        $this->data['title']    = 'เอกสารสำหรับดาวน์โหลด';
        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    function getAdd(){
        
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $this->data['content']  = $this->backend.".documents.add";
        $this->data['title']    = 'เพิ่มเอกสารสำหรับดาวน์โหลด';
        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }
    
    function addDocument(Request $request){
        
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $doc = new documents();
        
        $doc['doc_title'] = $request->doc_title;

        if($request->doc_link!=''){
            $doc['doc_url'] = $request->doc_link;
        }
        else{
            $doc['doc_url'] = $request->doc_url;
        }
        
        $doc->save();
        
         return redirect('/e-admin/documents')->with('message','เพิ่มเอกสารเรียบร้อย')->with('status','success');
        
    }
    
    function deleteDocument(Request $request){
        
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }
        
        $doc = documents::find($request->doc_id);
        
        if(!$doc){
            return response()->json(['success' => false, 'message'=>'ไม่พบเอกสาร'], 400);
        }
        
        $exploded = explode('/', $doc->doc_url);
        $filename = end($exploded);
        $path = '/uploads/webdocuments/';
        
        $doc->delete();
        
        return response()->json(['success' => true, 'message'=>'ลบเอกสารสำเร็จ'], 200);
    }
    
    

    function upload(Request $request){

        if($request->hasFile('file')) {

            $files = $request->file('file');

            $extension = $files->getClientOriginalExtension();


            //Path for save and display image
            $urlPath = '/uploads/webdocuments/';
            $name = time() . '_' . md5(rand(10000, 90000));
            $filename = $urlPath . $name . "." . $extension;

            //Path for save image
            $destinationPath = public_path() . "/uploads/webdocuments/";

            $files->move($destinationPath, $filename);
            $document_src = $urlPath . $name . "." . $extension;

            return response()->json(['success' => true, 'file' => url($document_src), 'message' => 'อัพโหลดไฟล์สำเร็จ'], 200);

        }
    }
}
