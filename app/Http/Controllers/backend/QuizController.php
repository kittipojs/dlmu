<?php

namespace App\Http\Controllers\backend;

use App\Models\Courses;
use App\Models\Coursecurriculum;
use App\Models\Quiz;
use App\Models\Quizequestions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class QuizController extends Controller{


    protected $data = array();
    protected $layout = "layouts.main";
    protected $backend = 'pages.backend';
    protected $template = array();

    public function __construct(){
        parent::__construct();
        $this->access = array();

        $this->data = array(
            'pageTitle' => 'Startup Thailand Academy',
            'pageNote' => 'Quiz',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );
    }

    function index(){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('')->with('message','You don\'t have permission')->with('status','error');
        }

        $quizs = quiz::get();

        $this->data['quizs'] = $quizs;
        $this->data['content']  = $this->backend.".quiz.index";
        $this->data['title']    = 'รายการแบบทดสอบ';

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    function getList(Request $request){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message', 'You don\'t have permission')->with('status','error');
        }

        $quiz = quiz::get();

        $this->data['quiz'] = $quiz;
        $this->data['title']    = 'รายการแบบทดสอบ';

        return view($this->backend.".quiz.list", $this->data);
    }

    function addQuizPage(Request $request){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('')->with('message','You don\'t have permission')->with('status','error');
        }

        $course_id = $request->input('course_id');
        $quiz_type = $request->input('quiz_type');

        $this->data['course_id'] = $course_id;
        $this->data['quiz_type'] = $quiz_type;

        $this->data['content']= $this->backend.".quiz.add";
        $this->data['title'] = 'สร้างแบบทดสอบใหม่';

        //$page_template = $this->template['backend'];

        return view($this->data['content'], $this->data);

    }

    function saveQuiz(){

        $input = Input::all();

        $course_id = $input['course_id'];
        $quiz_type = $input['quiz_type'];
        $quiz_title = $input['quiz_title'];
        $quiz_description = $input['quiz_description'];
        $quiz_timer = $input['quiz_timer'];
        $quiz_score_to_pass = $input['quiz_score_to_pass'];
        $quiz_is_random_question = $input['quiz_is_random_question'];
        $quiz_is_random_answer = $input['quiz_is_random_answer'];

        if(isset($input['edit'])){
            $quiz = quiz::find($input['quiz_id']);
        }else{
            $quiz = new quiz();
            $quiz->quiz_id = quiz::max('quiz_id') + 1;
        }

        $quiz->course_id = $course_id;
        $quiz->quiz_title = $quiz_title;
        $quiz->quiz_description = $quiz_description;
        $quiz->quiz_type = $quiz_type;
        $quiz->quiz_timer = $quiz_timer;
        $quiz->quiz_score_to_pass = $quiz_score_to_pass;
        $quiz->quiz_is_random_question = $quiz_is_random_question;
        $quiz->quiz_is_random_answer = $quiz_is_random_answer;

        $quiz->save();

        if($quiz_type=='quiz'){
            if(!isset($input['edit'])){
                //add to cirriculum
                $courseCurriculum = new coursecurriculum();
                $courseCurriculum->course_id = $input['course_id'];
                $courseCurriculum->object_id = quiz::max('quiz_id');
                $courseCurriculum->object_type = 'quiz';
                $courseCurriculum->object_order = coursecurriculum::where('course_id', $input['course_id'])->max('object_order') + 1;
                $courseCurriculum->save();
            }
        }
        else if($quiz_type=='pre_test'){
            Courses::where('course_id', $input['course_id'])->update(['pre_test_id' => quiz::max('quiz_id')]);
        }
        else if($quiz_type=='post_test'){
            Courses::where('course_id', $input['course_id'])->update(['post_test_id' => quiz::max('quiz_id')]);
            
        }

        //return back()->with('message',"บันทึกสำเร็จ")->with('status','success');
        return response()->json(['success' => true, 'quiz_id'=>quiz::max('quiz_id'), 'message'=>'เพิ่มแบบทดสอบสำเร็จ'], 200);
    }

    function updateQuiz($id){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('')->with('message','You don\'t have permission')->with('status','error');
        }

        $quiz = quiz::where('quiz_id', $id)->first();
        $questions = quizequestions::where('quiz_id', $id)->get();


        $this->data['quiz'] = $quiz;
        $this->data['questions'] = $questions;

        return view($this->backend.".quiz.update", $this->data);
    }
}
