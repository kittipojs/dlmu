<?php

namespace App\Http\Controllers\backend;

use App\Models\Certificates;
use App\Models\Courses;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class CertificateController extends Controller
{
    protected $data = array();
    protected $layout = "layouts.main";
    protected $certificate_dir = 'pages.backend.certificate';
    protected $template = array();

    public function __construct()
    {
        parent::__construct();
        $this->access = array();

        $this->data = array(
            'pageTitle' => 'Startup Thailand Academy',
            'pageNote' => 'ใบประกาศณียบัตร',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend' => 'layouts.default.template.backend'
        );

    }

    function index(){

        //TODO
        $certificates = certificates::get();

        $this->data['content'] = $this->certificate_dir . ".index";
        $this->data['title'] = 'ใบประกาศณียบัตร';
        $this->data['certs'] = $certificates;

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);

    }

    function getAddPage(Request $request){
        
        if($request->course_id){
            $this->data['course'] = courses::where('course_id', $request->course_id)->first();
        }
        

        $this->data['content'] = $this->certificate_dir . ".add";
        $this->data['title'] = 'สร้างใบประกาศณียบัตร';

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);

    }

    function getForm(Request $request){
        
        if (is_null($request->input('cert_id'))) {
            $input = Input::all();
            $style = $input['cer_style'];
            $name = $input['cer_name'];

            $this->data['name'] = $name;
            $this->data['style'] = $style;

            $elements = $this->style($style);
        }
        else {
            $cert_id = $request->input('cert_id');
            $certificates = certificates::where('id', $cert_id)->first();
            $elements = json_decode($certificates->cert_elements);

            $this->data['name'] = $certificates->cert_name;
            $this->data['style'] = $certificates->cert_type;
            $this->data['edit'] = $certificates->id;
        }
        
        if($request->course_id){
            $this->data['course_id'] = $request->course_id;
        }

        $this->data['elements'] = $elements;

        return view($this->certificate_dir . ".form", $this->data);
    }

    function save(){

        $input = Input::all();
        $style_element = $this->style($input['style']);

        foreach ($style_element as $key => $value) {
            if (Input::hasFile($value->name)) {
                $value->value = $this->imageUrl($input[$value->name], $value->name . '_' . uniqid());
            } else {
                $value->value = $input[$value->name];
            }
        }

        $element = json_encode($style_element, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

        if(isset($input['edit'])){
            $certificate = certificates::where('id', $input['edit'])->first();
        }
        else {
            $certificate = new certificates();
        }

        $certificate->cert_name = $input['name'];
        $certificate->cert_type = $input['style'];
        $certificate->cert_elements = $element;
        $certificate->save();
        
        $course_id = $input['course_id'];
        
        courses::where('course_id', $course_id)->update(['course_certificate_id' => certificates::max('id')]);
        
        return response()->json(['success' => false, 'data'=>array('redirect'=>url('e-admin/course/manage/'.$course_id))], 200);

    }


    function indexPreview(Request $request){

        $cert_id = $request->input('cert_id');
        $certificates = certificates::where('id', $cert_id)->first();
        $elements = json_decode($certificates->cert_elements);
        $this->data['elements'] = $elements;

        return view($this->certificate_dir . '.preview', $this->data);
    }


    function preview(){

        $input = Input::all();
        
        $style_element = $this->setElementValue($input);
        $this->data['elements'] = $style_element;

        return view($this->certificate_dir . '.preview', $this->data);

    }

    private function imageUrl($file, $file_name){
        $files = $file;

        $fileType = $files->getMimeType();

        if ($fileType !== 'image/jpeg' && $fileType !== 'image/png') {
            return back()->with('message', 'อัพโหลดไฟล์ผิดประเภท')->with('status', 'error');
        }

        if ($fileType !== 'image/jpeg') {
            $ext = '.jpg';
        } else if ($fileType !== 'image/png') {
            $ext = '.png';
        }
        //Path for save and display image
        $urlPath = '/uploads/certificate/logo/';
        $filename = $urlPath . $file_name . '' . $ext;

        //Path for save image
        $destinationPath = public_path() . '/uploads/certificate/logo/';

        //Store image to /public/uploads/files/pitching
        $files->move($destinationPath, $filename);

        $url = $urlPath . $file_name . '' . $ext;

        return $url;

    }

    function style($style){
        if ($style == 'A') {
            return json_decode('[
                      {
                        "name": "logo_a",
                        "title": "โลโก้ A ขนาด 400x400px",
                        "type": "image",
                        "size_w" : "400px",
                        "size_h" : "400px",
                        "value": "",
                        "position_x": "180px",
                        "position_y": "180px"
                      },
                      {
                        "name": "logo_b",
                        "title": "โลโก้ B ขนาด 400x400px",
                        "type": "image",
                        "size_w" : "400px",
                        "size_h" : "400px",
                        "value": "",
                        "position_x": "2900px",
                        "position_y": "180px",
                        "cssStyle": "color:red; font-weight:bold;"
                      },
                      {
                        "name": "body_a",
                        "title": "ข้อความ A",
                        "type": "text",
                        "value": "",
                        "position_x": "auto",
                        "position_y": "220px",
                        "cssStyle": "color:black; font-size:120px; left:auto; right:auto; width:100%; text-align:center;"
                      },
                      {
                        "name": "body_b",
                        "title": "ข้อความ B",
                        "type": "text",
                        "value": "",
                        "position_x": "auto",
                        "position_y": "390px",
                        "cssStyle": "color:black; font-size:120px; left:auto; right:auto; width:100%; text-align:center;"
                      },
                      {
                        "name": "fullname",
                        "title": "ชื่อ-นามสกุล",
                        "type": "text",
                        "value": "",
                        "position_x": "auto",
                        "position_y": "45%",
                        "cssStyle": "color:black; font-size:200px; left:auto; right:auto; width:100%; text-align:center;"
                      },
                      {
                        "name": "footer_a",
                        "title": "ข้อความด้านล่าง A",
                        "type": "text",
                        "value": "",
                        "position_x": "auto",
                        "position_y": "1880px",
                        "cssStyle": "color:black; font-size:120px; left:auto; right:auto; width:100%; text-align:center;"
                      },
                      {
                        "name": "footer_b",
                        "title": "ข้อความด้านล่าง B",
                        "type": "text",
                        "value": "",
                        "position_x": "auto",
                        "position_y": "2180px",
                        "cssStyle": "color:black; font-size:100px; left:auto; right:auto; width:100%; text-align:center;"
                      }
                    ]');
        } 
        elseif ($style == 'B') {
            return json_decode('[
                      {
                        "name": "logo_a",
                        "title": "โลโก้ A ขนาด 400x300px",
                        "type": "image",
                        "size_w" : "100%",
                        "size_h" : "",
                        "value": "",
                        "position_x": "1573px",
                        "position_y": "180px"
                      },
                      {
                        "name": "body_a",
                        "title": "ข้อความ A",
                        "type": "text",
                        "value": "วุฒิบัติฉบับนี้ให้เพื่อแสดงว่า",
                        "position_x": "auto",
                        "position_y": "580px",
                        "cssStyle": "color:black; font-size:120px; left:auto; right:auto; width:100%; text-align:center;"
                      },
                      {
                        "name": "fullname",
                        "title": "ชื่อ-นามสกุล",
                        "type": "text",
                        "value": "%first_name% %last_name%",
                        "position_x": "auto",
                        "position_y": "800px",
                        "cssStyle": "color:black; font-size:200px; left:auto; right:auto; width:100%; text-align:center;"
                      },
                      {
                        "name": "body_b",
                        "title": "ข้อความ B",
                        "type": "text",
                        "value": "ผ่านการเรียน %course_title% ควบถ้วนตามหลักสูตร",
                        "position_x": "auto",
                        "position_y": "1200px",
                        "cssStyle": "color:black; font-size:120px; left:auto; right:auto; width:100%; text-align:center;"
                      },
                      {
                        "name": "footer_a",
                        "title": "ข้อความด้านล่าง A",
                        "type": "text",
                        "value": "นาย...........................................",
                        "position_x": "auto",
                        "position_y": "1840px",
                        "cssStyle": "color:black; font-size:100px; left:auto; right:auto; width:100%; text-align:center;"
                      },
                      {
                        "name": "footer_b",
                        "title": "ข้อความด้านล่าง B",
                        "type": "text",
                        "value": "ตำแหน่ง.............................................",
                        "position_x": "auto",
                        "position_y": "2000px",
                        "cssStyle": "color:black; font-size:100px; left:auto; right:auto; width:100%; text-align:center;"
                      },
                      {
                        "name": "footer_c",
                        "title": "ข้อความด้านล่าง C",
                        "type": "text",
                        "value": "วุฒิบัติฉบับนี้ถูกสร้างขึ้นโดยระบบอัติโนมัติ Startup Thailand",
                        "position_x": "auto",
                        "position_y": "2200px",
                        "cssStyle": "color:black; font-size:80px; left:auto; right:auto; width:100%; text-align:center;"
                      },
                       {
                        "name": "footer_d",
                        "title": "ข้อความด้านล่าง D",
                        "type": "text",
                        "value": "สอบถามข้อมูลเพิ่มเติม กรุณาติดต่อ คุณพัชรีนาถ โทร 02 017 5555 ต่อ 209  อีเมล : startup@nia@or.th",
                        "position_x": "auto",
                        "position_y": "2300px",
                        "cssStyle": "color:black; font-size:80px; left:auto; right:auto; width:100%; text-align:center;"
                      }
                    ]');
        }
        
        else {
            return json_decode('[
  {
    "name": "logo_a",
    "type": "image",
    "value": "",
    "position_x": "100px",
    "position_y": "20px",
    "width": "150px"
  },
  {
    "name": "logo_b",
    "type": "image",
    "value": "",
    "position_x": "0px",
    "position_y": "300px",
    "width": "150px"
  },
  {
    "name": "body_a",
    "type": "text",
    "value": "",
    "position_x": "100px",
    "position_y": "60px",
    "cssStyle": "color:blue"
  },
  {
    "name": "body_b",
    "type": "text",
    "value": "",
    "position_x": "100px",
    "position_y": "100px"
  },
  {
    "name": "body_c",
    "type": "text",
    "value": "",
    "position_x": "100px",
    "position_y": "600px"
  },
  {
    "name": "footer_a",
    "type": "text",
    "value": "",
    "position_x": "100px",
    "position_y": "300px"
  },
  {
    "name": "footer_b",
    "type": "text",
    "value": "",
    "position_x": "300px",
    "position_y": "300px",
    "cssStyle": "color:red"
  }
]');
        }
    }

    function setElementValue($input){
        
        $style_element = $this->style($input['style']);
        
        foreach ($style_element as $key => $value) {
            
            if (Input::hasFile($value->name)) {
                $value->value = $this->imageUrl($input[$value->name], $value->name . '_' . uniqid());
            } else {
                $value->value = $input[$value->name];
            }
        }
        
        return $style_element;
    }
}
