<?php

namespace App\Http\Controllers\backend;

use App\Models\Certificates;
use App\Models\Events;
use App\Models\Eventcertificates;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class EventController extends Controller
{
    protected $data = array();
    protected $layout = "layouts.main";
    //protected $certificate_dir = 'pages.backend.event.certificate';
    protected $backend = 'pages.backend';
    protected $template = array();

    public function __construct(){

        parent::__construct();

        $this->access = array();

        $this->data = array(
            'pageTitle' => 'Startup Thailand Academy',
            'pageNote' => 'อีเว้นท์ทั้งหมด',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );
    }

    public function index(){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $this->data['content']      = $this->backend.".event.manage";
        $this->data['title']        = 'จัดการอีเว้นท์';
        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    public function certificates(){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $this->data['content']      = $this->backend.".event.certificates";
        $this->data['title']        = 'ประกาศณียบัตรทั้งหมด';
        $page_template = $this->template['backend'];

        $ec = eventcertificates::select('certificates.cert_name', 'certificates.cert_type', 'event_certificates.*')
            ->join('certificates', 'certificates.id', 'event_certificates.certificates_id')->get();

        $this->data['certs'] = $ec;

        return view($page_template, $this->data);
    }

    public function details(){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message', 'You don\'t have permission')->with('status','error');
        }

        $hl_events = Events::get();
        $highlights = array();

        foreach($hl_events as $event){
            $highlights[] = $event->event_id;
        }

        $this->data['events'] = array();

        $start = \Illuminate\Support\Facades\Input::get("start");
        $end = \Illuminate\Support\Facades\Input::get("end");

        $date_start = new \DateTime($start);
        $date_end = new \DateTime($end);

        //$events = self::getEvents(60, 1, $date_start->getTimestamp(), $date_end->getTimestamp());
        $ev = self::searchEvents('?pagesize=40&page=1&type=&isPast=true&start='.$date_start->getTimestamp().'&end='.$date_end->getTimestamp());
        foreach($ev as $item){
            $events[] = $item;
        }

        $ev = self::searchEvents('?pagesize=40&page=1&type=&isPast=false&start='.$date_start->getTimestamp().'&end='.$date_end->getTimestamp());
        foreach($ev as $item){
            $events[] = $item;
        }
        
        $ev = self::searchEvents('?pagesize=40&page=1&type=pitching&isPast=true&start='.$date_start->getTimestamp().'&end='.$date_end->getTimestamp());
        foreach($ev as $item){
            $events[] = $item;
        }
        
        $ev = self::searchEvents('?pagesize=40&page=1&type=pitching&isPast=false&start='.$date_start->getTimestamp().'&end='.$date_end->getTimestamp());
        foreach($ev as $item){
            $events[] = $item;
        }

        foreach ($events as $item) {

            $e = array();

            $e['id'] = $item->id;
            $e['name'] = $item->name;
            $e['venue'] = $item->venue;
            $e['type'] = $item->type;
            $e['start_date'] = self::dateThai( date('Y-m-d H:i:s', $item->start_date) );
            $e['end_date'] = self::dateThai( date('Y-m-d H:i:s', $item->end_date) );
            $e['registration'] = $item->registration;
            $e['remain'] = $item->registration-$item->total_registration;
            $e['cover'] = asset('/frontend/medilab/img/240_F_130344852_Q7IAj4dePv6kJqlqnBGuqfXzcapCSNEi.jpg');
            if(isset($item->cover)){
                $e['cover'] = $item->cover;
            }

            $e['url'] = $item->url;
            $e['is_register'] = $item->is_register;
            $e['is_checkin'] = $item->is_checkin;
            $e['type_text'] = $item->type;

            if($item->type=='Camp'){
                $e['type_text'] = 'Camp  กลาง';
            }
            else if($item->type=='UCamp'){
                $e['type_text'] = 'Camp  ของมหาวิทยาลัย';
            }

            $e['is_highlight'] = false;
            if( in_array($e['id'], $highlights) ){
                $e['is_highlight'] = true;
            }

            $this->data['events'][] = $e;
        }

        return view($this->backend.".event.details", $this->data);

    }

    public function addEvent(Request $request){
        if(!Auth::check() || Auth::user()->group_id > 2){
            return response()->json(['success' => false, 'message'=>'You don\'t have permission'], 200);
        }

        $count = Events::count();

        if($count<999){
            $event = new Events();

            $event->event_id = $request->event_id;

            $event->save();
        }
        else{
            return response()->json(['success' => false, 'message'=>'ตั้ง Hightlight อีเว้นท์ได้สูงสุด 3 อีเว้นท์'], 200);
        }

        return response()->json(['success' => true, 'message'=>'เพิ่มอีเว้นท์สำเร็จ'], 200);

    }

    public function deleteEvent(Request $request){
        if(!Auth::check() || Auth::user()->group_id > 2){
            return response()->json(['success' => false, 'message'=>'You don\'t have permission'], 200);
        }

        $event = Events::where('event_id', $request->event_id);
        $event->delete();

        return response()->json(['success' => true, 'message'=>'ลบอีเว้นท์สำเร็จ'], 200);

    }

    public function getAddPage(){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $this->data['content'] = $this->backend.".event.add-certificate";
        $this->data['title'] = 'สร้างใบประกาศณียบัตร';
        $this->data['events'] = array();

        $events = self::searchEvents('?pagesize=40&page=1&type=&isPast=false');
        foreach($events as $item){
            $this->data['events'][] = $item;
        }

        $events = self::searchEvents('?pagesize=40&page=1&type=&isPast=true');
        foreach($events as $item){
            $this->data['events'][] = $item;
        }

        $events = self::searchEvents('?pagesize=40&page=1&type=pitching&isPast=false');
        foreach($events as $item){
            $this->data['events'][] = $item;
        }

        $events = self::searchEvents('?pagesize=40&page=1&type=pitching&isPast=true');
        foreach($events as $item){
            $this->data['events'][] = $item;
        }

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    function getEditPage($certificates_id, Request $request){

        $this->data['content'] = $this->backend.".event.edit-certificate";
        $this->data['title'] = 'แก้ไขใบประกาศณียบัตร';

        $ec = eventcertificates::select('certificates.cert_name', 'certificates.cert_type', 'certificates.cert_elements', 'event_certificates.*')
            ->where('event_certificates.id', $certificates_id)
            ->join('certificates', 'certificates.id', 'event_certificates.certificates_id')
            ->first();

        $this->data['cert'] = $ec;
        $elements = json_decode($ec->cert_elements);
        $this->data['elements'] = $elements;

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    function getForm(Request $request){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $this->data['event_id'] = $request->input('event_id');


        if (is_null($request->input('cert_id'))) {
            $input = Input::all();
            $style = $input['cer_style'];
            $name = $input['cer_name'];

            $this->data['name'] = $name;
            $this->data['style'] = $style;

            $elements = $this->style($style);
        }
        else {
            $cert_id = $request->input('cert_id');
            $certificates = certificates::where('id', $cert_id)->first();
            $elements = json_decode($certificates->cert_elements);

            $this->data['name'] = $certificates->cert_name;
            $this->data['style'] = $certificates->cert_type;
            $this->data['edit'] = $certificates->id;
        }

        $this->data['elements'] = $elements;

        return view("pages.backend.event.form-certificate", $this->data);
    }

    function save(){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $input = Input::all();
        $style_element = $this->style($input['style']);

        foreach ($style_element as $key => $value) {
            if (Input::hasFile($value->name)) {
                $value->value = $this->imageUrl($input[$value->name], $value->name . '_' . uniqid());
            } else {
                $value->value = $input[$value->name];
            }
        }

        $element = json_encode($style_element, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

        if(isset($input['edit'])){
            $certificate = certificates::where('id', $input['edit'])->first();
        }
        else {
            $certificate = new certificates();
        }

        $certificate->cert_name = $input['name'];
        $certificate->cert_type = $input['style'];
        $certificate->cert_elements = $element;
        $certificate->save();

        if($input['event_id']){
            //add to event_certificates table

            $e = eventcertificates::where('event_id', $input['event_id']);

            if(!$e->first()){
                $ec = new eventcertificates();
                $ec->event_id = $input['event_id'];
                $ec->certificates_id = certificates::max('id');
                $ec->save();
            }
            else{

                eventcertificates::where('event_id', $input['event_id'])->update(['certificates_id' => certificates::max('id')]);
            }
        }

    }


    function indexPreview(Request $request){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $cert_id = $request->input('cert_id');
        $certificates = certificates::where('id', $cert_id)->first();
        $elements = json_decode($certificates->cert_elements);
        $this->data['elements'] = $elements;

        return view($this->certificate_dir . '.preview', $this->data);
    }

    function addPreview(){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $input = Input::all();
        $style_element = $this->setElementValue($input);
        $this->data['elements'] = $style_element;

        return view("pages.backend.event.preview-certificate", $this->data);
    }

    private function getEvents($pagesize, $page, $start, $end){

        $curl = new \anlutro\cURL\cURL;

        $rq = $curl->newRequest('get', config('app.zipevent_url').'/api/nia/event/search?pagesize='.$pagesize.'&page='.$page.'&start='.$start.'&end='.$end)
            ->setHeader('Content-Type', 'application/json')
            ->setHeader('Host', config('app.zipevent_host'))
            ->setHeader('client_id', config('app.zipevent_client_id'))
            ->setHeader('client_secret', config('app.zipevent_client_secret'))
            ->setHeader('device_id', '1')
            ->setHeader('Content-Length', '0');

        $ev = $rq->send();

        return json_decode($ev->body);
    }

    private function searchEvents($params){

        $curl = new \anlutro\cURL\cURL;

        $rq = $curl->newRequest('get', config('app.zipevent_url').'/api/nia/event/search'.$params)
            ->setHeader('Content-Type', 'application/json')
            ->setHeader('Host', config('app.zipevent_host'))
            ->setHeader('client_id', config('app.zipevent_client_id'))
            ->setHeader('client_secret', config('app.zipevent_client_secret'))
            ->setHeader('device_id', '1')
            ->setHeader('Content-Length', '0');

        $ev = $rq->send();

        return json_decode($ev->body);
    }

    private function dateThai($strDate){
        $strYear = date("Y", strtotime($strDate)) + 543;
        $strMonth = date("n", strtotime($strDate));
        $strDay = date("j", strtotime($strDate));
        $strHour = date("H", strtotime($strDate));
        $strMinute = date("i", strtotime($strDate));
        $strSeconds = date("s", strtotime($strDate));

        $strMonthCut = Array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");

        $strMonthThai = $strMonthCut[$strMonth];

        return "$strDay $strMonthThai $strYear, $strHour:$strMinute";
    }

    private function imageUrl($file, $file_name){
        $files = $file;

        $fileType = $files->getMimeType();

        if ($fileType !== 'image/jpeg' && $fileType !== 'image/png') {
            return back()->with('message', 'อัพโหลดไฟล์ผิดประเภท')->with('status', 'error');
        }

        if ($fileType !== 'image/jpeg') {
            $ext = '.jpg';
        } else if ($fileType !== 'image/png') {
            $ext = '.png';
        }
        //Path for save and display image
        $urlPath = '/uploads/certificate/logo/';
        $filename = $urlPath . $file_name . '' . $ext;

        //Path for save image
        $destinationPath = public_path() . '/uploads/certificate/logo/';

        //Store image to /public/uploads/files/pitching
        $files->move($destinationPath, $filename);

        $url = $urlPath . $file_name . '' . $ext;

        return $url;

    }

    function style($style){
        if ($style == 'A') {
            return json_decode('[
  {
    "name": "logo_a",
    "title": "โลโก้ A ขนาด 400x400px",
    "type": "image",
    "size_w" : "400px",
    "size_h" : "400px",
    "value": "",
    "position_x": "180px",
    "position_y": "180px"
  },
  {
    "name": "logo_b",
    "title": "โลโก้ B ขนาด 400x400px",
    "type": "image",
    "size_w" : "400px",
    "size_h" : "400px",
    "value": "",
    "position_x": "2900px",
    "position_y": "180px",
    "cssStyle": "color:red; font-weight:bold;"
  },
  {
    "name": "body_a",
    "title": "ข้อความ A",
    "type": "text",
    "value": "",
    "position_x": "auto",
    "position_y": "220px",
    "cssStyle": "color:black; font-size:120px; left:auto; right:auto; width:100%; text-align:center;"
  },
  {
    "name": "body_b",
    "title": "ข้อความ B",
    "type": "text",
    "value": "",
    "position_x": "auto",
    "position_y": "390px",
    "cssStyle": "color:black; font-size:120px; left:auto; right:auto; width:100%; text-align:center;"
  },
  {
    "name": "fullname",
    "title": "ชื่อ-นามสกุล",
    "type": "text",
    "value": "",
    "position_x": "auto",
    "position_y": "45%",
    "cssStyle": "color:black; font-size:200px; left:auto; right:auto; width:100%; text-align:center;"
  },
  {
    "name": "footer_a",
    "title": "ข้อความด้านล่าง A",
    "type": "text",
    "value": "",
    "position_x": "auto",
    "position_y": "1880px",
    "cssStyle": "color:black; font-size:120px; left:auto; right:auto; width:100%; text-align:center;"
  },
  {
    "name": "footer_b",
    "title": "ข้อความด้านล่าง B",
    "type": "text",
    "value": "",
    "position_x": "auto",
    "position_y": "2180px",
    "cssStyle": "color:black; font-size:100px; left:auto; right:auto; width:100%; text-align:center;"
  }
]');
        } else {
            return json_decode('[
  {
    "name": "logo_a",
    "type": "image",
    "value": "",
    "position_x": "100px",
    "position_y": "20px",
    "width": "150px"
  },
  {
    "name": "logo_b",
    "type": "image",
    "value": "",
    "position_x": "0px",
    "position_y": "300px",
    "width": "150px"
  },
  {
    "name": "body_a",
    "type": "text",
    "value": "",
    "position_x": "100px",
    "position_y": "60px",
    "cssStyle": "color:blue"
  },
  {
    "name": "body_b",
    "type": "text",
    "value": "",
    "position_x": "100px",
    "position_y": "100px"
  },
  {
    "name": "body_c",
    "type": "text",
    "value": "",
    "position_x": "100px",
    "position_y": "600px"
  },
  {
    "name": "footer_a",
    "type": "text",
    "value": "",
    "position_x": "100px",
    "position_y": "300px"
  },
  {
    "name": "footer_b",
    "type": "text",
    "value": "",
    "position_x": "300px",
    "position_y": "300px",
    "cssStyle": "color:red"
  }
]');
        }
    }

    function setElementValue($input){
        $style_element = $this->style($input['style']);
        foreach ($style_element as $key => $value) {
            if (Input::hasFile($value->name)) {
                $value->value = $this->imageUrl($input[$value->name], $value->name . '_' . uniqid());
            } else {
                $value->value = $input[$value->name];
            }
        }
        return $style_element;
    }

}