<?php

namespace App\Http\Controllers\backend;

use App\Models\Camping;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ReportCampingController extends Controller
{
    protected $data = array();
    protected $layout = "layouts.main";
    protected $backend = 'pages.backend';
    protected $template = array();

    public function __construct()
    {
        parent::__construct();
        
        $this->access = array();

        $this->data = array(
            'pageTitle' => 'Startup Thailand Academy',
            'pageNote' => 'รายงาน STL Camping',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );

    }

    public function index()
    {
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('')->with('message','You don\'t have permission')->with('status','error');
        }
        $this->data['content'] = $this->backend.".camping.report";
        $this->data['title'] = 'รายงาน STL Camping';
        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    public function details()
    {
        $param = \SiteHelpers::GetParam();

        $list = camping::join('tb_users', 'tb_users.id', 'startup_camping.user_id')
            ->join('user_nia', 'user_nia.user_id', 'startup_camping.user_id')
            ->join('startup_camping_round', 'startup_camping_round.round_id', 'startup_camping.round_id')
            ->join('university', 'university.university_id', 'startup_camping.university_id')
            ->join('faculty', 'faculty.faculty_id', 'startup_camping.faculty_id');

        if($param->round !== ""){
            $list->where('startup_camping.round_id',$param->round);
        }
        $list = $list->get();

        $this->data['row'] = $list;
        
        return view($this->backend.".camping.details",$this->data);
    }

}
