<?php

namespace App\Http\Controllers\backend;

use App\Models\Uleagueuser;
use App\Models\Usernia;
use App\Models\Uleagueteammembers;
use App\Models\Uleagueteam;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller{
    protected $data = array();
    protected $layout = "layouts.main";
    protected $backend = 'pages.backend';
    protected $template = array();

    public function __construct(){
        parent::__construct();

        $this->access = array();

        $this->data = array(
            'pageTitle' => 'Startup Thailand Academy',
            'pageNote' => 'รายงานสมาชิก',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );

    }

    public function index(){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $this->data['nia_user']       = usernia::count();
        $this->data['uleague_user']   = uleagueuser::count();
        $this->data['today_registerd'] = uleagueuser::whereDate('created_at', DB::raw('CURDATE()'))->count();

        $this->data['content']      = $this->backend.".user.report";
        $this->data['title']        = 'รายงานสมาชิก';

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    public function result(){
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message', 'You don\'t have permission')->with('status','error');
        }

        //query all nia user
        $user_nia = usernia::join('tb_users', 'tb_users.id', 'user_nia.user_id')->join('thailand_provinces', 'thailand_provinces.code', 'user_nia.province_id');

        //get filter from param
        $param      = \SiteHelpers::GetParam();
        $isFirst    = true;
        foreach ( $param as $key => $val){
            //first loop is 'round' but table nia_user does not exist this field.
            if($isFirst){
                $isFirst = false;
                continue;
            }
            //filter data from val. key is field name
            if($val !== ""){
                $user_nia = $user_nia->where($key, $val);
            }
        }

        $user_nia = $user_nia->get();
        $this->data['users'] = $user_nia;

        return view($this->backend.".user.details", $this->data);
    }

    public function today(){
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message', 'You don\'t have permission')->with('status','error');
        }

        //query all nia user
        $user_nia = usernia::join('tb_users', 'tb_users.id', 'user_nia.user_id')->join('thailand_provinces', 'thailand_provinces.code', 'user_nia.province_id')->whereDate('tb_users.created_at', DB::raw('CURDATE()'))->get();

        $this->data['users'] = $user_nia;

        return view($this->backend.".user.details", $this->data);
    }

    public function getInfo($user_id){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message', 'You don\'t have permission')->with('status','error');
        }

        $user = usernia::join('tb_users', 'tb_users.id', 'user_nia.user_id')->join('thailand_provinces', 'thailand_provinces.code', 'user_nia.province_id')->where('tb_users.id', $user_id)->first();

        $uleague = uleagueuser::where('user_id', $user_id)->first();

        $this->data['user'] = $user;
        $this->data['user_id'] = $user_id;
        $this->data['uleague'] = null;
        if($uleague){
            $this->data['uleague'] = $uleague;
        }
        $this->data['events'] = [];

        $my_orders = self::getMyOrders( $user->email );

        foreach ($my_orders as $order) {

            $item = $order->event;

            $e = array();

            $e['id'] = $item->id;
            $e['name'] = $item->name;
            $e['venue'] = $item->venue;
            $e['type'] = $item->type;
            $e['start_date'] = self::dateThai( date('Y-m-d H:i:s', $item->start_date) );
            $e['end_date'] = self::dateThai( date('Y-m-d H:i:s', $item->end_date) );
            $e['registration'] = $item->registration;
            $e['remain'] = $item->registration-$item->total_registration;
            $e['cover'] = asset('/frontend/medilab/img/event-banner.jpg');
            if(isset($item->cover)){
                $e['cover'] = $item->cover;
            }

            $e['url'] = $item->url;
            $e['is_register'] = $item->is_register;
            $e['is_checkin'] = $item->is_checkin;
            $e['type_text'] = $item->type;

            if($item->type=='Camp'){
                $e['type_text'] = 'Camp  กลาง';
            }
            else if($item->type=='UCamp'){
                $e['type_text'] = 'Camp  ของมหาวิทยาลัย';
            }

            $this->data['events'][] = $e;
        }
        
        $this->data['team'] = null;
        $this->data['is_member'] = null;

        $is_member = uleagueteammembers::where('user_id', $user_id)->first();

        if($is_member){
            $team = uleagueteam::where('team_id', $is_member->team_id)->first();
        $this->data['team'] = $team;
        }
        
        $this->data['is_member'] = $is_member;

        return view($this->backend.".user.info", $this->data);

    }

    public function updateInfo($user_id, Request $request){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return response()->json(['success' => false, 'data'=>'You don\'t have permission'], 200);
        }

        $user = User::find($user_id);
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $user->dob = $request->input('dob');

        if($request->input('name_title') === "นาย"){
            $sex = 'm';
        } else {
            $sex = 'f';
        }

        $user->gender = $sex;

        $nia_user = usernia::where('user_id', $user_id)->first();

        if(!$nia_user){
            $nia_user = new usernia();
            $nia_user->user_id = Auth::user()->id;
        }

        $nia_user->name_title = $request->input('name_title');
        $nia_user->citizen_id = $request->input('citizen_id');
        $nia_user->education = $request->input('education');
        $nia_user->occupation = $request->input('occupation');
        $nia_user->faculty_id = $request->input('faculty_id');
        $nia_user->sub_faculty = $request->input('sub_faculty');
        $nia_user->address = $request->input('address');
        $nia_user->mobile_number = $request->input('mobile_number');
        $nia_user->province_id = $request->input('province_id');
        $nia_user->postcode = $request->input('postcode');

        $user->save();
        $nia_user->save();

        return response()->json(['success' => true, 'data'=>'อัพเดทสำเร็จ'], 200);

    }

    public function search(){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $this->data['content']      = $this->backend.".user.search";
        $this->data['title']        = 'ค้นหาสมาชิก';
        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }


    public function userDetails(){
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message', 'You don\'t have permission')->with('status','error');
        }
        //query all nia user
        $user_nia = usernia::join('tb_users', 'tb_users.id', 'user_nia.user_id')->join('thailand_provinces', 'thailand_provinces.code', 'user_nia.province_id');

        //get filter from param
        $param      = \SiteHelpers::GetParam();
        foreach ( $param as $key => $val){
            //filter data from val. key is field name
            if($val !== ""){
                $user_nia = $user_nia->orWhere($key, 'like', '%'.$val.'%');
            }
        }

        $user_nia = $user_nia->get();
        $this->data['users'] = $user_nia;

        return view($this->backend.".user.details_delete", $this->data);
    }

    function delete($id){
        User::where('id', $id)->delete();
        usernia::where('user_id',$id)->delete();

        return response()->json(['success' => true, 'data'=>'ลบสำเร็จ'], 200);
    }

    function getMyOrders($email){

        $curl = new \anlutro\cURL\cURL;

        $rq = $curl->newRequest('get', config('app.zipevent_url').'/api/nia/ticket/list?email='.$email)
            ->setHeader('Content-Type', 'application/json')
            ->setHeader('Host', config('app.zipevent_host'))
            ->setHeader('client_id', config('app.zipevent_client_id'))
            ->setHeader('client_secret', config('app.zipevent_client_secret'))
            ->setHeader('device_id', '1')
            ->setHeader('Content-Length', '0');

        $ev = $rq->send();

        return json_decode($ev->body);
    }

    private function dateThai($strDate){
        $strYear = date("Y", strtotime($strDate)) + 543;
        $strMonth = date("n", strtotime($strDate));
        $strDay = date("j", strtotime($strDate));
        $strHour = date("H", strtotime($strDate));
        $strMinute = date("i", strtotime($strDate));
        $strSeconds = date("s", strtotime($strDate));

        $strMonthCut = Array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");

        $strMonthThai = $strMonthCut[$strMonth];

        return "$strDay $strMonthThai $strYear, $strHour:$strMinute";
    }
}
