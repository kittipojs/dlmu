<?php

namespace App\Http\Controllers\backend;

use App\User;
use App\Models\Usernia;
use App\Models\Uleagueuser;
use App\Models\Uleagueteam;
use App\Models\Info;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\Console\Input\InputOption;

class InfoController extends Controller
{
    protected $data = array();
    protected $layout = "layouts.main";
    protected $backend = 'pages.backend';
    protected $template = array();

    public function __construct(){
        parent::__construct();
        $this->access = array();

        $this->data = array(
            'pageTitle' => 'Startup Thailand Academy',
            'pageNote' => 'example note',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );

    }

    function index(){
        
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }
        
        $info = Info::find(1);

        $this->data['content'] = $this->backend.".info.view";
        $this->data['title'] = 'แก้ไขหน้า Info';
        $this->data['info'] = $info;

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    function saveInfo(Request $request){
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $input = Input::all();
        $info_detail  = $input['info_detail'];
        
        $info = Info::find(1);
        $info->info_detail = $info_detail;
        $info->save();

        return redirect('e-admin/info/view')->with(['message'=>'บันทึกสำเร็จ' ,'status'=>'success']);
    }
    
    
    function upload(Request $request){
        
        $files = $request->file('file');
        
        
        //Path for save and display image
        $urlPath = '/uploads/info/';
        $name = md5(rand(10000, 90000));
        $filename = $urlPath.$name.".jpg";

        //Path for save image
        $destinationPath = public_path(). "/uploads/info/";

        //Store image to /public/uploads/images/profile
        $files->move($destinationPath, $filename);
        $photo_src = $urlPath.$name.".jpg";

        return response()->json(['success' => true, 'file'=>url($photo_src), 'message'=>'อัพโหลดไฟล์สำเร็จ'], 200);

    }

}
