<?php

namespace App\Http\Controllers\backend;

use App\Models\Uleagueapply;
use App\Models\Uleagueevent;
use App\Models\Uleagueteam;
use App\Models\Uleagueteammembers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UleagueEventController extends Controller
{
    protected $data = array();
    protected $layout = "layouts.main";
    protected $backend = 'pages.backend';
    protected $template = array();

    public function __construct()
    {
        parent::__construct();

        $this->access = array();

        $this->data = array(
            'pageTitle' => 'Startup Thailand Academy',
            'pageNote' => 'รายงาน STL Event',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend' => 'layouts.default.template.backend'
        );

    }

    public function index(){
        if (!Auth::check() || Auth::user()->group_id > 2) {
            return redirect('')->with('message', 'You don\'t have permission')->with('status', 'error');
        }
        
        $this->data['content'] = $this->backend . ".event.report";
        $this->data['title'] = 'รายงาน U League 2018';
        
        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    public function details(){
        
        if (!Auth::check() || Auth::user()->group_id > 2) {
            return redirect('')->with('message', 'You don\'t have permission')->with('status', 'error');
        }
        
        $param = \SiteHelpers::GetParam();

        $list = uleagueapply::select('*', 'uleague_apply.status as apply_status')
            ->join('tb_users', 'tb_users.id', 'uleague_apply.user_id')
            ->join('uleague_user', 'uleague_user.user_id', 'uleague_apply.user_id')
            ->join('university', 'university.university_id', 'uleague_user.university_id')
            ->join('faculty', 'faculty.faculty_id', 'uleague_user.faculty_id')
            ->join('uleague_events', 'uleague_events.event_id', 'uleague_apply.reference_id')
            ->leftJoin('uleague_team', 'uleague_team.user_id', 'uleague_apply.user_id');
        
        //filter event from view param
        if ($param->round !== "") {
            $list->where('uleague_events.event_id', $param->round);
            $list = $list->get();
            //check result list event
            if (count($list) > 0) {
                if ($list[0]->event_type === 'camp') {
                    $event_type = 'camp';
                } elseif ($list[0]->event_type === 'pitching') {
                    $event_type = 'pitching';
                } else {
                    $event_type = null;
                }
            } else {
                $list = null;
                $event_type = null;
            }

        } else {
            $list = null;
            $event_type = null;
        }

        $this->data['row'] = $list;
        $this->data['event_type'] = $event_type;

        return view($this->backend . ".event.details", $this->data);
    }

    function teamInfo($user_id){
        
        if (!Auth::check() || Auth::user()->group_id > 2) {
            return redirect('')->with('message', 'You don\'t have permission')->with('status', 'error');
        }
        
        //get team
        $team = uleagueteam::join('university', 'university.university_id', 'uleague_team.university_id')
            ->join('uleague_apply', 'uleague_apply.user_id', 'uleague_team.user_id')
            ->join('tb_users', 'tb_users.id', 'uleague_team.user_id')
            ->where('uleague_team.user_id', $user_id)
            ->select('*', 'uleague_apply.status as apply_status')
            ->first();
        
        //get all members of the team
        $team['member'] = uleagueteammembers::join('tb_users', 'tb_users.id', 'uleague_team_members.user_id')
            ->join('uleague_user', 'uleague_user.user_id', 'uleague_team_members.user_id')
            ->where('uleague_team_members.team_id', $team['team_id'])
            ->get();

        $this->data['team'] = $team;

        return view($this->backend . ".uleague.team-info", $this->data);
    }
}
