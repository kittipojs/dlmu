<?php

namespace App\Http\Controllers\backend;

use App\Models\Certificates;
use App\Models\Courses;
use App\Models\Courselectures;
use App\Models\Coursecurriculum;
use App\Models\Coursesession;
use App\Models\Coursetags;
use App\Models\Quiz;
use App\Models\Quizequestions;
use App\Models\Quizscore;
use App\Models\Usercertificates;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class CourseController extends Controller{

    protected $data = array();
    protected $layout = "layouts.main";
    protected $backend = 'pages.backend';
    protected $template = array();

    public function __construct(){
        parent::__construct();
        $this->access = array();

        $this->data = array(
            'pageTitle' => 'Startup Thailand Academy',
            'pageNote' => 'Course',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend' => 'layouts.default.template.backend'
        );
    }

    function index(){

        if (!Auth::check() || Auth::user()->group_id > 2) {
            return redirect('/e-admin')->with('message', 'You don\'t have permission')->with('status', 'error');
        }

        $courses = courses::get();

        $this->data['courses'] = $courses;
        $this->data['content'] = $this->backend . ".course.index";
        $this->data['title'] = 'รายการคอร์สเรียน';

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    function allCourse(){

        if (!Auth::check() || Auth::user()->group_id > 2) {
            return redirect('/e-admin')->with('message', 'You don\'t have permission')->with('status', 'error');
        }

        $courses = courses::where('course_status', '>=', 0)->orderBy('created_at', 'DESC')->get();

        $this->data['courses'] = $courses;
        $this->data['content'] = $this->backend . ".course.view";
        $this->data['title'] = 'รายการหลักสูตรเรียน';

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    function addCourse(){

        if (!Auth::check() || Auth::user()->group_id > 2) {
            return redirect('/e-admin')->with('message', 'You don\'t have permission')->with('status', 'error');
        }

        $this->data['content'] = $this->backend . ".course.add";
        $this->data['title'] = 'สร้างคอร์สเรียนใหม่';

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);

    }

    function saveCourse(Request $request){

        if (!Auth::check() || Auth::user()->group_id > 2) {
            return redirect('/e-admin')->with('message', 'You don\'t have permission')->with('status', 'error');
        }

        $user_id = Auth::user()->id;
        $input = Input::all();

        $course_code  = $input['course_code'];
        $course_level = $input['course_level'];
        $course_title = $input['course_title'];
        $course_intro = $input['course_intro'];
        $course_detail = $input['course_detail'];
        $course_instructor = $input['course_instructor'];
        $course_objective = $input['course_objective'];
        $course_for_text = $input['course_for_text'];
        $course_prerequisite_id = null;
        if(isset($input['course_prerequisite_id'])){
            $course_prerequisite_id = $input['course_prerequisite_id'];
        }

        if(isset($input['edit'])){
            $message = 'แก้ไขคอร์สเรียนสำเร็จ';
            $course = courses::where('course_id', $input['course_id'])->first();

            $course_id = $input['course_id'];

            $tag = Coursetags::where('course_id', $course_id)->first();

            if($tag==null){
                $t = new coursetags();
                $t->course_id = $course_id;
                $t->tag_name = $input['course_tags'];
                $t->save();
            }
            else{
                $tag->tag_name = $input['course_tags'];
                $tag->save();
            }

        }else{
            $message = 'สร้างคอร์สเรียนสำเร็จ';
            $course = new courses();

            $course_id = time().''.rand(100,999);
            $course->course_id = $course_id;
        }

        $course_objective = array_values( $course_objective );

        $course->user_id = $user_id;
        $course->course_code = $course_code;
        $course->course_level = $course_level;
        $course->course_title = $course_title;
        $course->course_intro = $course_intro;
        $course->course_detail = $course_detail;
        $course->course_instructor = $course_instructor;
        $course->course_objective  = json_encode($course_objective, JSON_UNESCAPED_UNICODE);
        $course->course_for_text = $course_for_text;
        $course->course_prerequisite_id = $course_prerequisite_id;
        //$course->course_thumbnail = '';
        $course->save();

        return redirect('e-admin/course/manage/'.$course_id)->with('message', $message)->with('status', 'success');
    }

    function updateCourseSave(Request $request){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('')->with('message','You don\'t have permission')->with('status','error');
        }

        $input = Input::all();

    }

    function updatePercentage(Request $request){

        if (Auth::user()->group_id > 2 || !Auth::check()) {
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $input = Input::all();

        $course_id  = $input['course_id'];

        $course = courses::where('course_id', $course_id)->first();
        $course->percentage_lecture = $input['percentage_lecture'];
        $course->percentage_quiz = $input['percentage_quiz'];
        $course->course_certificate_id = $input['cert_id'];

        $course->save();

        return redirect('e-admin/course/manage/'.$course_id)->with('message', 'แก้ไขเกณฑ์การให้ประกาศณียบัตรเรียบร้อยแล้ว')->with('status', 'success');

    }

    function updateStatus(Request $request){

        if (Auth::user()->group_id > 2 || !Auth::check()) {
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $input = Input::all();

        $value  = $input['value'];
        $course_id = $input['course_id'];

        $course = Courses::where('course_id', $course_id)->first();
        $course->course_status = $value;

        $course->save();

        return redirect('e-admin/course/manage/'.$course_id)->with('message', 'แก้ไขสถานะหลักสูตรเรียนเรียบร้อยแล้ว')->with('status', 'success');

    }

    function uploadMedia(Request $request){

        if (Auth::user()->group_id > 2 || !Auth::check()) {
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $input = Input::all();

        $type  = $input['type'];
        $course_id = $input['course_id'];

        if($type=='preview'){
            if($request->hasFile('file')){
                $files = $request->file('file');

                //Path for save and display image
                $urlPath = 'uploads/courses/preview/';
                $filename = $urlPath.time()."_".$course_id.".mp4";

                //Path for save image
                $destinationPath = public_path(). "/uploads/courses/preview";

                //Store image to /public/uploads/images/profile
                $files->move($destinationPath, $filename);
                $url = $filename;

                $course = courses::where('course_id', $course_id)->first();
                $course->course_preview = $url;
                $course->course_preview_type = 'mp4';
                $course->save();

                return response()->json(['success' => true, 'message'=>'อัพโหลดไฟล์สำเร็จ'], 200);

            } else {

                if($request->input('youtube_link')){
                    $course = courses::where('course_id', $course_id)->first();
                    $course->course_preview = $request->input('youtube_link');
                    $course->course_preview_type = 'youtube';
                    $course->save();

                    return response()->json(['success' => true, 'message'=>'เพิ่มลิงค์สำเร็จ'], 200);
                }

                return response()->json(['success' => false, 'message'=>'อัพโหลดไฟล์ไม่สำเร็จ'], 200);
            }
        }
        else if($type=='document'){
            if($request->hasFile('file')){

                $files = $request->file('file');

                $extension = $files->getClientOriginalExtension();

                //Path for save and display image
                $urlPath = 'uploads/courses/documents/';
                $filename = $urlPath.time()."_".$course_id.".".$extension;

                //Path for save image
                $destinationPath = public_path(). "/uploads/courses/documents";

                //Store image to /public/uploads/images/profile
                $files->move($destinationPath, $filename);
                $url = $filename;

                $course = courses::where('course_id', $course_id)->first();
                $course->course_document = $url;
                $course->course_document_type = $extension;
                $course->save();

                return response()->json(['success' => true, 'message'=>'อัพโหลดเอกสารสำเร็จ'], 200);

            } else {

                if($request->input('document_link')){
                    $course = courses::where('course_id', $course_id)->first();
                    $course->course_document = $request->input('document_link');
                    $course->course_document_type = 'ext-link';
                    $course->save();

                    return response()->json(['success' => true, 'message'=>'เพิ่มลิงค์สำเร็จ'], 200);
                }

                return response()->json(['success' => false, 'message'=>'อัพโหลดเอกสารไม่สำเร็จ'], 200);
            }
        }
        else if($type=='cover'){
            if($request->hasFile('file')){

                $files = $request->file('file');

                //Path for save and display image
                $urlPath = 'uploads/courses/covers/';
                $filename = $urlPath.time().'_'.$course_id.".jpg";

                //Path for save image
                $destinationPath = public_path(). "/uploads/courses/covers";

                //Store image to /public/uploads/images/profile
                $files->move($destinationPath, $filename);
                $url = $filename;

                $course = courses::where('course_id', $course_id)->first();
                $course->course_thumbnail = $url;
                $course->save();

                return response()->json(['success' => true, 'message'=>'อัพโหลดเอกสารสำเร็จ'], 200);

            } else {
                return response()->json(['success' => false, 'message'=>'อัพโหลดเอกสารไม่สำเร็จ'], 200);
            }
        }
    }

    function manageCourse($id){

        if (Auth::user()->group_id > 2 || !Auth::check()) {
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $course = courses::where('course_id', $id)->get()->first();
        $all_courses = courses::orderBy('course_title', 'asc')->get();

        if(!$course){
            return redirect('/e-admin/course/add')->with('message','ไม่มีคอร์สที่เลือก')->with('status','error');
        }

        $curriculum = coursecurriculum::getDetail($id);

        $this->data['course'] = $course;
        $this->data['stat'] = array();

        if($course->pre_test_id!=null){
            $this->data['pre_test'] = self::loadQuiz($course->pre_test_id);
            $this->data['stat']['pre_test'] = quizscore::getStat($course->pre_test_id);
        }

        if($course->post_test_id!=null){
            $this->data['post_test'] = self::loadQuiz($course->post_test_id);
            $this->data['stat']['post_test'] = quizscore::getStat($course->post_test_id);
        }

        $ct = Coursetags::where('course_id', $course->course_id)->first();

        if($ct){
            $this->data['course']->course_tag = $ct;
        }
        else{ $this->data['course']->course_tag = null; }

        $this->data['curriculum'] = $curriculum;
        $this->data['certificate'] = certificates::find($course->course_certificate_id);
        $this->data['content'] = $this->backend . ".course.manage";
        $this->data['title'] = $course->course_title;
        $enroll_users = coursesession::countStudent($id);
        $graduated_users = usercertificates::where('issue_for', 'course')->where('object_id', $course->course_id)->count();
        //$this->data['stat'] = array('enroll_user'=>$enroll_users, 'graduated_users'=>$graduated_users);

        $this->data['stat']['enroll_user'] = $enroll_users;
        $this->data['stat']['graduated_users'] = $graduated_users;
        $this->data['all_courses'] = $all_courses;


        $this->data['tags'] = Coursetags::orderBy('tag_name', 'ASC')
            ->groupBy('tag_name')
            ->get();

        $sub1 = array('link'=>'e-admin/courses', 'text'=>'หลักสูตรทั้งหมด');

        $this->data['subnav'] = array( $sub1 );


        //PERSONAL STATs
        $sessions = coursesession::where('course_id', $id)->groupBy('user_id')->join('tb_users', 'tb_users.id', 'course_session.user_id')->get();

        $stat_list = array();
        foreach($sessions as $item){

            $row['id'] = $item->user_id;
            $row['name'] = $item->first_name.' '.$item->last_name;
            $row['pre_test'] = array('status'=>false);
            $row['post_test'] = array('status'=>false);

            if($course->pre_test_id!=null){
                $pre_test = quizscore::where('quiz_id', $course->pre_test_id)->where('user_id', $item->user_id)->first();

                if($pre_test){
                    if($pre_test->is_pass){
                        $row['pre_test'] = array('status'=>true);
                    }
                }
            }

            if($course->post_test_id!=null){
                $post_test = quizscore::where('quiz_id', $course->post_test_id)->where('user_id', $item->user_id)->first();
                if($post_test){
                    if($post_test->is_pass){
                        $row['post_test'] = array('status'=>true);
                    }
                }
            }

            $cert = usercertificates::where('issue_for', 'course')->where('user_id', $item->user_id)->where('object_id', $course->course_id)->first();

            $row['certificate'] = null;
            if($cert){
                $row['certificate'] = $cert; 
            }

            $stat_list[] = $row;
        }

        $this->data['stat_list'] = $stat_list;

        $page_template = $this->template['backend'];
        //dd($this->data);
        return view($page_template, $this->data);
    }

    function deleteCourseDocument(Request $request){
        
        $course = courses::where('course_id', $request->course_id)->first();
        $filePath = public_path($course->course_document);
        if(file_exists($filePath)){
            unlink($filePath);
            $response = ['status'=>'success','message'=>'ลบไฟล์สำเร็จ'];
            $course->course_document = '';
            $course->save();
        }
        else{
            $response = ['status'=>'error','message'=>'ไม่พบไฟล์ที่ต้องการลบ'];
        }

        return response()->json($response, 200);
    }

    function reorder(){
        $input = Input::all();

        if(!isset($input['object_order'])){
            return back()->with('message','ไม่พบข้อมูล')->with('status','error');
        }

        $course_id = $input['course_id'];
        $object_order = $input['object_order'];

        coursecurriculum::reorder($course_id, $object_order);

        return redirect('e-admin/course/manage/'.$course_id)->with('message','บันทึกโครงสร้างหลักสูตรสำเร็จ')->with('status','success');

    }

    function getAddTag(Request $request){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message', 'You don\'t have permission')->with('status','error');
        }

        $input = Input::all();

        $course_id  = $input['course_id'];

        $this->data['course_id'] = $course_id;

        $this->data['title'] = 'เพิ่มหมวดหมู่หลักสูตรเรียน';

        return view($this->backend.".coursetags.add", $this->data);
    }

    function saveTag(){

        $input = Input::all();

        $tag = Coursetags::where('course_id', $input['course_id'])->first();

        if(!$tag){
            $coursetags = new coursetags();
            $coursetags->course_id = $input['course_id'];
            $coursetags->tag_name = $input['tag_name'];
            $coursetags->save();
        }
        else{
            $tag->tag_name = $input['tag_name'];
            $tag->save();
        }

        return redirect('e-admin/course/manage/'.$input['course_id'])->with('message','บันทึกข้อมูลหลักสูตรสำเร็จ')->with('status','success');
    }

    function deleteCourse($course_id){

        $course = courses::where('course_id', $course_id)->first();

        $course->course_status = -99;

        $course->save();

        //courselectures::where('course_id', $course_id)->delete();

        /*
        $curriculum = coursecurriculum::where('course_id', $course_id)->get();
        */

        //coursecurriculum::where('course_id', $course_id)->delete();

        //coursesession::where('course_id', $course_id)->delete();

        return response()->json(['success' => true, 'message'=>'ลบหลักสูตรเรียนสำเร็จ'], 200);
    }

    function deleteQuiz(Request $request){

        $course_id = $request->input('course_id');
        $quiz_type = $request->input('quiz_type');

        if($quiz_type=='pre_test'){
            courses::where('course_id', $course_id)
                ->update(['pre_test_id' => null]);
        }
        else if($quiz_type=='post_test'){
            courses::where('course_id', $course_id)
                ->update(['post_test_id' => null]);
        }

        return response()->json(['success' => true, 'message'=>'ลบแบบทดสอบสำเร็จ'], 200);
    }

    public function loadQuiz($quiz_id){

        $result = array();

        $result['quiz'] = quiz::where('quiz_id', $quiz_id)->first();
        $result['questions'] = quizequestions::where('quiz_id', $quiz_id)->get();

        return $result;

    }
}
