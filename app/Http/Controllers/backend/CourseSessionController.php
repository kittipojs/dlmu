<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Coursesession;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class CourseSessionController extends Controller{
    
    protected $data = array();
    protected $template = array();

    public function __construct(){
        
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Pitching',
            'pageNote' => 'Register',
        );
        
        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );
    }
    
    public function checkSession(){
        
        if (!\Auth::check()){ return response()->json(['success' => false, 'message'=>'Not Authorized'], 200); }
        
        $user_id = Auth::user()->id; 
        
        $session = coursesession::where('user_id', $user_id)->get();
        
        return response()->json(['success' => true, 'message'=>$session], 200);
        
    }
    
    public function addSession(Request $request){
        
        if (!\Auth::check()){ return response()->json(['success' => false, 'message'=>'Not Authorized'], 200); }
        
        $course_id = $request->course_id;
        $user_id = Auth::user()->id;
        $curriculum_id = $request->curriculum_id;
        $object_id = $request->object_id;
        
        $session = coursesession::where('user_id', $user_id)->where('curriculum_id', $curriculum_id)->first();
        
        if(!$session){
            $s = new coursesession();
            $s->course_id = $course_id;
            $s->user_id = $user_id;
            $s->curriculum_id = $curriculum_id;
            $s->object_id = $object_id;
            $s->progress = 0;
            $s->is_pass = 0;
            $s->save();
        }
        
        return response()->json(['success' => true, 'message'=>'เพิ่ม session สำเร็จ'], 200);
        
    }
    
    public function updateSession(Request $request){
        
        if (!\Auth::check()){ return response()->json(['success' => false, 'message'=>'Not Authorized'], 200); }
        
        $course_id = $request->course_id;
        $user_id = Auth::user()->id;
        $curriculum_id = $request->curriculum_id;
        $data['object_id'] = $request->object_id;
        $data['progress'] = $request->progress;
        $data['is_pass'] = 0;
        if($request->is_pass){
        $data['is_pass'] = 1;
        }
        
        $session = coursesession::where('user_id', $user_id)->where('curriculum_id', $curriculum_id)->update($data);
        
        
        return response()->json(['success' => true, 'message'=>'อัพเดท session สำเร็จ'], 200);
        
    }
    
}