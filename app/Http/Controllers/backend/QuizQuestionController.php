<?php

namespace App\Http\Controllers\backend;

use App\Models\Courses;
use App\Models\Quiz;
use App\Models\Quizequestions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class QuizQuestionController extends Controller
{
    protected $data = array();
    protected $layout = "layouts.main";
    protected $backend = 'pages.backend';
    protected $template = array();

    public function __construct()
    {
        parent::__construct();
        $this->access = array();

        $this->data = array(
            'pageTitle' => 'Startup Thailand Academy',
            'pageNote' => 'QuizQuestion',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );

    }

    function index(){
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('')->with('message','You don\'t have permission')->with('status','error');
        }
        $questions = quizequestions::join('quiz', 'quiz.quiz_id', 'quiz_questions.quiz_id')->get();

        $this->data['questions'] = $questions;
        $this->data['content']  = $this->backend.".quizquestion.index";
        $this->data['title']    = 'Question';
        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    function addQuizQuestion(Request $request){
        
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('')->with('message','You don\'t have permission')->with('status','error');
        }

        if($request->quiz_id){
            $quiz_id = $request->quiz_id;
            $this->data['quiz_id'] = $quiz_id;
            $this->data['quiz'] = quiz::where('quiz_id', $quiz_id)->first();
        }

        $questions = quizequestions::where('quiz_id', $quiz_id)->get();
        $quiz = quiz::where('quiz_id', $quiz_id)->first();

        $this->data['questions'] = $questions;
        $this->data['content'] = $this->backend.".quizquestion.add";
        $this->data['title'] = 'เพิ่มคำถามใหม่';
        $page_template = $this->template['backend'];
        

        return view($page_template, $this->data);
    }

    function addQuestionPage(Request $request){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('')->with('message','You don\'t have permission')->with('status','error');
        }

        $quiz_id = $request->quiz_id;

        $this->data['quiz_id'] = $quiz_id;
        $this->data['quiz'] = quiz::where('quiz_id', $quiz_id)->first();
        $this->data['content'] = $this->backend.".quizquestion.add";
        $this->data['title'] = 'เพิ่มคำถามใหม่';
        //$page_template = $this->template['backend'];

        return view($this->data['content'], $this->data);
    }


    function saveQuizQuestion(){

        $input = Input::all();

        $quiz_id = $input['quiz_id'];
        //$question_number = $input['question_number'];
        $question_title = $input['question_title'];
        $choice_val = $input['choice_val'];
        $choice_type = $input['choice_type'];
        $question_choices_arr = array();
        $index = 1;

        foreach ($choice_val as $key => $value){
            if (isset($choice_type[$key])){
                $question_choices_arr[$index] = array('name' => $value,'correct' => 'true');
            } else {
                $question_choices_arr[$index] = array('name' => $value,'correct' => 'false');
            }
            $index++;
        }

        $question_choices = json_encode($question_choices_arr,JSON_UNESCAPED_UNICODE);

        if(isset($input['edit'])){
            $quiz_question = quizequestions::where('question_id', $input['question_id'])->first();
        }else{
            $quiz_question = new quizequestions();
            $quiz_question->question_id     = quizequestions::max('question_id') + 1;
        }

        $quiz_question->quiz_id  = $quiz_id;
        //$quiz_question->question_number = $question_number;
        $quiz_question->question_title  = $question_title;
        $quiz_question->question_choices = $question_choices;

        $quiz_question->save();

        return redirect('/e-admin/question/add?quiz_id='.$quiz_id)->with('message',"บันทึกสำเร็จ")->with('status','success');
    }

    function getQuestionsPage($quiz_id){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('')->with('message','You don\'t have permission')->with('status','error');
        }

        $questions = quizequestions::where('quiz_id', $quiz_id)->get();

        $this->data['questions'] = $questions;

        return view($this->backend.".quizquestion.list", $this->data);
    }

    function updateQuizQuestion($id){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('')->with('message','You don\'t have permission')->with('status','error');
        }

        $question = quizequestions::where('question_id', $id)->first();

        $this->data['content'] = $this->backend.".quizquestion.update";
        
        $this->data['title'] = 'แก้ไขคำถาม';

        $this->data['question'] = $question;
        

        $json = json_decode($this->data['question']->question_choices);

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }
}
