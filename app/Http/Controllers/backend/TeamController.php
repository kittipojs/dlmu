<?php

namespace App\Http\Controllers\backend;

use App\Models\Uleagueteaminvite;
use App\User;
use App\Models\Uleagueteam;
use App\Models\Uleagueteammembers;
use App\Models\Uleagueapply;
use Chumper\Zipper\Facades\Zipper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

class TeamController extends Controller
{
    protected $data = array();
    protected $layout = "layouts.main";
    protected $backend = 'pages.backend';
    protected $template = array();

    public function __construct(){

        parent::__construct();

        $this->access = array();

        $this->data = array(
            'pageTitle' => 'Startup Thailand Academy',
            'pageNote' => 'รายงานทีม',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );

    }

    public function searchPage(){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $this->data['all_team'] = uleagueteam::count();

        $this->data['content']      = $this->backend.".team.search";
        $this->data['title']        = 'จัดการทีม';

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    public function approvePage(){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $this->data['all_apply_team'] = uleagueapply::groupBy('user_id')->get()->count();
        /*
        $this->data['all_waiting_team'] = uleagueapply::groupBy('user_id')->get()->where('status', 1)->count();
        $this->data['all_approve_team'] = uleagueapply::groupBy('user_id')->get()->where('status', 2)->count();
        $this->data['all_fail_team'] = uleagueapply::groupBy('user_id')->get()->where('status', -98)->count();
        $this->data['all_wrong_doc_team'] = uleagueapply::get()->where('status', -99)->count();

        $this->data['fails'] = $this->data['all_fail_team']+$this->data['all_wrong_doc_team'];
        //$this->data['pitching_events'] = self::getEvents('Pitching', '');
        */

        $events = self::searchEvents('?pagesize=20&page=1&type=pitching&isPast=false');
        foreach($events as $item){
            $this->data['pitching_events'][] = $item;
        }

        $events = self::searchEvents('?pagesize=20&page=1&type=pitching&isPast=true');
        foreach($events as $item){
            $this->data['pitching_events'][] = $item;
        }


        $this->data['content'] = $this->backend.".team.apply";
        $this->data['title']   = 'จัดการการสมัคร Pitching';

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    public function results(){

        //get team
        $teams = uleagueteam::join('tb_users', 'tb_users.id', 'uleague_team.user_id')
            ->join('university', 'university.university_id', 'uleague_team.university_id')
            ->select('uleague_team.team_id', 'team_name', 'uleague_team.created_at', 'uleague_team.user_id', 'first_name', 'last_name', 'university.name', 'uleague_team.startup_sector', 'uleague_team.status')
            ->orderBy('uleague_team.created_at', 'DESC');

        $key = \Illuminate\Support\Facades\Input::get("key");
        $value = \Illuminate\Support\Facades\Input::get("value");

        $teams = $teams->orWhere($key, 'like', '%'.$value.'%');

        $teams = $teams->limit(50)->get();

        foreach($teams as $item => $field){
            if($teams[$item]['status']==1){
                $teams[$item]['data_status'] = 99;
            }
            else {
                $teams[$item]['data_status'] = $teams[$item]['status'];
            }
        }

        $this->data['teams'] = $teams;

        return view($this->backend.".team.details",$this->data);
    }

    public function applyResults(Request $request){

        $pitching_events = array();

        $pe = self::searchEvents('?pagesize=20&page=1&type=pitching&isPast=false');
        foreach($pe as $item){
            $pitching_events[$item->id] = $item->name;
        }

        $pe = self::searchEvents('?pagesize=20&page=1&type=pitching&isPast=true');
        foreach($pe as $item){
            $pitching_events[$item->id] = $item->name;
        }
        foreach($pe as $item){
            $pitching_events[$item->id] = $item->name;
        }

        $this->data['pitching_events'] = $pitching_events;

        $this->data['show_download_btn'] = false;
        $this->data['event_id'] = $request['event_id'];

        if($request['event_id']){
            if($request['event_id']==-1){
                $apply = uleagueapply::join('uleague_team', 'uleague_team.user_id', 'uleague_apply.user_id')
                    ->join('tb_users', 'tb_users.id', 'uleague_apply.user_id')
                    ->join('university', 'university.university_id', 'uleague_team.university_id')
                    ->orderBy('uleague_apply.created_at', 'DESC')
                    ->select('uleague_apply.*', 'tb_users.first_name', 'tb_users.last_name', 'university.university_id', 'university.name', 'uleague_team.team_id', 'uleague_team.team_name', 'uleague_team.startup_sector')
                    ->get();
            }
            else{

                $this->data['show_download_btn'] = true;

                $apply = uleagueapply::join('uleague_team', 'uleague_team.user_id', 'uleague_apply.user_id')
                    ->join('tb_users', 'tb_users.id', 'uleague_apply.user_id')
                    ->join('university', 'university.university_id', 'uleague_team.university_id')
                    ->where('uleague_apply.reference_id', $request['event_id'])
                    ->orderBy('uleague_apply.created_at', 'DESC')
                    ->select('uleague_apply.*', 'tb_users.first_name', 'tb_users.last_name', 'university.university_id', 'university.name', 'uleague_team.team_id', 'uleague_team.team_name', 'uleague_team.startup_sector')
                    ->get();
            }
        }
        else{
            $apply = uleagueapply::join('uleague_team', 'uleague_team.user_id', 'uleague_apply.user_id')
                ->join('tb_users', 'tb_users.id', 'uleague_apply.user_id')
                ->join('university', 'university.university_id', 'uleague_team.university_id')
                ->orderBy('uleague_apply.created_at', 'DESC')
                ->select('uleague_apply.*', 'tb_users.first_name', 'tb_users.last_name', 'university.university_id', 'university.name', 'uleague_team.team_id', 'uleague_team.team_name', 'uleague_team.startup_sector')
                ->get();
        }

        $this->data['approved'] = 0;
        $this->data['waiting'] = 0;
        $this->data['fail'] = 0;

        foreach($apply as $item => $field){
            if($apply[$item]['status']==1){
                $this->data['waiting']++;
                $apply[$item]['data_status'] = 99;
            }
            else {

                if($apply[$item]['status']==2){
                    $this->data['approved']++;
                }
                else{
                    $this->data['fail']++;
                }

                $apply[$item]['data_status'] = $apply[$item]['status'];
            }
        }

        $this->data['apply'] = $apply;
        $this->data['team_total'] = count($apply);

        return view($this->backend.".team-apply.details", $this->data);

    }

    public function applyInfo($id){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $apply = uleagueapply::where('id', $id)->where('apply_for', 'pitching')->first();

        if($apply){
            $this->data['pitching_event'] = self::getEventInfo( $apply->reference_id );
        }

        $team = uleagueteam::join('tb_users', 'tb_users.id', 'uleague_team.user_id')
            ->join('university', 'university.university_id', 'uleague_team.university_id')
            ->select('uleague_team.team_id',
                     'team_name', 'uleague_team.user_id',
                     'first_name', 'last_name',
                     'university.name',
                     'uleague_team.user_id',
                     'uleague_team.startup_sector',
                     'uleague_team.status',
                     'uleague_team.team_info',
                     'uleague_team.file_url',
                     'university.university_id')
            ->where('uleague_team.user_id', $apply->user_id)->first();
        
        $this->data['team'] = $team;
        $this->data['apply'] = $apply;

        return view($this->backend.".team-apply.info", $this->data);
    }

    public function teamInfo($user_id){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $this->data['show_delete'] = false;
        if(Auth::user()->group_id<=2){
            $this->data['show_delete'] = true;
        }

        $this->data['now'] = time();
        $this->data['team_member_num'] = false;
        $this->data['team_is_eligible'] = false;
        $this->data['pitching_event'] = array();
        

        //get team info
        $team = uleagueteam::join('tb_users', 'tb_users.id', 'uleague_team.user_id')
            ->join('university', 'university.university_id', 'uleague_team.university_id')
            ->select('uleague_team.team_id',
                     'team_name', 'uleague_team.user_id',
                     'first_name', 'last_name',
                     'university.name',
                     'uleague_team.user_id',
                     'uleague_team.startup_sector',
                     'uleague_team.status',
                     'uleague_team.team_info',
                     'uleague_team.file_url',
                     'university.university_id')
            ->where('uleague_team.user_id', $user_id)->first();

        $apply = uleagueapply::where('user_id', $user_id)->where('apply_for', 'pitching')->get();

        
        if($apply){
            foreach($apply as $item){
                $zip_event = self::getEventInfo( $item->reference_id );
                $d = array('event'=>$zip_event, 'apply'=>$item);
                $this->data['pitching_event'][] = $d;
            }
        }

        $members  = uleagueteammembers::getTeamMembers($team->team_id);

        $count_member = 0;
        $pass_member = 0;

        $num_faculty = [];
        $num_subfaculty = [];
        foreach($members as $item => $field){

            if(!in_array($members[$item]['faculty_id'], $num_faculty)){
                $num_faculty[] = $members[$item]['faculty_id'];
            }

            if(!in_array($members[$item]['sub_faculty'], $num_subfaculty)){
                $num_subfaculty[] = $members[$item]['sub_faculty'];
            }

            $z = self::getMyOrders($members[$item]['email']);

            $members[$item]['pass_camp'] = false;
            foreach($z as $o){
                $i = $o->event;
                if($i->is_checkin){
                    $members[$item]['pass_camp'] = true;
                    $pass_member++;
                    break;
                }
            }
            $count_member++;
        }

        if($count_member >= 3 && $count_member <= 5){
            $this->data['team_member_num'] = true;
            if($pass_member==$count_member){
                if(count($num_faculty)>=2 && count($num_subfaculty)>=3){
                    $this->data['team_is_eligible'] = true;
                }
            }
        }

        $this->data['team'] = $team;
        $this->data['members'] = $members;

        return view($this->backend.".team.team-info", $this->data);
    }

    public function teamInfoUpload(Request $request){

        if(!Auth::check() || Auth::user()->group_id > 2){
            return response()->json(['success' => false, 'You don\'t have permission'], 200);
        }

        $input  = Input::all();

        $team_id  = $input['team_id'];
        $team_name = uleagueteam::where('team_id', $team_id)->first()->team_name;


        if($request->hasFile('file')){
            $files = $request->file('file');

            $fileType = $files->getMimeType();

            if($fileType !== 'application/pdf' && $fileType !== 'image/jpeg' && $fileType !== 'image/png'){
                return response()->json(['success' => false, 'อัพโหลดไฟล์ผิดประเภท'], 200);
            }

            if($fileType == 'application/pdf'){
                $ext = '.pdf';
            }
            else if($fileType !== 'image/jpeg'){
                $ext = '.jpg';
            }
            else if($fileType !== 'image/png'){
                $ext = '.png';
            }

            //Path for save and display image
            $urlPath = '/uploads/uleague/pitch_deck/';
            $filename = $urlPath.$team_id.'_'.\DataHelpers::fileName($team_name).''.$ext;

            //Path for save image
            $destinationPath = public_path(). '/uploads/uleague/pitch_deck/';

            //Store image to /public/uploads/files/pitching
            $files->move($destinationPath, $filename);

            $url = $urlPath.$team_id.'_'.\DataHelpers::fileName($team_name).''.$ext;

            return response()->json(['success' => true, 'message'=>'อัพโหลดไฟล์สำเร็จ'], 200);

        } else {
            return response()->json(['success' => false, 'message'=>'อัพโหลดไฟล์ไม่สำเร็จ'], 200);
        }

    }

    public function teamApplyStatusEdit(Request $request){
        //all input
        $input  = Input::all();

        $id  = $input['id'];
        $status  = $input['status'];

        $apply = Uleagueapply::where('id', $id)->first();

        $apply->status = $status;
        $apply->save();

        //return redirect('/e-admin/team/approve');
        return response()->json(['success' => true, 'message'=>'บันทึกสำเร็จ'], 200);
    }

    public function teamApplyResultEdit(Request $request){
        //all input
        $input  = Input::all();

        $id  = $input['id'];
        $result  = $input['result'];

        $apply = Uleagueapply::where('id', $id)->first();

        $apply->result = $result;
        $apply->save();

        //return redirect('/e-admin/team/approve');
        return response()->json(['success' => true, 'message'=>'บันทึกสำเร็จ'], 200);
    }

    public function teamStatusEdit(Request $request){
        //all input
        $input  = Input::all();

        $team_id  = $input['team_id'];
        $status  = $input['team_status'];

        $team = uleagueteam::where('uleague_team.team_id', $team_id)->first();
        $team->status = $status;
        $team->save();

        Uleagueapply::where('user_id', $team->user_id)->update(array('status' => $status));

        $user = user::find($team->user_id);

        $to = $user->email;
        $subject = "ตอบกลับ: สถานะการสมัครแข่งขัน U League 2018 Pitching";

        if($status==2){
            $template = 'emails.team-approve';
            self::sendMail($to, $subject, $template);
        }
        else if($status==-99 || $status==-98){
            $template = 'emails.team-reject';
            self::sendMail($to, $subject, $template);
        }

        return response()->json(['success' => true, 'message'=>'บันทึกสำเร็จ'], 200);
    }

    public function teamInfoEdit(Request $request){

        //all input
        $input  = Input::all();

        $team_id  = $input['team_id'];
        $team_name = $input['team_name'];
        $team_info = $input['team_info'];
        $university = $input['university'];
        $startup_sector = $input['startup_sector'];

        //save team
        $team = uleagueteam::where('uleague_team.team_id', $team_id)->first();
        $team->team_name        = $team_name;
        $team->team_info        = $team_info;
        $team->university_id    = $university;
        $team->startup_sector   = $startup_sector;

        // Get the UploadedFile object
        if($request->hasFile('file')){
            $files = $request->file('file');

            $fileType = $files->getMimeType();

            if($fileType !== 'application/pdf' && $fileType !== 'image/jpeg' && $fileType !== 'image/png'){
                return back()->with('message', 'อัพโหลดไฟล์ผิดประเภท')->with('status', 'error');
            }

            if($fileType == 'application/pdf'){
                $ext = '.pdf';
            }
            else if($fileType !== 'image/jpeg'){
                $ext = '.jpg';
            }
            else if($fileType !== 'image/png'){
                $ext = '.png';
            }

            //Path for save and display image
            $urlPath = '/uploads/uleague/pitch_deck/';
            $filename = $urlPath.$team_id.'_'.\DataHelpers::fileName($team_name).''.$ext;

            //Path for save image
            $destinationPath = public_path(). '/uploads/uleague/pitch_deck/';

            //Store image to /public/uploads/files/pitching
            $files->move($destinationPath, $filename);

            $url = $urlPath.$team_id.'_'.\DataHelpers::fileName($team_name).''.$ext;

            $team->pitch_deck       = $url;

        }
        //save team
        $team->save();

        return response()->json(['success' => true, 'message'=>'บันทึกสำเร็จ'], 200);
    }

    public function verifyTeam(Request $request){

        if (!Auth::check() || Auth::user()->group_id > 2) {
            return response()->json(['success' => false, 'message'=>'You don\'t have permission' ], 200);
        }

        $input = $request->all();

        $team_id = $input['team_id'];
        $status = $input['status'];

        $team = Uleagueteam::find($team_id);

        Uleagueapply::where('user_id', $team->user_id)->update(array('status' => $status));

        $apply = Uleagueapply::where('user_id', $team->user_id)->first();

        $team->status = $status;
        $team->save();

        return response()->json(['success' => true, 'data'=>array('event_id'=>$apply->reference_id) ], 200);
    }
    
    public function removeMember(Request $request){
        $team_id = $request->team_id;
        $user_id = $request->user_id;
        
        $row = uleagueteammembers::where('team_id', $team_id)->where('user_id', $user_id)->first();
        
        $row->delete();
        
        return response()->json(['success' => true ], 200);
    }

    private function getMyOrders($email){

        $curl = new \anlutro\cURL\cURL;

        $rq = $curl->newRequest('get', config('app.zipevent_url').'/api/nia/ticket/list?email='.$email)
            ->setHeader('Content-Type', 'application/json')
            ->setHeader('Host', config('app.zipevent_host'))
            ->setHeader('client_id', config('app.zipevent_client_id'))
            ->setHeader('client_secret', config('app.zipevent_client_secret'))
            ->setHeader('device_id', '1')
            ->setHeader('Content-Length', '0');

        $ev = $rq->send();

        return json_decode($ev->body);
    }

    private function getEvents($type, $email){

        $curl = new \anlutro\cURL\cURL;

        $rq = $curl->newRequest('get', config('app.zipevent_url').'/api/nia/event/uleague?type='.$type.'&email='.$email)
            ->setHeader('Content-Type', 'application/json')
            ->setHeader('Host', config('app.zipevent_host'))
            ->setHeader('client_id', config('app.zipevent_client_id'))
            ->setHeader('client_secret', config('app.zipevent_client_secret'))
            ->setHeader('device_id', '1')
            ->setHeader('Content-Length', '0');

        $ev = $rq->send();

        return json_decode($ev->body);
    }

    private function searchEvents($params){

        $curl = new \anlutro\cURL\cURL;

        $rq = $curl->newRequest('get', config('app.zipevent_url').'/api/nia/event/search'.$params)
            ->setHeader('Content-Type', 'application/json')
            ->setHeader('Host', config('app.zipevent_host'))
            ->setHeader('client_id', config('app.zipevent_client_id'))
            ->setHeader('client_secret', config('app.zipevent_client_secret'))
            ->setHeader('device_id', '1')
            ->setHeader('Content-Length', '0');

        $ev = $rq->send();

        return json_decode($ev->body);
    }

    private function getEventInfo($event_id){

        $curl = new \anlutro\cURL\cURL;

        $rq = $curl->newRequest('get', config('app.zipevent_url').'/api/nia/event/info?id='.$event_id)
            ->setHeader('Content-Type', 'application/json')
            ->setHeader('Host', config('app.zipevent_host'))
            ->setHeader('client_id', config('app.zipevent_client_id'))
            ->setHeader('client_secret', config('app.zipevent_client_secret'))
            ->setHeader('device_id', '1')
            ->setHeader('Content-Length', '0');

        $ev = $rq->send();

        return json_decode($ev->body);
    }

    private function sendMail($to, $subject, $template){

        $data['subject'] = $subject;
        $data['email'] = $to;

        if (config('sximo.cnf_mail') == 'swift') {
            \Mail::send($template, $data, function ($message) use ($data) {
                $message->to($data['email'])->subject($data['subject']);
            });

        } else {

            $message = view($template, $data);
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: ' . config('sximo.cnf_appname') . ' <' . config('sximo.cnf_email') . '>' . "\r\n";
            mail($to, $subject, $message, $headers);
        }

        return true;
    }

    function deleteTeam(){

        if (!Auth::check() || Auth::user()->group_id > 2) {
            return response()->json(['success' => false, 'message'=>'You don\'t have permission' ], 200);
        }

        $input = Input::all();

        $team_id = $input['team_id'];
        $user_id = $input['user_id'];

        if($team_id!='' && $user_id!=''){

            uleagueapply::where('user_id', $user_id)->delete();
            uleagueteam::where('team_id', $team_id)->delete();
            uleagueteaminvite::where('team_id', $team_id)->delete();
            uleagueteammembers::where('team_id', $team_id)->delete();

            return response()->json(['success' => true, 'message'=>'ลบทีมสำเร็จ'], 200);
        }
        else{
            return response()->json(['success' => false, 'message'=>'เกิดข้อผิดพลาดขึ้นระหว่างลบทีม'], 200);
        }

    }

    function downloadZip($event_id){

        File::delete(public_path('/uploads/zip/pitch_deck.zip'));


        $all_verify_team = uleagueapply::where('status', '>', 1)->where('reference_id', $event_id)->get();

        //create zip
        $zip = Zipper::make(public_path('/uploads/zip/pitch_deck.zip'));


        foreach ($all_verify_team as $verify) {

            //get file path
            $path = uleagueteam::select('file_url')->where('user_id', $verify->user_id)->first();


            //get real file path
            $file = glob(public_path($path->file_url));

            $path = public_path('/uploads/uleague/pitch_deck/');

            $name = basename($file[0]);

            //add file to zip
            $zip->add($file);
        }

        //archived
        $zip->close();

        return response()->download(public_path('/uploads/zip/pitch_deck.zip'));

    }

}