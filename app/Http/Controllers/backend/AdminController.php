<?php

namespace App\Http\Controllers\backend;

use App\User;
use App\Models\Usernia;
use App\Models\Uleagueuser;
use App\Models\Uleagueteam;
use App\Models\Uleagueapply;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\Console\Input\InputOption;

class AdminController extends Controller
{
    protected $data = array();
    protected $layout = "layouts.main";
    protected $backend = 'pages.backend';
    protected $template = array();

    public function __construct(){
        parent::__construct();
        $this->access = array();

        $this->data = array(
            'pageTitle' => 'Startup Thailand Academy',
            'pageNote' => 'example note',
        );

        $this->template = array(
            'frontend' => 'layouts.medilab.index',
            'backend'  => 'layouts.default.template.backend'
        );

    }

    function index(){
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }


        $this->data['nia_user']       = usernia::count();
        $this->data['uleague_user']   = uleagueuser::count();
        $this->data['today_registerd'] = usernia::whereDate('created_at', DB::raw('CURDATE()'))->count();
        $this->data['lasted_register'] = usernia::join('tb_users', 'user_nia.user_id', 'tb_users.id')
                                         ->orderBy('user_nia.created_at', 'desc')->take(4)->get();

        $this->data['all_team'] = uleagueteam::count();
        //$this->data['all_waiting_team'] = uleagueteam::where('status', 1)->count();
        $this->data['lasted_team'] = uleagueteam::join('university', 'university.university_id', 'uleague_team.university_id')
            ->orderBy('uleague_team.created_at', 'desc')->take(4)->get();

        
        $this->data['all_apply_team'] = uleagueapply::count();
        $this->data['all_waiting_team'] = uleagueapply::where('status', 1)->count();
        
        //$this->data['all_verify_team'] = uleagueteam::where('status', '>=', 2)->count();
        $this->data['all_approve_team'] = uleagueapply::where('status', 2)->count();
        //$this->data['all_apply_team'] = $this->data['all_waiting_team']+$this->data['all_verify_team'];

        $all_approve_team = uleagueapply::select('*', DB::raw('count(*) as total'))->groupBy('user_id')->get();
        
        $this->data['all_approve_team'] = count($all_approve_team);
        
        $this->data['content'] = $this->backend.".dashboard";
        $this->data['title'] = 'Control Panel - Startup Camping';

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    public function getLogin() {

        if(\Auth::check()){
            //return redirect('/e-admin/login')->with(['message'=>'success','Youre already login','status'=>'success']);

            $page_template = $this->template['frontend'];

            $this->data['title'] = 'Login';
            $this->data['content'] = "user.login";

            return view($page_template, $this->data);

        } elseif (Auth::user()->group_id > 2){
            return redirect('user/login');

        }
    }

    function userList(){
        if(!Auth::check() || Auth::user()->group_id > 2){
            return redirect('/e-admin/login')->with('message','You don\'t have permission')->with('status','error');
        }

        $this->data['users']       = User::where('group_id', 2)->get();

        $this->data['content'] = $this->backend.".admin.user-list";
        $this->data['title'] = 'Control Panel - Startup Camping';

        $page_template = $this->template['backend'];

        return view($page_template, $this->data);
    }

    function addAdmin(){
        
        if(!Auth::check() || Auth::user()->group_id > 2){
            return response()->json(['status' => 'error', 'message'=>'เกิดข้อผิดพลาด โปรดลองอีกครั้ง'], 200);
        }

        $input = Input::all();
        $email = $input['email'];
        $user  = User::where('email', $email)->first();
        if($user){
            $user->group_id = 2;
            $user->save();
            return response()->json(['status' => 'success', 'message'=>'เพิ่ม Admin '.$user->first_name.' สำเร็จ'], 200);
        }
        else {
            return response()->json(['status' => 'error', 'message'=>'เกิดข้อผิดพลาด โปรดลองอีกครั้ง'], 200);
        }

    }
    
    function deleteAdmin(){
        
        if(!Auth::check() || Auth::user()->group_id > 2){
            return response()->json(['status' => 'error', 'message'=>'เกิดข้อผิดพลาด โปรดลองอีกครั้ง'], 200);
        }

        $input = Input::all();
        $email = $input['email'];
        $user  = User::where('email', $email)->first();
        
        if($user){
            $user->group_id = 3;
            $user->save();
            return response()->json(['status' => 'success', 'message'=>'เพิ่ม Admin '.$user->first_name.' สำเร็จ'], 200);
        }
        else {
            return response()->json(['status' => 'error', 'message'=>'เกิดข้อผิดพลาด โปรดลองอีกครั้ง'], 200);
        }

    }
}
