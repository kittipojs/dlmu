<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\Libary\SiteHelpers;
use Socialize;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect;

class CustomLoginController extends Controller
{
    protected $layout = "layouts.main";

    public function __construct()
    {
        parent::__construct();
        $this->data = array();

    }

    public function getLogin() {
        if(\Auth::check())
        {
            return response()->json(['status' => 'info', 'message' =>'You\'re already login']);
            //return Redirect::to('')->with('message',\SiteHelpers::alert('success','You\'re already login'));

        } else {
            $this->data['socialize'] =  config('services');
            return View('pages.frontend.login',$this->data);

        }
    }

    public function getRegister() {

        if($this->config['cnf_regist'] =='false') :
            if(\Auth::check()):
                return Redirect::to('')->with('message',\SiteHelpers::alert('success','Youre already login'));
            else:
                return Redirect::to('user/login');
            endif;

        else :
            $this->data['socialize'] =  config('services');
            return view('user.register', $this->data);
        endif ;

    }

    public function postCreate(Request $request)
    {
        $rules = array(
            'firstname' => 'required|alpha_num|min:2',
            'lastname' => 'required|alpha_num|min:2',
            'email' => 'required|email|unique:tb_users',
            'password' => 'required|between:6,12|confirmed',
            'password_confirmation' => 'required|between:6,12',
        );

        if (config('sximo.cnf_recaptcha') == 'true') {
            $return = $this->reCaptcha($request->all());
            if ($return !== false) {
                if ($return['success'] != 'true') {
                    return response()->json(['status' => $return['success'], 'message' => 'Invalid reCpatcha']);
                }

            }
        }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            $code = rand(10000, 10000000);
            $authen = new User;
            $authen->first_name = $request->input('firstname');
            $authen->last_name  = $request->input('lastname');
            $authen->email      = trim($request->input('email'));
            $authen->dob        = $request->input('dob');
            if($request->input('dob') === "นาย"){
                $sex = 'm';
            } else {
                $sex = 'f';
            }
            $authen->gender     = $sex;
            $authen->activation = $code;
            $authen->group_id   = $this->config['cnf_group'];
            $authen->password   = \Hash::make($request->input('password'));
            if ($this->config['cnf_activation'] == 'auto') {
                $authen->active = '1';
            } else {
                $authen->active = '0';
            }

            $authen->save();

            $user_nia = new usernia();
            $user_nia->user_id      = $authen->id;
            $user_nia->name_title   = $request->input('name_title');
            $user_nia->birthday     = $request->input('birthday');
            $user_nia->education    = $request->input('education');
            $user_nia->faculty_id   = $request->input('faculty_id');
            $user_nia->address      = $request->input('address');
            $user_nia->province_id  = $request->input('province_id');
            $user_nia->startup_lookup = $request->input('start_lookup');
            $user_nia->save();

            $data = array(
                'firstname' => $request->input('firstname'),
                'lastname' => $request->input('lastname'),
                'email' => $request->input('email'),
                'password' => $request->input('password'),
                'code' => $code,
                'subject' => "[ " . $this->config['cnf_appname'] . " ] REGISTRATION "
            );
            if (config('sximo.cnf_activation') == 'confirmation') {
                $to = $request->input('email');
                $subject = "[ " . $this->config['cnf_appname'] . " ] REGISTRATION ";
                if ($this->config['cnf_mail'] == 'swift') {
                    \Mail::send('user.emails.registration', $data, function ($message) use ($data) {
                        $message->to($data['email'])->subject($data['subject']);
                    });
                } else {

                    $message = view('user.emails.registration', $data);

                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers .= 'From: ' . $this->config['cnf_appname'] . ' <' . $this->config['cnf_email'] . '>' . "\r\n";

                    mail($to, $subject, $message, $headers);
                }

                $message = "Thanks for registering! . Please check your inbox and follow activation link";

            } elseif ($this->config['cnf_activation'] == 'manual') {
                $message = "Thanks for registering! . We will validate you account before your account active";
            } else {
                $message = "Thanks for registering! . Your account is active now ";

            }


            return redirect('user/login')->with(['message' => $message, 'status' => 'success']);
        } else {
            return redirect('user/register')->with(['message' => 'The following errors occurred', 'status' => 'success'])
                ->withErrors($validator)->withInput();
        }
    }
}




