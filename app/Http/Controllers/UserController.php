<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\Libary\SiteHelpers;
use App\Models\Usernia;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect;
use Socialize;

class UserController extends Controller
{

    protected $layout = "layouts.main";

    public function __construct(){
        parent::__construct();
        $this->data = array();

    }

    public function getRegister(){

        if (config('sximo.cnf_regist') == 'false') :
        if (\Auth::check()):
        return redirect('')->with(['message' => 'Youre already login', 'status' => 'error']);
        else:
        return redirect('user/login');
        endif;
        else :
        $this->data['socialize'] = config('services');

        $this->data['autofill'] = array('email'=>'', 'firstname'=>'', 'lastname'=>'');

        if(isset($user)){
            $this->data['autofill'] = array('email'=>$user->email, 'firstname'=>explode(" ", $user->name)[0], 'lastname'=>explode(" ", $user->name)[1]);
        }

        return view('user.register', $this->data);

        endif;
    }

    public function postCreate(Request $request){
        $rules = array(
            'firstname' => 'required|min:2',
            'lastname' => 'required|min:2',
            'email' => 'required|email|unique:tb_users',
            'password' => 'required|between:6,32|confirmed',
            'password_confirmation' => 'required|between:6,32',
            'citizen_id' => 'required|unique:user_nia',
            'mobile_number' => 'required|numeric',
        );

        if (config('sximo.cnf_recaptcha') == 'true') {
            $return = $this->reCaptcha($request->all());
            if ($return !== false) {
                if ($return['success'] != 'true') {
                    return back()->with('status','error')->with('message','ข้อมูล Recapcha ไม่ถูกต้อง');
                }
            }
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->passes()) {
            $code = rand(10000, 10000000);
            $authen = new User;
            $authen->first_name = ucfirst($request->input('firstname'));
            $authen->last_name  = ucfirst($request->input('lastname'));
            $authen->email      = strtolower(trim($request->input('email')));
            $authen->dob        = $request->input('dob');
            if($request->input('name_title') === "นาย"){
                $sex = 'm';
            } else {
                $sex = 'f';
            }
            $authen->gender     = $sex;
            $authen->activation = $code;
            $authen->group_id   = $this->config['cnf_group'];
            $authen->password   = \Hash::make($request->input('password'));
            if ($this->config['cnf_activation'] == 'auto') {
                $authen->active = '1';
            } else {
                $authen->active = '0';
            }

            $authen->save();

            $user_nia = new usernia();
            $user_nia->user_id          = $authen->id;
            $user_nia->name_title       = $request->input('name_title');
            $user_nia->birthday         = $request->input('dob');
            $user_nia->citizen_id       = $request->input('citizen_id');
            $user_nia->education        = $request->input('education');
            $user_nia->occupation       = $request->input('occupation');
            $user_nia->faculty_id       = $request->input('faculty_id');
            $user_nia->sub_faculty      = $request->input('sub_faculty');
            $user_nia->address          = $request->input('address');
            $user_nia->mobile_number    = $request->input('mobile_number');
            $user_nia->province_id      = $request->input('province_id');
            $user_nia->postcode         = $request->input('postcode');
            //$user_nia->is_subscribe     = (!is_null($request->input('is_subscribe')) ? '1' : '0');
            $user_nia->is_subscribe     = $request->input('is_subscribe');
            $user_nia->startup_lookup   = implode(",", $request->input('startup_lookup'));

            $user_nia->save();

            $data = array(
                'firstname' => $request->input('firstname'),
                'lastname' => $request->input('lastname'),
                'email' => $request->input('email'),
                'password' => $request->input('password'),
                'code' => $code,
                'subject' => "[ " . $this->config['cnf_appname'] . " ] REGISTRATION "
            );

            if (config('sximo.cnf_activation') == 'confirmation') {
                $to = $request->input('email');
                $subject = "[ " . $this->config['cnf_appname'] . " ] REGISTRATION ";

                if ($this->config['cnf_mail'] == 'swift') {
                    \Mail::send('user.emails.registration', $data, function ($message) use ($data) {
                        $message->to($data['email'])->subject($data['subject']);
                    });
                } else {

                    $message = view('user.emails.registration', $data);

                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers .= 'From: ' . $this->config['cnf_appname'] . ' <' . $this->config['cnf_email'] . '>' . "\r\n";

                    mail($to, $subject, $message, $headers);
                }

                $message = "Thanks for registering! . Please check your inbox and follow activation link";

            } elseif ($this->config['cnf_activation'] == 'manual') {

                $message = "Thanks for registering! . We will validate you account before your account active";

            } else {
                $message = "ขอบคุณสำหรับการสมัครสมาชิก!";

            }
            return redirect('user/login')->with(['message' => $message, 'status' => 'success']);

        } else {
            return redirect('user/register')->with(['message' => 'เกิดข้อผิดพลาดในการสมัครสมาชิก', 'status' => 'success'])->withErrors($validator)->withInput();
        }
    }

    public function getActivation(Request $request){
        $num = $request->input('code');
        if ($num == '')
            return redirect('user/login')->with(['message' => 'Invalid Code Activation!', 'status' => 'error']);

        $user = User::where('activation', '=', $num)->get();
        if (count($user) >= 1) {
            \DB::table('tb_users')->where('activation', $num)->update(array('active' => 1, 'activation' => ''));
            return redirect('user/login')->with(['message' => 'Your account is active now!', 'status' => 'success']);

        } else {
            return redirect('user/login')->with(['message' => 'Invalid Code Activation!', 'status' => 'error']);
        }


    }

    public function getLogin(Request $request){


        $segment  = explode('/', $request->path())[0];

        $this->data['admin_mode'] = false;

        if ($request->has('redirect')) {
            $redirect = $request->input('redirect');
            $this->data['redirect'] = $redirect;
        }
        else{
            $this->data['redirect'] = '';
        }

        if (\Auth::check()) {
            if($segment=='e-admin'){
                return redirect('/e-admin')->with(['message' => 'success', 'คุณได้เข้าสู่ระบบเรียบร้อยแล้ว', 'status' => 'success']);
            }
            else{
                return redirect('')->with(['message' => 'success', 'คุณได้เข้าสู่ระบบเรียบร้อยแล้ว', 'status' => 'success']);
            }


        } else {

            if($segment=='e-admin'){
                $this->data['admin_mode'] = true;
                $this->data['redirect'] = '/e-admin';
            }

            $this->data['socialize'] = config('services');
            return View('user.login', $this->data);
        }
    }

    public function reCaptcha($request){
        if (!is_null($request['g-recaptcha-response'])) {
            $api_url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . config('sximo.cnf_recaptchaprivatekey') . '&response=' . $request['g-recaptcha-response'];
            $response = @file_get_contents($api_url);
            $data = json_decode($response, true);

            return $data;
        } else {
            return false;
        }
    }

    public function postSignin(Request $request){

        $rules = array(
            'email' => 'required',
            'password' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {

            $remember = (!is_null($request->get('remember')) ? 'true' : 'false');

            if (\Auth::attempt(array('email' => $request->input('email'), 'password' => $request->input('password')), $remember)
                or
                \Auth::attempt(array('username' => $request->input('email'), 'password' => $request->input('password')), $remember)

               ) {
                if (\Auth::check()) {
                    $row = User::find(\Auth::user()->id);

                    if ($row->active == 'Inactive') {
                        // inactive
                        if ($request->ajax() == true) {
                            return response()->json(['status' => 'error', 'message' => 'Your Account is not active']);
                        } else {
                            \Auth::logout();
                            return redirect('user/login')->with(['status' => 'error', 'message' => 'Your Account is not active']);
                        }

                    } else if ($row->active == 'Banned') {

                        if ($request->ajax() == true) {
                            return response()->json(['status' => 'error', 'message' => 'Your Account is BLocked']);
                        } else {
                            // BLocked users
                            \Auth::logout();
                            return redirect('user/login')->with(['status' => 'error', 'message' => 'Your Account is BLocked']);
                        }
                    } else {

                        \DB::table('tb_users')->where('id', '=', $row->id)->update(array('last_login' => date("Y-m-d H:i:s")));
                        $level = 99;
                        $sql = \DB::table('tb_groups')->where('group_id', $row->group_id)->get();
                        if (count($sql)) {
                            $l = $sql[0];
                            $level = $l->level;
                        }

                        $session = array(
                            'gid' => $row->group_id,
                            'uid' => $row->id,
                            'eid' => $row->email,
                            'll' => $row->last_login,
                            'fid' => $row->first_name.' '.$row->last_name,
                            //'username' => $row->username,
                            'join' => $row->created_at,
                            'level' => $level
                        );
                        /* Set Lang if available */
                        if (!is_null($request->input('language'))) {
                            $session['lang'] = $request->input('language');
                        } else {
                            $session['lang'] = config('sximo.cnf_lang');

                        }

                        session($session);

                        if ($request->ajax() == true) {
                            if ($session['gid'] <= 2) :
                            return response()->json(['status' => 'success', 'url' => url('e-admin')]);
                            else :
                            return response()->json(['status' => 'success', 'url' => url(''), 'data'=>$data]);
                            endif;
                        } else {

                            //custom redirect

                            if ($session['gid'] <= 2) {
                                return redirect('e-admin')->with(['message' => 'Login Success', 'status' => 'success']);;
                            } 
                            else {

                                $url = '';

                                if($request->has('redirect')) {
                                    $redirect = $request->input('redirect');

                                    if($redirect=='invite'){
                                        $url = '/uleague/pitching';
                                    }
                                }
                                return redirect($url)->with(['message' => 'Login Success', 'status' => 'success']);;
                            }
                        }
                    }
                }

            } else {

                if ($request->ajax() == true) {
                    return response()->json(['status' => 'error', 'message' => 'อีเมลหรือรหัสผ่านไม่ถูกต้อง']);
                } else {
                    return redirect('user/login')
                        ->with(['status' => 'error', 'message' => 'อีเมลหรือรหัสผ่านไม่ถูกต้อง'])
                        ->withInput();
                }
            }
        } else {

            if ($request->ajax() == true) {
                return response()->json(['status' => 'error', 'message' => 'The following  errors occurred']);
            } else {

                return redirect('user/login')
                    ->with(['status' => 'error', 'message' => 'The following  errors occurred'])
                    ->withErrors($validator)->withInput();
            }

        }
    }

    public function getProfile(){

        if (!\Auth::check()) return redirect('user/login');

        $user_info = User::where('tb_users.id', \Auth::user()->id)
            ->leftjoin('user_nia','tb_users.id','user_nia.user_id')
            ->leftjoin('thailand_provinces','thailand_provinces.code','user_nia.province_id')
            ->leftjoin('faculty','faculty.faculty_id','user_nia.faculty_id')
            ->first();

        $this->data = array(
            'pageTitle' => 'ข้อมูลส่วนตัว',
            'pageNote' => 'View Detail My Info',
            'info' => $user_info,
        );

        if(\Auth::user()->group_id > 2) return view('user.profile', $this->data);

        return view('user.profile', $this->data);

    }

    public function postSaveprofile(Request $request){
        if (!\Auth::check()) return redirect('user/login');


        if(!$request->input('startup_lookup')){
            $rules = array(
                'first_name' => 'required|alpha_num|min:2',
                'last_name' => 'required|alpha_num|min:2',
            );

            if ($request->input('email') != \Session::get('eid')) {
                $rules['email'] = 'required|email|unique:tb_users';
            }

            if (!is_null($request->file('avatar'))) $rules['avatar'] = 'mimes:jpg,jpeg,png,gif,bmp';

            $validator = Validator::make($request->all(), $rules);

            if ($validator->passes()) {


                if (!is_null($request->file('avatar'))) {

                    $file = $request->file('avatar');
                    $destinationPath = './uploads/users/';
                    $filename = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension(); //if you need extension of the file
                    $newfilename = \Session::get('uid') . '.' . $extension;
                    $uploadSuccess = $request->file('avatar')->move($destinationPath, $newfilename);
                    if ($uploadSuccess) {
                        $data['avatar'] = $newfilename;
                    }
                    $orgFile = $destinationPath . '/' . $newfilename;
                    \SiteHelpers::cropImage('512', '512', $orgFile, $extension, $orgFile);

                }

                $user = User::find(\Session::get('uid'));
                $user->first_name = $request->input('first_name');
                $user->last_name = $request->input('last_name');
                $user->email = $request->input('email');
                $user->dob = $request->input('dob');

                if($request->input('name_title') === "นาย"){
                    $sex = 'm';
                } else {
                    $sex = 'f';
                }

                $user->gender = $sex;

                $nia_user = usernia::where('user_id',\Auth::user()->id)->first();

                if(!$nia_user){
                    $nia_user = new usernia();
                    $nia_user->user_id = Auth::user()->id;
                }

                $nia_user->name_title = $request->input('name_title');
                $nia_user->birthday = $request->input('dob');
                $nia_user->citizen_id = $request->input('citizen_id');
                $nia_user->education = $request->input('education');
                $nia_user->occupation = $request->input('occupation');
                $nia_user->faculty_id = $request->input('faculty_id');
                $nia_user->sub_faculty = $request->input('sub_faculty');
                $nia_user->address = $request->input('address');
                $nia_user->mobile_number = $request->input('mobile_number');
                $nia_user->province_id = $request->input('province_id');
                $nia_user->postcode = $request->input('postcode');
                //$nia_user->startup_lookup   = implode(",", $request->input('startup_lookup'));

                if (isset($data['avatar'])) $user->avatar = $newfilename;

                $user->save();
                $nia_user->save();

                $newUser = User::find(\Session::get('uid'));

                \Session::put('fid', $newUser->first_name);

                return redirect('user/profile')->with('message', 'บันทึกข้อมูลส่วนตัวเรียบร้อยแล้ว!')->with('status', 'success');
            } else {
                return redirect('user/profile')->with('message', 'The following errors occurred')->with('status', 'error')
                    ->withErrors($validator)->withInput();
            }

        }
        else{
            $nia_user = usernia::where('user_id',\Auth::user()->id)->first();

            $nia_user->startup_lookup   = implode(",", $request->input('startup_lookup'));

            $nia_user->save();

            return redirect('user/profile')->with('message', 'บันทึกหัวข้อที่สนใจเรียบร้อยแล้ว')->with('status', 'success');
        }

    }

    public function postSavepassword(Request $request)
    {
        $rules = array(
            'password' => 'required|between:6,12',
            'password_confirmation' => 'required|between:6,12'
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            $user = User::find(\Session::get('uid'));
            $user->password = \Hash::make($request->input('password'));
            $user->save();

            return redirect('user/profile')->with(['status' => 'success', 'message' => 'Password has been saved!']);
        } else {
            return redirect('user/profile')->with(['status' => 'error', 'message' => 'The following errors occurred'])
                ->withErrors($validator)->withInput();
        }

    }

    public function getReminder()
    {

        return view('user.remind');
    }

    public function postRequest(Request $request)
    {

        $rules = array(
            'credit_email' => 'required|email'
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {

            $user = User::where('email', '=', $request->input('credit_email'));
            if ($user->count() >= 1) {
                $user = $user->get();
                $user = $user[0];
                $data = array('token' => $request->input('_token'));
                $to = $request->input('credit_email');
                $subject = "[ " . config('sximo.cnf_appname') . " ] REQUEST PASSWORD RESET ";
                $data['subject'] = $subject;
                $data['email'] = $to;

                if (config('sximo.cnf_mail') == 'swift') {

                    \Mail::send('user.emails.auth.reminder', $data, function ($message) use ($data) {
                        $message->to($data['email'])->subject($data['subject']);
                    });


                } else {


                    $message = view('user.emails.auth.reminder', $data);
                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers .= 'From: ' . config('sximo.cnf_appname') . ' <' . config('sximo.cnf_email') . '>' . "\r\n";
                    mail($to, $subject, $message, $headers);
                }


                $affectedRows = User::where('email', '=', $user->email)
                    ->update(array('reminder' => $request->input('_token')));

                return redirect('user/login')->with(['message' => 'Please check your email', 'status' => 'success']);

            } else {
                return redirect('user/login?reset')->with(['message' => 'Cant find email address', 'status' => 'error']);
            }

        } else {
            returnredirect('user/login?reset')->with(['message' => 'The following errors occurred', 'status' => 'error'])->withErrors($validator)->withInput();
        }
    }

    public function getReset($token = '')
    {
        if (\Auth::check()) return redirect('e-admin');

        $user = User::where('reminder', '=', $token);;
        if ($user->count() >= 1) {
            $this->data['verCode'] = $token;
            return view('user.remind', $this->data);

        } else {
            return redirect('user/login')->with(['message' => 'Cant find your reset code', 'status' => 'error']);
        }

    }

    public function postDoreset(Request $request, $token = '')
    {
        $rules = array(
            'password' => 'required|alpha_num|between:6,12|confirmed',
            'password_confirmation' => 'required|alpha_num|between:6,12'
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {

            $user = User::where('reminder', '=', $token);
            if ($user->count() >= 1) {
                $data = $user->get();
                $user = User::find($data[0]->id);
                $user->reminder = '';
                $user->password = \Hash::make($request->input('password'));
                $user->save();
            }

            return redirect('user/login')->with(['message' => 'Password has been saved!', 'status' => 'success']);
        } else {
            return redirect('user/reset/' . $token)->with(['message' => 'The following errors occurred', 'status' => 'error'])->withErrors($validator)->withInput();
        }

    }

    public function getLogout()
    {
        \Auth::logout();
        \Session::flush();
        return redirect('')->with(['message' => 'Your are now logged out!', 'status' => 'success']);
    }

    function socialize($social)
    {
        return Socialize::driver($social)->redirect();
    }

    function autosocialize($social)
    {
        try {
            $user = \Socialize::driver($social)->user();
        }
        catch (\Exception $e) {
            return redirect('user/login')->with(['message' => 'เกิดข้อผิดพลาด', 'status' => 'error']);
        }

        $users = \DB::table('tb_users')->where('email', $user->email)->get();

        if (count($users)) {
            $row = $users[0];
            return self::autoSignin($row->id);
        } else {
            //            $fb_name = explode(" ", $user->name);
            //            $firstName = $fb_name[0];
            //            $lastName = $fb_name[1];
            //
            //            $authen = new User;
            //            $authen->first_name = $firstName;
            //            $authen->last_name = $lastName;
            //            $authen->email = $user->email;
            //            $authen->group_id = $this->config['cnf_group'];
            //            $authen->save();
            //
            //            $users = \DB::table('tb_users')->where('email', $user->email)->first();
            //            $row = $users;
            //
            //            return self::autoSignin($row->id);
            $data['user'] = $user;

            $data['autofill'] = array('email'=>'', 'firstname'=>'', 'lastname'=>'');

            if(isset($user)){
                $data['autofill'] = array('email'=>$user->email, 'firstname'=>explode(" ", $user->name)[0], 'lastname'=>explode(" ", $user->name)[1]);
            }

            return view('user.register', $data)->with(['message'=>'คุณยังไม่ได้สมัครสมาชิก', 'status'=>'error']);
            //			return redirect('api/register',$user)
            //				->with(['message'=>'You have not registered yet ','status'=>'error']);
        }

    }

    function autoSignin($id)
    {

        if (is_null($id)) {
            return redirect('user/login')
                ->with(['message' => 'You have not registered yet ', 'status' => 'error']);
        } else {

            \Auth::loginUsingId($id);
            if (\Auth::check()) {
                $row = User::find(\Auth::user()->id);

                if ($row->active == '0') {
                    // inactive
                    \Auth::logout();
                    return redirect('user/login')->with(['message' => 'Your Account is not active', 'status' => 'error']);

                } else if ($row->active == '2') {
                    // BLocked users
                    \Auth::logout();
                    return redirect('user/login')->with(['message' => 'Your Account is BLocked', 'status' => 'error']);
                } else {
                    $session = array(
                        'gid' => $row->group_id,
                        'uid' => $row->id,
                        'eid' => $row->email,
                        'll' => $row->last_login,
                        'fid' => $row->first_name,
                        //                        'username' => $row->username,
                        'join' => $row->created_at
                    );

                    //                    if ($this->config['cnf_front'] == 'false') :
                    //                        return redirect('e-admin');
                    //                    else :
                    //                        return redirect('');
                    //                    endif;
                    //custom redirect
                    if ($session['gid'] <= 2) :
                    return redirect('e-admin')->with(['message' => 'Login Success', 'status' => 'success']);
                    else :
                    return redirect('')->with(['message' => 'Login Success', 'status' => 'success']);
                    endif;



                }
            }
        }

    }

    public function postMobileNumber(Request $request){

        if(!\Auth::check()){
            return response()->json(['success' => false, 'message' => 'คุณยังไม่ได้เข้าสู่ระบบ']);
        }

        $nia_user = usernia::where('user_id',\Auth::user()->id)->first();

        $nia_user->mobile_number = $request->input('mobile_number');

        $nia_user->save();

        return response()->json(['success' => true, 'message' => 'อัพเดทเบอร์มือถือเรียบร้อย']);

    }


}