<?php

namespace App\Http\Controllers;

use App\Models\Campinground;
use App\Models\Certificates;
use App\Models\Courses;
use App\Models\Faculty;
use App\Models\Pitching;
use App\Models\Pitchinground;
use App\Models\Province;
use App\Models\Quiz;
use App\Models\Timeset;
use App\Models\Uleagueevent;
use App\Models\University;
use App\Models\Usernia;
use App\Models\Worktype;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class Select2Controller extends Controller
{
    public function GetUniversity()
    {
        $rows = university::orderBy('name', 'asc')->get();

        $items = array();
        foreach ($rows as $row) {
            $name = $row->name;
            $items[] = array($row->university_id, $name);
        }
        return json_encode($items);
    }

    public function GetFaculty()
    {
        $rows = faculty::get();

        $items = array();
        foreach ($rows as $row) {
            $name = $row->faculty_name;
            $items[] = array($row->faculty_id, $name);
        }
        return json_encode($items);
    }

    public function GetProvince()
    {
        $rows = province::orderBY('name_th','asc')->get();

        $items = array();
        foreach ($rows as $row) {
            $name = $row->name_th;
            $items[] = array($row->code, $name);
        }
        return json_encode($items);
    }

    public function SelectPitchMember()
    {
        $rows = pitching::join('tb_users', 'tb_users.id', 'startup_pitching.user_id')
//            ->whereNotIn('tb_users.id', [Auth::user()->id])
            ->get();

        $items = array();
        foreach ($rows as $row) {
            $name = $row->email;
            $items[] = array($row->user_id, $name);
        }
        if(empty($items)){
            $items[] = array("",'ไม่พบข้อมูล');
        }

        return json_encode($items);
    }

    public function getCampingRound()
    {
        $rows = campinground::get();

        $items = array();
        foreach ($rows as $row) {
            $name = $row->round_title;
            $items[] = array($row->round_id, $name);
        }
        if(empty($items)){
            $items[] = array("",'ไม่พบข้อมูล');
        }

        return json_encode($items);
    }

    public function getPitchingRound()
    {
        $rows = pitchinground::get();

        $items = array();
        foreach ($rows as $row) {
            $name = $row->round_title;
            $items[] = array($row->round_id, $name);
        }
        if(empty($items)){
            $items[] = array("",'ไม่พบข้อมูล');
        }

        return json_encode($items);
    }

    public function getCourse()
    {
        $rows = courses::get();

        $items = array();
        foreach ($rows as $row) {
            $name = $row->course_code . " : " . $row->course_title;
            $items[] = array($row->course_id, $name);
        }
        if(empty($items)){
            $items[] = array("",'ไม่พบข้อมูล');
        }

        return json_encode($items);
    }

    public function getQuiz()
    {
        $rows = quiz::get();

        $items = array();
        foreach ($rows as $row) {
            $name = $row->quiz_title;
            $items[] = array($row->quiz_id, $name);
        }
        if(empty($items)){
            $items[] = array("",'ไม่พบข้อมูล');
        }

        return json_encode($items);
    }

    public function getEvent()
    {
        $rows = uleagueevent::get();

        $items = array();
        foreach ( $rows as $row )
        {
            $name = $row['event_title'];
            $items[] = array($row['event_id'], $name);
        }

        if(empty($items)){
            $items[] = array("",'ไม่พบข้อมูล');
        }

        return json_encode($items);
    }

    public function getEmail()
    {
        $rows = User::where('group_id', 3)->get();

        $items = array();
        foreach ( $rows as $row )
        {
            $name = $row['email'];
            $items[] = array($row['email'], $name);
        }

        if(empty($items)){
            $items[] = array("",'ไม่พบข้อมูล');
        }

        return json_encode($items);
    }

    public function getCitizenId()
    {
        $rows = usernia::get();

        $items = array();
        foreach ( $rows as $row )
        {
            $name = $row['citizen_id'];
            $items[] = array($row['citizen_id'], $name);
        }

        if(empty($items)){
            $items[] = array("",'ไม่พบข้อมูล');
        }

        return json_encode($items);
    }

    public function getCertificates(){
        
        //$rows = certificates::get();
        
        $rows = courses::where('course_certificate_id', '!=', 0)->join('certificates', 'certificates.id', 'courses.course_certificate_id')->get();
        

        $items = array();
        foreach ( $rows as $row ){
            $name = $row['cert_name'];
            $items[] = array($row['id'], $name);
        }

        if(empty($items)){
            $items[] = array("",'ไม่พบข้อมูล');
        }

        return json_encode($items);
    }
}
